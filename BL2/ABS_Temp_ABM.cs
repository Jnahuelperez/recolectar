﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BL
{
    public abstract class ABS_Temp_ABM<T>
    {
        public abstract void Crear(T pObject);
        public abstract void Modificar(T pObject);
        public abstract void Baja(T pObject);
        public abstract List<T> Listar();
        public abstract T Obtener(T pObject);
    }
}
