﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO2;

namespace BL
{
    public class Bitacora : iBitacora, ICloneable
    {
        static public void GrabarBitacora(Bitacora pRegistro)
        {
            DAL.BitacoraDAL.CrearRegistro(pRegistro);

        }
        public List<Bitacora> Listar(Bitacora pBitacora)
        {
            Usuario mUsuario = new Usuario();
            if (pBitacora.Usuario != "ALL")
            {
                mUsuario.user = pBitacora.Usuario;
                pBitacora.idUsuario = mUsuario.Obtener(mUsuario).id;
            }
            if (pBitacora.Nivel == "ALL")
            {
                pBitacora.Nivel = null;
            }
            List<Bitacora> Registros = new List<Bitacora>();
            foreach (Bitacora mReg in BitacoraDAL.Listar(pBitacora))
                Registros.Add(mReg);
            return Registros;
        }
        public object Clone()
        {
            //Clonado profundo
            Bitacora mB;
            this.idUsuario = Login.UsuarioLogeado.id;
            this.Nivel = "INFO";
            return mB = this.MemberwiseClone() as Bitacora;
        }
        static public List<string> ListarUsuarios()
        {
            return DAL.BitacoraDAL.ListarUsuarios();
        }

        #region interfaz
        public iBitacora Crear()
        {
            return new Bitacora();
        }
        public string Usuario { get; set; }
        public int id { get; set; }
        public string Detalle { get; set; }
        public DateTime Fecha { get; set; }
        public string Nivel { get; set; }
        public int? idUsuario { get; set; }
        public DateTime FechaDesde { get; set; }
        public DateTime FechaHasta { get; set; }
        #endregion
    }
}
