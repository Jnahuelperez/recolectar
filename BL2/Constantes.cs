﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    static public class Constantes
    {
        public enum EstadoContenedor { Activo, Bloqueado, Recolectar}
        public enum EstadoRuta { Pendiente, En_curso, Finalizada }
    }
}
