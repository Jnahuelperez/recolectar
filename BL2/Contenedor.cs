﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO2;
using SERV;

namespace BL
{
    public class Contenedor : ABS_Temp_ABM<Contenedor>, iContenedor
    {
        ContenedorDAL mContDal = new ContenedorDAL();
        public enum ContenedorEstado { Activo, Bloqueado, Recolectar, Inactivo }

        #region Interfaz
        public iContenedor Crear()
        {
            return new Contenedor();
        }

        public bool Anegado { get; set; }
        public int Capacidad { get; set; }
        public string Detalle { get; set; }
        public string Estado { get; set; }
        public int id { get; set; }
        public int IndiceContaminacion { get; set; }
        public bool Inusual { get; set; }
        public bool NivelCritico { get; set; }
        public int Peso { get; set; }
        public string Ubicacion { get; set; }
        #endregion

        /* Patron Template */
        public override void Baja(Contenedor pObject)
        {
            pObject.Estado = ContenedorEstado.Inactivo.ToString();
            mContDal.Baja(pObject);
        }

        public override void Crear(Contenedor pObject)
        {
            pObject.IndiceContaminacion = 0;
            pObject.Estado = ContenedorEstado.Activo.ToString();
            pObject.Anegado = false;
            pObject.Inusual = false;
            pObject.NivelCritico = false;
            mContDal.Crear(pObject);
        }

        public override List<Contenedor> Listar()
        {
            List<Contenedor> mContenedores = new List<Contenedor>();
            foreach (Contenedor mCont in mContDal.Listar(new Contenedor()))
                mContenedores.Add(mCont);
            return mContenedores;
        }

        public override void Modificar(Contenedor pObject)
        {
            mContDal.Modificar(pObject);
        }

        public override Contenedor Obtener(Contenedor pObject)
        {
            return (Contenedor)mContDal.Obtener(pObject);
        }

        public Contenedor ObtenerByName(string pContName)
        {
            return (Contenedor)mContDal.ObtenerByName(new Contenedor(), pContName) ;
        }

        public void NotificarEstado(Contenedor.Movimiento pContMov, Contenedor pContenedor)
        {
            pContMov.Fecha = DateTime.Now;
            pContenedor.Peso += pContMov.PesoAgregado;
            pContenedor.IndiceContaminacion += pContMov.IndiceCont;
            pContenedor.Anegado = pContMov.Anegamiento;
            EvaluarEstadoContenedor(pContenedor, pContMov);
            pContMov.Estado = pContenedor.Estado;
            this.Modificar(pContenedor);
            mContDal.AddMovimiento(pContMov);
        }

        private void EvaluarEstadoContenedor(Contenedor pContenedor, Movimiento pMovimiento)
        {
            //CUANDO SETEAMOS EL NIVEL CRITICO?
            if (pContenedor.NivelCritico || pContenedor.Anegado)
                pContenedor.Estado = ContenedorEstado.Recolectar.ToString();
            if (pContenedor.IndiceContaminacion >= 80 || pContenedor.Inusual)
                pContenedor.Estado = ContenedorEstado.Bloqueado.ToString();
            if (pContenedor.Estado != ContenedorEstado.Activo.ToString())
            {
                this.Modificar(pContenedor);
                Notificador.email.Send(
                    "El contenedor: " + pContenedor.Detalle + " cambio su estado a: " + pContenedor.Estado + "."+"\n"+"Detalles del ultimo movimiento realizado: " +
                    "\n" + "Fecha:"+"\t" + pMovimiento.Fecha +
                    "\n" + "Peso agregado:" + "\t" + pMovimiento.PesoAgregado +
                    "\n" + "Indice Cont:" + "\t" + pMovimiento.IndiceCont +
                    "\n" + "Anegamiento:" + "\t" + pMovimiento.Anegamiento
                    , "Actualizacion contenedor", "jnahuelperez@hotmail.com");
            }

        }

        public List<Movimiento> ListarMovimientos()
        {
            List<Movimiento> mMovimientos = new List<Movimiento>();
            foreach (Movimiento mM in mContDal.ListarMovimientos(new Movimiento()))
                mMovimientos.Add(mM);
            return mMovimientos;
        }

        public List<Contenedor> GetContByRute(int pRutaId)
        {
            List<Contenedor> Contenedores = new List<Contenedor>();
            foreach (Contenedor mC in mContDal.GetContByRute(pRutaId, new Contenedor()))
                Contenedores.Add(mC);
            return Contenedores;
            
        }

        public class Movimiento : iMovimiento
        {
            public int id { get; set; }
            public int idContenedor { get; set; }
            public DateTime Fecha { get; set; }
            public int PesoAgregado { get; set; }
            public int IndiceCont { get; set; }
            public bool Anegamiento { get; set; }
            public string Estado { get; set; }

            public iMovimiento Crear()
            {
                return new Movimiento();
            }
        }
    }
}
