﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using DAL;

namespace BL
{
    public class DBAdmin
    {
        int CantidadArchivos { get; set; }
        public string RealizarBackup(int pCantidadArchivos, string pPath= "C:\\RecolectDBBKP\\")
        {
            try
            {
                return DALDBAdmin.RealizarBackup(pCantidadArchivos);
            }
            catch(Exception ex) { throw (ex); }
        }
        public void RestaurarBackup(List<string> pArchivos)
        {
            try
            {
                DALDBAdmin.RestaurarBackup(pArchivos);
            }
            catch (Exception ex) { throw (ex); }
        }

        public string GetDVV(string pTabla)
        {
            return servDAL.GetDVV(pTabla);
        }

    }
}
