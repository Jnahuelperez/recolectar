﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO2;


namespace BL
{
    public class Familia : iFamilia
    {
        Patente mPatente = new Patente();
        Bitacora mRegistro = new Bitacora();
        #region Interfaz
        public int id { get; set; }
        public string detalle { get; set; }
        public int idUsuario { get; set; }
        public List<iPatente> Patentes { get; set; }
        public List<Patente> ListaPatentes { get; set; }
        public int idPatente { get; set; }
        public bool Activo { get; set; }

        private List<Patente> mListaPatentes = new List<Patente>();
        public iFamilia CrearFamilia()
        {
            return new Familia();
        }
        #endregion
        public void InsertarUsuFam(Familia pFamilia, Usuario pUsuario)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            mB.Detalle = "Uso de privilegio elevado: Se agrego el usuario " + pUsuario.user + " a la familia " + pFamilia.detalle + ".";
            mB.Nivel = "INFO";
            mB.Fecha = DateTime.Now;
            BL.Bitacora.GrabarBitacora(mB);
            FamiliaDAL.InsertarUsuFam(pFamilia);
        }
        private void InsertarFamPat(Familia pFamila, Patente pPatente)
        {
            try
            {
                FamiliaDAL.InsertarFamPat(pFamila, pPatente);
            }
            catch (Exception ex) { throw (ex); }
        }
        private void QuitarFamPat(Familia pFamila, Patente pPatente)
        {
            List<Patente> mPatentes = new List<Patente>();
            try
            {
                //foreach (Patente mP in pFamila.ListaPatentes)
                //{
                    if (!(mPatente.ValidPatHuerf(pPatente, pFamila,false) && mPatente.ValidPatHuerf(pPatente, pFamila)))
                        mPatentes.Add(pPatente);
                    else
                        throw (new Exception("La accion crearia patetes huerfanas. Patente: "+pPatente.detalle));
                //}
                FamiliaDAL.QuitarFamPat(pFamila, pPatente);
            }
            catch (Exception ex) { throw (ex); }
        }
        public void QuitarFamUsu(Usuario pUsario, Familia pFamilia)
        {
            List<Familia> mFamilias = new List<Familia>();
            try
            {
                pFamilia.idUsuario = pUsario.id;
                pFamilia.ListaPatentes = GetPatentesFamilia(pFamilia);
                    foreach (Patente mP in pFamilia.ListaPatentes)
                    {
                    mP.idUsuario = pUsario.id;
                        if (!(mPatente.ValidPatHuerf(mP, pFamilia,true) && mPatente.ValidPatHuerf(mP, pUsario)))
                            mFamilias.Add(pFamilia);
                        else
                            throw (new Exception("La accion crearia patentes huerfanas. Patente: "+mP.detalle));
                    }
                foreach (Familia mF in mFamilias)
                    FamiliaDAL.DeleteUsuFam(mF);
            }
            catch (Exception ex) { throw (ex); }
        }
        public void GuardarUsuairoFamilia(Familia mF, Usuario pUsuario)
        {
            Usuario mUViejo = new Usuario();
            mUViejo = pUsuario.Obtener(pUsuario);
            Bitacora mB = mRegistro.Clone() as Bitacora;

            InsertarUsuFam(mF, pUsuario);
            mB.Detalle = "Uso de privilegio elevado: Se agrego la familia" + mF.detalle + " al usuario " + pUsuario.user + ".";
            mB.Nivel = "INFO";
            mB.Fecha = DateTime.Now;
            BL.Bitacora.GrabarBitacora(mB);
        }
        public List<Patente> GetPatentesFamilia(Familia pFamilia)
        {
            List<Patente> mPatentes = new List<Patente>();
            foreach (Patente mP in FamiliaDAL.GetPatentesByFamilia(new Patente(), pFamilia))
                mPatentes.Add(mP);
            return mPatentes;
        }
        public Familia Obtener(Familia pFamilia)
        {
            Familia mF = new Familia();
            mF = (Familia)FamiliaDAL.Obtener(pFamilia);
            mF.ListaPatentes = GetPatentesFamilia(mF);
            return mF;
        }
        public void Insertar(Familia pFam)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            try
            {
                int mIdFamilia = FamiliaDAL.Insertar(pFam);
            }
            catch (Exception ex) { throw (ex); }
            try
            {
                mB.Detalle = "Uso de privilegio elevado: Se creo nuevo usuario.";
                mB.Nivel = "INFO";
                mB.Fecha = DateTime.Now;
                BL.Bitacora.GrabarBitacora(mB);
            }
            catch (Exception ex) { throw (ex); }
            try
            {
                foreach (Patente mP in pFam.ListaPatentes)
                {
                    InsertarFamPat(pFam, mP);
                }
            }
            catch (Exception ex) { throw (ex); }
        }
        public List<Familia> Listar()
        {
            List<Familia> mLista = new List<Familia>();

            foreach (Familia mFam in FamiliaDAL.Listar(new Familia()))
            {
                mLista.Add(mFam);
            }
            return mLista;
        }
        public void Guardar(Familia pFamilia)
        {
            //Guardamos cambios en la familia
            try
            {
                FamiliaDAL.Guardar(pFamilia);
            } catch (Exception ex) { throw (ex); }
            //Bitacora
            try
            {
                Bitacora mB = mRegistro.Clone() as Bitacora;
                mB.Detalle = "Uso de privilegio elevado: Se actualizaron las propiedades de la familia " + pFamilia.detalle + " desde el usuario " + Login.UsuarioLogeado.user + ".";
                mB.Nivel = "WARN";
                mB.Fecha = DateTime.Now;
                BL.Bitacora.GrabarBitacora(mB);
            }
            catch (Exception ex) { throw (ex); }
            //Guardamos cambios en la relacion de patentes
            try
            {
                //Comparar si alguna de pFamilia.ListaPatente no esta en lista de retorno
                ////Agregarla
                foreach (Patente mP in pFamilia.ListaPatentes)
                {
                    if (!GetPatentesFamilia(new Familia() { id = pFamilia.id }).Any(x => x.id == mP.id))
                    {
                        InsertarFamPat(pFamilia, mP);
                    }
                }
                //Comparar si alguna del retorno no esta incluida en la lista de pFamilia.ListaPatentes
                ////Borrarla
                foreach (Patente mP in GetPatentesFamilia(new Familia() { id = pFamilia.id }))
                {
                    if (!pFamilia.ListaPatentes.Any(x => x.id == mP.id))
                    {
                        try {
                            QuitarFamPat(pFamilia, mP);
                        }
                        catch (Exception ex) { throw (ex); }
                    }
                }
            }
            catch (Exception ex) { throw (ex); }
        }
        public void Eliminar(Familia pFamilia)
        {
            try
            {
                foreach (Patente mP in pFamilia.ListaPatentes)
                {
                    if (!mPatente.ValidPatHuerf(mP, pFamilia)) { /*noting*/ }
                    else
                        throw (new Exception("La accion crearia patentes huerfanas. Patente: " + mP.detalle));
                }
                FamiliaDAL.Delete(pFamilia);
            }
            catch(Exception ex) { throw (ex); }
        }
    }
}
