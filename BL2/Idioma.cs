﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO2;
using DAL;

namespace BL
{
    public class Idioma : iIdioma
    {
        List<string> NombreControles = new List<string>();
        public string Detalle { get; set; }
        public int id { get; set; }
        public iIdioma Crear()
        {
            return new Idioma();
        }
        public static List<Idioma> ListarIdiomas()
        {
            List<Idioma> mIdiomas = new List<Idioma>();
            foreach (Idioma mI in IdiomaDAL.Listar(new Idioma()))
            {
                mIdiomas.Add(mI);
            }
            return mIdiomas;
        }
        public static Idioma Obtener(int pID)
        {
            return (Idioma)IdiomaDAL.Obtener(new Idioma() { id = pID });
        }
        public static List<Controles> ObtenerIdiomaDeControles(string pIdioma)
        {
            List<Controles> Controles = new List<Controles>();
            foreach (Controles mC in IdiomaDAL.ObtenerIdiomaControls(new Controles() { TipoIdioma = pIdioma }))
                Controles.Add(mC);
            return Controles;
        }

        public static string GetMSGLanguaje(string pObjeto, string pIdioma)
        {
            return IdiomaDAL.GetMSGLanguaje(pObjeto,pIdioma);
        }
        public class Controles : iControles
        {
            public int id { get; set; }
            public string objeto { get; set; }
            public string valor { get; set; }
            public string TipoIdioma { get; set; }
            public iControles Crear()
            {
                return new Controles();
            }
        }
    }
}
