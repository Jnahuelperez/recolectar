﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using DAL;
using DTO2;

namespace BL
{
    public class Iniciador
    {
        Bitacora mRegistro = new Bitacora();
        List<string> mListaTablasProtegidas = new List<string>();

        private class DVV : iDVV
        {
            public int idTabla { get; set; }
            public string Tabla { get; set; }
            public string Registro { get; set; }
            public string DigitoVertical { get; set; }
            public iDVV Crear()
            {
                return new DVV();
            }
        }

        public void ConnectToDB()
        {
            try
            {
                DAL.IniciadorDAL.DBTestConnection();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public bool CheckConfigFile(string pPath)
        {
            bool result;
            try
            {
               return result = File.Exists(pPath);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        public void CheckIntegrity()
        {
            try
            {
                CheckDVH();
                CheckDVV();
            }
            catch(Exception ex) { throw (ex); }
        }

        private void CheckDVH()
        {
            if (IniciadorDAL.CheckIntegrity(new DVV()).Count == 0) { }
            else
            {
                string mDetalle = "";
                foreach (DVV mDVV in IniciadorDAL.CheckIntegrity(new DVV()))
                {
                    //Crear Registro en bitacora
                    Bitacora mB = mRegistro.Clone() as Bitacora;
                    mB.Detalle = "Fallo comprobacion de DVH en " + mDVV.Tabla + " registro: " + mDVV.Registro;
                    mB.Nivel = "CRIT";
                    mB.Fecha = DateTime.Now;
                    mB.idUsuario = 0;
                    mDetalle = mB.Detalle;
                    DAL.BitacoraDAL.CrearRegistro(mB);
                    throw new Exception(mDetalle);
                }
            }
        }

        private void CheckDVV()
        {
            if (IniciadorDAL.CheckDVV(new DVV()).Count == 0) { }
            else
            {
                string mDetalle = "";
                foreach (DVV mDVV in IniciadorDAL.CheckDVV(new DVV()))
                {
                    //Crear Registro en bitacora
                    Bitacora mB = mRegistro.Clone() as Bitacora;
                    mB.Detalle = "Fallo comprobacion de DVV en " + mDVV.Tabla;
                    mB.Nivel = "CRIT";
                    mB.Fecha = DateTime.Now;
                    mB.idUsuario = 0;
                    mDetalle = mB.Detalle;
                    DAL.BitacoraDAL.CrearRegistro(mB);
                    throw new Exception(mDetalle);
                }
            }
        }
    }
}
