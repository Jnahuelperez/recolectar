﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using BL;
using DTO2;
using System.Runtime.CompilerServices;

namespace BL
{
    public class Login
    {
        Usuario mUsuario = new Usuario();
        Familia mFamilia = new Familia();
        Patente mPatente = new Patente();
        Bitacora mRegistro = new Bitacora();

        public static Usuario UsuarioLogeado = new Usuario();

        public void ValidarLogin(string pUser, string pPass)
        {
            //Por defecto instanacia de bitacora inicia con Nivel=INFO
            Bitacora mB = mRegistro.Clone() as Bitacora;
            mUsuario.user = pUser;
            Usuario mUsu = new Usuario();
            mUsu = mUsuario.Obtener(mUsuario);
            if (mUsu != null)
            {
                if (mUsu.Activo == true && mUsu.Bloqueado == false)
                {
                    if (mUsu.BadLogins >= 3)
                    {
                        //Crear Registro en bitacora
                        mB.Detalle = "Oper. no autorizada: intento de inicio de sesio con usuario bloqueado";
                        mB.Fecha = DateTime.Now;
                        mB.idUsuario = mUsu.id;
                        mB.Nivel = "CRIT";
                        BL.Bitacora.GrabarBitacora(mB);
                        throw new LoginException("Usuario esta bloqueado");
                    }
                    else
                    {
                        ValidarPWD(mUsu, pPass);
                    }
                }
                else
                {
                    //Crear Registro en bitacora
                    mB.Detalle = "Oper. no autorizada: intento de inicio de sesion con usuario no activo";
                    mB.Fecha = DateTime.Now;
                    mB.idUsuario = mUsu.id;
                    mB.Nivel = "CRIT";
                    BL.Bitacora.GrabarBitacora(mB);
                    throw new LoginException("Usuario no puede iniciar sesion, contactese con el administrador.");
                }
            }
            else
            {
                //Crear Registro en bitacora
                mB.Detalle = "Oper. no autorizada: intento de inicio de sesio con usuario inexistente";
                mB.Fecha = DateTime.Now;
                mB.idUsuario = mUsu.id;
                mB.Nivel = "INFO";
                BL.Bitacora.GrabarBitacora(mB);
                throw new LoginException("Usuario no esta registrado");
            }
        }
        private void ResetearBadPWD(Usuario pUsuario)
        {
            pUsuario.BadLogins = 0;
            mUsuario.Actualizar(pUsuario);
        }
        private void IncrementBadPwd(Usuario pUser)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            pUser.BadLogins ++;
            //Crear Registro en bitacora
            mB.Detalle = "Inicio de sesion erroneo: se incrementa contador de ingreso erroneo";
            mB.Nivel = "WARN";
            mB.Fecha = DateTime.Now;
            mB.idUsuario = pUser.id;
            BL.Bitacora.GrabarBitacora(mB);

            if (pUser.BadLogins == 3)
            {
                //Bloquear usuario
                pUser.Bloqueado = true;
                mB.Detalle = "Accion sobre usuario: se bloqueo el usuario.";
                mB.Nivel = "CRIT";
                mB.Fecha = DateTime.Now;
                mB.idUsuario = pUser.id;
                DAL.BitacoraDAL.CrearRegistro(mB);
                mUsuario.Actualizar(pUser);
            }
            mUsuario.Actualizar(pUser);
        }
        public bool ValidarPWD(Usuario pUser, string pPwd)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            string mPass = DAL.LoginDAL.GetHash(pPwd);
            if (pUser.pwd == mPass)
            {
                pUser.LastLogin = DateTime.Now.Date;
                ResetearBadPWD(pUser);
                //Crear Registro en bitacora
                mB.Detalle = "Inicio de sesion satisfactorio";
                mB.Fecha = DateTime.UtcNow;
                mB.idUsuario = pUser.id;
                BL.Bitacora.GrabarBitacora(mB);
                UsuarioLogeado = pUser;
                return true;
            }
            else
            {
                IncrementBadPwd(pUser);
                throw new LoginException("Usuario o clave incorrectos");
            }

        }
        public class LoginException : Exception
        {
            public LoginException(string pString) :
                base("Se produjo un error al iniciar sesion: " + pString)
            { }
        }
        public void Logout(Usuario pUser)
        {
            //Crear Registro en bitacora
            Bitacora mB = mRegistro.Clone() as Bitacora;

            mB.Detalle = "Usuario cerro sesion";
            mB.Nivel = "INFO";
            mB.Fecha = DateTime.Now;
            mB.idUsuario = pUser.id;
            BL.Bitacora.GrabarBitacora(mB);
        }

        //public List<Familia> getFamilias(string pUsuario)
        //{
        //    List<Familia> mFamilias = new List<Familia>();
        //    Usuario mUsu = new Usuario();
        //    mUsu = mUsuario.Obtener(mUsuario);
        //    mFamilia.idUsuario = mUsu.id;
        //    foreach (Familia mF in FamiliaDAL.getRolesUsuario(mFamilia))
        //        mFamilias.Add(mF);
        //    return mFamilias;
        //}
        //public List<Patente> getPatentes(string pUsuario)
        //{
        //    List<Patente> mPatentes = new List<Patente>();
        //    Usuario mUsu = new Usuario();
        //    mUsu = mUsuario.Obtener(mUsuario);
        //    mPatente.idUsuario = mUsu.id;
        //    foreach (Patente mP in PatenteDAL.getPatentes(mPatente))
        //        mPatentes.Add(mP);
        //    return mPatentes;
        //}
    }
}
