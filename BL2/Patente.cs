﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO2;

namespace BL
{
    public class Patente : iPatente
    {
        Bitacora mRegistro = new Bitacora();
        #region Interfaz
        public int id { get; set; }
        public string detalle { get; set; }
        public int idUsuario { get; set; }
        public iPatente CrearPatente()
        {
            return new Patente();
        }
        #endregion

        #region CRUD
        public Patente Obtener(Patente pPatente)
        {
            return (Patente)DAL.PatenteDAL.Obtener(pPatente);
        }
        public List<Patente> Listar()
        {
            List<Patente> ListaPatentes = new List<Patente>();

            foreach (Patente mPat in DAL.PatenteDAL.Listar(new Patente()))
            {
                ListaPatentes.Add(mPat);
            }
            return ListaPatentes;
        }
        public void InsertarUsuPat(Patente pPat, Usuario pUsuario)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            mB.Detalle = "Uso de privilegio elevado: Se agrego el usuario " + pUsuario.user + " a la patente " + pPat.detalle + ".";
            mB.Nivel = "INFO";
            mB.Fecha = DateTime.Now;
            BL.Bitacora.GrabarBitacora(mB);
            PatenteDAL.InsertarUsuPat(pPat);
        }
        public void GuardarUsuarioPat(Patente pPat, Usuario pUsuario)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;

            mB.Detalle = "Uso de privilegio elevado: Se agrego la patente " + pPat.detalle + " al usuario " + pUsuario.user+ ".";
            mB.Nivel = "INFO";
            mB.Fecha = DateTime.Now;
            BL.Bitacora.GrabarBitacora(mB);
            InsertarUsuPat(pPat, pUsuario);
        }
        public void QuitarUsuPat(Usuario pUsuario, Patente pPatente)
        {
            List<Patente> mPatentes = new List<Patente>();
            try
            {
                pPatente.idUsuario = pUsuario.id;
                if (!(ValidPatHuerf(pPatente, pUsuario) 
                    && ValidPatHuerf(pPatente, new Familia(), true)
                    && ValidPatHuerfFam(pPatente)))
                    mPatentes.Add(pPatente);
                else
                    throw (new Exception("La accion crearia patetes huerfanas. Patente: "+ pPatente.detalle));
                foreach (Patente mP in mPatentes)
                    PatenteDAL.DeleteUsuPat(mP);
            }
            catch(Exception ex) { throw (ex); }
        }
        public bool ValidPatHuerf(Patente pPat, Usuario pUsuario)
        {
            return PatenteDAL.ValidPatHuerf(pPat, pUsuario.id);
        }
        public bool ValidPatHuerf(Patente pPat, Familia pFamilia)
        {
            return PatenteDAL.ValidPatHuerf(pPat, pFamilia);
        }

        public bool ValidPatHuerf(Patente pPat, Familia pFamilia, bool pCheckpCheckByUser)
        {
            return PatenteDAL.ValidPatHuerf(pPat, pFamilia, pCheckpCheckByUser);
        }

        public bool ValidPatHuerfFam(Patente pPat)
        {
            return PatenteDAL.ValidPatHuerfFam(pPat);
        }
        #endregion
    }
}
