﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO2;

namespace BL
{
    public class Ruta : ABS_Temp_ABM<Ruta>, iRuta
    {
        #region Interfaz
        public int id { get; set; }
        public string Detalle { get; set; }
        public int idZona { get; set; }
        public string Criticidad { get; set; }
        public int Longitud { get; set; }
        public bool Activo { get; set; }
        public List<Contenedor> ListaContenedores { get; set; }
        public int idUsuarioRuta { get; set; }
        public string Estado { get; set; }

        public iRuta Crear()
        {
            return new Ruta();
        }

        #endregion
        
        RutaDAL mDal = new RutaDAL();
        Contenedor mContenedorBL = new Contenedor();

        public override void Baja(Ruta pObject)
        {
            pObject.Activo = false;
            try
            {
                foreach (Contenedor mC in pObject.ListaContenedores)
                    mDal.QuitarRutaCont(mC);
                if (mDal.GetRutasAsigByUser(pObject).Count > 0)
                    DesAsignarRuta(pObject);
                mDal.Baja(pObject);
            } catch(Exception ex) { throw(ex); }
        }

        public override void Crear(Ruta pObject)
        {
            //Creamos la ruta
            try
            {
                pObject.Activo = true;
                pObject.Estado = Constantes.EstadoRuta.Pendiente.ToString();
                mDal.Crear(pObject);
            } catch(Exception ex) { throw ex; }

            //Guardamos relacion Ruta contenedor
            try
            {
                pObject.id = mDal.ObtenerByName(pObject).id;
                if(pObject.ListaContenedores.Count>0)
                    foreach(Contenedor mC in pObject.ListaContenedores)
                        InsertRutaCont(mC, pObject);
            }
            catch (Exception ex) { throw ex; }
        }

        public override List<Ruta> Listar()
        {
            List<Ruta> Rutas = new List<Ruta>();
            foreach (Ruta mR in mDal.Listar(new Ruta()))
            {
                mR.ListaContenedores = mContenedorBL.GetContByRute(mR.id);
                Rutas.Add(mR);
            }
            return Rutas;
        }

        public override void Modificar(Ruta pObject)
        {
            mDal.Modificar(pObject);
            //Actualizamos Contenedores
            try
            {
                //Comparar si algun contenedor de la ListaContenedores no esta.
                ////Agregarlo
                foreach (Contenedor mC in pObject.ListaContenedores)
                    if (!mContenedorBL.GetContByRute(pObject.id).Any(x => x.id == mC.id))
                        InsertRutaCont(mC, pObject);

                //Comparar si alguno del retorno no esta incluida en la lista de ListaContenedores
                ////Borrarlo
                foreach (Contenedor mC in mContenedorBL.GetContByRute(pObject.id))
                    if (!pObject.ListaContenedores.Any(x => x.id == mC.id))
                        mDal.QuitarRutaCont(mC);
            }
            catch (Exception ex) { throw (ex); }
        }

        public override Ruta Obtener(Ruta pObject)
        {
            Ruta mR = new Ruta();
            mR = (Ruta)mDal.Obtener(pObject);
            mR.ListaContenedores = mContenedorBL.GetContByRute(pObject.id);
            return mR;
        }

        private void InsertRutaCont(Contenedor pContenedor, Ruta pRuta)
        {
            mDal.InsertRutaCont(pContenedor, pRuta);
        }

        public void AsignarRuta(Ruta pRuta, Usuario pUsuario)
        {
            if (mDal.GetRutasAsigByUser(pRuta).Count > 0)
                throw new Exception("La ruta se encuentra asignada a otro usuario");
            else
            {
                mDal.AsignarRuta(pRuta, pUsuario);
            }
        }

        public void DesAsignarRuta(Ruta pRuta)
        {
            mDal.DesAsignarRuta(pRuta);
        }

        public List<Ruta> GetRutasAsigByUser(Ruta pRuta)
        {
            List<Ruta> Rutas = new List<Ruta>();
            foreach(Ruta mR in mDal.GetRutasAsigByUser(pRuta))
            {
                mR.ListaContenedores = mContenedorBL.GetContByRute(mR.id);
                Rutas.Add(mR);
            }
            return Rutas;
        }
    }
}
