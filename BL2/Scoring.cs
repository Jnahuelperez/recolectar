﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL;
using DAL;
using DTO2;

namespace BL
{
    public class Scoring : ABS_Temp_ABM<Scoring>, iScoring
    {
        ScoringDAL mScoDAL = new ScoringDAL();
        public int id { get; set; }
        public int idRuta { get; set; }
        public int idUsuario { get; set; }
        public int TiempoRecCalculado { get; set; }
        public int Score { get; set; }
        public DateTime FechaComienzo { get; set; }
        public DateTime FechaFin { get; set; }
        public int idZona { get; set; }
        public override void Baja(Scoring pObject)
        {
            throw new NotImplementedException();
        }

        public override void Crear(Scoring pObject)
        {
            mScoDAL.Crear(pObject);
        }

        public iScoring Crear()
        {
            return new Scoring();
        }

        public override List<Scoring> Listar()
        {
            List<Scoring> mListaScoring = new List<Scoring>();
            foreach (Scoring mS in mScoDAL.Listar(new Scoring()))
                mListaScoring.Add(mS);
            return mListaScoring;
        }

        public override void Modificar(Scoring pObject)
        {
            mScoDAL.Modificar(pObject);
        }

        public override Scoring Obtener(Scoring pObject)
        {
            return (Scoring)mScoDAL.Obtener(pObject);
        }
    }
}
