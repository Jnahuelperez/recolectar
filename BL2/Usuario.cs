﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO2;
using SERV;

namespace BL
{
    public class Usuario : iUsuario, ICloneable
    {
        public List<Familia> Familias
        {get;set;}
        public List<Patente> Patentes { get; set; }
        Familia mFam = new Familia();
        Patente mPat = new Patente();
        Bitacora mRegistro = new Bitacora();

        #region Interfaz implementada
        public int id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get;set;}
        public string email { get; set; }
        public string user { get; set; }
        public string pwd { get; set; }
        public bool Bloqueado { get; set; }
        public bool Activo { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime CreatedOn { get; set; }
        public int BadLogins { get; set; }
        public int idIdioma { get; set; }
        public iPuesto Puesto { get; set; }
        public string UserIdioma { get; set; }
        public bool pwdCambio { get; set; }

        //public int IntentosLogin { get; set; }
        //public int idFamilia { get; set; }
        public iUsuario Crear()
        {
           return new Usuario();
        }
        #endregion

        #region Prototype
        public object Clone()
        {
            return this.MemberwiseClone();
        }
        #endregion

        public void Insertar(Usuario pUsuario)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            try
            {
                ChequearUsuRep(pUsuario);
                pUsuario.pwd = Seguridad.Password.CrearRandomPassword(pUsuario.email);
                int mIdUsuario = UsuarioDAL.Insertar(pUsuario);
                mB.Detalle = "Uso de privilegio elevado: Se creo nuevo usuario.";
                mB.Nivel = "INFO";
                mB.Fecha = DateTime.Now;
                BL.Bitacora.GrabarBitacora(mB);
                try
                {
                    foreach (Familia mF in pUsuario.Familias)
                    {
                        mF.idUsuario = mIdUsuario;
                        mFam.InsertarUsuFam(mF, pUsuario);
                    }
                    foreach (Patente mP in pUsuario.Patentes)
                    {
                        mP.idUsuario = mIdUsuario;
                        mPat.InsertarUsuPat(mP, pUsuario);
                    }
                }
                catch (Exception ex) { throw (ex); }
            }
            catch (Exception ex) { throw (ex); }
        }
        public void Guardar(Usuario pUsuario, bool pResetPWD=false)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            if (pResetPWD)
            {
                pUsuario.pwd = Seguridad.Password.CrearRandomPassword(pUsuario.email);
                pwdCambio = true;
            }
            if (!pUsuario.Bloqueado)
                pUsuario.BadLogins = 0;

            try
            {
                ChequearUsuRep(pUsuario);
            }
            catch(Exception ex) { throw (ex); }
            mB.Detalle = "Uso de privilegio elevado: Se actualizaron las propiedades del usuario "+pUsuario.user+" desde el usuario "+Login.UsuarioLogeado.user+".";
            mB.Nivel = "WARN";
            mB.Fecha = DateTime.Now;
            BL.Bitacora.GrabarBitacora(mB);
            //Actualizamos Familias
            try
            {
                //Comparar si alguna de Familia de pUsuario.Familias no esta en lista de retorno
                ////Agregarla
                foreach (Familia mF in pUsuario.Familias)
                {
                    if (!GetFamiliasByUser(new Usuario() { id = pUsuario.id, user = pUsuario.user }).Any(x => x.id == mF.id))
                    {
                        mFam.GuardarUsuairoFamilia(mF, pUsuario);
                    }
                }
                //Comparar si alguna del retorno no esta incluida en la lista de pUsuario.Familias
                ////Borrarla
                foreach (Familia mF in GetFamiliasByUser(new Usuario() { id = pUsuario.id, user = pUsuario.user }))
                {
                    if (!pUsuario.Familias.Any(x => x.id == mF.id))
                    {
                        try
                        {
                            mFam.QuitarFamUsu(pUsuario,mF);
                        }
                        catch (Exception ex) { throw (ex); }
                    }
                }
            }
            catch (Exception ex) { throw (ex); }

            //Actualizamos Patentes
            try
            {
                //Comparar si alguna Patente de pUsuario.Patentes no esta en lista de retorno
                ////Agregarla
                foreach (Patente mP in pUsuario.Patentes)
                {
                    if (!GetPatentesByUser(new Usuario() { id = pUsuario.id, user = pUsuario.user }).Any(x => x.id == mP.id))
                    {
                        mPat.GuardarUsuarioPat(mP, pUsuario);
                    }
                }
                //Comparar si alguna del retorno no esta incluida en la lista de pUsuario.Patentes
                ////Borrarla
                foreach (Patente mP in GetPatentesByUser(new Usuario() { id = pUsuario.id, user = pUsuario.user }))
                {
                    if (!pUsuario.Patentes.Any(x => x.id == mP.id))
                    {
                        try
                        {
                            mPat.QuitarUsuPat(pUsuario,mP);
                        }
                        catch (Exception ex) { throw (ex); }
                    }
                }
            }
            catch (Exception ex) { throw (ex); }

            //Guardamos el usuario
            try
            {
                UsuarioDAL.Guardar(pUsuario);
            }
            catch (Exception ex) { throw (ex); }
        }
        public void PWDSelfService(Usuario pUsuario)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            try
            {
                Seguridad.Password.ValidPWD(pUsuario.pwd);
                UsuarioDAL.Guardar(pUsuario);
                mB.Detalle = "Uso de privilegio elevado: El usuario " + pUsuario.user + " realizo un blanqueo de su clave.";
                mB.Nivel = "WARN";
                mB.Fecha = DateTime.Now;
                BL.Bitacora.GrabarBitacora(mB);
            }
            catch(Exception ex) { throw (ex); }
        }
        public void Eliminar(Usuario pUsuario)
        {
            Bitacora mB = mRegistro.Clone() as Bitacora;
            mB.Detalle = "Uso de privilegio elevado: Se deshabilito el usuario "+pUsuario.user;
            mB.Nivel = "WARN";
            mB.Fecha = DateTime.Now;
            try
            {
                foreach(Patente mP in pUsuario.Patentes)
                    mPat.QuitarUsuPat(pUsuario, mP);
                foreach(Familia mF in pUsuario.Familias)
                    mFam.QuitarFamUsu(pUsuario, mF);
                UsuarioDAL.Eliminar(pUsuario);
            }
            catch(Exception ex) { throw (ex); }
        }
        public List<Usuario> Listar()
        {
            List<Usuario> mListaUsuarios = new List<Usuario>();

            foreach (Usuario mUser in UsuarioDAL.Listar(new Usuario()))
            {
                mListaUsuarios.Add(mUser);
            }
            return mListaUsuarios;
        }
        public Usuario Obtener(Usuario pUsuario)
        {
            //this.user = pUsuario.user;
            return (Usuario)UsuarioDAL.Read(pUsuario);
        }
        public void Actualizar(Usuario pUsuario)
        {
            UsuarioDAL.Actualizar(pUsuario);   
        }
        public List<Familia> GetFamiliasByUser(Usuario pUsuario)
        {
            List<Familia> mFamilias = new List<Familia>();
            foreach (Familia mF in UsuarioDAL.GetFamiliasByUser(Obtener(pUsuario), new Familia()))
                mFamilias.Add(mF);
            return mFamilias;
        }
        public List<Patente> GetPatentesByUser(Usuario pUsuario)
        {
            List<Patente> mPatentes = new List<Patente>();
            foreach (Patente mP in UsuarioDAL.GetPatentesByUser(Obtener(pUsuario), new Patente()))
                mPatentes.Add(mP);
            return mPatentes;
        }
        public string ObtenerIdiomaUsuario(Usuario pUser)
        {
            return UsuarioDAL.ObtenerIdiomaUsuario(pUser);
        }
        private void ChequearUsuRep(Usuario pUsuario)
        {
            int count = UsuarioDAL.ChequearUsuRep(pUsuario).Count(a => a.user == pUsuario.user);
            if (count > 0)
                throw new Exception(Idioma.GetMSGLanguaje("ExcepUsuExist", Login.UsuarioLogeado.UserIdioma));
            count = UsuarioDAL.ChequearUsuRep(pUsuario).Count(a => a.email == pUsuario.email);
            if (count > 0)
                throw new Exception(Idioma.GetMSGLanguaje("ExcepUsuMailExist", Login.UsuarioLogeado.UserIdioma));
        }
    }
}
