﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO2;
using DAL;

namespace BL
{
    public class Zona : iZona
    {
        public int id { get; set; }
        public string Detalle { get; set; }

        public iZona Crear()
        {
            return new Zona();
        }

        static ZonaDAL mZonaDAL = new ZonaDAL();

        static public List<Zona> ObtenerZonas()
        {
            List<Zona> Zonas = new List<Zona>();
            foreach (Zona mZ in mZonaDAL.Listar(new Zona()))
                Zonas.Add(mZ);
            return Zonas;
        }

        static public Zona GetZonaByName(Zona pZona)
        {
            return (Zona)mZonaDAL.GetZonaByName(pZona);
        }

        static public Zona GetZonaById(Zona pZona)
        {
            return (Zona)mZonaDAL.GetZonaById(pZona);
        }
    }
}
