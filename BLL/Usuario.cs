﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO2;

namespace BLL
{
    public class Usuario : DTO2.iUsuario
    {
        public bool Activo { get; set; }
        public string Apellido { get; set; }
        public bool Bloqueado { get; set; }
        public int id { get; set; }
        public iIdioma Idioma { get; set; }
        public int IntentosLogin { get; set; }
        public string Nombre
        { get; set; }
        public iPuesto Puesto
        { get; set; }
        public string pwd
        { get; set; }
        public string user
        { get; set; }
        public iUsuario Crear()
        {
            return new Usuario();
        }
    }
}
