﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SERV;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public abstract class ABS_Temp_ABM<T>
    {
        static SERV.Integridad mIntegridad = new SERV.Integridad();
        public abstract string Query { get; set; }
        public abstract string DVH { get; set; }
        public abstract string Serializado { get; set; }
        public abstract void Crear(T pObject);
        public abstract void Modificar(T pObject);
        public abstract void Baja(T pObject);
        public abstract List<T> Listar(T pObject);

        public abstract T Obtener(T pObject);

        #region Propiedades y metodos de soporte
        static int mID;
        public static int ProximoID(string pTabla)
        {
            string mQuery = "SELECT ISNULL(MAX(id),0) FROM " + pTabla;
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }
        #endregion

        public void ExecuteNonQuery(string pString)
        {
            DAO.GetInstance().ExecuteNonQuery(pString);
        }
        public DataSet GetDataset(string pString)
        {
            return DAO.GetInstance().ExecuteDataSet(pString);
        }
        public int ExecuteScalar(string pString)
        {
            return int.Parse(DAO.GetInstance().ExecuteScalar(pString).ToString());
        }
        public string GetDVH(string pString)
        {
            return mIntegridad.GetDVH(pString);
        }

        static public void UpdateDVV(string pTabla)
        {
            servDAL.UpdateDVV(servDAL.GetDVH(pTabla), pTabla);
        }
    }
}
