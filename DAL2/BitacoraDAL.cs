﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO2;
using System.Data;

namespace DAL
{
    public class BitacoraDAL
    {
        #region Propiedades y metodos de soporte
        static int mID;
        static string mQuery;
        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(id),0) FROM BITACORA";
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }

        static SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        static SERV.Integridad mIntegridad = new SERV.Integridad();
        #endregion

        public static List<iBitacora> Listar(iBitacora pRegistro)
        {
            /* MANEJO DE FECHAS INPUT
             * Si el usuario no especifica fecha, se toma la mas antigua
             * y un dia despues a dia de ejecucion para abarcar todos los 
             * registros que existen. Los parametros de fecha nunca son
             * nulos.
            */
            if (pRegistro.FechaDesde == null)
                pRegistro.FechaDesde = DateTime.Parse("01/01/1970");
            if (pRegistro.FechaHasta == null)
                pRegistro.FechaDesde = DateTime.Now.AddDays(1);
            //Ciframos los valores que vienen como input
            if(pRegistro.Nivel != null)
                pRegistro.Nivel = SERV.Seguridad.Cifrado.CifrarTexto(pRegistro.Nivel);
            
            //Armamos a Query de acuerdo a los parametros recibidos.
            mQuery = ArmarQuery(pRegistro);
            
            List<iBitacora> mRegistros = new List<iBitacora>();
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);
            foreach (DataRow mRow in mDataset.Tables[0].Rows)
            {
                iBitacora mReg = pRegistro.Crear();
                mRegistros.Add(ValorizarEntidad(mRow, mReg));
            }
            return mRegistros;
        }
        public static void CrearRegistro(iBitacora pRegistro)
        {
            
            try
            {
                pRegistro.id = ProximoID();
                pRegistro.Detalle = SERV.Seguridad.Cifrado.CifrarTexto(pRegistro.Detalle);
                string mFecha = SERV.Seguridad.Cifrado.CifrarTexto(pRegistro.Fecha.ToString());
                pRegistro.Nivel = SERV.Seguridad.Cifrado.CifrarTexto(pRegistro.Nivel);
                string DVH = mIntegridad.GetDVH(
                pRegistro.id.ToString()
                + pRegistro.Detalle
                + pRegistro.Fecha
                + pRegistro.Nivel
                + pRegistro.idUsuario.ToString());
                mQuery = "INSERT INTO BITACORA (id, Detalle, Fecha, Nivel, idUsuario, DVH) VALUES (" +
                "'" + pRegistro.id + "','" +
                "" + pRegistro.Detalle + "','" +
                "" + pRegistro.Fecha + "','" +
                "" + pRegistro.Nivel + "','" +
                "" + pRegistro.idUsuario + "','" + DVH + "')";
                DAO.GetInstance().ExecuteNonQuery(mQuery);
                //if (servDAL.GetDVH("BITACORA").Count > 0)
                //{
                //    servDAL.UpdateDVV(servDAL.GetDVH("BITACORA"), "BITACORA");
                //}
                //else
                //{
                //    List<string> mLista = new List<string>();
                //    mLista.Add(DVH);
                //    servDAL.UpdateDVV(mLista, "BITACORA");
                //}
                servDAL.UpdateDVV(servDAL.GetDVH("BITACORA"), "BITACORA");
            }
            catch (Exception ex) { throw (ex); }
        }
        public static iBitacora ValorizarEntidad(DataRow pRow, iBitacora pReg)
        {
            pReg.id = int.Parse(pRow["id"].ToString());
            pReg.Detalle = SERV.Seguridad.Cifrado.DescifrarTexto(pRow["Detalle"].ToString());
            pReg.Fecha = DateTime.Parse(pRow["Fecha"].ToString());
            pReg.Nivel = SERV.Seguridad.Cifrado.DescifrarTexto(pRow["Nivel"].ToString());
            pReg.idUsuario = int.Parse(pRow["idUsuario"].ToString());
            pReg.Usuario = SERV.Seguridad.Cifrado.DescifrarTexto(pRow["Usuario"].ToString());
            return pReg;
        }
        public static List<string> ListarUsuarios()
        {
            mQuery = "SELECT DISTINCT(idUsuario), u.usuario FROM BITACORA b INNER JOIN USUARIOS u on b.idUsuario = u.id";
            List<string> mRegistros = new List<string>();
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);
            foreach (DataRow mRow in mDataset.Tables[0].Rows)
            {
                string mR;
                mR = SERV.Seguridad.Cifrado.DescifrarTexto(mRow["usuario"].ToString());
                mRegistros.Add(mR);
            }
            return mRegistros;
        }
        private static string ArmarQuery(iBitacora pB)
        {
            //Query base trae todos los resultados
            mQuery = "SELECT b.id, b.Detalle, b.Nivel, b.idUsuario, b.Fecha, u.usuario " +
                   "FROM BITACORA b " +
                   "INNER JOIN USUARIOS u on b.idUsuario = u.id " +
                   "WHERE Fecha >= '"+pB.FechaDesde+"' AND Fecha <= '"+pB.FechaHasta+"' ";
            //Si se especifico algun filtro, hay que armar la query apropiada
            if (pB.Nivel != null || pB.idUsuario != null)
            {
                //NIVEL, USUARIO
                if (pB.Nivel != null && pB.idUsuario != null)
                {
                    mQuery += "AND b.Nivel='" + pB.Nivel + "' AND " +
                        "b.idUsuario='" + pB.idUsuario + "'";
                }
                //NIVEL
                else if (pB.Nivel != null && pB.idUsuario == null)
                {
                    mQuery += "AND b.Nivel='" + pB.Nivel + "' ";
                }
                //Usuario
                else if (pB.idUsuario != null && pB.Nivel == null)
                {
                    mQuery += "AND b.idUsuario='" + pB.idUsuario + "' ";
                }
            }
            return mQuery;
        }
    }
}
