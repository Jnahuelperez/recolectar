﻿using DTO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Data;
using SERV;

namespace DAL
{
    public class ContenedorDAL : ABS_Temp_ABM<iContenedor>
    {
        public override string Query { get; set; }
        public override string DVH { get; set; }
        public override string Serializado { get; set; }

        public override void Baja(iContenedor pObject)
        {
            DVH = GetDVH(
                pObject.id.ToString() + pObject.Detalle + pObject.Ubicacion + pObject.Capacidad.ToString() + 
                pObject.IndiceContaminacion.ToString() +
                pObject.Peso.ToString() + pObject.Estado + pObject.Anegado.ToString() + 
                pObject.Inusual.ToString() + pObject.NivelCritico.ToString());
            Query = "UPDATE CONTENEDORES SET Estado='" + pObject.Estado + "', DVH='"+DVH+"' " +
                "WHERE id='" + pObject.id + "'";
            ExecuteNonQuery(Query);
            UpdateDVV("Contenedores");
        }

        public override void Crear(iContenedor pObject)
        {
            pObject.id = ProximoID("Contenedores");
            DVH = GetDVH(pObject.id.ToString() + pObject.Detalle + pObject.Ubicacion + pObject.Capacidad.ToString() + pObject.IndiceContaminacion.ToString() +
                pObject.Peso.ToString() + pObject.Estado + pObject.Anegado.ToString() + pObject.Inusual.ToString() + pObject.NivelCritico.ToString());
            Query = "INSERT INTO[dbo].[CONTENEDORES] ([id] ,[Detalle] ,[Ubicacion] ,[Capacidad] ,[IndiceCont] ,[Peso] ,[Estado] ,[Anegamiento] ,[Inusual] ,[NivelCritico] ,[DVH]) " +
                "VALUES ('" + pObject.id + "', " +
                "'" + pObject.Detalle + "', " +
                "'" + pObject.Ubicacion + "', " +
                "'" + pObject.Capacidad + "', " +
                "'" + pObject.IndiceContaminacion + "', " +
                "'" + pObject.Peso + "', " +
                "'" + pObject.Estado + "', " +
                "'" + pObject.Anegado + "', " +
                "'" + pObject.Inusual + "', " +
                "'" + pObject.NivelCritico + "', " +
                "'" + DVH + "')";
            ExecuteNonQuery(Query);
            UpdateDVV("Contenedores");
        }

        public override List<iContenedor> Listar(iContenedor pObject)
        {
            List<iContenedor> mContenedores = new List<iContenedor>();
            Query = "SELECT * FROM Contenedores";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iContenedor mC = pObject.Crear();
                mC.id = int.Parse(mROW["id"].ToString());
                mC.Detalle = mROW["Detalle"].ToString();
                mC.Ubicacion = mROW["Ubicacion"].ToString();
                mC.Capacidad = int.Parse(mROW["Capacidad"].ToString());
                mC.IndiceContaminacion = int.Parse(mROW["IndiceCont"].ToString());
                mC.Peso = int.Parse(mROW["Peso"].ToString());
                mC.Estado = mROW["Estado"].ToString();
                mC.Anegado = bool.Parse(mROW["Anegamiento"].ToString());
                mC.Inusual = bool.Parse(mROW["Inusual"].ToString());
                mC.NivelCritico = bool.Parse(mROW["NivelCritico"].ToString());
                mContenedores.Add(mC);
            }
            return mContenedores;
        }

        public override void Modificar(iContenedor pObject)
        {
            DVH = GetDVH(pObject.id.ToString() + pObject.Detalle + pObject.Ubicacion + pObject.Capacidad.ToString() + pObject.IndiceContaminacion.ToString() +
                pObject.Peso.ToString() + pObject.Estado + pObject.Anegado.ToString() + pObject.Inusual.ToString() + pObject.NivelCritico.ToString());
            Query ="UPDATE Contenedores SET " +
                "Detalle='"+pObject.Detalle+"', " +
                "Ubicacion='" + pObject.Ubicacion + "', " +
                "Capacidad='" + pObject.Capacidad + "', " +
                "IndiceCont='" + pObject.IndiceContaminacion + "', " +
                "Peso='" + pObject.Peso + "', " +
                "Estado='" + pObject.Estado + "', " +
                "Anegamiento='" + pObject.Anegado + "', " +
                "Inusual='" + pObject.Inusual + "', " +
                "NivelCritico='" + pObject.NivelCritico + "', " +
                "DVH='" + DVH + "' " +
                "WHERE id='"+pObject.id+"'";
            ExecuteNonQuery(Query);
            UpdateDVV("Contenedores");
        }

        public override iContenedor Obtener(iContenedor pObject)
        {
            Query="Select * FROM Contenedores WHERE id='"+pObject.id+"'";

            DataSet mDataset = new DataSet();
            mDataset = GetDataset(Query);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iContenedor mC = pObject.Crear();
                mC.id = int.Parse(mROW["id"].ToString());
                mC.Detalle = mROW["Detalle"].ToString();
                mC.Ubicacion = mROW["Ubicacion"].ToString();
                mC.Capacidad = int.Parse(mROW["Capacidad"].ToString());
                mC.IndiceContaminacion = int.Parse(mROW["IndiceCont"].ToString());
                mC.Peso = int.Parse(mROW["Peso"].ToString());
                mC.Estado = mROW["Estado"].ToString();
                mC.Anegado = bool.Parse(mROW["Anegamiento"].ToString());
                mC.Inusual = bool.Parse(mROW["Inusual"].ToString());
                mC.NivelCritico = bool.Parse(mROW["NivelCritico"].ToString());

                return mC;
            }
            else
            {
                return null;
            }
        }

        public iContenedor ObtenerByName(iContenedor pObject, string pContName)
        {
            Query = "Select * FROM Contenedores WHERE Detalle='" + pContName + "'";

            DataSet mDataset = new DataSet();
            mDataset = GetDataset(Query);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iContenedor mC = pObject.Crear();
                mC.id = int.Parse(mROW["id"].ToString());
                mC.Detalle = mROW["Detalle"].ToString();
                mC.Ubicacion = mROW["Ubicacion"].ToString();
                mC.Capacidad = int.Parse(mROW["Capacidad"].ToString());
                mC.IndiceContaminacion = int.Parse(mROW["IndiceCont"].ToString());
                mC.Peso = int.Parse(mROW["Peso"].ToString());
                mC.Estado = mROW["Estado"].ToString();
                mC.Anegado = bool.Parse(mROW["Anegamiento"].ToString());
                mC.Inusual = bool.Parse(mROW["Inusual"].ToString());
                mC.NivelCritico = bool.Parse(mROW["NivelCritico"].ToString());

                return mC;
            }
            else
            {
                return null;
            }
        }

        public List<iContenedor> GetContByRute(int pRutaId, iContenedor pObject)
        {
            List<iContenedor> Contenedores = new List<iContenedor>();
            Query = "SELECT c.[id] ,c.[Detalle] ,c.[Ubicacion] " +
                ",c.[Capacidad] ,c.[IndiceCont] ,c.[Peso] " +
                ",c.[Estado] ,c.[Anegamiento] ,c.[Inusual] ,c.[NivelCritico] " +
                "FROM[dbo].[CONTENEDORES] c " +
                "INNER JOIN RUTA_CONTENEDOR rc ON c.id = rc.idContenedor " +
                "WHERE rc.idRuta = '" + pRutaId + "'";

            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iContenedor mC = pObject.Crear();
                mC.id = int.Parse(mROW["id"].ToString());
                mC.Detalle = mROW["Detalle"].ToString();
                mC.Ubicacion = mROW["Ubicacion"].ToString();
                mC.Capacidad = int.Parse(mROW["Capacidad"].ToString());
                mC.IndiceContaminacion = int.Parse(mROW["IndiceCont"].ToString());
                mC.Peso = int.Parse(mROW["Peso"].ToString());
                mC.Estado = mROW["Estado"].ToString();
                mC.Anegado = bool.Parse(mROW["Anegamiento"].ToString());
                mC.Inusual = bool.Parse(mROW["Inusual"].ToString());
                mC.NivelCritico = bool.Parse(mROW["NivelCritico"].ToString());
                Contenedores.Add(mC);
            }
            return Contenedores;
        }

        public void AddMovimiento(iMovimiento pObject)
        {
            pObject.id = ProximoID("CONTENEDOR_MOVIMIENTOS");
            DVH = GetDVH(pObject.id.ToString() + pObject.idContenedor.ToString() + pObject.Fecha
                + pObject.PesoAgregado.ToString() + pObject.IndiceCont.ToString() + pObject.Anegamiento.ToString() + pObject.Estado);
            Query = "INSERT INTO CONTENEDOR_MOVIMIENTOS (id,idContenedor,Fecha,PesoAgregado,IndiceCont,Anegamiento,Estado,DVH) VALUES (" +
                "'" + pObject.id + "', " +
                "'" + pObject.idContenedor + "', " +
                "'" + pObject.Fecha + "', " +
                "'" + pObject.PesoAgregado + "', " +
                "'" + pObject.IndiceCont + "', " +
                "'" + pObject.Anegamiento + "', " +
                "'" + pObject.Estado + "', " +
                "'" + DVH + "')";
            ExecuteNonQuery(Query);
            UpdateDVV("CONTENEDOR_MOVIMIENTOS");
        }

        public List<iMovimiento> ListarMovimientos(iMovimiento pObject)
        {
            List<iMovimiento> Movimientos = new List<iMovimiento>();
            Query = "SELECT * FROM Contenedor_Movimientos";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);
            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow mROW in mDataset.Tables[0].Rows)
                {
                    iMovimiento mM = pObject.Crear();
                    mM.id = int.Parse(mROW["id"].ToString());
                    mM.idContenedor = int.Parse(mROW["idContenedor"].ToString());
                    mM.Fecha = DateTime.Parse(mROW["Fecha"].ToString());
                    mM.PesoAgregado = int.Parse(mROW["PesoAgregado"].ToString());
                    mM.IndiceCont = int.Parse(mROW["IndiceCont"].ToString());
                    mM.Anegamiento = bool.Parse(mROW["Anegamiento"].ToString());
                    mM.Estado = mROW["Estado"].ToString();
                    Movimientos.Add(mM);
                }
            }
            return Movimientos;
        }
    }
}
