﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO2;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class DALDBAdmin
    {
        public static void RestoreMultiUser()
        {
            SqlCommand mComando = new SqlCommand();
            mComando.CommandType = CommandType.StoredProcedure;
            SqlCommand mMensaje = DAO.GetInstance().ExecuteProcedure("dbo.SetMultiUser", mComando);
        }
        public static string RealizarBackup(int pCantidad)
        {
            try
            {
                //KillDeConexiones();
                SqlCommand mComando = new SqlCommand();
                mComando.CommandType = CommandType.StoredProcedure;
                mComando.Parameters.Add("@nParts", SqlDbType.TinyInt).Value = byte.Parse(pCantidad.ToString());
                mComando.Parameters.Add("@primaryDrive", SqlDbType.Char).Value = char.Parse("C");
                mComando.Parameters.Add("@backupDir", SqlDbType.NVarChar).Value = "RecolectDBBKP";
                mComando.Parameters.Add("@SQLCommand", SqlDbType.NVarChar, 2000);
                mComando.Parameters["@SQLCommand"].Direction = ParameterDirection.Output;

                SqlCommand mMensaje = DAO.GetInstance().ExecuteProcedure("dbo.CreateBKP", mComando);
                RestoreMultiUser();
                return mMensaje.Parameters["@SQLCommand"].Value.ToString();
            }
            catch (Exception ex) { throw (ex); }
        }
        public static void RestaurarBackup(List<string> pArchivos)
        {
            string mResult = "";
            string mQuery = "USE MASTER " +
                "ALTER DATABASE RecolectARv3 SET SINGLE_USER WITH ROLLBACK IMMEDIATE " +
                "RESTORE DATABASE RecolectARv3 FROM";
            foreach (string pS in pArchivos)
                mQuery += " DISK = '" + pS + "',";
            mQuery = mQuery.Substring(0, mQuery.Length-1);
            mQuery += " ALTER DATABASE RecolectARv3 SET MULTI_USER";
            try
            {

                mResult = DAO.GetInstance().ExecuteNonQuery(mQuery).ToString();
            }
            catch (Exception ex) { throw (ex); }
        }
    }
}
