﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using SERV;
using System.Text.RegularExpressions;

namespace DAL
{
    public class DAO
    {
        #region Singleton
        private static DAO _instance;
        private DAO() { }
        public static DAO GetInstance()
        {
            if (_instance == null)
            {
                _instance = new DAO();
            }
            return _instance;
        }
        #endregion

        static public string DecryptConnectionString()
        {
            string pName = "RecolectAR";
            Seguridad.Cifrado mCifrado = new Seguridad.Cifrado();
            //Obtenemos los datos del App.Config
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[pName];
            string mConnectionString = settings.ConnectionString;
            //Parseamos el resultado
            string[] parts = mConnectionString.Split(';');
            var mServer = Regex.Match(parts[0], @"server=(.+)").Groups[1].Value;
            var mDB = Regex.Match(parts[1], @"database=(.+)").Groups[1].Value;
            var mUser = Regex.Match(parts[2], @"uid=(.+)").Groups[1].Value;
            var mPass = Regex.Match(parts[3], @"password=(.+)").Groups[1].Value;

            string mConexion = 
                "server="+ mServer +
                ";database=" + mDB +
                ";uid=" + SERV.Seguridad.Cifrado.DescifrarTexto(mUser) + 
                ";password=" + SERV.Seguridad.Cifrado.DescifrarTexto(mPass) + "";
            return mConexion;
        }

       static SqlConnection mConexion = new SqlConnection(DecryptConnectionString());

       public void TestConnection()
        {
            try
            {
                mConexion.Open();
            }

            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                mConexion.Close();
            }
        }
        public int ExecuteNonQuery(string pQuery)
        {
            try
            {
                mConexion.Open();
                SqlCommand mComando = new SqlCommand(pQuery, mConexion);
                return mComando.ExecuteNonQuery();
            }

            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                mConexion.Close();
            }
        }
        public DataSet ExecuteDataSet(string pQuery)
        {
            try
            {
                SqlDataAdapter mAdapter = new SqlDataAdapter(pQuery, mConexion);
                DataSet mDataset = new DataSet();
                mAdapter.Fill(mDataset);
                return mDataset;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                mConexion.Close();
            }

        }
        public int ObtenerID(string pQuery)
        {
            try
            {
                mConexion.Open();
                SqlCommand mComando = new SqlCommand(pQuery, mConexion);
                return int.Parse(mComando.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                mConexion.Close();
            }
        }
        public SqlCommand ExecuteProcedure(string pStoreName, SqlCommand pComando)
        {
          try
            {
                DataSet mDataset = new DataSet();
                pComando.CommandText = pStoreName;
                mConexion.Open();
                pComando.Connection = mConexion;
                int i = pComando.ExecuteNonQuery();
                return pComando;
            }

            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                mConexion.Close();
            }
        }
        public object ExecuteScalar(string pQuery)
        {
            try
            {
                mConexion.Open();
                SqlCommand mComando = new SqlCommand(pQuery, mConexion);
                return mComando.ExecuteScalar();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                mConexion.Close();
            }
        }
    }
}
