﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DTO2;

namespace DAL
{
    public class FamiliaDAL
    {
        #region Propiedades y metodos de soporte
        static SERV.Integridad mIntegridad = new SERV.Integridad();
        static SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        static int mID;
        static string mQuery;
        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(id),0) FROM FAMILIAS";
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }
        #endregion

        //Funciones base
        public static int Insertar(iFamilia pFamilia)
        {
            pFamilia.id = ProximoID();
            mQuery = "INSERT INTO Familias(id, detalle, activo) VALUES (" +
                "'" + pFamilia.id + "','" +
                "" + pFamilia.detalle + "','" +
                "" + pFamilia.Activo + "')";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            return pFamilia.id;
        }
        public static iFamilia Obtener(iFamilia pFam)
        {
            mQuery = "SELECT * FROM FAMILIAS WHERE id = '" + pFam.id + "'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                pFam.id = int.Parse(mDataset.Tables[0].Rows[0]["id"].ToString());
                pFam.detalle = mDataset.Tables[0].Rows[0]["detalle"].ToString();
                pFam.Activo = bool.Parse(mDataset.Tables[0].Rows[0]["activo"].ToString());
                return pFam;
            }
            else
            {
                return null;
            }
        }
        static public void Guardar(iFamilia pFamilia)
        {
            mQuery = "UPDATE FAMILIAS SET " +
                "Detalle='" + pFamilia.detalle + "', " +
                "Activo='" + pFamilia.Activo + "' " +
                "WHERE id ='"+pFamilia.id+"'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
        }
        static public void Delete(iFamilia pFamilia)
        {
            mQuery = "UPDATE FAMILIAS SET Activo='"+pFamilia.Activo+"' "+
                "WHERE id ='" + pFamilia.id + "'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
        }
        static public List<iFamilia> Listar(iFamilia pFamilia)
        {
            List<iFamilia> mLista = new List<iFamilia>();
            mQuery = "SELECT id, detalle, Activo FROM FAMILIAS";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iFamilia mFamilia = pFamilia.CrearFamilia();
                mFamilia.id = int.Parse(mROW["id"].ToString());
                mFamilia.detalle = mROW["detalle"].ToString();
                mFamilia.Activo = bool.Parse(mROW["Activo"].ToString());
                mLista.Add(mFamilia);
            }
            return mLista;
        }

        //Funciones con Patente
        static public List<iPatente> GetPatentesByFamilia(iPatente pPatente, iFamilia pFamilia)
        {
            List<iPatente> Patentes = new List<iPatente>();
            mQuery = "SELECT P.id, P.detalle FROM PATENTES P " +
                "INNER JOIN FAMILIA_PATENTE FP ON FP.idPatente = P.id WHERE FP.idFamilia = '" +pFamilia.id+"'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iPatente mPat = pPatente.CrearPatente();
                mPat.id = int.Parse(mROW["id"].ToString());
                mPat.detalle = mROW["detalle"].ToString();
                Patentes.Add(mPat);
            }
            return Patentes;
        }
        static public void QuitarFamPat(iFamilia pFam, iPatente pPatente)
        {
            try
            {
                mQuery = "DELETE FAMILIA_PATENTE WHERE " +
                    "idPatente='"+pPatente.id+"' " +
                    "AND idFamilia='"+pFam.id+"'";
                DAO.GetInstance().ExecuteNonQuery(mQuery);
                servDAL.UpdateDVV(servDAL.GetDVH("FAMILIA_PATENTE"), "FAMILIA_PATENTE");
            }
            catch (Exception ex) { throw (ex); }
        }
        static public void InsertarFamPat(iFamilia pFam, iPatente pPatente)
        {
            try
            {
                string DVH = mCifra.GetHASH(pFam.id.ToString() + pPatente.id.ToString());
                mQuery = "INSERT INTO FAMILIA_PATENTE(idFamilia, idPatente, DVH) VALUES (" +
                    "'" + pFam.id + "','" +
                     "" + pPatente.id + "','" +
                     "" + DVH + "')";
                DAO.GetInstance().ExecuteNonQuery(mQuery);
                servDAL.UpdateDVV(servDAL.GetDVH("FAMILIA_PATENTE"), "FAMILIA_PATENTE");
            }
            catch(Exception ex) { throw (ex); }
        }
        //Funciones con usuario
        static public void DeleteUsuFam(iFamilia pFam)
        {
            mQuery = "DELETE USUARIO_FAMILIA WHERE idUsuario='" + pFam.idUsuario + "' AND idFamilia='" + pFam.id + "'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIO_FAMILIA"), "USUARIO_FAMILIA");
        }
        static public void InsertarUsuFam(iFamilia pFam)
        {
            string DVH = mIntegridad.GetDVH(pFam.idUsuario.ToString()+pFam.id.ToString());
            mQuery = "INSERT INTO USUARIO_FAMILIA (idUsuario,idFamilia,DVH) VALUES('" + pFam.idUsuario + "','" + pFam.id + "','" + DVH + "')";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIO_FAMILIA"), "USUARIO_FAMILIA");
        }

    }
}
