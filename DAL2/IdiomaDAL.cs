﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DTO2;

namespace DAL
{
    public class IdiomaDAL
    {
        #region Propiedades y metodos de soporte
        static int mID;
        static string mQuery;

        /* COMPORTAMIENTO: ProximoID
         * Funcion que controla los ID's en memoria.
         * No usamos autoincremental para dejar toda la logica del lado de la solucion
        */

        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(id),0) FROM Idiomas";
            if (mID == 0)
            {
                mID = DAL.DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }

        #endregion
        public static List<iIdioma> Listar(iIdioma pIdioma)
        {
            List<iIdioma> mLista = new List<iIdioma>();
            mQuery = "SELECT id, detalle FROM IDIOMAS";
            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iIdioma mIdioma = pIdioma.Crear();
                mIdioma.id = int.Parse(mROW["id"].ToString());
                mIdioma.Detalle = mROW["detalle"].ToString();
                mLista.Add(mIdioma);
            }
            return mLista;
        }

        public static iIdioma Obtener(iIdioma pIdi)
        {
            
            mQuery = "SELECT id, detalle FROM IDIOMAS WHERE id='"+pIdi.id+"'";
            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);

            iIdioma mI = pIdi.Crear();
            mI.Detalle = mDataset.Tables[0].Rows[0]["detalle"].ToString();
            mI.id = int.Parse(mDataset.Tables[0].Rows[0]["id"].ToString());
            return mI;
        }
        public static List<iControles> ObtenerIdiomaControls(iControles pControles)
        {
            List<iControles> ListaControles = new List<iControles>();
            mQuery = "SELECT id, objeto, "+pControles.TipoIdioma+" FROM IDIOMAS_OBJETOS";
            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mRow in mDataset.Tables[0].Rows)
            {
                iControles mC = pControles.Crear();
                mC.objeto = mRow["objeto"].ToString();
                mC.valor = mRow[pControles.TipoIdioma].ToString();
                ListaControles.Add(mC);
            }
            return ListaControles;
        }

        public static string GetMSGLanguaje(string pObjeto,string pIdioma)
        {
            mQuery = "SELECT "+ pIdioma + " FROM IDIOMAS_OBJETOS WHERE objeto='" + pObjeto + "'";
            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);

            return mDataset.Tables[0].Rows[0][pIdioma].ToString();
        }
    }
}
