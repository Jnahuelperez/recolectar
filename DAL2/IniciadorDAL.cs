﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DTO2;

namespace DAL
{
    public class IniciadorDAL
    {
        static string mQuery;
        static private SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        static public void DBTestConnection()
        {
            try
            {
                DAO.GetInstance().TestConnection();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        static private List<iDVV> GetTablasProtegidas(iDVV pDVV)
        {
            List<iDVV> mTablasProtegidas = new List<iDVV>();
            mQuery = "SELECT * FROM DIGITOSVERTICALES";
            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);
            
            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iDVV mDVV = pDVV.Crear();
                mDVV.idTabla = int.Parse(mROW["idTabla"].ToString());
                mDVV.Tabla = mROW["Tabla"].ToString();
                mDVV.DigitoVertical = mROW["DVV"].ToString();

                mTablasProtegidas.Add(mDVV);
            }
            return mTablasProtegidas;
        }
        static private List<string> GetRecords(string pTabla)
        {
            List<string> mRegistros = new List<string>();
            string mRecord = "";

            mQuery = "SELECT * FROM " + pTabla;
            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);
            if (mDataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow mROW in mDataset.Tables[0].Rows)
                {
                    for (int i = 0; i < mDataset.Tables[0].Columns.Count; i++)
                    {
                        string mCol = mDataset.Tables[0].Columns[i].ColumnName.ToString();
                        if (mCol != "DVH") //Primero cargamos el registro concatenado
                            mRecord += mROW[mCol].ToString();
                        if (mCol == "DVH") //Agregamos el DVH con ; para parsear a posterior
                            mRecord += ";" + mROW[mCol].ToString();
                    }
                    mRegistros.Add(mRecord);
                    mRecord = "";
                }
            }
            return mRegistros;
        }
        static public List<iDVV> CheckIntegrity(iDVV pDVV)
        {
            List<iDVV> TablasVerificadas = new List<iDVV>();
            string mHashCalculado;
            String[] mRegistroSplit;
            foreach (iDVV mDVV in GetTablasProtegidas(pDVV))//Por cada tabla protegida
            {
                foreach (string mReg in GetRecords(mDVV.Tabla))//Traer los registros
                {
                    mRegistroSplit = mReg.Split(char.Parse(";"));//split DVH almacenado
                    mHashCalculado = mCifra.GetHASH(mRegistroSplit[0]);//Calculamos hash con la primer parte
                    if (mHashCalculado != mRegistroSplit[1]) //Si el hash calculado no coincide con el obtenido...
                    {
                        iDVV miDVV = pDVV.Crear();
                        miDVV.Registro = mRegistroSplit[0];
                        miDVV.Tabla = mDVV.Tabla;
                        TablasVerificadas.Add(miDVV);
                    }
                }
            }
            return TablasVerificadas;
        }

        static public List<iDVV> CheckDVV(iDVV pDVV)
        {
            string DVV = "";
            List<iDVV> TablasVerificadas = new List<iDVV>();
            foreach (iDVV mDVV in GetTablasProtegidas(pDVV))//Por cada tabla protegida
            {
                DVV = servDAL.GetDVV(mDVV.Tabla); // Calcular el digito vertical
                if(DVV!=mDVV.DigitoVertical) // y compararlo con el almacenado en la DB.
                    TablasVerificadas.Add(mDVV);
            }
            return TablasVerificadas;
        }
    }
}
