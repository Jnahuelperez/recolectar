﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DTO2;

namespace DAL
{
    public class PatenteDAL
    {
        static SERV.Integridad mIntegridad = new SERV.Integridad();
        static string mQuery;
        static int mID;
        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(ID_Usuario),0) FROM Usuarios";
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }

        static public List<iPatente> Listar(iPatente pPatente)
        {
            List<iPatente> mLista = new List<iPatente>();
            mQuery = "SELECT id, detalle FROM PATENTES";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iPatente mPatente = (iPatente)pPatente.CrearPatente();
                mPatente.id = int.Parse(mROW["id"].ToString());
                mPatente.detalle = mROW["detalle"].ToString();
                mLista.Add(mPatente);
            }
            return mLista;

        }
        static public iPatente Obtener(iPatente pPatente)
        {
            mQuery = "SELECT id, Detalle, idPatente, DetallePatente FROM FAMILIA";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iPatente mPatente = (iPatente)pPatente.CrearPatente();
                mPatente.id = int.Parse(mROW["ID_Usuario"].ToString());
                return mPatente;
            }
            else
            {
                return null;
            }

        }
        static public void InsertarUsuPat(iPatente pPat)
        {
            string DVH = mIntegridad.GetDVH(
            pPat.id.ToString()
            + pPat.idUsuario.ToString());
            mQuery = "INSERT INTO USUARIO_PATENTE (idUsuario,idPatente,DVH) VALUES('" + pPat.idUsuario + "','" + pPat.id + "','" + DVH + "')";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIO_PATENTE"), "USUARIO_PATENTE");
        }
        static public void DeleteUsuPat(iPatente pPat)
        {
            mQuery = "DELETE FROM USUARIO_PATENTE WHERE idUsuario='" + pPat.idUsuario + "' AND idPatente='"+ pPat.id +"'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIO_PATENTE"), "USUARIO_PATENTE");
        }

        static public bool ValidPatHuerf(iPatente pPat,int pUser)
        {
            int mResult = 0;
            /*Esas patentes estan en otro usuario activo?*/
            mQuery = "SELECT COUNT(*) FROM USUARIO_PATENTE up " +
                "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                "WHERE up.idPatente = '" + pPat.id + "' " +
                "AND u.Activo = '1' " +
                "AND u.id <> '" + pPat.idUsuario + "'";
            mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());

            /* La patente, esta en alguna otra familia ACTIVA asignada a otro usuario ACTIVO? */
            mQuery = "SELECT COUNT(*) FROM FAMILIA_PATENTE fp " +
                "INNER JOIN USUARIO_PATENTE up ON fp.idPatente = up.idPatente " +
                "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                "INNER JOIN FAMILIAS f ON fp.idFamilia = f.id " +
                "WHERE f.activo = '1' " +
                "AND fp.idPatente = '"+pPat.id+"' " +
                "AND u.id <> '"+ pPat.idUsuario + "'";
            mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
            if (mResult != 0)
                return false;
            else
                return true;
        }
        static public bool ValidPatHuerf(iPatente pPat, iFamilia pFam, bool pCheckByUser=true)
        {
            int mResult = 0;
            if (pCheckByUser)
            {
                /* La familia esta asignada a otro usuario activo? */
                mQuery = "SELECT COUNT(*) FROM USUARIO_FAMILIA uf " +
                    "INNER JOIN USUARIOS u ON uf.idUsuario = u.id " +
                    "WHERE uf.idFamilia = '"+pFam.id+"' " +
                    "AND u.Activo = '1' " +
                    "AND u.id <> '"+pFam.idUsuario+"'";
                mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());

                mQuery = "SELECT count(*) FROM FAMILIA_PATENTE fp " +
                    "INNER JOIN FAMILIAS f ON f.id = fp.idFamilia " +
                    "INNER JOIN USUARIO_FAMILIA uF ON f.id = uF.idFamilia " +
                    "INNER JOIN USUARIOS u ON uF.idUsuario = u.id " +
                    "WHERE fp.idPatente = '"+pPat.id+"' " +
                    "AND f.activo = '1' " +
                    "AND uF.idUsuario <> '"+pPat.idUsuario+"' " +
                    "AND u.Activo = '1'";
                mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());

                mQuery = "SELECT COUNT(*) FROM FAMILIA_PATENTE fp " +
                    "INNER JOIN USUARIO_PATENTE up ON fp.idPatente = up.idPatente " +
                    "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                    "INNER JOIN FAMILIAS f ON fp.idFamilia = f.id " +
                    "WHERE f.activo = '1' AND u.Activo = '1' " +
                    "AND fp.idPatente = '" + pPat.id + "' " +
                    "AND fp.idFamilia <> '" + pFam.id + "' " +
                    "AND u.id<>'"+ pPat.idUsuario + "'";
                mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
            }
            else
            {
                /* Esas patentes estan en otro usuario activo? */
                mQuery = "SELECT COUNT(*) FROM USUARIO_PATENTE up " +
                    "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                    "WHERE up.idPatente = '" + pPat.id + "' " +
                    "AND u.Activo = '1'";
                mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
                /* 
                 * Esta es para verificar si las patentes de una familia estan asociadas a otro usuario/Familia
                 * resulta util cuando estamos verificando patente huerfanas sin tener un usuario de referencia 
                 */
                mQuery = "SELECT count(*) FROM FAMILIA_PATENTE fp " +
                    "INNER JOIN USUARIO_PATENTE up ON fp.idPatente = up.idPatente " +
                    "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                    "INNER JOIN FAMILIAS f ON fp.idFamilia = f.id " +
                    "WHERE fp.idPatente = '" + pPat.id+"' " +
                    "AND u.Activo = '1' AND f.Activo='1'";
            }
            mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
            if (mResult != 0 || !(ValidPatHuerf(pPat,pFam.idUsuario)))
                return false;
            else
                return true;
        }
        static public bool ValidPatHuerf(iPatente pPat, iFamilia pFam)
        {
            int mResult = 0;
            /* Esas patentes estan en otra familia activa asignada otro usuario activo? */
            mQuery = "SELECT COUNT(*) FROM FAMILIA_PATENTE fp " +
                    "INNER JOIN USUARIO_PATENTE up ON fp.idPatente = up.idPatente " +
                    "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                    "INNER JOIN FAMILIAS f ON fp.idFamilia = f.id " +
                    "WHERE f.activo = '1' AND u.Activo = '1' " +
                    "AND fp.idPatente = '" + pPat.id + "' " +
                    "AND fp.idFamilia <> '" + pFam.id + "'";
            mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());

            mQuery = "SELECT COUNT(*) FROM FAMILIA_PATENTE fp " +
                    "INNER JOIN USUARIO_PATENTE up ON fp.idPatente = up.idPatente " +
                    "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                    "INNER JOIN FAMILIAS f ON fp.idFamilia = f.id " +
                    "WHERE f.activo = '1' AND u.Activo = '1' " +
                    "AND fp.idPatente = '" + pPat.id + "'";
            mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
            if (mResult != 0)
                return false;
            else
                return true;
        }

        static public bool ValidPatHuerfFam(iPatente pPat)
        {
            int mResult = 0;
            /* La patente, esta en alguna otra familia ACTIVA asignada a otro usuario ACTIVO? */
            mQuery = "SELECT COUNT(*) FROM FAMILIA_PATENTE fp " +
                "INNER JOIN USUARIO_PATENTE up ON fp.idPatente = up.idPatente " +
                "INNER JOIN USUARIOS u ON up.idUsuario = u.id " +
                "INNER JOIN FAMILIAS f ON fp.idFamilia = f.id " +
                "INNER JOIN USUARIO_FAMILIA uF ON uF.idUsuario=up.idUsuario " +
                "WHERE f.activo = '1' " +
                "AND fp.idPatente = '" + pPat.id + "'";
            mResult += int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
            if (mResult != 0)
                return false;
            else
                return true;
        }
    }
}
