﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DTO2;

namespace DAL
{
    public class PuestoDAL
    {
        #region Propiedades y metodos de soporte
        static int mID;
        static string mQuery;

        /* COMPORTAMIENTO: ProximoID
         * Funcion que controla los ID's en memoria.
         * No usamos autoincremental para dejar toda la logica del lado de la solucion
        */

        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(id),0) FROM Puestos";
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }

        #endregion


        #region Metodos Core DAL

        public static List<iPuesto> Listar(iPuesto pPuesto)
        {
            List<iPuesto> mLista = new List<iPuesto>();
            mQuery = "SELECT ID, Detalle FROM Puestos";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iPuesto mPuesto = pPuesto.Crear();
                mPuesto.ID = int.Parse(mROW["ID"].ToString());
                mPuesto.Detalle = mROW["Detalle"].ToString();
                mLista.Add(mPuesto);
            }
            return mLista;
        }

        public static int Guardar(iPuesto pPuesto)
        {
            if (pPuesto.ID == 0)
            {
                pPuesto.ID = DAO.GetInstance().ObtenerID("Puestos");
                mQuery = "INSERT INTO PUESTOS(ID, Detalle) VALUES ('" + pPuesto.ID + "','" + pPuesto.Detalle + "')";
                return DAO.GetInstance().ExecuteNonQuery(mQuery);
            }
            else
            {
                mQuery = "UPDATE PUESTOS SET Detalle= '" + pPuesto.Detalle + "' WHERE ID_Puesto = " + pPuesto.ID;
                return DAO.GetInstance().ExecuteNonQuery(mQuery);
            }
        }
        public static int Eliminar(iPuesto pPuesto)
        {
            mQuery = "DELETE PUESTO WHERE ID= " + pPuesto.ID;
            return DAO.GetInstance().ExecuteNonQuery(mQuery);
        }
        public static iPuesto Obtener(iPuesto pPuesto)
        {
            mQuery = "SELECT ID, Detalle Detalle FROM PuestoFamilia WHERE ID = " + pPuesto.ID;

            DataSet mDataset = new DataSet();
            mDataset = DAL.DAO.GetInstance().ExecuteDataSet(mQuery);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iPuesto mPuesto = pPuesto.Crear();
                mPuesto.Detalle = mROW["Detalle"].ToString();
                return mPuesto;
            }
            else
            {
                return null;
            }
            #endregion
        }
    }
}
