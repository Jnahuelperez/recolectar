﻿using DTO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace DAL
{
    public class RutaDAL : ABS_Temp_ABM<iRuta>
    {
        public override string Query { get; set; }
        public override string DVH { get; set; }
        public override string Serializado { get; set; }

        public override void Baja(iRuta pObject)
        {
            Query = "DELETE RUTAS " +
               "WHERE id = '" + pObject.id + "'";
            ExecuteNonQuery(Query);
        }
        public override void Crear(iRuta pObject)
        {
            pObject.id = ProximoID("RUTAS");
            Query = "INSERT INTO [dbo].[RUTAS] (id, Detalle, idZona, Criticidad, Longitud, Activo,Estado) VALUES(" +
                "'" + pObject.id + "', " +
                "'" + pObject.Detalle + "', " +
                "'" + pObject.idZona + "', " +
                "'" + pObject.Criticidad + "', " +
                "'" + pObject.Longitud + "', " +
                "'" + pObject.Activo + "', " +
                "'" + pObject.Estado + "')";
            ExecuteNonQuery(Query);
        }
        public List<iRuta> GetRutasAsigByUser (iRuta pRuta)
        {
            List<iRuta> Rutas = new List<iRuta>();
            Query = "SELECT * FROM RUTAS R " +
                "INNER JOIN RUTAUSUARIO rU ON R.id = rU.idRuta " +
                "WHERE rU.idUsuario = '"+pRuta.idUsuarioRuta+"'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iRuta mR = pRuta.Crear();
                mR.id = int.Parse(mROW["id"].ToString());
                mR.Detalle = mROW["Detalle"].ToString();
                mR.idZona = int.Parse(mROW["idZona"].ToString());
                mR.Criticidad = mROW["Criticidad"].ToString();
                mR.Longitud = int.Parse(mROW["Longitud"].ToString());
                mR.Activo = bool.Parse(mROW["Activo"].ToString());
                mR.Estado = mROW["Estado"].ToString();
                mR.idUsuarioRuta = int.Parse(mROW["idUsuario"].ToString());

                Rutas.Add(mR);
            }
            return Rutas;
            
        }
        public override List<iRuta> Listar(iRuta pObject)
        {
            List<iRuta> Rutas = new List<iRuta>();
            Query = "SELECT * FROM Rutas";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iRuta mR = pObject.Crear();
                mR.id = int.Parse(mROW["id"].ToString());
                mR.Detalle = mROW["Detalle"].ToString();
                mR.idZona = int.Parse(mROW["idZona"].ToString());
                mR.Criticidad = mROW["Criticidad"].ToString();
                mR.Longitud = int.Parse(mROW["Longitud"].ToString());
                mR.Activo = bool.Parse(mROW["Activo"].ToString());
                mR.Estado = mROW["Estado"].ToString();

                Rutas.Add(mR);
            }
            return Rutas;
        }
        public override void Modificar(iRuta pObject)
        {
            Query= "UPDATE [dbo].[RUTAS] SET " +
                "[Detalle] = '"+pObject.Detalle+"', " +
                "[idZona] = '" +pObject.idZona+ "', " +
                "[Criticidad] = '"+pObject.Criticidad+"', " +
                "[Longitud] = '"+pObject.Longitud+"', " +
                "[Activo] = '"+pObject.Activo+"', " +
                "[Estado] = '" + pObject.Estado + "' " +
                "WHERE id = '" +pObject.id+"'";
            ExecuteNonQuery(Query);
        }
        public override iRuta Obtener(iRuta pObject)
        {
            Query = "SELECT R.id, R.Detalle,R.idZona,R.Criticidad,R.Longitud,R.Activo,R.Estado,UR.idUsuario " +
                "FROM Rutas R LEFT JOIN RUTAUSUARIO UR ON R.id = UR.idRuta " +
                "WHERE R.id = '" + pObject.id + "'";

            DataSet mDataset = new DataSet();
            mDataset = GetDataset(Query);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iRuta mR = pObject.Crear();
                mR.id = int.Parse(mROW["id"].ToString());
                mR.Detalle = mROW["Detalle"].ToString();
                mR.idZona = int.Parse(mROW["idZona"].ToString());
                mR.Criticidad = mROW["Criticidad"].ToString();
                mR.Longitud = int.Parse(mROW["Longitud"].ToString());
                mR.Activo = bool.Parse(mROW["Activo"].ToString());
                mR.Estado = mROW["Estado"].ToString();
                mR.idUsuarioRuta = ExecuteScalar("SELECT ISNULL(MAX(UR.idUsuario ),0) " +
                    "as IdUsuario FROM Rutas R LEFT JOIN RUTAUSUARIO UR ON R.id = UR.idRuta WHERE R.id = '"+ pObject.id + "'");

                return mR;
            }
            else
            {
                return null;
            }

        }
        public iRuta ObtenerByName(iRuta pObject)
        {
            Query = "Select * FROM Rutas WHERE Detalle='" + pObject.Detalle + "'";

            DataSet mDataset = new DataSet();
            mDataset = GetDataset(Query);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iRuta mR = pObject.Crear();
                mR.id = int.Parse(mROW["id"].ToString());
                mR.Detalle = mROW["Detalle"].ToString();
                mR.idZona = int.Parse(mROW["idZona"].ToString());
                mR.Criticidad = mROW["Criticidad"].ToString();
                mR.Longitud = int.Parse(mROW["Longitud"].ToString());
                mR.Activo = bool.Parse(mROW["Activo"].ToString());
                mR.Estado = mROW["Estado"].ToString();
                return mR;
            }
            else
            {
                return null;
            }
        }
        public void InsertRutaCont(iContenedor pContenedor, iRuta pRuta)
        {
            DVH = GetDVH(pRuta.id.ToString() + pContenedor.id.ToString());
            Query = "INSERT INTO RUTA_CONTENEDOR (idRuta, idContenedor, DVH) VALUES(" +
                "'" + pRuta.id + "', " +
                "'" + pContenedor.id + "', " +
                "'" + DVH + "')";
            ExecuteNonQuery(Query);
            UpdateDVV("RUTA_CONTENEDOR");
        }
        public void QuitarRutaCont(iContenedor pContenedor)
        {
            Query = "DELETE RUTA_CONTENEDOR WHERE idContenedor='" + pContenedor.id + "'";
            ExecuteNonQuery(Query);
            UpdateDVV("RUTA_CONTENEDOR");
        }
        public void AsignarRuta(iRuta pRuta, iUsuario pUsuario)
        {
            DVH = GetDVH(pRuta.id.ToString() + pUsuario.id.ToString());
            Query= "INSERT INTO RUTAUSUARIO (idRuta, idUsuario, DVH) VALUES (" +
                "'"+pRuta.id+"', " +
                "'"+pUsuario.id+"', " +
                "'"+DVH+"')";
            ExecuteNonQuery(Query);
            UpdateDVV("RUTAUSUARIO");
        }
        public void DesAsignarRuta(iRuta pRuta)
        {
            DVH = GetDVH(pRuta.id.ToString() + pRuta.idUsuarioRuta.ToString());
            Query = "DELETE RUTAUSUARIO WHERE idRuta='"+pRuta.id+"'";
            ExecuteNonQuery(Query);
            UpdateDVV("RUTAUSUARIO");
        }
    }
}
