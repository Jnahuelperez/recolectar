﻿using DTO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DAL
{
    public class ScoringDAL : ABS_Temp_ABM<iScoring>
    {
        public override string Query { get; set; }
        public override string DVH { get; set; }
        public override string Serializado { get; set; }

        public override void Baja(iScoring pObject)
        {
            Query="Delete SCORING where id = '"+pObject.id+"'";
        }

        public override void Crear(iScoring pObject)
        {
            pObject.id = ProximoID("SCORING");
            DVH = GetDVH(pObject.id.ToString() + pObject.idRuta.ToString() + pObject.idZona.ToString() + pObject.idUsuario.ToString() +
                pObject.FechaComienzo + pObject.FechaFin + pObject.TiempoRecCalculado.ToString() + pObject.Score.ToString());
            Query = "INSERT INTO SCORING [id],[idRuta],[idZona],[idUsuario],[FechaComienzo],[FechaFin],[TRC],[Score],[DVH] " +
                "VALUES ('" + pObject.id + "', " +
                "'" + pObject.idRuta + "', " +
                "'" + pObject.idZona + "', " +
                "'" + pObject.idUsuario + "', " +
                "'" + pObject.FechaComienzo + "', " +
                "'" + pObject.FechaFin + "', " +
                "'" + pObject.TiempoRecCalculado + "', " +
                "'" + pObject.Score + "', " +
                "'" + DVH + "')";
            ExecuteNonQuery(Query);
        }

        public override List<iScoring> Listar(iScoring pObject)
        {
            List<iScoring> mListaScoring = new List<iScoring>();
            Query = "SELECT * FROM Scoring";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iScoring mScore = pObject.Crear();
                mScore.id = int.Parse(mROW["id"].ToString());
                mScore.idRuta = int.Parse(mROW["idRuta"].ToString());
                mScore.idZona = int.Parse(mROW["idZona"].ToString());
                mScore.idUsuario = int.Parse(mROW["idUsuario"].ToString());
                mScore.FechaComienzo = DateTime.Parse(mROW["IndiceCont"].ToString());
                mScore.FechaFin = DateTime.Parse(mROW["Peso"].ToString());
                mScore.TiempoRecCalculado = int.Parse(mROW["TRC"].ToString());
                mScore.Score = int.Parse(mROW["Score"].ToString());
                mListaScoring.Add(mScore);
            }
            return mListaScoring;
        }

        public override void Modificar(iScoring pObject)
        {
            throw new NotImplementedException();
        }

        public override iScoring Obtener(iScoring pObject)
        {
            Query = "SELECT * FROM Scoring where id='"+pObject.id+"'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);
            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iScoring mScore = pObject.Crear();
                    mScore.id = int.Parse(mROW["id"].ToString());
                    mScore.idRuta = int.Parse(mROW["idRuta"].ToString());
                    mScore.idZona = int.Parse(mROW["idZona"].ToString());
                    mScore.idUsuario = int.Parse(mROW["idUsuario"].ToString());
                    mScore.FechaComienzo = DateTime.Parse(mROW["IndiceCont"].ToString());
                    mScore.FechaFin = DateTime.Parse(mROW["Peso"].ToString());
                    mScore.TiempoRecCalculado = int.Parse(mROW["TRC"].ToString());
                    mScore.Score = int.Parse(mROW["Score"].ToString());
                return mScore;
            }
            else
            {
                return null;
            }

        }
    }
}
