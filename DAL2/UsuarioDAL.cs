﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DTO2;

namespace DAL
{
    public class UsuarioDAL
    {
        #region Propiedades y metodos de soporte
        static int mID;
        static string mQuery;
        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(id),0) FROM USUARIOS";
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }

        static SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        static SERV.Integridad mIntegridad = new SERV.Integridad();

        #endregion

        public static List<iUsuario> Listar(iUsuario pUsuario)
        {
            List<iUsuario> mListUsuarios = new List<iUsuario>();
            mQuery = "SELECT " +
                "u.id, " +
                "u.Nombre, " +
                "u.Apellido, " +
                "u.email, " +
                "u.usuario, " +
                "u.pwd, " +
                "u.Bloqueado, " +
                "u.Activo, " +
                "u.LastLogin, " +
                "u.CreatedOn, " +
                "u.BadLogins, " +
                "u.idIdioma, " +
                "idi.detalle " +
                "FROM USUARIOS u " +
                "INNER JOIN IDIOMAS idi ON u.idIdioma=idi.id ";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);
            foreach(DataRow mRow in mDataset.Tables[0].Rows)
            {
                iUsuario mUser = pUsuario.Crear();
                mListUsuarios.Add(ValorizarEntidad(mRow, mUser));
            }
            return mListUsuarios;
        }
        public static iUsuario Read(iUsuario pUsuario)
        {
            pUsuario.user = SERV.Seguridad.Cifrado.CifrarTexto(pUsuario.user);
            mQuery = "SELECT " +
                "u.id, " +
                "u.Nombre, " +
                "u.Apellido, " +
                "u.email, " +
                "u.usuario, " +
                "u.pwd, " +
                "u.Bloqueado, " +
                "u.Activo, " +
                "u.LastLogin, " +
                "u.CreatedOn, " +
                "u.BadLogins, " +
                "u.idIdioma, " +
                "idi.detalle " +
                "FROM USUARIOS u " +
                "INNER JOIN IDIOMAS idi ON u.idIdioma=idi.id " +
                "WHERE u.usuario = '" + pUsuario.user + "'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                return ValorizarEntidad(mDataset.Tables[0].Rows[0],pUsuario);
            }
            else
            {
                return null;
            }
        }
        public static void Actualizar(iUsuario pUsuario)
        {
            pUsuario.user = SERV.Seguridad.Cifrado.CifrarTexto(pUsuario.user);
            string DVH = mIntegridad.GetDVH(pUsuario.id.ToString() 
                + pUsuario.Nombre 
                + pUsuario.Apellido 
                + pUsuario.email 
                + pUsuario.user 
                + pUsuario.pwd 
                + pUsuario.Bloqueado.ToString()
                + pUsuario.Activo.ToString()
                + pUsuario.LastLogin
                + pUsuario.CreatedOn
                + pUsuario.BadLogins.ToString()
                + pUsuario.idIdioma.ToString());
            mQuery = "UPDATE USUARIOS SET " +
            "Nombre= '" + pUsuario.Nombre
            + "', Apellido = '" + pUsuario.Apellido
            + "', email= '" + pUsuario.email
            + "', usuario= '" + pUsuario.user
            + "', pwd = '" + pUsuario.pwd
            + "', Bloqueado = '" + pUsuario.Bloqueado
            + "', Activo = '" + pUsuario.Activo
            + "', LastLogin = '" + pUsuario.LastLogin
            + "', CreatedOn = '" + pUsuario.CreatedOn
            + "', BadLogins = '" + pUsuario.BadLogins
            + "', idIdioma = '" + pUsuario.idIdioma
            + "', DVH = '" + DVH
            + "' WHERE usuario = '" + pUsuario.user + "'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIOS"), "USUARIOS");
        }
        static public List<iFamilia> GetFamiliasByUser(iUsuario pUsuario, iFamilia pFam)
        {
            List<iFamilia> mLista = new List<iFamilia>();
            mQuery = "SELECT FA.id, FA.detalle, US.id AS idUsuario FROM FAMILIAS FA " +
                "INNER JOIN USUARIO_FAMILIA UF ON UF.idFamilia = FA.id " +
                "INNER JOIN USUARIOS US ON UF.idUsuario = US.id " +
                "WHERE US.id = '" + pUsuario.id+ "'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iFamilia mFamilia = pFam.CrearFamilia();
                mFamilia.id = int.Parse(mROW["id"].ToString());
                mFamilia.detalle = mROW["detalle"].ToString();
                mFamilia.idUsuario = int.Parse(mROW["idUsuario"].ToString());
                mLista.Add(mFamilia);
            }
            return mLista;
        }
        static public List<iPatente> GetPatentesByUser(iUsuario pUsuario, iPatente pPat)
        {
            List<iPatente> mLista = new List<iPatente>();
            mQuery = "SELECT PA.id, PA.detalle, US.id AS idUsuario  FROM PATENTES PA " +
                "INNER JOIN USUARIO_PATENTE UP ON UP.idPatente = PA.id " +
                "INNER JOIN USUARIOS US ON UP.idUsuario = US.id " +
                "WHERE US.id = '" + pUsuario.id + "'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iPatente mPat = pPat.CrearPatente();
                mPat.id = int.Parse(mROW["id"].ToString());
                mPat.detalle = mROW["detalle"].ToString();
                mPat.idUsuario = int.Parse(mROW["idUsuario"].ToString());
                mLista.Add(mPat);
            }
            return mLista;
        }
        public static void Guardar(iUsuario pUsuario)
        {
            iUsuario mU = pUsuario.Crear();
            mU.user = pUsuario.user;

            try
            {
                mU.user = SERV.Seguridad.Cifrado.CifrarTexto(mU.user);
                if (pUsuario.pwdCambio)
                {
                    pUsuario.pwd = mCifra.GetHASH(pUsuario.pwd);
                    mQuery = "UPDATE USUARIOS SET " +
                        "Nombre= '" + pUsuario.Nombre + "', " +
                        "Apellido = '" + pUsuario.Apellido + "', " +
                        "usuario= '" + mU.user + "', " +
                        "pwd='" + pUsuario.pwd + "', " +
                        "email= '" + pUsuario.email + "', " +
                        "Bloqueado= '" + pUsuario.Bloqueado + "', " +
                        "Activo= '" + pUsuario.Activo + "', " +
                        "LastLogin= '" + pUsuario.LastLogin + "', " +
                        "idIdioma= '" + pUsuario.idIdioma + "', " +
                        "BadLogins= '" + pUsuario.BadLogins + "' " +
                        "WHERE id = '" + pUsuario.id + "'";
                    DAO.GetInstance().ExecuteNonQuery(mQuery);
                    string DVH = mIntegridad.GetDVH(pUsuario.id.ToString()
                    + pUsuario.Nombre
                    + pUsuario.Apellido
                    + pUsuario.email
                    + mU.user
                    + pUsuario.pwd
                    + pUsuario.Bloqueado.ToString()
                    + pUsuario.Activo.ToString()
                    + pUsuario.LastLogin
                    + pUsuario.CreatedOn
                    + pUsuario.BadLogins.ToString()
                    + pUsuario.idIdioma.ToString());
                    mQuery = "UPDATE USUARIOS SET DVH='" + DVH + "' " +
                        "WHERE id = '" + pUsuario.id + "'";
                    DAO.GetInstance().ExecuteNonQuery(mQuery);
                }
                else
                {
                    mQuery = "UPDATE USUARIOS SET " +
                        "Nombre= '" + pUsuario.Nombre + "', " +
                        "Apellido = '" + pUsuario.Apellido + "', " +
                        "usuario= '" + mU.user + "', " +
                        "email= '" + pUsuario.email + "', " +
                        "Bloqueado= '" + pUsuario.Bloqueado + "', " +
                        "Activo= '" + pUsuario.Activo + "', " +
                        "LastLogin= '" + pUsuario.LastLogin + "', " +
                        "idIdioma= '" + pUsuario.idIdioma + "', " +
                        "BadLogins= '" + pUsuario.BadLogins + "' " +
                        "WHERE id = '" + pUsuario.id + "'";
                    DAO.GetInstance().ExecuteNonQuery(mQuery);
                    pUsuario.user = mU.user;
                    string DVH = mIntegridad.GetDVH(
                    pUsuario.id.ToString()
                    + pUsuario.Nombre
                    + pUsuario.Apellido
                    + pUsuario.email
                    + pUsuario.user
                    + pUsuario.pwd
                    + pUsuario.Bloqueado.ToString()
                    + pUsuario.Activo.ToString()
                    + pUsuario.LastLogin
                    + pUsuario.CreatedOn
                    + pUsuario.BadLogins.ToString()
                    + pUsuario.idIdioma.ToString());
                    mQuery = "UPDATE USUARIOS SET DVH='" + DVH + "' " +
                        "WHERE id = '" + pUsuario.id + "'";
                    DAO.GetInstance().ExecuteNonQuery(mQuery);
                }
                servDAL.UpdateDVV(servDAL.GetDVH("USUARIOS"), "USUARIOS");
            }
            catch(Exception ex) { throw (ex); }
        }
        public static void Eliminar(iUsuario pUsuario)
        {
            pUsuario.Activo = false;
            pUsuario.user = SERV.Seguridad.Cifrado.CifrarTexto(pUsuario.user);
            string DVH = mIntegridad.GetDVH(pUsuario.id.ToString()
            + pUsuario.Nombre
            + pUsuario.Apellido
            + pUsuario.email
            + pUsuario.user
            + pUsuario.pwd
            + pUsuario.Bloqueado.ToString()
            + pUsuario.Activo.ToString()
            + pUsuario.LastLogin
            + pUsuario.CreatedOn
            + pUsuario.BadLogins.ToString()
            + pUsuario.idIdioma.ToString());
            mQuery = "UPDATE USUARIOS SET Activo='0', DVH= '"+DVH+"' WHERE id = '" + pUsuario.id + "'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIOS"), "USUARIOS");
        }
        public static int Insertar(iUsuario pUsuario)
        {
            pUsuario.user = SERV.Seguridad.Cifrado.CifrarTexto(pUsuario.user);
            pUsuario.id = ProximoID();
            pUsuario.pwd = mCifra.GetHASH(pUsuario.pwd);
            string DVH = mIntegridad.GetDVH(pUsuario.id.ToString()
            + pUsuario.Nombre
            + pUsuario.Apellido
            + pUsuario.email
            + pUsuario.user
            + pUsuario.pwd
            + pUsuario.Bloqueado.ToString()
            + pUsuario.Activo.ToString()
            + pUsuario.LastLogin
            + pUsuario.CreatedOn
            + pUsuario.BadLogins.ToString()
            + pUsuario.idIdioma.ToString());
            mQuery = "INSERT INTO USUARIOS(id, Nombre, Apellido, email, usuario, pwd, Bloqueado, Activo, LastLogin, CreatedOn, BadLogins,idIdioma,DVH) VALUES (" +
                "'" + pUsuario.id + "','" +
                "" + pUsuario.Nombre + "','" +
                "" + pUsuario.Apellido + "','" +
                "" + pUsuario.email + "','" +
                "" + pUsuario.user + "','" +
                "" + pUsuario.pwd + "','" +
                "" + pUsuario.Bloqueado + "','" +
                "" + pUsuario.Activo + "','" +
                "" + pUsuario.LastLogin + "','" +
                "" + pUsuario.CreatedOn + "','" +
                "" + pUsuario.BadLogins + "','" +
                "" + pUsuario.idIdioma + "','" +
                "" + DVH + "')";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            servDAL.UpdateDVV(servDAL.GetDVH("USUARIOS"), "USUARIOS");
            return pUsuario.id;
        }
        public static iUsuario ValorizarEntidad(DataRow pRow, iUsuario pUser)
        {
            string mLoginClearText;
            mLoginClearText = SERV.Seguridad.Cifrado.DescifrarTexto(pRow["usuario"].ToString());
            pUser.id = int.Parse(pRow["id"].ToString());
            pUser.Nombre = pRow["Nombre"].ToString();
            pUser.Apellido = pRow["Apellido"].ToString();
            pUser.user = mLoginClearText;
            pUser.email = pRow["email"].ToString();
            pUser.Activo = bool.Parse(pRow["Activo"].ToString());
            pUser.Bloqueado = bool.Parse(pRow["Bloqueado"].ToString());
            pUser.LastLogin = DateTime.Parse(pRow["LastLogin"].ToString());
            pUser.CreatedOn = DateTime.Parse(pRow["CreatedOn"].ToString());
            pUser.idIdioma = int.Parse(pRow["idIdioma"].ToString());
            pUser.BadLogins = int.Parse(pRow["BadLogins"].ToString());
            pUser.pwd = pRow["pwd"].ToString();
            pUser.UserIdioma = pRow["detalle"].ToString();
            return pUser;
        }
        public static string ObtenerIdiomaUsuario(iUsuario pUsu)
        {
            mQuery = "SELECT idi.detalle " +
                "FROM USUARIOS u " +
                "INNER JOIN IDIOMAS idi on u.idIdioma = idi.id " +
                "WHERE u.idIdioma = '"+ pUsu.idIdioma + "' AND u.id = '"+pUsu.id+"'";
            return DAO.GetInstance().ExecuteScalar(mQuery).ToString();
        }
        public static List<iUsuario> ChequearUsuRep(iUsuario pUsuario)
        {
            string mLoginClearText;
            List<iUsuario> mListUsuarios = new List<iUsuario>();
            string userAux = SERV.Seguridad.Cifrado.CifrarTexto(pUsuario.user);
            mQuery = "SELECT " +
                "u.email, " +
                "u.usuario, " +
                "u.Activo " +
                "FROM USUARIOS u " +
                "WHERE u.id <> '"+pUsuario.id+"'";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);
            foreach (DataRow mRow in mDataset.Tables[0].Rows)
            {
                iUsuario mUser = pUsuario.Crear();
                mLoginClearText = SERV.Seguridad.Cifrado.DescifrarTexto(mRow["usuario"].ToString());
                mUser.user = mLoginClearText;
                mUser.email = mRow["email"].ToString();
                mUser.Activo = bool.Parse(mRow["Activo"].ToString());
                mListUsuarios.Add(mUser);
            }
            return mListUsuarios;
        }
    }
}
