﻿using DTO2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
namespace DAL
{
    public class ZonaDAL : ABS_Temp_ABM<iZona>
    {
        public override string Query { get; set; }
        public override string DVH { get; set; }
        public override string Serializado { get; set; }

        public override void Baja(iZona pObject)
        {
            throw new NotImplementedException();
        }

        public override void Crear(iZona pObject)
        {
            throw new NotImplementedException();
        }

        public override List<iZona> Listar(iZona pObject)
        {
            List<iZona> Zonas = new List<iZona>();
            Query = "SELECT * FROM Zonas";
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(Query);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                iZona mZ = pObject.Crear();
                mZ.id = int.Parse(mROW["id"].ToString());
                mZ.Detalle = mROW["Detalle"].ToString();
                Zonas.Add(mZ);
            }
            return Zonas;
        }

        public iZona GetZonaByName(iZona pObj)
        {
            Query = "Select * FROM Zonas WHERE Detalle='" + pObj.Detalle + "'";

            DataSet mDataset = new DataSet();
            mDataset = GetDataset(Query);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iZona mZ = pObj.Crear();
                mZ.id = int.Parse(mROW["id"].ToString());
                mZ.Detalle = mROW["Detalle"].ToString();
                return mZ;
            }
            else
            {
                return null;
            }
        }

        public iZona GetZonaById(iZona pObj)
        {
            Query = "Select * FROM Zonas WHERE id='" + pObj.id + "'";

            DataSet mDataset = new DataSet();
            mDataset = GetDataset(Query);

            if (mDataset.Tables.Count > 0 && mDataset.Tables[0].Rows.Count > 0)
            {
                DataRow mROW = mDataset.Tables[0].Rows[0];
                iZona mZ = pObj.Crear();
                mZ.id = int.Parse(mROW["id"].ToString());
                mZ.Detalle = mROW["Detalle"].ToString();
                return mZ;
            }
            else
            {
                return null;
            }
        }

        public override void Modificar(iZona pObject)
        {
            throw new NotImplementedException();
        }

        public override iZona Obtener(iZona pObject)
        {
            throw new NotImplementedException();
        }
    }
}
