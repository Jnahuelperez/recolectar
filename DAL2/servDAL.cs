﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Data;
using Microsoft.SqlServer.Server;

namespace DAL
{
    public class servDAL
    {
        #region Propiedades y metodos de soporte
        static int mID;
        static string mQuery;
        public static int ProximoID()
        {
            string mQuery = "SELECT ISNULL(MAX(idTabla),0) FROM DIGITOSVERTICALES";
            if (mID == 0)
            {
                mID = DAO.GetInstance().ObtenerID(mQuery);
            }
            return mID += 1;
        }

        static SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        static SERV.Integridad mIntegridad = new SERV.Integridad();

        #endregion

        //Actualiza digitos verticales
        static public void UpdateDVV(List<string> pLista, string pTabla)
        {
            string DVV = "";
            if (pLista.Count > 0)
                DVV = mIntegridad.GetDVV(pLista);
            if (GetRecords(pTabla) == 0)
                InsertFirst(pTabla, DVV);
            else
            { 
            mQuery = "UPDATE DIGITOSVERTICALES SET DVV='" + DVV + "' WHERE Tabla='" + pTabla + "'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
            }
            if (pLista.Count == 0)
                DeleteDVV(pTabla);
        }
        //Devuelve un listado de todos lso DVG de cada tabla
        static public List<string> GetDVH(string pTabla)
        {
            List<string> mLista = new List<string>();
            mQuery = "SELECT DVH FROM "+pTabla;
            DataSet mDataset = new DataSet();
            mDataset = DAO.GetInstance().ExecuteDataSet(mQuery);

            foreach (DataRow mROW in mDataset.Tables[0].Rows)
            {
                mLista.Add(mROW["DVH"].ToString());
            }
            return mLista;
        }
        static private int GetRecords(string pTabla)
        {
            List<string> mLista = new List<string>();
            mQuery = "SELECT COUNT(*) FROM DIGITOSVERTICALES WHERE Tabla='" + pTabla+"'";

            return int.Parse(DAO.GetInstance().ExecuteScalar(mQuery).ToString());
        }
        static private void InsertFirst(string pTabla,string pDVV)
        {
            int mID = ProximoID();
            mQuery = "INSERT INTO DIGITOSVERTICALES (idTabla, Tabla, DVV) VALUES ('" + mID+"', '"+pTabla+"', '"+pDVV+"')";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
        }
        static public string GetDVV(string pTabla)
        {
            return mIntegridad.GetDVV(GetDVH(pTabla));
        }

        static private void DeleteDVV(string pTabla)
        {
            mQuery="DELETE DIGITOSVERTICALES WHERE Tabla='"+pTabla+"'";
            DAO.GetInstance().ExecuteNonQuery(mQuery);
        }
    }
}
