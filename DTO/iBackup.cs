﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iBackup
    {
        string Tabla { get; set; }
        string Columna { get; set; }
        string Contenido {get;set;}

        List<string> Tablas { get; set; }
    }
}
