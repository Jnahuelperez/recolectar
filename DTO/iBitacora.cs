﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iBitacora
    {
        int id { get; set; }
        string Usuario { get; set; }
        string Detalle { get; set; }
        DateTime Fecha { get; set; }
        string Nivel { get; set; }
        int? idUsuario { get; set; }
        DateTime FechaDesde { get; set; }
        DateTime FechaHasta { get; set; }
        iBitacora Crear();
    }
}
