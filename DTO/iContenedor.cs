﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iContenedor
    {
        bool Anegado { get; set; }
        int Capacidad { get; set; }
        string Detalle { get; set; }
        string Estado { get; set; }
        int id { get; set; }
        int IndiceContaminacion { get; set; }
        bool Inusual { get; set; }
        bool NivelCritico { get; set; }
        int Peso { get; set; }
        string Ubicacion { get; set; }
        iContenedor Crear();
    }
}
