﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iControles
    {
        int id { get; set; }
        string objeto { get; set; }
        string TipoIdioma { get; set; }
        string valor { get; set; }

        iControles Crear();
    }
}
