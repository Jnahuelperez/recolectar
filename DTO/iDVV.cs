﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iDVV
    {
        int idTabla { get; set; }
        string Tabla { get; set; }
        string DigitoVertical { get; set; }
        string Registro { get; set; }
        iDVV Crear();
    }
}
