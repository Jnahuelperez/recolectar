﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iFamilia
    {
        int id { get; set; }
        string detalle { get; set; }
        int idUsuario { get; set; }
        bool Activo { get; set; }
        List<iPatente> Patentes { get; set; }
        int idPatente { get; set; }
        iFamilia CrearFamilia();
    }
}
