﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iMovimiento
    {
        int id { get; set; }
        int idContenedor { get; set; }
        DateTime Fecha { get; set; }
        int PesoAgregado { get; set; }
        int IndiceCont { get; set; }
        bool Anegamiento { get; set; }
        string Estado { get; set; }
        iMovimiento Crear();
    }
}
