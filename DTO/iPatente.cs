﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iPatente
    {
        int id { get; set; }
        string detalle { get; set; }
        int idUsuario { get; set; }
        iPatente CrearPatente();
    }
}
