﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iRuta
    {
        int id { get; set; }
        string Detalle { get; set; }
        int idZona { get; set; }
        string Criticidad { get; set; }
        int Longitud { get; set; }
        bool Activo { get; set; }
        int idUsuarioRuta { get; set; }
        string Estado { get; set; }
        iRuta Crear();
    }
}
