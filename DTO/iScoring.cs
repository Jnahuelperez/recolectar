﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iScoring
    {
        int id { get; set; }
        int idRuta { get; set; }
        int idUsuario { get; set; }
        int TiempoRecCalculado { get; set; }
        int Score { get; set; }
        int idZona { get; set; }
        DateTime FechaComienzo { get; set; }
        DateTime FechaFin { get; set; }
        iScoring Crear();
    }
}
