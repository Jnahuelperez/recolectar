﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iIdioma
    {
        int id { get; set; }
        string Detalle { get; set; }
        iIdioma Crear();
    }
}
