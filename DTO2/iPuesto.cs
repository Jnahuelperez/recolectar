﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iPuesto
    {
        
        int ID { get; set; }
        string Detalle { get; set; }
        iPuesto Crear();
    }
}
