﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO2
{
    public interface iUsuario
    {
        int id { get; set; }
        string Nombre { get; set; }
        string Apellido { get; set; }
        string email { get; set; }
        string user { get; set; }
        string pwd { get; set; }
        bool Bloqueado { get; set; }
        bool Activo { get; set; }
        DateTime LastLogin { get; set; }
        DateTime CreatedOn { get; set; }
        int BadLogins { get; set; }
        int idFamilia { get; set; }
        int idIdioma { get; set; }
        iUsuario Crear();

    }
}
