﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace SERV
{
    public class Integridad
    {
        static Seguridad.Cifrado mCifra = new Seguridad.Cifrado();

        public string GetDVH(string pStringConcatenado)
        {
            return mCifra.GetHASH(pStringConcatenado);
        }
        public string GetDVV(List<string> pRegistros)
        {
            string bytes = "";
            foreach (string mRegistro in pRegistros)
            {
                bytes += ToBinary(ConvertToByteArray(mRegistro, Encoding.ASCII));
            }
            return mCifra.GetHASH(bytes.ToString());
        }
        public static byte[] ConvertToByteArray(string str, Encoding encoding)
        {
            return encoding.GetBytes(str);
        }
        public static String ToBinary(Byte[] data)
        {
            return string.Join(" ", data.Select(byt => Convert.ToString(byt, 2).PadLeft(8, '0')));
        }
    }
}
