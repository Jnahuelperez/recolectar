﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Text.RegularExpressions;

namespace SERV
{
    public class Seguridad
    {
        public class Cifrado
        {
            private const string mSecretKey = "esUnaClaveSecretaDe32Caracteres_";
            private const string mSalt = "este es un salto";
            public String GetHASH(String value)
            {
                StringBuilder Sb = new StringBuilder();
                using (SHA256 hash = SHA256Managed.Create())
                {
                    Encoding enc = Encoding.UTF8;
                    Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                    foreach (Byte b in result)
                        Sb.Append(b.ToString("x2"));
                }
                return Sb.ToString();
            }
            //Cifrado base
            private static RijndaelManaged NewRijndaelManaged()
            {
                var saltBytes = Encoding.ASCII.GetBytes(mSalt);
                var key = new Rfc2898DeriveBytes(mSecretKey, saltBytes);

                var aesAlg = new RijndaelManaged();
                aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
                aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);

                return aesAlg;
            }

            //Funcion Cifrar
            public static string CifrarTexto(string text)
            {
                if (string.IsNullOrEmpty(text))
                    throw new ArgumentNullException("text");

                var aesAlg = NewRijndaelManaged();

                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                var msEncrypt = new MemoryStream();
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(text);
                }

                return Convert.ToBase64String(msEncrypt.ToArray());
            }
            public static bool IsBase64String(string base64String)
            {
                base64String = base64String.Trim();
                return (base64String.Length % 4 == 0) &&
                       Regex.IsMatch(base64String, @"^[a-zA-Z0-9\+/]*={0,3}$", RegexOptions.None);
            }
            public static string DescifrarTexto(string cipherText)
            {
                if (string.IsNullOrEmpty(cipherText))
                    throw new ArgumentNullException("cipherText");

                if (!IsBase64String(cipherText))
                    throw new Exception("The cipherText input parameter is not base64 encoded");

                string text;

                var aesAlg = NewRijndaelManaged();
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                var cipher = Convert.FromBase64String(cipherText);

                using (var msDecrypt = new MemoryStream(cipher))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            text = srDecrypt.ReadToEnd();
                        }
                    }
                }
                return text;
            }
            //public string DescifrarTexto(string textoCifrado)
            //{
            //    return Decrypt(Convert.FromBase64String(textoCifrado));
            //}

            //private byte[] Encrypt(string texto)
            //{
            //    Byte[] encrypted = null;
            //    Byte[] bytesText = mTextEncoder.GetBytes(texto);
            //    using (MemoryStream memStream = new MemoryStream())
            //    {
            //        using (CryptoStream encryptStream = new CryptoStream(memStream, this.EncryptorTransform, CryptoStreamMode.Write))
            //        {
            //            /* Agrego 16 bytes. La longitud del vector de inicializacion */
            //            encryptStream.Write(new Byte[16], 0, 16);
            //            encryptStream.Write(bytesText, 0, bytesText.Length);
            //            encryptStream.FlushFinalBlock();
            //        }
            //        encrypted = memStream.ToArray();
            //    }
            //    return encrypted;
            //}
            //private string Decrypt(byte[] pEncriptado)
            //{
            //    Byte[] decryptedBytes = null;
            //    using (MemoryStream memStream = new MemoryStream())
            //    {
            //        using (CryptoStream decryptStream = new CryptoStream(memStream, this.DecryptorTransform, CryptoStreamMode.Write))
            //        {
            //            decryptStream.Write(pEncriptado, 0, pEncriptado.Length);
            //        }
            //        decryptedBytes = memStream.ToArray();
            //    }
            //    /* Remuevo 16 bytes. La longitud del vector de inicializacion */
            //    return this.mTextEncoder.GetString(decryptedBytes.Skip(16).Take(decryptedBytes.Length).ToArray());
            //}
        }
        public class Password
        {
            static string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%abcdefghijklmnopqrstuvwxyz0123456789";
            static char[] stringChars = new char[12];
            static Random random = new Random();

            public static void ValidPWD(string pString) 
            {
                Regex mPWDRegex = new Regex(@"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{11,}$");
                if(!mPWDRegex.IsMatch(pString))
                {
                    PasswordException mPWDExcept = new PasswordException("La contrasena no cumple con los requisitos minimos");
                    throw (mPWDExcept);
                }
            }
            public static string CrearRandomPassword(string pTo)
            {
                try
                {
                    for (int i = 0; i < stringChars.Length; i++)
                        stringChars[i] = chars[random.Next(chars.Length)];
                    String finalString = new String(stringChars);
                    EnviarPWDByMail(finalString, pTo);
                    return finalString;
                }
                catch (Exception ex)
                {
                    throw (ex);
                }
            }
            private static void EnviarPWDByMail(string pPWD,string pTo)
            {
                try
                {
                    string mAsunto = "Acceso a RecolectAR";
                    string mMensaje = "La clave para que accedas al sistema es:\t" + pPWD;
                    Notificador.email.Send(mMensaje, mAsunto, pTo);
                }
                catch (Exception) { }
            }
            private class PasswordException : Exception
            {
                public PasswordException(string pString) :
                    base("Se produjo un error al validar la contrasenia: " + pString)
                { }
            }
        }
    }
}
