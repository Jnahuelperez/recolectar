'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''  DB.vb
''  Implementation of the Class DB
''  Generated by Enterprise Architect
''  Created on:      24-jul.-2016 15:09:39
''  Original author: Hernan
''  
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''  Modification history:
''  
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



Option Explicit On
Option Strict On

Public Class DB


	Private connection As SqlConnection
	Private connectionString As String

	''' 
	''' <param name="sql"></param>
	Public Function ExecuteNonQuery(ByVal sql As String) As Integer
		ExecuteNonQuery = 0
	End Function

	''' 
	''' <param name="sql"></param>
	Public Function ExecuteScalar(ByVal sql As String) As Object
		Set ExecuteScalar = Nothing
	End Function

	''' 
	''' <param name="sql"></param>
	Public Function GetDataSet(ByVal sql As String) As DataSet
		GetDataSet = Nothing
	End Function

	''' 
	''' <param name="destino"></param>
	''' <param name="multivolumen"></param>
	''' <param name="tama�o"></param>
	Public Function RealizarBackup(ByVal destino As String, ByVal multivolumen As Boolean, ByVal tama�o As Integer) As Boolean
		RealizarBackup = False
	End Function

	''' 
	''' <param name="origen"></param>
	Public Function RestaurarBackup(ByVal origen As String) As Boolean
		RestaurarBackup = False
	End Function


End Class ' DB