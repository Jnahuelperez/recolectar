Option Explicit On
Option Strict On

Public Class BitacoraDAL


    Public Shared Function Consultar() As DataSet
        Dim sql = "SELECT b.*, u.[user] FROM bitacora b INNER JOIN usuarios u ON u.id=b.usuarioId"
        Dim ds = DB.GetDataSet(sql)
        Return ds
    End Function

    Public Shared Function GrabarMovimiento(ByVal usuarioId As Integer, ByVal criticidad As Integer, ByVal descripcion As String) As Integer
        Dim _criticidad As String = ""

        Select Case criticidad
            Case 0
                _criticidad = "BAJA"
            Case 1
                _criticidad = "MEDIA"
            Case 2
                _criticidad = "ALTA"
        End Select

        Dim sql As String = "INSERT INTO bitacora (usuarioId, criticidad, accion) VALUES(" + usuarioId.ToString() + ", '" + _criticidad + "', '" + descripcion + "'); SELECT @@IDENTITY"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))

        Return id
    End Function


End Class ' BitacoraDAL