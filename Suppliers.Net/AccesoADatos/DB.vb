Option Explicit On
Option Strict On

Imports System.Data.SqlClient

Public Class DB

    Public Shared connectionString As String
    'Public Shared connectionString As String = "Data Source=LAPTOP-S0TFOEEH;Initial Catalog=suppliers.net;Integrated Security=True"

    Shared Function GetDataSet(sql As String) As DataSet
        Dim ds As New DataSet()

        Dim da As New SqlDataAdapter(sql, connectionString)
        da.Fill(ds)
        da.Dispose()

        Return ds

    End Function

    Shared Function ExecuteNonQuery(sql As String) As Integer
        Dim mCon = New SqlConnection(connectionString)
        Dim cmd = mCon.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sql
        mCon.Open()
        Dim res = cmd.ExecuteNonQuery()
        mCon.Close()
        mCon.Dispose()
        Return res
    End Function

    Shared Function ExecuteScalar(sql As String) As Object
        Dim mCon = New SqlConnection(connectionString)
        Dim cmd = mCon.CreateCommand()
        cmd.CommandType = CommandType.Text
        cmd.CommandText = sql
        mCon.Open()
        Dim res = cmd.ExecuteScalar()
        mCon.Close()
        mCon.Dispose()
        Return res
    End Function

	Public Function RealizarBackup(ByVal destino As String, ByVal multivolumen As Boolean, ByVal tama�o As Integer) As Boolean
		RealizarBackup = False
	End Function

	Public Function RestaurarBackup(ByVal origen As String) As Boolean
		RestaurarBackup = False
	End Function


End Class ' DB