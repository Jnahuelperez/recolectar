Option Explicit On
Option Strict On

Public Class FamiliaDAL


	''' 
	''' <param name="dto"></param>
    Shared Function Agregar(ByVal dto As FamiliaDTO) As Integer
        Dim sql As String = "INSERT INTO familias (nombre, inactivo) VALUES('" + dto.nombre + "', " + CInt(dto.inactivo).ToString + "); SELECT @@IDENTITY"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

	''' 
	''' <param name="id"></param>
    Shared Sub Eliminar(ByVal id As Integer)
        Dim sql = "UPDATE familias SET inactivo = 1 WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

    Shared Sub EliminarPatente(ByVal familiaId As Integer, ByVal patenteId As Integer)
        Dim sql = "DELETE FROM familia_patente WHERE familiaId = " + familiaId.ToString + " AND patenteId = " + patenteId.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

	''' 
	''' <param name="id"></param>
    Shared Function EnUso(ByVal id As Integer) As Boolean
        Dim sql = "SELECT count(*) FROM usuario_familia WHERE familiaId = " + id.ToString
        Dim count = CInt(DB.ExecuteScalar(sql))
        Return (count > 0)
    End Function

	''' 
	''' <param name="id"></param>
    Public Shared Function GetById(id As Integer) As FamiliaDTO
        Dim sql = "SELECT * FROM familias WHERE id = " + id.ToString
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New FamiliaDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.nombre = r.Item("nombre").ToString
            dto.inactivo = CBool(r.Item("inactivo"))

            sql = "SELECT * FROM familia_patente WHERE familiaId = " + r.Item("id").ToString
            Dim dsP As DataSet = DB.GetDataSet(sql)
            If dsP.Tables(0).Rows.Count > 0 Then
                For Each f As DataRow In dsP.Tables(0).Rows
                    dto.patentes.Add(CInt(f.Item("patenteId")))
                Next
            End If
        End If

        Return dto
    End Function

    Shared Function GrabarPatente(ByVal familiaId As Integer, ByVal patenteId As Integer) As Integer
        Dim sql = "INSERT INTO familia_patente(familiaId, patenteId) OUTPUT INSERTED.id VALUES (" + familiaId.ToString + ", " + patenteId.ToString + ")"
        Return CInt(DB.ExecuteScalar(sql))
    End Function

    Shared Function Listar() As List(Of FamiliaDTO)
        Dim ds As DataSet = DB.GetDataSet("Select * from Familias")

        Dim lista As New List(Of FamiliaDTO)

        For Each dr As DataRow In ds.Tables(0).Rows
            Dim dto As New FamiliaDTO
            dto.id = CInt(dr.Item("id"))
            dto.nombre = dr.Item("nombre").ToString
            dto.inactivo = CBool(dr.Item("inactivo"))
            lista.Add(dto)

            Dim sql = "SELECT * FROM familia_patente WHERE familiaId = " + dr.Item("id").ToString
            Dim dsP As DataSet = DB.GetDataSet(sql)
            If dsP.Tables(0).Rows.Count > 0 Then
                For Each f As DataRow In dsP.Tables(0).Rows
                    dto.patentes.Add(CInt(f.Item("patenteId")))
                Next
            End If
        Next

        Return lista
    End Function

    Shared Sub Modificar(ByVal dto As FamiliaDTO)
        Dim sql As String = "UPDATE familias SET "
        sql += "inactivo = " + IIf(dto.inactivo, "1", "0").ToString + ", "
        sql += "nombre = '" + dto.nombre + "' "
        sql += "WHERE id = " + dto.id.ToString()
        DB.ExecuteNonQuery(sql)
    End Sub

End Class ' FamiliaDAL