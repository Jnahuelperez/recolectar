
Option Explicit On
Option Strict On

Public Class MovimientosDAL

    Shared Function Agregar(ByVal dto As MovimientoDTO) As Integer
        Dim sql As String = ""

        sql = sql + "INSERT INTO [dbo].[movimientos]"
        sql = sql + "           ([productoId]"
        sql = sql + "           ,[tipoMovimientoId]"
        sql = sql + "           ,[fecha]"
        sql = sql + "           ,[usuarioId]"
        sql = sql + "           ,[cantidad])"
        sql = sql + "VALUES"
        sql = sql + "           (" + dto.producto.ToString
        sql = sql + "           ," + dto.tipoMovimientoId.ToString
        sql = sql + "           ,'" + DateTime.Now.ToString("yyyyMMdd hh:mm:ss") + "'"
        sql = sql + "           ," + dto.usuario.ToString
        sql = sql + "           ," + dto.cantidad.ToString + ")"
        sql = sql + ";SELECT @@IDENTITY"

        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

    Public Sub Eliminar(ByVal id As Integer)

    End Sub

    Public Function GetById(ByVal id As Integer) As MovimientoDTO
        GetById = Nothing
    End Function

    Public Function Listar() As List(Of MovimientoDTO)
        Listar = Nothing
    End Function

    Public Sub Modificar(ByVal dto As MovimientoDTO)

    End Sub


End Class