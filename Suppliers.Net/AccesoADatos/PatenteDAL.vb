'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''  PatenteDAL.vb
''  Implementation of the Class PatenteDAL
''  Generated by Enterprise Architect
''  Created on:      24-jul.-2016 15:09:40
''  Original author: Hernan
''  
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''  Modification history:
''  
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



Option Explicit On
Option Strict On

Public Class PatenteDAL


	''' 
	''' <param name="id"></param>
    Shared Function GetById(ByVal id As Integer) As PatenteDTO
        Dim sql = "SELECT * FROM patentes WHERE id = " + id.ToString
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New PatenteDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.descripcion = r.Item("descripcion").ToString
        End If

        Return dto
    End Function

    Shared Function Listar() As List(Of PatenteDTO)
        Dim ds As DataSet = DB.GetDataSet("Select * from Patentes")

        Dim lista As New List(Of PatenteDTO)

        For Each dr As DataRow In ds.Tables(0).Rows
            Dim dto As New PatenteDTO
            dto.id = CInt(dr.Item("id"))
            dto.descripcion = dr.Item("descripcion").ToString
            lista.Add(dto)
        Next

        Return lista
    End Function


End Class ' PatenteDAL