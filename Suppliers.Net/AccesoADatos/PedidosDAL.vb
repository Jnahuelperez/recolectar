Option Explicit On
Option Strict On

Public Class PedidosDAL

    Shared Function Agregar(ByVal dto As PedidoDTO) As Integer
        Dim sql As String = ""

        sql = sql + "INSERT INTO [dbo].[pedidos] "
        sql = sql + "([proveedorId]"
        sql = sql + ",[usuarioEmisorId]"
        sql = sql + ",[fechaEmision]"
        sql = sql + ",[usuarioReceptorId]"
        sql = sql + ",[fechaRecepcion])"
        sql = sql + "VALUES "
        sql = sql + "(" + dto.proveedor.ToString
        sql = sql + "," + dto.emisorId.ToString
        sql = sql + ",'" + DateTime.Now.ToString("yyyyMMdd hh:mm:ss") + "'"
        sql = sql + ",null"
        sql = sql + ",null) "
        sql = sql + ";SELECT @@IDENTITY"

        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

    Public Sub AgregarProducto(ByVal productoId As Integer, ByVal pedidoId As Integer)

    End Sub

    Shared Sub Eliminar(ByVal id As Integer)
        Dim sql = "DELETE FROM pedidos WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

    Shared Function GetById(ByVal id As Integer) As PedidoDTO
        Dim sql = "SELECT * FROM pedidos WHERE id = " + id.ToString
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New PedidoDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            If Not IsDBNull(r.Item("fechaRecepcion")) Then
                dto.fechaRecepcion = CDate(r.Item("fechaRecepcion"))
            End If
            dto.ferchaEmision = CDate(r.Item("fechaEmision"))
            dto.proveedor = CInt(r.Item("proveedorId"))
            dto.emisorId = CInt(r.Item("usuarioEmisorId"))
        End If

        Return dto
    End Function

    Shared Function GrabarProducto(ByVal idPedido As Integer, ByVal idProducto As Integer, ByVal cantidad As Integer) As Integer

        Dim sql As String = ""
        sql = sql + "INSERT INTO [dbo].[pedido_productos]"
        sql = sql + "    ([pedidoId]"
        sql = sql + "    ,[productoId]"
        sql = sql + "    ,[cantidadPedida]"
        sql = sql + "    ,[cantidadRecibida])"
        sql = sql + "VALUES"
        sql = sql + "(" + idPedido.ToString
        sql = sql + "," + idProducto.ToString
        sql = sql + "," + cantidad.ToString
        sql = sql + "," + "0)"
        sql = sql + ";SELECT @@IDENTITY"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id

    End Function

    Shared Function Listar() As List(Of PedidoDTO)
        Dim ds As DataSet = DB.GetDataSet("Select * from pedidos")

        Dim lista As New List(Of PedidoDTO)

        For Each r As DataRow In ds.Tables(0).Rows
            Dim dto As New PedidoDTO
            dto.id = CInt(r.Item("id"))

            dto.fechaRecepcion = CDate(r.Item("ferchaEmision"))
            dto.ferchaEmision = CDate(("ferchaEmision"))
            dto.proveedor = CInt(IIf(IsDBNull(r.Item("proveedorId")), 0, r.Item("proveedorId")))
            lista.Add(dto)
        Next

        Return lista
    End Function
    Public Function ListarProductos(ByVal pedidoId As Integer) As List(Of Integer)
        ListarProductos = Nothing
    End Function
    Public Shared Sub Modificar(ByVal dto As PedidoDTO)
        Dim sql As String = ""
        sql = sql + "UPDATE [dbo].[pedidos]"
        sql = sql + "SET [proveedorId] = " + dto.proveedor.ToString
        sql = sql + "      ,[usuarioEmisorId] = " + dto.emisorId.ToString
        sql = sql + "      ,[fechaEmision] = '" + dto.ferchaEmision.ToString("yyyyMMdd hh:mm:ss") + "'"
        sql = sql + "      ,[usuarioReceptorId] = " + dto.receptorId.ToString
        sql = sql + "      ,[fechaRecepcion] = '" + dto.fechaRecepcion.Value.ToString("yyyyMMdd hh:mm:ss") + "'"
        sql = sql + "WHERE id = " + dto.id.ToString
        DB.ExecuteNonQuery(sql)

    End Sub
    Public Sub QuitarProducto(ByVal productoId As Integer, ByVal pedidoId As Integer)

    End Sub


End Class