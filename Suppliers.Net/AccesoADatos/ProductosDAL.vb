
Option Explicit On
Option Strict On

Public Class ProductosDAL

    Shared Function Agregar(ByVal dto As ProductoDTO) As Integer
        Dim sql As String = "INSERT INTO productos ([codigo], [descripcion], [proveedorId], [minimo], [optimo], [inactivo]) " & _
                            "VALUES('" + dto.codigo + "', '" + dto.descripcion + "', " + IIf(dto.proveedor = 0, "null", dto.proveedor.ToString()).ToString + ", " + dto.minimo.ToString + ", " + dto.optimo.ToString + ", " + CInt(dto.inactivo).ToString + "); SELECT @@IDENTITY"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

	''' 
	''' <param name="id"></param>
    Public Shared Sub Eliminar(ByVal id As Integer)
        Dim sql = "UPDATE productos SET inactivo = 1 WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

	''' 
	''' <param name="codigo"></param>
    Public Shared Function GetByCodigo(ByVal codigo As String) As ProductoDTO

        Dim sql = "SELECT * FROM productos WHERE codigo = '" + codigo + "'"
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New ProductoDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.codigo = r.Item("codigo").ToString
            dto.descripcion = r.Item("descripcion").ToString
            dto.proveedor = CInt(IIf(IsDBNull(r.Item("proveedorId")), 0, r.Item("proveedorId")))
            dto.minimo = CInt(r.Item("minimo"))
            dto.optimo = CInt(r.Item("optimo"))
            dto.inactivo = CBool(r.Item("inactivo"))
        End If

        Return dto
    End Function

	''' 
	''' <param name="id"></param>
    Public Shared Function GetById(ByVal id As Integer) As ProductoDTO
        Dim sql = "SELECT * FROM productos WHERE id = " + id.ToString
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New ProductoDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.codigo = r.Item("codigo").ToString
            dto.descripcion = r.Item("descripcion").ToString
            dto.proveedor = CInt(IIf(IsDBNull(r.Item("proveedorId")), 0, r.Item("proveedorId")))
            dto.minimo = CInt(r.Item("minimo"))
            dto.optimo = CInt(r.Item("optimo"))
            dto.inactivo = CBool(r.Item("inactivo"))
        End If

        Return dto
    End Function

    Shared Function GetByPedidoId(ByVal pedidoId As Integer) As List(Of ProductoDTO)
        Dim ds As DataSet = DB.GetDataSet("Select p.*, pp.cantidadRecibida, pp.cantidadPedida from productos p inner join pedido_productos pp on p.id=pp.productoId where pp.pedidoId = " & pedidoId.ToString)

        Dim lista As New List(Of ProductoDTO)

        For Each r As DataRow In ds.Tables(0).Rows
            Dim dto As New ProductoDTO
            dto.id = CInt(r.Item("id"))
            dto.codigo = r.Item("codigo").ToString
            dto.descripcion = r.Item("descripcion").ToString
            dto.proveedor = CInt(IIf(IsDBNull(r.Item("proveedorId")), 0, r.Item("proveedorId")))
            dto.minimo = CInt(r.Item("minimo"))
            dto.optimo = CInt(r.Item("optimo"))
            dto.inactivo = CBool(r.Item("inactivo"))
            dto.cantidadRecibida = CInt(r.Item("cantidadRecibida"))
            dto.cantidadPedida = CInt(r.Item("cantidadPedida"))
            lista.Add(dto)
        Next

        Return lista
    End Function

	''' 
	''' <param name="id"></param>
    Shared Function GetStock(ByVal id As Integer) As Integer
        Dim sql As String = "SELECT dbo.stock(" + id.ToString + ")"
        Return CInt(DB.ExecuteScalar(sql))
    End Function

    Public Shared Function Listar() As List(Of ProductoDTO)
        Dim ds As DataSet = DB.GetDataSet("Select * from productos")

        Dim lista As New List(Of ProductoDTO)

        For Each r As DataRow In ds.Tables(0).Rows
            Dim dto As New ProductoDTO
            dto.id = CInt(r.Item("id"))
            dto.codigo = r.Item("codigo").ToString
            dto.descripcion = r.Item("descripcion").ToString
            dto.proveedor = CInt(IIf(IsDBNull(r.Item("proveedorId")), 0, r.Item("proveedorId")))
            dto.minimo = CInt(r.Item("minimo"))
            dto.optimo = CInt(r.Item("optimo"))
            dto.inactivo = CBool(r.Item("inactivo"))
            lista.Add(dto)
        Next

        Return lista
    End Function

	''' 
	''' <param name="pedidoId"></param>
	Public Function ListarMovimientos(ByVal pedidoId As Integer) As List(Of Integer)
		ListarMovimientos = Nothing
	End Function

	''' 
	''' <param name="proveedorId"></param>
    Public Shared Function ListarParaPedido(ByVal proveedorId As Integer) As List(Of ProductoDTO)

        Dim ds As DataSet = DB.GetDataSet("Select * from productos where dbo.stock(id) < minimo and proveedorId = " & proveedorId.ToString)

        Dim lista As New List(Of ProductoDTO)

        For Each r As DataRow In ds.Tables(0).Rows
            Dim dto As New ProductoDTO
            dto.id = CInt(r.Item("id"))
            dto.codigo = r.Item("codigo").ToString
            dto.descripcion = r.Item("descripcion").ToString
            dto.proveedor = CInt(IIf(IsDBNull(r.Item("proveedorId")), 0, r.Item("proveedorId")))
            dto.minimo = CInt(r.Item("minimo"))
            dto.optimo = CInt(r.Item("optimo"))
            dto.inactivo = CBool(r.Item("inactivo"))
            lista.Add(dto)
        Next

        Return lista
    End Function

	''' 
	''' <param name="dto"></param>
    Public Shared Sub Modificar(ByVal dto As ProductoDTO)
        Dim sql As String = "UPDATE [dbo].[productos] SET "
        sql = sql + "[codigo] = '" + dto.codigo + "', "
        sql = sql + "[descripcion] = '" + dto.descripcion + "', "
        sql = sql + "[proveedorId] = " + IIf(dto.proveedor = 0, "null", dto.proveedor.ToString()).ToString + ", "
        sql = sql + "[minimo] = " + CInt(dto.minimo).ToString + ", "
        sql = sql + "[optimo] = " + CInt(dto.optimo).ToString + ", "
        sql = sql + "[inactivo] = " + CInt(dto.inactivo).ToString + " "
        sql += "WHERE id = " + dto.id.ToString()
        DB.ExecuteNonQuery(sql)
    End Sub


End Class ' ProductosDAL