Option Explicit On
Option Strict On

Public Class ProveedoresDAL


	''' 
	''' <param name="dto"></param>
    Public Shared Function Agregar(ByVal dto As ProveedorDTO) As Integer
        Dim sql As String = "INSERT INTO proveedores (razonsocial, telefono, inactivo) VALUES('" + dto.razonSocial + "', '" + dto.telefono + "', " + CInt(dto.inactivo).ToString + "); SELECT @@IDENTITY"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

	''' 
	''' <param name="id"></param>
    Public Shared Sub Eliminar(ByVal id As Integer)
        Dim sql = "UPDATE proveedores SET inactivo = 1 WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

	''' 
	''' <param name="id"></param>
    Public Shared Function GetById(ByVal id As Integer) As ProveedorDTO
        Dim sql = "SELECT * FROM proveedores WHERE id = " + id.ToString
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New ProveedorDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.razonSocial = r.Item("razonsocial").ToString
            dto.telefono = r.Item("telefono").ToString
            dto.inactivo = CBool(r.Item("inactivo"))
        End If

        Return dto
    End Function

	''' 
	''' <param name="nombre"></param>
    Public Shared Function getIdByNombre(ByVal nombre As String) As Integer
        Dim sql As String = "SELECT id FROM proveedores WHERE razonsocial ='" + nombre + "'"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

    Public Shared Function Listar() As List(Of ProveedorDTO)
        Dim ds As DataSet = DB.GetDataSet("Select * from proveedores")

        Dim lista As New List(Of ProveedorDTO)

        For Each r As DataRow In ds.Tables(0).Rows
            Dim dto As New ProveedorDTO
            dto.id = CInt(r.Item("id"))
            dto.razonSocial = r.Item("razonsocial").ToString
            dto.telefono = r.Item("telefono").ToString
            dto.inactivo = CBool(r.Item("inactivo"))
            lista.Add(dto)
        Next

        Return lista
    End Function

	''' 
	''' <param name="proveedorId"></param>
	Public Function ListarPedidos(ByVal proveedorId As Integer) As List(Of Integer)
		ListarPedidos = Nothing
	End Function

	''' 
	''' <param name="dto"></param>
    Public Shared Sub Modificar(ByVal dto As ProveedorDTO)
        Dim sql As String = "UPDATE [dbo].[proveedores] SET "
        sql = sql + "[razonsocial] = '" + dto.razonSocial + "', "
        sql = sql + "[telefono] = '" + dto.telefono + "', "
        sql = sql + "[inactivo] = " + CInt(dto.inactivo).ToString + " "
        sql += "WHERE id = " + dto.id.ToString()
        DB.ExecuteNonQuery(sql)

    End Sub


End Class ' ProveedoresDAL