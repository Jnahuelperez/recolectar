Option Explicit On
Option Strict On

Imports System.Data.SqlClient

Public Class SeguridadDAL
    ''' 
    ''' <param name="usuarioId"></param>
    ''' <param name="patente"></param>
    Public Shared Function CheckPatente(ByVal usuarioId As Integer, ByVal patente As String) As Boolean
        Dim sql As String = ""

        sql = sql + "DECLARE @usuarioid int = " + usuarioId.ToString + vbCrLf
        sql = sql + "DECLARE @patente varchar(50) = '" + patente + "'" + vbCrLf
        sql = sql + "SELECT 1" + vbCrLf
        sql = sql + "WHERE EXISTS( SELECT * FROM usuario_patente up INNER JOIN patentes p ON p.id = up.patenteId WHERE up.usuarioId=@usuarioid AND p.descripcion=@patente AND up.habilitado=1 )" + vbCrLf
        sql = sql + "OR EXISTS(SELECT * FROM usuario_familia uf INNER JOIN familias f ON f.id = uf.familiaId INNER JOIN familia_patente fp ON fp.familiaId = f.id INNER JOIN patentes p ON p.id = fp.patenteId WHERE p.descripcion = @patente AND uf.usuarioId = @usuarioid)" + vbCrLf
        sql = sql + "AND NOT EXISTS( SELECT * FROM usuario_patente up INNER JOIN patentes p ON p.id = up.patenteId WHERE up.usuarioId=@usuarioid AND p.descripcion=@patente AND up.habilitado=0 )"

        Dim ds = DB.GetDataSet(sql)
        Return ds.Tables(0).Rows.Count > 0
    End Function

	''' 
	''' <param name="patente"></param>
    Public Shared Function GetCantidadUsuariosConPatente(ByVal patente As String) As Integer
        Dim sql As String = ""

        sql = sql + "SELECT COUNT(*) "
        sql = sql + "FROM usuarios "
        sql = sql + "WHERE id IN "
        sql = sql + "("
        sql = sql + "   SELECT "
        sql = sql + "        usuarioId "
        sql = sql + "     FROM "
        sql = sql + "        usuario_patente up "
        sql = sql + "	INNER JOIN patentes p "
        sql = sql + "		ON p.id = up.patenteId "
        sql = sql + "	INNER JOIN usuarios u "
        sql = sql + "		ON u.id = up.usuarioId "
        sql = sql + "   WHERE "
        sql = sql + "	    p.descripcion = '" + patente + "' AND "
        sql = sql + "       up.habilitado = 1 AND "
        sql = sql + "       u.inactivo = 0 "
        sql = sql + "   UNION ALL SELECT "
        sql = sql + "       usuarioId "
        sql = sql + "   FROM "
        sql = sql + "        usuario_familia uf "
        sql = sql + "	INNER JOIN familias f "
        sql = sql + "		ON f.id = uf.familiaId "
        sql = sql + "	INNER JOIN familia_patente fp "
        sql = sql + "		ON fp.familiaId = f.id "
        sql = sql + "	INNER JOIN patentes p "
        sql = sql + "		ON p.id = fp.patenteId "
        sql = sql + "	INNER JOIN usuarios u "
        sql = sql + "		ON u.id = uf.usuarioId "
        sql = sql + "   WHERE u.inactivo = 0 AND "
        sql = sql + "	    p.descripcion = '" + patente + "' "
        sql = sql + ")"

        Return CInt(DB.ExecuteScalar(sql))
    End Function

	''' 
    ''' 
    ''' <param name="tabla"></param>
    Public Function GetForVerificarIntegridadH(ByVal tabla As String) As DataSet
        GetForVerificarIntegridadH = Nothing
    End Function

    ''' 
    ''' <param name="tabla"></param>
    Public Function GetForVerificarIntegridadV(ByVal tabla As String) As Long
        GetForVerificarIntegridadV = 0
    End Function

    Public Shared Function ReadForDVH(ByVal tabla As String, ByVal registroId As Integer) As DataSet
        Dim sql As String = "SELECT * FROM " + tabla + " WHERE id = " + registroId.ToString()
        Return DB.GetDataSet(sql)
    End Function

    Public Shared Function ReadForDVH(ByVal tabla As String) As DataSet
        Dim sql As String = "SELECT * FROM " + tabla
        Return DB.GetDataSet(sql)
    End Function

    Public Shared Function ReadForDVV(ByVal tabla As String) As DataSet
        Dim sql As String = "SELECT sum(dvh) as dvh FROM " + tabla
        Return DB.GetDataSet(sql)
    End Function

    Public Shared Function GetDVV(ByVal tabla As String) As Long
        Dim sql As String = "SELECT dvv FROM dvvs WHERE tabla = '" + tabla + "'"
        Dim ds = DB.GetDataSet(sql)
        If ds.Tables(0).Rows.Count > 0 Then
            Return CLng(ds.Tables(0).Rows(0).Item("dvv"))
        Else
            Return 0
        End If
    End Function

	Public Shared Function RealizarBackup(ByVal destino As String, ByVal multivolumen As Boolean, ByVal tama�o As Integer) As Boolean		
		DB.ExecuteNonQuery("BACKUP DATABASE [suppliers.net] TO DISK = '" & destino & "'")
        Return True
	End Function

	Public Shared Function RestaurarBackup(ByVal origen As String) As Boolean
		DB.ExecuteNonQuery("use master; RESTORE DATABASE [suppliers.net] FROM DISK = '" & origen & "' WITH REPLACE")	
		return true
	End Function

    Public Shared Sub SaveDVH(ByVal tabla As String, ByVal registroId As Integer, ByVal dvh As Long)
        Dim sql As String = "UPDATE " + tabla + " SET dvh = " + dvh.ToString() + " WHERE id = " + registroId.ToString()
        DB.ExecuteNonQuery(sql)
    End Sub

    Public Shared Sub SaveDVV(ByVal table As String, ByVal dvv As Long)
        Dim sql = "IF EXISTS(SELECT TOP 1 1 FROM dvvs WHERE tabla='" + table + "') " + _
                    "UPDATE dvvs SET dvv = " + dvv.ToString + " WHERE tabla='" + table + "'; " + _
                    "ELSE " + _
                    "INSERT INTO dvvs (tabla, dvv) VALUES ('" + table + "', " + dvv.ToString + ");"
        DB.ExecuteNonQuery(sql)
    End Sub

    Public Shared Function isConnectionStringValid(connectionString As String) As Boolean
        Dim mCon As SqlConnection

        Try
            mCon = New SqlConnection(connectionString)
            mCon.Open()
            mCon.Close()
            mCon.Dispose()
            Return True
        Catch ex As Exception
            If Not mCon Is Nothing Then
                mCon.Dispose()
            End If
            Return False
        End Try

    End Function

End Class ' SeguridadDAL