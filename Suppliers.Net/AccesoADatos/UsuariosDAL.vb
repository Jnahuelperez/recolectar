Option Explicit On
Option Strict On

Public Class UsuariosDAL


	''' 
	''' <param name="dto"></param>
    Shared Function Agregar(ByVal dto As UsuarioDTO) As Integer
        Dim sql As String = "INSERT INTO usuarios (nombre, email, [user], password, inactivo) VALUES('" + dto.nombre + "', '" + dto.email + "', '" + dto.user + "', '" + dto.password + "', " + CInt(dto.inactivo).ToString + "); SELECT @@IDENTITY"
        Dim id As Integer = CInt(DB.ExecuteScalar(sql))
        Return id
    End Function

	''' 
	''' <param name="idiomaId"></param>
    Shared Sub CambiarIdioma(ByVal idiomaId As Integer, id As Integer)
        Dim sql As String = "update usuarios set idiomaid=" + idiomaId.ToString + " where id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

	''' 
	''' <param name="id"></param>
	''' <param name="pass"></param>
    Shared Sub CambiarPass(ByVal id As Integer, ByVal pass As String)
        Dim sql = "UPDATE usuarios SET password = '" + pass + "' WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

	''' 
	''' <param name="id"></param>
    Shared Sub Desbloquear(ByVal id As Integer)
        Dim sql = "UPDATE usuarios SET bloqueado = 0 WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

	''' 
	''' <param name="id"></param>
    Shared Sub Eliminar(ByVal id As Integer)
        Dim sql = "UPDATE usuarios SET inactivo = 1 WHERE id = " + id.ToString
        DB.ExecuteNonQuery(sql)
    End Sub


	''' 
	''' <param name="id"></param>
    Shared Function GetById(ByVal id As Integer) As UsuarioDTO
        Dim sql = "SELECT * FROM usuarios WHERE id = " + id.ToString
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New UsuarioDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.nombre = r.Item("nombre").ToString
            dto.email = r.Item("email").ToString
            dto.user = r.Item("user").ToString
            dto.inactivo = CBool(r.Item("inactivo"))
            dto.bloqueado = CBool(r.Item("bloqueado"))

            sql = "SELECT * FROM usuario_familia WHERE usuarioId = " + id.ToString
            Dim dsF As DataSet = DB.GetDataSet(sql)
            If dsF.Tables(0).Rows.Count > 0 Then
                For Each f As DataRow In dsF.Tables(0).Rows
                    dto.familias.Add(CInt(f.Item("familiaId")))
                Next
            End If

            sql = "SELECT * FROM usuario_patente WHERE usuarioId = " + id.ToString
            Dim dsP As DataSet = DB.GetDataSet(sql)
            If dsP.Tables(0).Rows.Count > 0 Then
                For Each p As DataRow In dsP.Tables(0).Rows
                    dto.patentes.Add(CInt(p.Item("patenteId")), CBool(p.Item("habilitado")))
                Next
            End If
            Return dto
        Else
            Return Nothing
        End If

    End Function

	''' 
	''' <param name="user"></param>
    Public Shared Function GetByUser(ByVal user As String) As UsuarioDTO
        Dim sql = "SELECT * FROM usuarios WHERE [user] = '" + user + "'"
        Dim ds As DataSet = DB.GetDataSet(sql)

        Dim dto As New UsuarioDTO()

        If ds.Tables(0).Rows.Count > 0 Then
            Dim r = ds.Tables(0).Rows(0)
            dto.id = CInt(r.Item("id"))
            dto.nombre = r.Item("nombre").ToString
            dto.email = r.Item("email").ToString
            dto.user = r.Item("user").ToString
            dto.inactivo = CBool(r.Item("inactivo"))
            dto.bloqueado = CBool(r.Item("bloqueado"))

            sql = "SELECT * FROM usuario_familia WHERE usuarioId = " + dto.id.ToString
            Dim dsF As DataSet = DB.GetDataSet(sql)
            If dsF.Tables(0).Rows.Count > 0 Then
                For Each f As DataRow In dsF.Tables(0).Rows
                    dto.familias.Add(CInt(f.Item("familiaId")))
                Next
            End If

            sql = "SELECT * FROM usuario_patente WHERE usuarioId = " + dto.id.ToString
            Dim dsP As DataSet = DB.GetDataSet(sql)
            If dsP.Tables(0).Rows.Count > 0 Then
                For Each p As DataRow In dsP.Tables(0).Rows
                    dto.patentes.Add(CInt(p.Item("patenteId")), CBool(p.Item("habilitado")))
                Next
            End If
            Return dto
        Else
            Return Nothing
        End If
    End Function

    Shared Sub EliminarFamilia(ByVal usuarioId As Integer, ByVal familiaId As Integer)
        Dim sql = "DELETE FROM usuario_familia WHERE usuarioId = " + usuarioId.ToString + " AND familiaId = " + familiaId.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

    Shared Sub EliminarPatente(ByVal usuarioId As Integer, ByVal patenteId As Integer)
        Dim sql = "DELETE FROM usuario_patente WHERE usuarioId = " + usuarioId.ToString + " AND patenteId = " + patenteId.ToString
        DB.ExecuteNonQuery(sql)
    End Sub

    Shared Function GrabarFamilia(ByVal usuarioId As Integer, ByVal familiaId As Integer) As Integer
        Dim sql = "INSERT INTO usuario_familia(usuarioId, familiaId) OUTPUT INSERTED.id VALUES (" + usuarioId.ToString + ", " + familiaId.ToString + ")"
        Return CInt(DB.ExecuteScalar(sql))
    End Function
    Shared Function GrabarPatente(ByVal usuarioId As Integer, ByVal patenteId As Integer, ByVal habilitado As Boolean) As Integer
        Dim sql = "INSERT INTO usuario_patente(usuarioId, patenteId, habilitado) OUTPUT INSERTED.id VALUES (" + usuarioId.ToString + ", " + patenteId.ToString + ", " + CInt(habilitado).ToString + ")"
        Return CInt(DB.ExecuteScalar(sql))
    End Function

    Shared Function ModificarPatente(ByVal usuarioId As Integer, ByVal patenteId As Integer, ByVal habilitado As Boolean) As Integer
        Dim sql = "UPDATE usuario_patente SET habilitado = " + CInt(habilitado).ToString + " OUTPUT INSERTED.id WHERE usuarioId = " + usuarioId.ToString + " AND patenteId = " + patenteId.ToString
        Return CInt(DB.ExecuteScalar(sql))
    End Function

    Public Shared Function Listar() As List(Of UsuarioDTO)
        Dim ds As DataSet = DB.GetDataSet("Select * from Usuarios")

        Dim lista As New List(Of UsuarioDTO)

        For Each r As DataRow In ds.Tables(0).Rows
            Dim dto As New UsuarioDTO
            dto.id = CInt(r.Item("id"))
            dto.nombre = r.Item("nombre").ToString
            dto.email = r.Item("email").ToString
            dto.user = r.Item("user").ToString
            dto.inactivo = CBool(r.Item("inactivo"))
            dto.bloqueado = CBool(r.Item("bloqueado"))
            lista.Add(dto)
        Next

        Return lista
    End Function

    Public Shared Function Login(ByVal user As String, ByVal pass As String) As UsuarioDTO
        Dim sql = "SELECT * FROM usuarios WHERE inactivo=0 AND [user] = '" + user + "'"
        Dim ds = DB.GetDataSet(sql)

        ' si el user est� mal, retorno null
        If (ds.Tables(0).Rows.Count = 0) Then
            Return Nothing
        Else
            'si el user est� ok, lo devuelvo, indicando si  est� logueado
            Dim r = ds.Tables(0).Rows(0)
            Dim dto As New UsuarioDTO
            dto.bloqueado = CBool(r.Item("bloqueado"))
            dto.email = r.Item("email").ToString
            dto.id = CInt(r.Item("id"))
            dto.idiomaId = CInt(IIf(IsDBNull(r.Item("idiomaId")), 0, r.Item("idiomaId")))
            dto.inactivo = CBool(r.Item("inactivo"))
            dto.intentos = CInt(r.Item("intentos"))
            dto.logueado = (r.Item("password").ToString = pass)
            dto.nombre = r.Item("nombre").ToString
            'dto.password = CBool(r.Item("password"))
            dto.user = r.Item("user").ToString
            If (IsDBNull(r.Item("idiomaId"))) Then
                dto.idiomaId = 1
            Else
                dto.idiomaId = CInt(r.Item("idiomaId"))
            End If

            Return dto
        End If
    End Function

	Public Sub Logout()

	End Sub

	''' 
	''' <param name="dto"></param>
    Public Shared Sub Modificar(ByVal dto As UsuarioDTO)
        Dim sql As String = "UPDATE usuarios SET "
        sql += "bloqueado = " + IIf(dto.bloqueado, "1", "0").ToString + ", "
        sql += "email = '" + dto.email + "', "
        sql += "idiomaId= " + IIf(dto.idiomaId = 0, "null", dto.idiomaId.ToString).ToString + ", "
        sql += "inactivo = " + IIf(dto.inactivo, "1", "0").ToString + ", "
        sql += "intentos = " + dto.intentos.ToString + ", "
        sql += "nombre = '" + dto.nombre + "', "
        sql += "[user] = '" + dto.user + "' "
        sql += "WHERE id = " + dto.id.ToString()
        DB.ExecuteNonQuery(sql)
    End Sub

End Class ' UsuariosDAL