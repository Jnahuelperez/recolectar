'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''  ProveedorDTO.vb
''  Implementation of the Class ProveedorDTO
''  Generated by Enterprise Architect
''  Created on:      24-jul.-2016 15:08:59
''  Original author: Hernan
''  
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''  Modification history:
''  
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



Option Explicit On
Option Strict On

Public Class ProveedorDTO


	Public id As Integer
	Public inactivo As Boolean
	Public razonSocial As String
	Public telefono As String


End Class ' ProveedorDTO