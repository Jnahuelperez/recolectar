
Option Explicit On
Option Strict On

Public Class Bitacora


    Public Shared Function Consultar() As DataSet
        Return BitacoraDAL.Consultar()
    End Function

    Public Shared Sub GrabarMovimiento(ByVal usuarioId As Integer, ByVal criticidad As Integer, ByVal descripcion As String)
        'descripcion = Seguridad.EncriptarReversible(descripcion)
        Dim id As Integer = BitacoraDAL.GrabarMovimiento(usuarioId, criticidad, Seguridad.EncriptarReversible(descripcion))
        Seguridad.CalcularDV("bitacora", id)
    End Sub


End Class ' Bitacora