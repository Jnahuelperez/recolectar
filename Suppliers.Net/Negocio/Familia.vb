Option Explicit On
Option Strict On

Public Class Familia

	Public id As Integer
	Public inactivo As Boolean
	Public nombre As String
    Public patentes As New List(Of Patente)

    Public Sub Agregar()
        Dim dto = Me.ToDTO

        Me.id = FamiliaDAL.Agregar(dto)
    End Sub

    Shared Sub Eliminar(ByVal id As Integer)
        FamiliaDAL.Eliminar(id)
    End Sub

    Shared Function EnUso(ByVal id As Integer) As Boolean
        Return FamiliaDAL.EnUso(id)
    End Function

    Shared Function Listar() As List(Of Familia)
        Dim datos As List(Of FamiliaDTO) = FamiliaDAL.Listar()

        Dim lista As New List(Of Familia)

        For Each dto As FamiliaDTO In datos
            Dim a As New Familia(dto)
            lista.Add(a)
        Next

        Return lista
    End Function

	Public Sub Modificar()
        FamiliaDAL.Modificar(Me.ToDTO)
	End Sub

	Public Sub New(ByVal dto As FamiliaDTO)
        Me.id = dto.id
        Me.nombre = Seguridad.Desencriptar(dto.nombre)
        Me.inactivo = dto.inactivo

        If Not dto.patentes Is Nothing Then
            For Each p As Integer In dto.patentes
                Me.patentes.Add(New Patente(p))
            Next
        End If
	End Sub

	Public Sub New()

	End Sub

    Public Sub New(ByVal id As Integer)
        Me.New(FamiliaDAL.GetById(id))
    End Sub

    Public Sub GrabarPatente(ByVal patenteId As Integer)
        Dim id = FamiliaDAL.GrabarPatente(Me.id, patenteId)
        Seguridad.CalcularDV("familia_patente", id)
    End Sub

    Public Sub EliminarPatente(ByVal patenteId As Integer)
        FamiliaDAL.EliminarPatente(Me.id, patenteId)
        Seguridad.CalcularDV("familia_patente", 0)
    End Sub

    Public Function ToDTO() As FamiliaDTO
        Dim dto As New FamiliaDTO

        dto.id = Me.id
        dto.nombre = Seguridad.EncriptarReversible(Me.nombre)
        dto.inactivo = Me.inactivo

        Return dto
    End Function


End Class ' Familia