Option Explicit On
Option Strict On

Public Class Mailer


    Shared Sub EnviarMail(ByVal destinatary As String, ByVal message As String)
        Try
            Dim fileName As String = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\pass.txt"
            Dim file As System.IO.StreamWriter

            If System.IO.File.Exists(fileName) = True Then
                System.IO.File.Delete(fileName)
            End If

            file = My.Computer.FileSystem.OpenTextFileWriter(fileName, True)
            file.WriteLine(message)
            file.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        End Try
    End Sub

End Class