
Option Explicit On
Option Strict On

Public Class Movimiento


	Public cantidad As Integer
	Public fecha As Datetime
	Public id As Integer
	Public producto As Producto
	Public tipoMovimientoId As Integer
	Public usuario As Usuario
	Public m_Producto As Producto
	Public m_Usuario As Usuario

	Public Sub Agregar()
        Dim dto = Me.ToDTO
        Me.id = MovimientosDAL.Agregar(dto)
        Seguridad.CalcularDV("movimientos", Me.id)
	End Sub

	Public Sub Eliminar(ByVal id As Integer)

	End Sub

	Public Function Listar() As List(Of Movimiento)
		Listar = Nothing
	End Function

	Public Sub Modificar()

	End Sub

	Public Sub New()

	End Sub

	Public Sub New(ByVal id As Integer)

	End Sub

	Public Sub New(ByVal dto As MovimientoDTO)

	End Sub

    Public Function ToDTO() As MovimientoDTO
        Dim dto As New MovimientoDTO

        dto.cantidad = Me.cantidad
        dto.fecha = Me.fecha
        dto.id = Me.id
        dto.producto = Me.producto.id
        dto.tipoMovimientoId = Me.tipoMovimientoId
        dto.usuario = Me.usuario.id

        Return dto
    End Function


End Class