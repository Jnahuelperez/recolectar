
Option Explicit On
Option Strict On

Imports System.Data
Imports System.Windows.Forms

Public Class MultiIdioma


	''' 
	''' <param name="frm"></param>
    Public Shared Sub ConfigurarForm(ByVal frm As Windows.Forms.Form)
        If (Seguridad.currentUser Is Nothing) Then Return

        Dim dsLiterales = MultiIdiomaDAL.GetLiterales(frm.Name, Seguridad.currentUser.idiomaId)

        For Each dr As DataRow In dsLiterales.Tables(0).Rows
            'veo si es el propio formulario
            If (frm.Name = dr.Item("control").ToString()) Then
                setCtrlProp(Of Form)(frm, dr)
            Else
                'veo si es un control del formulario
                For Each ctrl As System.Windows.Forms.Control In frm.Controls
                    'veo si es un control gen�rico
                    If (ctrl.Name = dr.Item("control").ToString()) Then
                        Select Case ctrl.GetType.ToString
                            Case "System.Windows.Forms.Button"
                                setCtrlProp(Of Button)(ctrl, dr)
                                Exit For
                            Case "System.Windows.Forms.Label"
                                setCtrlProp(Of Label)(ctrl, dr)
                                Exit For
                            Case "System.Windows.Forms.DataGridView"
                                setCtrlProp(Of DataGridView)(ctrl, dr)
                                Exit For
                            Case "System.Windows.Forms.DateTimePicker"
                                setCtrlProp(Of DateTimePicker)(ctrl, dr)
                                Exit For
                            Case "System.Windows.Forms.ComboBox"
                                setCtrlProp(Of ComboBox)(ctrl, dr)
                                Exit For
                        End Select
                    End If

                    'veo si es la columna de alguna grilla
                    If ctrl.GetType.ToString() = "System.Windows.Forms.DataGridView" Then
                        Dim obj = DirectCast(ctrl, DataGridView)
                        For Each c As DataGridViewColumn In obj.Columns
                            If c.Name = dr.Item("control").ToString() Then
                                setCtrlProp(Of DataGridViewColumn)(c, dr)
                                Exit For
                            End If
                        Next
                    End If

                    'veo si es un item de menu
                    If ctrl.GetType.ToString() = "System.Windows.Forms.MenuStrip" Then
                        Dim obj = DirectCast(ctrl, MenuStrip)

                        For Each item As ToolStripMenuItem In obj.Items
                            If item.Name = dr.Item("control").ToString() Then
                                setCtrlProp(Of ToolStripMenuItem)(item, dr)
                                Exit For
                            Else
                                ' veo si es in item del segundo nivel
                                For Each subitem As ToolStripMenuItem In item.DropDown.Items
                                    If subitem.Name = dr.Item("control").ToString() Then
                                        setCtrlProp(Of ToolStripMenuItem)(subitem, dr)
                                        Exit For
                                    End If
                                Next
                            End If
                        Next
                    End If

                    If (ctrl.Controls.Count > 0) Then
                        For Each ctrl1 As System.Windows.Forms.Control In ctrl.Controls
                            'veo si es un control gen�rico
                            If (ctrl1.Name = dr.Item("control").ToString()) Then
                                Select Case ctrl1.GetType.ToString
                                    Case "System.Windows.Forms.Button"
                                        setCtrlProp(Of Button)(ctrl1, dr)
                                        Exit For
                                    Case "System.Windows.Forms.Label"
                                        setCtrlProp(Of Label)(ctrl1, dr)
                                        Exit For
                                    Case "System.Windows.Forms.DataGridView"
                                        setCtrlProp(Of DataGridView)(ctrl1, dr)
                                        Exit For
                                    Case "System.Windows.Forms.DateTimePicker"
                                        setCtrlProp(Of DateTimePicker)(ctrl1, dr)
                                        Exit For
                                    Case "System.Windows.Forms.ComboBox"
                                        setCtrlProp(Of ComboBox)(ctrl1, dr)
                                        Exit For
                                End Select
                            End If

                            'veo si es la columna de alguna grilla
                            If ctrl1.GetType.ToString() = "System.Windows.Forms.DataGridView" Then
                                Dim obj = DirectCast(ctrl1, DataGridView)
                                For Each c As DataGridViewColumn In obj.Columns
                                    If c.Name = dr.Item("control").ToString() Then
                                        setCtrlProp(Of DataGridViewColumn)(c, dr)
                                        Exit For
                                    End If
                                Next
                            End If
                        Next
                    End If
                Next
            End If
        Next
    End Sub

    Private Shared Sub setCtrlProp(Of T)(ctrl As Object, dr As DataRow)
        Dim obj = DirectCast(ctrl, T)

        For Each p As System.Reflection.PropertyInfo In obj.GetType.GetProperties()
            If p.Name = dr.Item("propiedad").ToString() Then
                p.SetValue(ctrl, dr.Item("text").ToString())
            End If
        Next
    End Sub

    Shared Function GetIdiomas() As DataSet
        Return MultiIdiomaDAL.GetIdiomas
    End Function

    'de aca para abajo, eliminar
    Public Shared Sub PrintLiterals(ByVal frm As Windows.Forms.Form)

        Console.WriteLine(GenerateInsertLine(frm.Name, frm.Name, "Text", frm.Text))

        For Each ctrl As System.Windows.Forms.Control In frm.Controls
            'veo si es un control gen�rico
            Select Case ctrl.GetType.ToString
                Case "System.Windows.Forms.Button"
                    Console.WriteLine(GenerateInsertLine(frm.Name, ctrl.Name, "Text", ctrl.Text))
                Case "System.Windows.Forms.Label"
                    Console.WriteLine(GenerateInsertLine(frm.Name, ctrl.Name, "Text", ctrl.Text))
                Case "System.Windows.Forms.DataGridView"
                    Dim obj = DirectCast(ctrl, DataGridView)
                    For Each c As DataGridViewColumn In obj.Columns
                        Console.WriteLine(GenerateInsertLine(frm.Name, c.Name, "HeaderText", c.HeaderText))
                    Next
                Case "System.Windows.Forms.MenuStrip"
                    Dim obj = DirectCast(ctrl, MenuStrip)
                    For Each item As ToolStripMenuItem In obj.Items
                        Console.WriteLine(GenerateInsertLine(frm.Name, item.Name, "Text", item.Text))
                        For Each subitem As ToolStripMenuItem In item.DropDown.Items
                            Console.WriteLine(GenerateInsertLine(frm.Name, subitem.Name, "Text", subitem.Text))
                        Next
                    Next
            End Select
            If (Not ctrl.Tag Is Nothing) Then
                Console.WriteLine(GenerateInsertLine(frm.Name, ctrl.Name, "Tag", ctrl.Tag.ToString))
            End If

            If (ctrl.Controls.Count > 0) Then
                For Each ctrl1 As System.Windows.Forms.Control In ctrl.Controls
                    'veo si es un control gen�rico
                    Select Case ctrl1.GetType.ToString
                        Case "System.Windows.Forms.Button"
                            Console.WriteLine(GenerateInsertLine(frm.Name, ctrl1.Name, "Text", ctrl1.Text))
                        Case "System.Windows.Forms.Label"
                            Console.WriteLine(GenerateInsertLine(frm.Name, ctrl1.Name, "Text", ctrl1.Text))
                        Case "System.Windows.Forms.DataGridView"
                            Dim obj = DirectCast(ctrl1, DataGridView)
                            For Each c As DataGridViewColumn In obj.Columns
                                Console.WriteLine(GenerateInsertLine(frm.Name, c.Name, "HeaderText", c.HeaderText))
                            Next
                        Case "System.Windows.Forms.MenuStrip"
                            Dim obj = DirectCast(ctrl1, MenuStrip)
                            For Each item As ToolStripMenuItem In obj.Items
                                Console.WriteLine(GenerateInsertLine(frm.Name, item.Name, "Text", item.Text))
                                For Each subitem As ToolStripMenuItem In item.DropDown.Items
                                    Console.WriteLine(GenerateInsertLine(frm.Name, subitem.Name, "Text", subitem.Text))
                                Next
                            Next
                    End Select
                    If (Not ctrl1.Tag Is Nothing) Then
                        Console.WriteLine(GenerateInsertLine(frm.Name, ctrl1.Name, "Tag", ctrl1.Tag.ToString))
                    End If
                Next
            End If
        Next
    End Sub

    Public Shared Function GenerateInsertLine(frmName As String, ctrlName As String, propertyName As String, text As String) As String
        'Return "INSERT INTO literales (idiomaId, formulario, [control], [text], propiedad) VALUES (1,  " +
        Return "'" + frmName + "', '" + ctrlName + "', '" + text + "', '" + propertyName + "')"
    End Function


End Class ' MultiIdioma