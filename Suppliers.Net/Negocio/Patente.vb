Option Explicit On
Option Strict On

Public Class Patente


	Public descripcion As String
	Public id As Integer

    Shared Function Listar() As List(Of Patente)
        Dim datos As List(Of PatenteDTO) = PatenteDAL.Listar()

        Dim lista As New List(Of Patente)

        For Each dto As PatenteDTO In datos
            Dim a As New Patente(dto)
            lista.Add(a)
        Next

        Return lista
    End Function

	''' 
	''' <param name="dto"></param>
	Public Sub New(ByVal dto As PatenteDTO)
        Me.id = dto.id
        Me.descripcion = dto.descripcion
	End Sub

	Public Sub New()

	End Sub

	''' 
	''' <param name="id"></param>
    Public Sub New(ByVal id As Integer)
        Me.New(PatenteDAL.GetById(id))
    End Sub

	Public Function ToDTO() As PatenteDTO
		ToDTO = Nothing
	End Function


End Class ' Patente