
Option Explicit On
Option Strict On

Public Class Pedido

    Public fechaRecepcion As DateTime?
	Public ferchaEmision As Datetime
	Public id As Integer
	Public productos As List(Of Producto)
	Public proveedor As Proveedor
	Public usuarioEmisor As Usuario
	Public usuarioReceptor As Usuario
	Public m_Usuario As Usuario

	Public Sub Agregar()
        Dim dto = Me.ToDTO
        Me.id = PedidosDAL.Agregar(dto)
        Seguridad.CalcularDV("pedidos", Me.id)
	End Sub

    Public Sub AgregarProducto(ByVal productoId As Integer, ByVal cantidad As Integer)
        Dim id = PedidosDAL.GrabarProducto(Me.id, productoId, cantidad)
        Seguridad.CalcularDV("pedido_productos", id)
    End Sub

    Shared Sub Eliminar(ByVal id As Integer)
        PedidosDAL.Eliminar(id)
        Seguridad.CalcularDV("pedidos", 0)
    End Sub

    Shared Function Listar() As List(Of Pedido)
        Dim datos As List(Of PedidoDTO) = PedidosDAL.Listar()

        Dim lista As New List(Of Pedido)

        For Each dto As PedidoDTO In datos
            Dim a As New Pedido(dto)
            lista.Add(a)
        Next

        Return lista
    End Function

    Public Sub Modificar()
        PedidosDAL.Modificar(Me.ToDTO)
        Seguridad.CalcularDV("pedidos", Me.id)
    End Sub

    Public Sub New(ByVal dto As PedidoDTO)

    End Sub

	Public Sub New()

	End Sub

	Public Sub New(ByVal id As Integer)
        Dim dto = PedidosDAL.GetById(id)

        Me.id = dto.id
        Me.fechaRecepcion = dto.fechaRecepcion
        Me.ferchaEmision = dto.ferchaEmision
        Me.proveedor = New Proveedor(dto.proveedor)
        Me.usuarioEmisor = New Usuario(dto.emisorId)
        Me.productos = Producto.GetByPedidoId(id)
    End Sub

	Public Function ToDTO() As PedidoDTO
        Dim dto As New PedidoDTO
        dto.id = Me.id
        dto.fechaRecepcion = Me.fechaRecepcion
        dto.ferchaEmision = Me.ferchaEmision
        dto.proveedor = Me.proveedor.id
        dto.emisorId = Me.usuarioEmisor.id
        If Not (usuarioReceptor Is Nothing) Then
            dto.receptorId = Me.usuarioReceptor.id
        End If

        Return dto

    End Function


End Class