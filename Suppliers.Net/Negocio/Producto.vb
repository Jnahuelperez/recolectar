
Option Explicit On
Option Strict On

Public Class Producto
	Public codigo As String
	Public descripcion As String
	Public id As Integer
	Public inactivo As Boolean
	Public minimo As Integer
	Public optimo As Integer
	Public proveedor As Proveedor
	Public m_Proveedor As Proveedor
    Public cantidadPedida As Integer
    Public cantidadRecibida As Integer

    Public Sub Agregar()
        Dim dto = Me.ToDTO

        Me.id = ProductosDAL.Agregar(dto)
    End Sub

    ''' 
    ''' <param name="id"></param>
    Public Shared Sub Eliminar(ByVal id As Integer)
        ProductosDAL.Eliminar(id)
    End Sub

    ''' 
    ''' <param name="pedidoId"></param>
    Shared Function GetByPedidoId(ByVal pedidoId As Integer) As List(Of Producto)
        Dim datos = ProductosDAL.GetByPedidoId(pedidoId)
        Dim lista As New List(Of Producto)

        For Each dto As ProductoDTO In datos
            Dim a As New Producto(dto)
            lista.Add(a)
        Next

        Return lista
    End Function

    Public Function GetStock() As Integer
        Return ProductosDAL.GetStock(Me.id)
    End Function

    Public Shared Function Listar() As List(Of Producto)
        Dim datos As List(Of ProductoDTO) = ProductosDAL.Listar()

        Dim lista As New List(Of Producto)

        For Each dto As ProductoDTO In datos
            Dim a As New Producto(dto)
            lista.Add(a)
        Next

        Return lista
    End Function

    Public Shared Function ListarParaPedido(ByVal proveedorId As Integer) As List(Of Producto)
        Dim datos = ProductosDAL.ListarParaPedido(proveedorId)
        Dim lista As New List(Of Producto)

        For Each dto As ProductoDTO In datos
            Dim a As New Producto(dto)
            lista.Add(a)
        Next

        Return lista

    End Function

    Public Sub Modificar()
        ProductosDAL.Modificar(Me.ToDTO)
    End Sub

    Public Sub New(ByVal dto As ProductoDTO)
        Me.codigo = Seguridad.Desencriptar(dto.codigo)
        Me.descripcion = Seguridad.Desencriptar(dto.descripcion)
        Me.id = dto.id
        Me.inactivo = dto.inactivo
        Me.minimo = dto.minimo
        Me.optimo = dto.optimo
        Me.proveedor = New Proveedor(dto.proveedor)
        Me.cantidadPedida = dto.cantidadPedida
        Me.cantidadRecibida = dto.cantidadRecibida
    End Sub

    Public Sub New(ByVal codigo As String)
        Dim dto = ProductosDAL.GetByCodigo(Seguridad.EncriptarReversible(codigo))
        Me.codigo = Seguridad.Desencriptar(dto.codigo)
        Me.descripcion = Seguridad.Desencriptar(dto.descripcion)
        Me.id = dto.id
        Me.inactivo = dto.inactivo
        Me.minimo = dto.minimo
        Me.optimo = dto.optimo
        Me.proveedor = New Proveedor(dto.proveedor)
    End Sub

	Public Sub New()

	End Sub

	Public Sub New(ByVal id As Integer)

        Dim dto = ProductosDAL.GetById(id)
        Me.codigo = Seguridad.Desencriptar(dto.codigo)
        Me.descripcion = Seguridad.Desencriptar(dto.descripcion)
        Me.id = dto.id
        Me.inactivo = dto.inactivo
        Me.minimo = dto.minimo
        Me.optimo = dto.optimo
        Me.proveedor = New Proveedor(dto.proveedor)
	End Sub

    Public Function ToDTO() As ProductoDTO
        Dim dto = New ProductoDTO
        dto.codigo = Seguridad.EncriptarReversible(Me.codigo)
        dto.descripcion = Seguridad.EncriptarReversible(Me.descripcion)
        dto.id = Me.id
        dto.inactivo = Me.inactivo
        dto.minimo = Me.minimo
        dto.optimo = Me.optimo
        dto.proveedor = Me.proveedor.id
        Return dto
    End Function


End Class