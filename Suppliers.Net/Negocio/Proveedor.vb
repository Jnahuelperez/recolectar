Option Explicit On
Option Strict On

Public Class Proveedor


	Public id As Integer
	Public inactivo As Boolean
	Public razonSocial As String
	Public telefono As String

	Public Sub Agregar()
        Dim dto = Me.ToDTO
        Me.id = ProveedoresDAL.Agregar(dto)
	End Sub

    Public Shared Sub Eliminar(ByVal id As Integer)
        ProveedoresDAL.Eliminar(id)
    End Sub

    Public Shared Function getIdByNombre(ByVal nombre As String) As Integer
        Return ProveedoresDAL.getIdByNombre(nombre)
    End Function

    Public Shared Function Listar() As List(Of Proveedor)
        Dim datos As List(Of ProveedorDTO) = ProveedoresDAL.Listar()

        Dim lista As New List(Of Proveedor)

        For Each dto As ProveedorDTO In datos
            Dim a As New Proveedor(dto)
            lista.Add(a)
        Next

        Return lista
    End Function

    Public Sub Modificar()
        ProveedoresDAL.Modificar(Me.ToDTO)
    End Sub

    Public Sub New(ByVal dto As ProveedorDTO)
        Me.id = dto.id
        Me.razonSocial = Seguridad.Desencriptar(dto.razonSocial)
        Me.telefono = Seguridad.Desencriptar(dto.telefono)
        Me.inactivo = dto.inactivo
    End Sub

	Public Sub New()

	End Sub

    Public Sub New(ByVal id As Integer)
        Me.New(ProveedoresDAL.GetById(id))
    End Sub

    Public Function ToDTO() As ProveedorDTO
        Dim dto As New ProveedorDTO

        dto.id = Me.id
        dto.razonSocial = Seguridad.EncriptarReversible(Me.razonSocial)
        dto.telefono = Seguridad.EncriptarReversible(Me.telefono)
        dto.inactivo = Me.inactivo

        Return dto
    End Function


End Class