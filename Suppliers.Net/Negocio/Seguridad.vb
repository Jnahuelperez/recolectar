﻿Option Explicit On
Option Strict On

Imports System
Imports System.IO
Imports System.Security
Imports System.Security.Cryptography
Imports System.Runtime.InteropServices
Imports System.Text
Imports SevenZip

Public Class Seguridad
    Private Shared patron_busqueda As String = "9HKLXmUJ2hSGNQEq4CtexriMDljOu p0kPAVyBgonFsfa13c7wTdvWYZRz6b8I5"
    Private Shared Patron_encripta As String = "zSpWuRkxJGYZDlQMmLNHUB1j8n6g5ifP20yFc4 IhKOrb7aAsTo3CvqE9XdeVtw"

    Public Shared ErroresDeIntegridad As New List(Of String)

    Private Shared RegistriesToFix As New Dictionary(Of String, List(Of Integer))()

    Public Shared Sub CalcularDV(ByVal tabla As String, ByVal registroId As Integer)
        Dim dsH As DataSet = SeguridadDAL.ReadForDVH(tabla, registroId)
        If dsH.Tables(0).Rows.Count > 0 Then
            ''calculo el dvh
            Dim r As DataRow = dsH.Tables(0).Rows(0)
            Dim dvh As Long = 0
            For Each c As DataColumn In dsH.Tables(0).Columns
                Dim columnName = c.ColumnName
                If columnName <> "dvh" Then
                    Dim value As String = r.Item(columnName).ToString()
                    For i = 0 To Len(value) - 1
                        dvh = dvh + Asc(value.Chars(i))
                    Next i
                End If
            Next
            SeguridadDAL.SaveDVH(tabla, registroId, dvh)
        End If

        ''calculo el dvv
        If (ErroresDeIntegridad.Count = 0) Then
            Dim dsV As DataSet = SeguridadDAL.ReadForDVV(tabla)
            Dim dvv As Long = 0
            For Each dr As DataRow In dsV.Tables(0).Rows

                If Not IsDBNull(dr.Item("dvh")) Then
                    dvv = dvv + CLng(dr.Item("dvh"))
                End If
                'dvv = dvv + CLng(dr.Item("dvh"))
            Next
            SeguridadDAL.SaveDVV(tabla, dvv)
        End If
    End Sub

    Public Shared Function CheckPatente(ByVal usuarioId As Integer, ByVal patente As String) As Boolean
        Return SeguridadDAL.CheckPatente(usuarioId, patente)
    End Function

    Public Shared Function Desencriptar(ByVal cadena As String) As String
        Try
            Dim idx As Integer
            Dim result As String = ""

            'Dim x = patron_busqueda.ToCharArray
            'For i As Integer = 0 To x.Length - 1
            '    Dim j = CInt(Math.Floor(Rnd() * x.Length))
            '    Dim tmp = x(i)
            '    x(i) = x(j)
            '    x(j) = tmp
            'Next
            'Console.WriteLine(New String(x))

            'For i As Integer = 0 To x.Length - 1
            '    Dim j = CInt(Math.Floor(Rnd() * x.Length))
            '    Dim tmp = x(i)
            '    x(i) = x(j)
            '    x(j) = tmp
            'Next
            'Console.WriteLine(New String(x))

            For idx = 0 To cadena.Length - 1
                result += DesEncriptarCaracter(cadena.Substring(idx, 1), cadena.Length, idx)
            Next

            Return result
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Private Shared Function DesEncriptarCaracter(ByVal caracter As String, ByVal variable As Integer, ByVal a_indice As Integer) As String
        Dim indice As Integer

        If Patron_encripta.IndexOf(caracter) <> -1 Then
            If (Patron_encripta.IndexOf(caracter) - variable - a_indice) > 0 Then
                indice = (Patron_encripta.IndexOf(caracter) - variable - a_indice) Mod Patron_encripta.Length
            Else
                indice = (patron_busqueda.Length) + ((Patron_encripta.IndexOf(caracter) - variable - a_indice) Mod Patron_encripta.Length)
            End If
            indice = indice Mod Patron_encripta.Length
            Return patron_busqueda.Substring(indice, 1)
        Else
            Return caracter
        End If
    End Function

    Public Shared Function EncriptarMD5(ByVal texto As String) As String

        Using hasher As MD5 = MD5.Create()    ' create hash object

            ' Convert to byte array and get hash
            Dim dbytes As Byte() = hasher.ComputeHash(Encoding.UTF8.GetBytes(texto))

            ' sb to create string from bytes
            Dim sBuilder As New StringBuilder()

            ' convert byte data to hex string
            For n As Integer = 0 To dbytes.Length - 1
                sBuilder.Append(dbytes(n).ToString("X2"))
            Next n

            Return sBuilder.ToString()

        End Using
    End Function

    Public Shared Function EncriptarReversible(ByVal cadena As String) As String
        Dim idx As Integer
        Dim result As String = ""

        For idx = 0 To cadena.Length - 1
            result += EncriptarCaracter(cadena.Substring(idx, 1), cadena.Length, idx)
        Next
        Return result
    End Function


    Private Shared Function EncriptarCaracter(ByVal caracter As String, ByVal variable As Integer, ByVal a_indice As Integer) As String
        Dim indice As Integer
        If patron_busqueda.IndexOf(caracter) <> -1 Then
            indice = (patron_busqueda.IndexOf(caracter) + variable + a_indice) Mod patron_busqueda.Length
            Return Patron_encripta.Substring(indice, 1)
        End If
        Return caracter
    End Function

    Public Shared Function GenerarContraseña() As String
        Dim sResult As String = ""
        Dim rdm As New Random()

        For i As Integer = 1 To 8
            sResult &= ChrW(rdm.Next(32, 126))
        Next

        Return sResult
    End Function

    ''' 
    ''' <param name="patente"></param>
    Public Shared Function GetCantidadUsuariosConPatente(ByVal patente As String) As Integer
        Return SeguridadDAL.GetCantidadUsuariosConPatente(patente)
    End Function

    ''' 
    ''' <param name="destino"></param>
    ''' <param name="multivolumen"></param>
    ''' <param name="tamaño"></param>
    Public Shared Function RealizarBackup(ByVal destino As String, ByVal multivolumen As Boolean, ByVal tamaño As Integer) As Boolean
        Try
            Dim folder = My.Application.Info.DirectoryPath + "\tmp\"
            IO.Directory.CreateDirectory(folder)

            SeguridadDAL.RealizarBackup(folder + "suppliers.Bak", multivolumen, tamaño)
            '            DB.ExecuteNonQuery("BACKUP DATABASE [suppliers.net] TO DISK = '" & folder & "suppliers.Bak'")

            Dim compressor = New SevenZipCompressor()
            compressor.ArchiveFormat = OutArchiveFormat.SevenZip
            compressor.CompressionMode = CompressionMode.Create
            compressor.TempFolderPath = System.IO.Path.GetTempPath()
            If (multivolumen) Then
                compressor.VolumeSize = tamaño * 1024
            End If

            compressor.CompressDirectory(folder, destino + "\backup")

            Bitacora.GrabarMovimiento(currentUser.id, 1, "Realización de backup")
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

        Return True
    End Function

    ''' 
    ''' <param name="origen"></param>
    Public Shared Function RestaurarBackup(ByVal origen As String) As Boolean
        Dim folder = My.Application.Info.DirectoryPath + "\tmp\"
        '1.	El usuario selecciona un archivo de backup y presiona el botón aceptar.
        '2.	El sistema realiza una copia de la base de datos actual.
        Try
            SeguridadDAL.RealizarBackup(folder + "suppliers_sec.Bak", False, 0)
            '            DB.ExecuteNonQuery("BACKUP DATABASE [suppliers.net] TO DISK = '" & folder & "suppliers_sec.Bak'")
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

        Try
            '3.	El sistema recupera la base de datos desde el archivo de backup seleccionado.
            Dim decompressor = New SevenZipExtractor(origen)
            decompressor.ExtractArchive(folder)
            SeguridadDAL.RestaurarBackup(folder + "suppliers.Bak")
            'DB.ExecuteNonQuery("use master; RESTORE DATABASE [suppliers.net] FROM DISK = '" & folder & "suppliers.Bak' WITH REPLACE")
            '6.	El sistema registra la operación en la bitácora.
            Bitacora.GrabarMovimiento(currentUser.id, 1, "Restauración de backup")
            '5.	El sistema elimina la copia que hizo en el paso 2.
            IO.File.Delete("suppliers_sec.Bak")
        Catch ex As Exception
            MsgBox(ex.Message)
            SeguridadDAL.RestaurarBackup(folder + "suppliers_sec.Bak")
            IO.File.Delete("suppliers_sec.Bak")
            Return False
        End Try

        '7.	El sistema informa al usuario que el backup se realizó correctamente.
        Return True
    End Function

    Public Shared Function VerificarIntegridad() As Boolean
        Dim lstTablas As New List(Of String)
        lstTablas.Add("bitacora")
        lstTablas.Add("usuario_patente")
        lstTablas.Add("usuario_familia")
        lstTablas.Add("familia_patente")
        lstTablas.Add("movimientos")
        lstTablas.Add("pedidos")
        lstTablas.Add("pedido_productos")
        lstTablas.Add("tipoMovimientos")

        ErroresDeIntegridad.Clear()
        RegistriesToFix.Clear()

        For Each tabla In lstTablas
            Dim calculatedDVV As Long = 0
            Dim ds = SeguridadDAL.ReadForDVH(tabla)

            RegistriesToFix.Add(tabla, New List(Of Integer))

            'verifico dvh
            For Each r As DataRow In ds.Tables(0).Rows
                Dim dvh As Long = 0
                For Each c As DataColumn In ds.Tables(0).Columns
                    Dim columnName = c.ColumnName
                    If columnName <> "dvh" Then
                        Dim value As String = r.Item(columnName).ToString()
                        For i = 0 To Len(value) - 1
                            dvh = dvh + Asc(value.Chars(i))
                        Next i
                    End If
                Next
                If dvh <> CLng(r.Item("dvh")) Then
                    ErroresDeIntegridad.Add("Error en DVH. Tabla: [" + tabla + "] | Registro ID: " + r.Item("id").ToString())
                    RegistriesToFix(tabla).Add(CInt(r.Item("id")))
                End If
            Next

            'verifico dvv
            Dim dsV As DataSet = SeguridadDAL.ReadForDVV(tabla)
            Dim dvv As Long = 0
            For Each dr As DataRow In dsV.Tables(0).Rows
                If Not IsDBNull(dr.Item("dvh")) Then
                    dvv = dvv + CLng(dr.Item("dvh"))
                End If

            Next
            If dvv <> SeguridadDAL.GetDVV(tabla) Then
                ErroresDeIntegridad.Add("Error en DVV. Tabla: [" + tabla + "]")
            End If
        Next

        Return (ErroresDeIntegridad.Count = 0)
    End Function

    Public Shared Sub RecuperarIntegridad()
        Dim lstTablas As New List(Of String)
        lstTablas.Add("bitacora")
        lstTablas.Add("usuario_patente")
        lstTablas.Add("usuario_familia")
        lstTablas.Add("familia_patente")
        lstTablas.Add("movimientos")
        lstTablas.Add("pedidos")
        lstTablas.Add("pedido_productos")
        lstTablas.Add("tipoMovimientos")

        ErroresDeIntegridad.Clear()

        For Each tabla In lstTablas
            Dim regs = RegistriesToFix(tabla)

            If regs.Count = 0 Then
                CalcularDV(tabla, 0)
            Else
                For Each id As Integer In RegistriesToFix(tabla)
                    Dim ds = SeguridadDAL.ReadForDVH(tabla, id)
                    For Each r As DataRow In ds.Tables(0).Rows
                        CalcularDV(tabla, CInt(r.Item("id")))
                    Next
                Next
            End If
        Next
    End Sub

    Public Shared Function isConnectionStringValid(connectionString As String) As Boolean
        Return SeguridadDAL.isConnectionStringValid(connectionString)
    End Function

    Public Shared Property currentUser As Usuario

    Public Shared Property connectionString() As String
        Get
            Dim cs As String = ""

            Try
                Dim fileName As String = "config"

                If System.IO.File.Exists(fileName) = True Then
                    Dim objReader As New System.IO.StreamReader(fileName)

                    If objReader.Peek() <> -1 Then
                        cs = objReader.ReadLine()
                    End If

                    objReader.Close()
                End If

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
            End Try

            Return Desencriptar(cs)
        End Get

        Set(ByVal value As String)
            Try
                Dim fileName As String = "config"
                Dim file As System.IO.StreamWriter

                If System.IO.File.Exists(fileName) = True Then
                    System.IO.File.Delete(fileName)
                End If

                file = My.Computer.FileSystem.OpenTextFileWriter(fileName, True)
                file.WriteLine(EncriptarReversible(value))
                file.Close()

                DB.connectionString = value
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
            End Try
        End Set
    End Property


End Class ' Seguridad