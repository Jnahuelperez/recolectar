Option Explicit On
Option Strict On

Public Class Usuario

	Public bloqueado As Boolean
	Public email As String
    Public familias As New List(Of Familia)
	Public id As Integer
	Public idiomaId As Integer
	Public inactivo As Boolean
	Public intentos As Integer
	Public logueado As Boolean
	Public nombre As String
	Public password As String
    Public patentes As New Dictionary(Of Patente, Boolean)
	Public user As String

    Public Sub Agregar()
        Dim dto As New UsuarioDTO

        dto.nombre = Me.nombre
        dto.user = Seguridad.EncriptarReversible(Me.user)
        dto.email = Me.email
        dto.password = Seguridad.EncriptarMD5(Me.password)
        dto.inactivo = Me.inactivo

        Me.id = UsuariosDAL.Agregar(dto)

        Mailer.EnviarMail(Me.email, Me.user & vbCrLf & Me.password)

        Seguridad.CalcularDV("usuarios", Me.id)
    End Sub

	Public Sub CambiarIdioma(ByVal idiomaId As Integer)
        UsuariosDAL.CambiarIdioma(idiomaId, Me.id)
        Me.idiomaId = idiomaId
	End Sub

    Public Shared Sub Desbloquear(ByVal id As Integer)
        UsuariosDAL.Desbloquear(id)
    End Sub

    Shared Sub Eliminar(ByVal id As Integer)
        UsuariosDAL.Eliminar(id)
        Seguridad.CalcularDV("usuarios", 0)
    End Sub

    Public Shared Function Listar() As List(Of Usuario)
        Dim datos = UsuariosDAL.Listar()

        Dim lista As New List(Of Usuario)

        For Each dto As UsuarioDTO In datos
            Dim a As New Usuario(dto)
            lista.Add(a)
        Next

        Return lista

    End Function

    Public Shared Function Login(ByVal user As String, ByVal pass As String) As Usuario
        Dim dto = UsuariosDAL.Login(user, pass)
        If dto Is Nothing Then
            'si el nombre de usuario estaba mal
            Return Nothing
        ElseIf dto.bloqueado Then
            'si está bloqueado, no hacemos nada
        ElseIf dto.logueado Then
            'si se logró el login
            dto.intentos = 0
            Bitacora.GrabarMovimiento(dto.id, 0, "Login")
            UsuariosDAL.Modificar(dto)
        Else
            'si el login no se logró
            dto.intentos = dto.intentos + 1
            Bitacora.GrabarMovimiento(dto.id, 0, "Login erróneo. Intento: " + dto.intentos.ToString)
            If dto.intentos >= 3 Then
                If Seguridad.GetCantidadUsuariosConPatente("Desbloquear Usuario") > 1 Then
                    Bitacora.GrabarMovimiento(dto.id, 0, "Usuario Bloqueado")
                    dto.bloqueado = True
                End If
            End If
            UsuariosDAL.Modificar(dto)
        End If
        Return New Usuario(dto)

    End Function

	Public Sub Logout()
        Bitacora.GrabarMovimiento(Me.id, 0, "Logout")
	End Sub

    Public Sub Modificar()
        UsuariosDAL.Modificar(Me.ToDTO)
    End Sub

	''' 
	''' <param name="id"></param>
	Public Sub New(ByVal id As Integer)
        Me.New(UsuariosDAL.GetById(id))
	End Sub

    Shared Function UserExists(user As String) As Boolean
        user = Seguridad.EncriptarReversible(user)
        Dim dto = UsuariosDAL.GetByUser(user)
        Return Not dto Is Nothing
    End Function

    Public Sub New()

    End Sub

    ''' 
    ''' <param name="dto"></param>
    Public Sub New(ByVal dto As UsuarioDTO)
        Me.bloqueado = dto.bloqueado
        'Me.email = Seguridad.Desencriptar(dto.email)
        Me.email = dto.email
        Me.id = dto.id
        Me.idiomaId = dto.idiomaId
        Me.inactivo = dto.inactivo
        Me.intentos = dto.intentos
        Me.logueado = dto.logueado
        Me.nombre = dto.nombre
        Me.password = dto.password
        Me.user = Seguridad.Desencriptar(dto.user)

        If Not dto.familias Is Nothing Then
            For Each i As Integer In dto.familias
                Me.familias.Add(New Familia(i))
            Next
        End If

        If Not dto.patentes Is Nothing Then
            For Each i As KeyValuePair(Of Integer, Boolean) In dto.patentes
                Me.patentes.Add(New Patente(i.Key), i.Value)
            Next
        End If

    End Sub

    Public Sub PersistirFamilias()

    End Sub

    Public Sub PersistirPatentes()

    End Sub

    ''' 
    ''' <param name="user"></param>
    Shared Sub RecuperarContraseña(ByVal user As String)
        Dim dto = UsuariosDAL.GetByUser(Seguridad.EncriptarReversible(user))

        Dim pass = Seguridad.GenerarContraseña()

        UsuariosDAL.CambiarPass(dto.id, Seguridad.EncriptarMD5(pass))

        Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 2, user + ": recupero de contraseña")

        Mailer.EnviarMail(dto.email, user & vbCrLf & pass)
    End Sub

    Public Sub CambiarContraseña(ByVal pass As String)
        UsuariosDAL.CambiarPass(Me.id, Seguridad.EncriptarMD5(pass))

        Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 2, Me.user + ": cambio de contraseña")
    End Sub

    Public Function ToDTO() As UsuarioDTO
        Dim dto As New UsuarioDTO
        dto.id = Me.id
        dto.nombre = Me.nombre
        dto.user = Seguridad.EncriptarReversible(Me.user)
        dto.email = Me.email
        dto.bloqueado = Me.bloqueado
        dto.idiomaId = Me.idiomaId
        dto.inactivo = Me.inactivo
        dto.intentos = Me.intentos
        Return dto
    End Function

    Public Sub EliminarFamilia(ByVal familiaId As Integer)
        UsuariosDAL.EliminarFamilia(Me.id, familiaId)
        Seguridad.CalcularDV("usuario_familia", 0)
    End Sub

    Public Sub EliminarPatente(ByVal patenteId As Integer)
        UsuariosDAL.EliminarPatente(Me.id, patenteId)
        Seguridad.CalcularDV("usuario_patente", 0)
    End Sub

    Public Sub GrabarFamilia(ByVal familiaId As Integer)
        Dim id = UsuariosDAL.GrabarFamilia(Me.id, familiaId)
        Seguridad.CalcularDV("usuario_familia", id)
    End Sub

    Public Sub GrabarPatente(ByVal patenteId As Integer, ByVal habilitado As Boolean)
        Dim id = UsuariosDAL.GrabarPatente(Me.id, patenteId, habilitado)
        Seguridad.CalcularDV("usuario_patente", id)
    End Sub

    Public Sub ModificarPatente(ByVal patenteId As Integer, ByVal habilitado As Boolean)
        Dim id = UsuariosDAL.ModificarPatente(Me.id, patenteId, habilitado)
        Seguridad.CalcularDV("usuario_patente", id)
    End Sub

End Class ' Usuario