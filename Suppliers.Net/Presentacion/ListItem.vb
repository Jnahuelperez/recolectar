﻿Public Class ListItem
    Public Property Name As String
    Public Property Id As Integer
    Public Overrides Function ToString() As String
        Return Name
    End Function
    Public Sub New(id As Integer, name As String)
        Me.Name = name
        Me.Id = id
    End Sub
End Class