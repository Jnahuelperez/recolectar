﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBitacora
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grd = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cFecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cAccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cCriticidad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.grpFiltrar = New System.Windows.Forms.GroupBox()
        Me.lblDesde = New System.Windows.Forms.Label()
        Me.dtHasta = New System.Windows.Forms.DateTimePicker()
        Me.dtDesde = New System.Windows.Forms.DateTimePicker()
        Me.btnAplicarFiltro = New System.Windows.Forms.Button()
        Me.cboCriticidad = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblCriticidad = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpFiltrar.SuspendLayout()
        Me.SuspendLayout()
        '
        'grd
        '
        Me.grd.AllowUserToAddRows = False
        Me.grd.AllowUserToDeleteRows = False
        Me.grd.AllowUserToResizeColumns = False
        Me.grd.AllowUserToResizeRows = False
        Me.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cFecha, Me.cUsuario, Me.cAccion, Me.cCriticidad})
        Me.grd.Location = New System.Drawing.Point(12, 130)
        Me.grd.MultiSelect = False
        Me.grd.Name = "grd"
        Me.grd.ReadOnly = True
        Me.grd.RowHeadersVisible = False
        Me.grd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grd.Size = New System.Drawing.Size(417, 194)
        Me.grd.TabIndex = 0
        '
        'cId
        '
        Me.cId.HeaderText = "Id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cFecha
        '
        Me.cFecha.HeaderText = "Fecha"
        Me.cFecha.Name = "cFecha"
        Me.cFecha.ReadOnly = True
        '
        'cUsuario
        '
        Me.cUsuario.HeaderText = "Usuario"
        Me.cUsuario.Name = "cUsuario"
        Me.cUsuario.ReadOnly = True
        '
        'cAccion
        '
        Me.cAccion.HeaderText = "Accion"
        Me.cAccion.Name = "cAccion"
        Me.cAccion.ReadOnly = True
        '
        'cCriticidad
        '
        Me.cCriticidad.HeaderText = "Criticidad"
        Me.cCriticidad.Name = "cCriticidad"
        Me.cCriticidad.ReadOnly = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Location = New System.Drawing.Point(354, 330)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(75, 23)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.Tag = "Imprimir grilla"
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'grpFiltrar
        '
        Me.grpFiltrar.Controls.Add(Me.lblDesde)
        Me.grpFiltrar.Controls.Add(Me.dtHasta)
        Me.grpFiltrar.Controls.Add(Me.dtDesde)
        Me.grpFiltrar.Controls.Add(Me.btnAplicarFiltro)
        Me.grpFiltrar.Controls.Add(Me.cboCriticidad)
        Me.grpFiltrar.Controls.Add(Me.Label1)
        Me.grpFiltrar.Controls.Add(Me.lblCriticidad)
        Me.grpFiltrar.Controls.Add(Me.Label2)
        Me.grpFiltrar.Controls.Add(Me.txtUsuario)
        Me.grpFiltrar.Location = New System.Drawing.Point(14, 8)
        Me.grpFiltrar.Name = "grpFiltrar"
        Me.grpFiltrar.Size = New System.Drawing.Size(414, 116)
        Me.grpFiltrar.TabIndex = 5
        Me.grpFiltrar.TabStop = False
        Me.grpFiltrar.Text = "Filtro"
        '
        'lblDesde
        '
        Me.lblDesde.AutoSize = True
        Me.lblDesde.Location = New System.Drawing.Point(8, 24)
        Me.lblDesde.Name = "lblDesde"
        Me.lblDesde.Size = New System.Drawing.Size(38, 13)
        Me.lblDesde.TabIndex = 20
        Me.lblDesde.Text = "Desde"
        '
        'dtHasta
        '
        Me.dtHasta.Checked = False
        Me.dtHasta.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtHasta.Location = New System.Drawing.Point(83, 40)
        Me.dtHasta.Name = "dtHasta"
        Me.dtHasta.ShowCheckBox = True
        Me.dtHasta.Size = New System.Drawing.Size(121, 20)
        Me.dtHasta.TabIndex = 13
        Me.dtHasta.Tag = "Fecha hasta para filtrar"
        '
        'dtDesde
        '
        Me.dtDesde.Checked = False
        Me.dtDesde.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtDesde.Location = New System.Drawing.Point(83, 17)
        Me.dtDesde.Name = "dtDesde"
        Me.dtDesde.ShowCheckBox = True
        Me.dtDesde.Size = New System.Drawing.Size(121, 20)
        Me.dtDesde.TabIndex = 19
        Me.dtDesde.Tag = "Fecha desde para filtrar"
        '
        'btnAplicarFiltro
        '
        Me.btnAplicarFiltro.Location = New System.Drawing.Point(333, 85)
        Me.btnAplicarFiltro.Name = "btnAplicarFiltro"
        Me.btnAplicarFiltro.Size = New System.Drawing.Size(75, 23)
        Me.btnAplicarFiltro.TabIndex = 12
        Me.btnAplicarFiltro.Tag = "Aplicar filtro"
        Me.btnAplicarFiltro.Text = "Aplicar"
        Me.btnAplicarFiltro.UseVisualStyleBackColor = True
        '
        'cboCriticidad
        '
        Me.cboCriticidad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCriticidad.FormattingEnabled = True
        Me.cboCriticidad.Items.AddRange(New Object() {"", "ALTA", "BAJA", "MEDIA"})
        Me.cboCriticidad.Location = New System.Drawing.Point(83, 85)
        Me.cboCriticidad.Name = "cboCriticidad"
        Me.cboCriticidad.Size = New System.Drawing.Size(121, 21)
        Me.cboCriticidad.TabIndex = 18
        Me.cboCriticidad.Tag = "Criticidad para filtrar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 13)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Hasta"
        '
        'lblCriticidad
        '
        Me.lblCriticidad.AutoSize = True
        Me.lblCriticidad.Location = New System.Drawing.Point(8, 93)
        Me.lblCriticidad.Name = "lblCriticidad"
        Me.lblCriticidad.Size = New System.Drawing.Size(50, 13)
        Me.lblCriticidad.TabIndex = 17
        Me.lblCriticidad.Text = "Criticidad"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 70)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Usuario"
        '
        'txtUsuario
        '
        Me.txtUsuario.Location = New System.Drawing.Point(83, 63)
        Me.txtUsuario.MaxLength = 20
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(121, 20)
        Me.txtUsuario.TabIndex = 16
        Me.txtUsuario.Tag = "Usuario para filtrar"
        '
        'frmBitacora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(441, 365)
        Me.Controls.Add(Me.grpFiltrar)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.grd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmBitacora"
        Me.Text = "Bitácora"
        CType(Me.grd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpFiltrar.ResumeLayout(False)
        Me.grpFiltrar.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grd As System.Windows.Forms.DataGridView
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cFecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cUsuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cAccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cCriticidad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents grpFiltrar As System.Windows.Forms.GroupBox
    Friend WithEvents lblDesde As System.Windows.Forms.Label
    Friend WithEvents dtHasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtDesde As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnAplicarFiltro As System.Windows.Forms.Button
    Friend WithEvents cboCriticidad As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblCriticidad As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox

End Class
