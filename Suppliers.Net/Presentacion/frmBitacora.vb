﻿Public Class frmBitacora
    Public ds As DataSet

    Private Sub frmBitacora_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ds = Bitacora.Consultar()

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        grd.Rows.Clear()
        For Each dr As DataRow In ds.Tables(0).Rows
            grd.Rows.Add(dr.Item("id"), CDate(dr.Item("fecha")).ToString("dd/MM/yyyy hh:mm"), Seguridad.Desencriptar(dr.Item("user")), Seguridad.Desencriptar(dr.Item("accion")), dr.Item("criticidad"))
        Next
    End Sub


    Private Sub btnAplicarFiltro_Click(sender As Object, e As EventArgs) Handles btnAplicarFiltro.Click
        If ds Is Nothing Then
            grd.Rows.Clear()
            Return
        End If

        Dim s As String = ""

        If dtDesde.Checked Then
            If Not String.IsNullOrEmpty(s) Then
                s = s + " AND "
            Else
                s = " "
            End If
            s = s + "fecha >= #" + dtDesde.Value.ToString("MM/dd/yyyy") + " 00:00:00#"
        End If

        If dtHasta.Checked Then
            If Not String.IsNullOrEmpty(s) Then
                s = s + " AND "
            Else
                s = " "
            End If
            s = s + "fecha <= #" + dtHasta.Value.ToString("MM/dd/yyyy") + " 23:59:59#"
        End If

        If (cboCriticidad.SelectedIndex > 0) Then
            If Not String.IsNullOrEmpty(s) Then
                s = s + " AND "
            Else
                s = " "
            End If
            s = s + "criticidad = '" + cboCriticidad.SelectedItem.ToString() + "'"
        End If

        Dim dv As DataView
        dv = New DataView(ds.Tables(0), s, "fecha ASC", DataViewRowState.CurrentRows)

        grd.Rows.Clear()
        For Each dr As DataRowView In dv
            Dim usuario As String = Seguridad.Desencriptar(dr.Item("user"))

            If Not String.IsNullOrEmpty(txtUsuario.Text.Trim) Then
                If usuario.Contains(txtUsuario.Text.Trim) Then
                    grd.Rows.Add(dr.Item("id"), CDate(dr.Item("fecha")).ToString("dd/MM/yyyy"), usuario, dr.Item("accion"), dr.Item("criticidad"))
                End If
            Else
                grd.Rows.Add(dr.Item("id"), CDate(dr.Item("fecha")).ToString("dd/MM/yyyy"), usuario, dr.Item("accion"), dr.Item("criticidad"))
            End If
        Next
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        Dim frm As New frmBitacoraReport

        Dim dt As New DataTable
        dt.Columns.Add("fecha", GetType(DateTime))
        dt.Columns.Add("usuario", GetType(String))
        dt.Columns.Add("accion", GetType(String))
        dt.Columns.Add("criticidad", GetType(String))

        For Each r In grd.Rows
            dt.Rows.Add(r.Cells("cFecha").Value, r.Cells("cUsuario").Value, r.Cells("cAccion").Value, r.Cells("cCriticidad").Value)
        Next

        Dim rprtDTSource = New Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt)

        Console.WriteLine(frm.ReportViewer1.LocalReport.ReportPath)
        frm.ReportViewer1.LocalReport.DataSources.Clear()
        frm.ReportViewer1.LocalReport.DataSources.Add(rprtDTSource)
        frm.ReportViewer1.RefreshReport()


        frm.Show()
    End Sub

    Private Sub frmBitacora_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Bitácora")
        End If
    End Sub

    Private Sub cboCriticidad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCriticidad.SelectedIndexChanged

    End Sub
End Class
