﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambiarPass
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.lblPass = New System.Windows.Forms.Label()
        Me.txtRepass = New System.Windows.Forms.TextBox()
        Me.lblRepass = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblMsgOK = New System.Windows.Forms.Label()
        Me.lblMsgNoPass = New System.Windows.Forms.Label()
        Me.lblMsgPassNotMatch = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtPass
        '
        Me.txtPass.Location = New System.Drawing.Point(102, 19)
        Me.txtPass.MaxLength = 8
        Me.txtPass.Name = "txtPass"
        Me.txtPass.Size = New System.Drawing.Size(100, 20)
        Me.txtPass.TabIndex = 4
        Me.txtPass.Tag = "Nueva password para el usuario"
        '
        'lblPass
        '
        Me.lblPass.AutoSize = True
        Me.lblPass.Location = New System.Drawing.Point(15, 22)
        Me.lblPass.Name = "lblPass"
        Me.lblPass.Size = New System.Drawing.Size(65, 13)
        Me.lblPass.TabIndex = 3
        Me.lblPass.Text = "Nueva Pass"
        '
        'txtRepass
        '
        Me.txtRepass.Location = New System.Drawing.Point(102, 45)
        Me.txtRepass.MaxLength = 8
        Me.txtRepass.Name = "txtRepass"
        Me.txtRepass.Size = New System.Drawing.Size(100, 20)
        Me.txtRepass.TabIndex = 7
        Me.txtRepass.Tag = "Repita la nueva password para el usuario"
        '
        'lblRepass
        '
        Me.lblRepass.AutoSize = True
        Me.lblRepass.Location = New System.Drawing.Point(15, 48)
        Me.lblRepass.Name = "lblRepass"
        Me.lblRepass.Size = New System.Drawing.Size(64, 13)
        Me.lblRepass.TabIndex = 6
        Me.lblRepass.Text = "Repite Pass"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(15, 84)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 8
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(127, 84)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblMsgOK
        '
        Me.lblMsgOK.AutoSize = True
        Me.lblMsgOK.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgOK.Location = New System.Drawing.Point(12, 175)
        Me.lblMsgOK.Name = "lblMsgOK"
        Me.lblMsgOK.Size = New System.Drawing.Size(132, 13)
        Me.lblMsgOK.TabIndex = 65
        Me.lblMsgOK.Text = "Se actualizó la contraseña"
        Me.lblMsgOK.Visible = False
        '
        'lblMsgNoPass
        '
        Me.lblMsgNoPass.AutoSize = True
        Me.lblMsgNoPass.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoPass.Location = New System.Drawing.Point(12, 149)
        Me.lblMsgNoPass.Name = "lblMsgNoPass"
        Me.lblMsgNoPass.Size = New System.Drawing.Size(150, 13)
        Me.lblMsgNoPass.TabIndex = 66
        Me.lblMsgNoPass.Text = "Debe ingresar una contraseña"
        Me.lblMsgNoPass.Visible = False
        '
        'lblMsgPassNotMatch
        '
        Me.lblMsgPassNotMatch.AutoSize = True
        Me.lblMsgPassNotMatch.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgPassNotMatch.Location = New System.Drawing.Point(12, 162)
        Me.lblMsgPassNotMatch.Name = "lblMsgPassNotMatch"
        Me.lblMsgPassNotMatch.Size = New System.Drawing.Size(197, 13)
        Me.lblMsgPassNotMatch.TabIndex = 67
        Me.lblMsgPassNotMatch.Text = "Las contraseñas indicadas no coinciden"
        Me.lblMsgPassNotMatch.Visible = False
        '
        'frmCambiarPass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(217, 122)
        Me.Controls.Add(Me.lblMsgPassNotMatch)
        Me.Controls.Add(Me.lblMsgNoPass)
        Me.Controls.Add(Me.lblMsgOK)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtRepass)
        Me.Controls.Add(Me.lblRepass)
        Me.Controls.Add(Me.txtPass)
        Me.Controls.Add(Me.lblPass)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmCambiarPass"
        Me.Text = "Cambiar Pass"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPass As System.Windows.Forms.TextBox
    Friend WithEvents lblPass As System.Windows.Forms.Label
    Friend WithEvents txtRepass As System.Windows.Forms.TextBox
    Friend WithEvents lblRepass As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblMsgOK As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoPass As System.Windows.Forms.Label
    Friend WithEvents lblMsgPassNotMatch As System.Windows.Forms.Label
End Class
