﻿Public Class frmCambiarPass

    Private Sub frmCambiarPass_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If Not ValidateInput() Then
            Return
        End If

        Negocio.Seguridad.currentUser.CambiarContraseña(Me.txtPass.Text)

        MsgBox(lblMsgOK.Text)

        txtPass.Clear()
        txtRepass.Clear()
    End Sub

    Private Function ValidateInput() As Boolean
        If String.IsNullOrEmpty(txtPass.Text.Trim) Then
            MsgBox(lblMsgNoPass.Text)
            txtPass.Focus()
            Return False
        End If

        If txtPass.Text.Trim <> Me.txtRepass.Text.Trim Then
            MsgBox(Me.lblMsgPassNotMatch.Text)
            txtPass.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmCambiarPass_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Cambiar contraseña")
        End If
    End Sub
End Class