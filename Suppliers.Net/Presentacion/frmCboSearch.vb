﻿Public Class frmCboSearch

    Public Property SelectedId As Integer
    Public Property SelectedName As String

    Public Sub AddItem(s As ListItem)
        ListBox1.Items.Add(s)
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If (Me.ListBox1.SelectedIndex >= 0) Then
            Dim i = DirectCast(Me.ListBox1.SelectedItem, ListItem)
            Me.SelectedId = i.Id
            Me.SelectedName = i.Name
            Me.Close()
        Else
            Beep()
        End If
    End Sub

    Private Sub frmCboSearch_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Sub txtFiltro_TextChanged(sender As Object, e As EventArgs) Handles txtFiltro.TextChanged
        For i As Integer = 0 To ListBox1.Items.Count - 1
            Dim o = ListBox1.Items(i).ToString
            If o.IndexOf(txtFiltro.Text) >= 0 Then
                ListBox1.SetSelected(i, True)
                Return
            Else
                ListBox1.SetSelected(i, False)
            End If

        Next
    End Sub
End Class