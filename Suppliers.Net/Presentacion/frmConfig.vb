﻿Imports System.Data.SqlClient
Imports System.IO

Public Class frmConfig
    Private Sub frmConfig_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        'Dim dt As Data.DataTable = Nothing, dr As Data.DataRow = Nothing

        'Try
        '    dt = Sql.SqlDataSourceEnumerator.Instance.GetDataSources()

        '    Me.cboServers.Items.Clear()
        '    For Each dr In dt.Rows
        '        Dim servername As String
        '        Dim instanceName = dr.Item("InstanceName").ToString()

        '        If (Not String.IsNullOrEmpty(instanceName)) Then
        '            '   servername = dr.Item("ServerName").ToString() + "\SQLSLOPEZ"
        '            servername = dr.Item("ServerName").ToString() + "\" + dr.Item("InstanceName").ToString()
        '        Else
        '            servername = dr.Item("ServerName").ToString()
        '        End If

        '        Me.cboServers.Items.Add(servername)
        '    Next

        'Catch ex As System.Data.SqlClient.SqlException
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        'Finally
        '    dr = Nothing
        '    dt = Nothing
        'End Try

    End Sub

    Private Sub cboDB_DropDown(sender As Object, e As EventArgs)
        'Dim ds As New DataSet()
        'Dim da As SqlDataAdapter
        'Dim connectionString As String = ""

        'Try
        '    If cboServers.SelectedItem Is Nothing Then
        '        connectionString = connectionString & "Data Source=null"
        '    Else
        '        connectionString = connectionString & "Data Source=" & cboServers.SelectedItem.ToString
        '    End If

        '    If optAuthWindows.Checked Then
        '        connectionString = connectionString & ";Integrated Security=True"
        '    Else
        '        connectionString = connectionString & ";User Id=" & txtUser.Text & ";Password=" & txtPass.Text
        '    End If

        '    Me.cboDB.Items.Clear()

        '    da = New SqlDataAdapter("Select Name FROM master.dbo.sysdatabases", connectionString)
        '    da.Fill(ds)
        '    da.Dispose()

        '    For Each dr As DataRow In ds.Tables(0).Rows
        '        Me.cboDB.Items.Add(dr.Item("Name"))
        '    Next
        'Catch ex As System.Data.SqlClient.SqlException
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation, "Error!")
        'Finally
        '    da = Nothing
        '    ds = Nothing
        'End Try

    End Sub

    Private Sub cboServers_SelectedIndexChanged(sender As Object, e As EventArgs)
        ' Me.cboDB.Items.Clear()
    End Sub

    Private Sub optAuthWindows_CheckedChanged(sender As Object, e As EventArgs) Handles optAuthWindows.CheckedChanged
        'Me.cboDB.Items.Clear()
    End Sub

    Private Sub optAuthSQL_CheckedChanged(sender As Object, e As EventArgs) Handles optAuthSQL.CheckedChanged
        '  Me.cboDB.Items.Clear()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click

        If (ValidarInput()) Then
            If GrabarConfiguracion(txtServidor.Text, txtDB.Text) Then
                Close()
            End If
        End If

    End Sub

    Private Function ValidarInput() As Boolean
        If txtServidor.Text Is Nothing Then
            MsgBox("Indique un servidor", MsgBoxStyle.Exclamation, "Error!")
            txtServidor.Focus()
            Return False
        End If

        If txtDB.Text Is Nothing Then
            MsgBox("Indique una base de datos", MsgBoxStyle.Exclamation, "Error!")
            txtDB.Focus()
            Return False
        End If

        Return True
    End Function

    Private Function GrabarConfiguracion(ByVal server As String, ByVal base As String) As Boolean
        'Dim connectionString As String = ""

        ''armo la connectionString
        'If cboServers.SelectedItem Is Nothing Then
        '    connectionString = connectionString & "Data Source=null"
        'Else
        '    connectionString = connectionString & "Data Source=" & cboServers.SelectedItem.ToString
        'End If

        'If cboDB.SelectedItem Is Nothing Then
        '    connectionString = connectionString & ";Initial Catalog=null"
        'Else
        '    connectionString = connectionString & ";Initial Catalog=" & cboDB.SelectedItem.ToString
        'End If

        'If optAuthWindows.Checked Then
        '    connectionString = connectionString & ";Integrated Security=True"
        'Else
        '    connectionString = connectionString & ";User Id=" & txtUser.Text & ";Password=" & txtPass.Text
        'End If

        ''compruebo si la connectionString sirve
        'If Seguridad.isConnectionStringValid(connectionString) Then
        '    'la guardo en el archivo
        '    Seguridad.connectionString = connectionString
        '    Return True
        'Else
        '    MsgBox("Los datos de conexión no son válidos", vbExclamation)
        '    Return False
        'End If
        Dim Setconecction As String
        ' Dim SetConecctionMaster As String
        Setconecction = "Data Source=" + server + ";Initial Catalog=" + base + ";Integrated Security=True"
        '   SetConecctionMaster = "Data Source=" + server + ";Initial Catalog=master" + ";Integrated Security=True"

        If Seguridad.isConnectionStringValid(Setconecction) Then
            'la guardo en el archivo
            Seguridad.connectionString = Setconecction
            Return True
        Else
            MsgBox("Los datos de conexión no son válidos", vbExclamation)
            Return False
        End If
    End Function

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        End
    End Sub
End Class