﻿Public Class frmEmitirPedido

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboProveedor.SelectedIndexChanged
        Dim productos = Producto.ListarParaPedido(DirectCast(cboProveedor.SelectedItem, ListItem).Id)

        grd.Rows.Clear()
        If Not productos Is Nothing Then
            For Each f As Producto In productos
                grd.Rows.Add(f.id, f.codigo + " - " + f.descripcion, f.optimo - f.GetStock(), True)
            Next
        End If

    End Sub

    Private Sub frmEmitirPedido_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next



        Dim sourceproductoes As New Dictionary(Of Integer, String)()
        Dim proveedores = Proveedor.Listar()
        For Each f As Proveedor In proveedores
            If Not f.inactivo Then
                cboProveedor.Items.Add(New ListItem(f.id, f.razonSocial))
            End If
        Next
        cboProveedor.ValueMember = "Id"
        cboProveedor.DisplayMember = "Name"
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim frm As New frmCboSearch

        For Each o As Object In cboProveedor.Items
            Dim c = DirectCast(o, ListItem)
            frm.AddItem(c)
        Next

        If frm.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            cboProveedor.SelectedIndex = cboProveedor.FindStringExact(frm.SelectedName)
        End If
    End Sub

    Private Sub grd_CellContentClick(sender As Object, e As DataGridViewCellEventArgs)

    End Sub

    Private Sub grd_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles grd.CellValidating
        Dim v As Integer = 0
        If (grd.Columns(e.ColumnIndex).Name = "cCantidad") Then

            If Not Integer.TryParse(e.FormattedValue, v) Then
                e.Cancel = True
            Else
                If v < 0 Then
                    e.Cancel = True
                End If
            End If

        End If
    End Sub

    Private Sub grd_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles grd.CellEndEdit
        grd.Rows(e.RowIndex).ErrorText = String.Empty
    End Sub

    Private Sub grd_CellContentClick_1(sender As Object, e As DataGridViewCellEventArgs) Handles grd.CellContentClick

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()

    End Sub

    Private Sub btnEmitir_Click(sender As Object, e As EventArgs) Handles btnEmitir.Click
        Dim p = New Pedido()

        p.usuarioEmisor = Seguridad.currentUser
        p.proveedor = New Proveedor(DirectCast(cboProveedor.SelectedItem, ListItem).Id)
        p.Agregar()

        For Each r As DataGridViewRow In grd.Rows
            If (r.Cells("cPedir").Value) Then
                p.AgregarProducto(CInt(r.Cells("cId").Value), CInt(r.Cells("cCantidad").Value))
            End If
        Next

        Imprimir(p.id)

        Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Emisión de pedido: " + p.id.ToString)

        Me.Close()
    End Sub

    Private Sub Imprimir(id As Integer)
        Dim frm As New frmPedidoReport

        Dim dt As New DataTable
        dt.Columns.Add("idPedido", GetType(Integer))
        dt.Columns.Add("producto", GetType(String))
        dt.Columns.Add("cantidad", GetType(Integer))

        For Each r In grd.Rows
            If (r.Cells("cPedir").Value) Then
                dt.Rows.Add(id, r.Cells("cProducto").Value, r.Cells("cCantidad").Value)
            End If
        Next

        Dim rprtDTSource = New Microsoft.Reporting.WinForms.ReportDataSource("DataSet1", dt)

        Console.WriteLine(frm.ReportViewer1.LocalReport.ReportPath)
        frm.ReportViewer1.LocalReport.DataSources.Clear()
        frm.ReportViewer1.LocalReport.DataSources.Add(rprtDTSource)
        frm.ReportViewer1.RefreshReport()


        frm.Show()
    End Sub

    Private Sub frmEmitirPedido_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Emitir Pedidos")
        End If
    End Sub
End Class