﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmErroresDeIntegridad
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSalie = New System.Windows.Forms.Button()
        Me.btnRecuperar = New System.Windows.Forms.Button()
        Me.lstErrores = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(356, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Se han detectado los siguientes errores de integridad en la base de datos:"
        '
        'btnSalie
        '
        Me.btnSalie.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnSalie.Location = New System.Drawing.Point(515, 276)
        Me.btnSalie.Name = "btnSalie"
        Me.btnSalie.Size = New System.Drawing.Size(75, 23)
        Me.btnSalie.TabIndex = 2
        Me.btnSalie.Text = "Salir"
        Me.btnSalie.UseVisualStyleBackColor = True
        '
        'btnRecuperar
        '
        Me.btnRecuperar.Location = New System.Drawing.Point(376, 276)
        Me.btnRecuperar.Name = "btnRecuperar"
        Me.btnRecuperar.Size = New System.Drawing.Size(133, 23)
        Me.btnRecuperar.TabIndex = 3
        Me.btnRecuperar.Tag = "Recuperar integridad de la base de datos"
        Me.btnRecuperar.Text = "Recuperar Integridad"
        Me.btnRecuperar.UseVisualStyleBackColor = True
        '
        'lstErrores
        '
        Me.lstErrores.FormattingEnabled = True
        Me.lstErrores.Location = New System.Drawing.Point(15, 34)
        Me.lstErrores.Name = "lstErrores"
        Me.lstErrores.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lstErrores.Size = New System.Drawing.Size(575, 225)
        Me.lstErrores.TabIndex = 4
        '
        'frmErroresDeIntegridad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(606, 311)
        Me.ControlBox = False
        Me.Controls.Add(Me.lstErrores)
        Me.Controls.Add(Me.btnRecuperar)
        Me.Controls.Add(Me.btnSalie)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.Name = "frmErroresDeIntegridad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Errores de integridad"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnSalie As System.Windows.Forms.Button
    Friend WithEvents btnRecuperar As System.Windows.Forms.Button
    Friend WithEvents lstErrores As System.Windows.Forms.ListBox
End Class
