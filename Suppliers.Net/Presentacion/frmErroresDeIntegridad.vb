﻿Public Class frmErroresDeIntegridad

    Private Sub btnSalie_Click(sender As Object, e As EventArgs) Handles btnSalie.Click
        End
    End Sub

    Private Sub btnRecuperar_Click(sender As Object, e As EventArgs) Handles btnRecuperar.Click
        Seguridad.RecuperarIntegridad()
        Me.Close()
    End Sub

    Private Sub frmErroresDeIntegridad_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each s As String In Seguridad.ErroresDeIntegridad
            lstErrores.Items.Add(s)
        Next


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Sub lstErrores_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstErrores.SelectedIndexChanged

    End Sub
End Class