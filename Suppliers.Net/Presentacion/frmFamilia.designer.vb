﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFamilia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtDescuento1 = New System.Windows.Forms.TextBox()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.chkInactivo = New System.Windows.Forms.CheckBox()
        Me.grpPatentes = New System.Windows.Forms.GroupBox()
        Me.lblPatente = New System.Windows.Forms.Label()
        Me.cboPatente = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.grdPatente = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblMsgNoName = New System.Windows.Forms.Label()
        Me.lblMsgNameDuplicated = New System.Windows.Forms.Label()
        Me.lblMsgCantDelete = New System.Windows.Forms.Label()
        Me.grpPatentes.SuspendLayout()
        CType(Me.grdPatente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtId
        '
        Me.txtId.Enabled = False
        Me.txtId.Location = New System.Drawing.Point(40, 331)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(36, 20)
        Me.txtId.TabIndex = 0
        Me.txtId.Text = "0"
        Me.txtId.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 334)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "id"
        Me.Label1.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(40, 271)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(213, 271)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtDescuento1
        '
        Me.txtDescuento1.Location = New System.Drawing.Point(102, 64)
        Me.txtDescuento1.Name = "txtDescuento1"
        Me.txtDescuento1.Size = New System.Drawing.Size(242, 20)
        Me.txtDescuento1.TabIndex = 4
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(11, 9)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(44, 13)
        Me.lblNombre.TabIndex = 49
        Me.lblNombre.Text = "Nombre"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(105, 6)
        Me.txtNombre.MaxLength = 50
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(212, 20)
        Me.txtNombre.TabIndex = 50
        Me.txtNombre.Tag = "Nombre de la familia"
        '
        'chkInactivo
        '
        Me.chkInactivo.AutoSize = True
        Me.chkInactivo.Location = New System.Drawing.Point(105, 32)
        Me.chkInactivo.Name = "chkInactivo"
        Me.chkInactivo.Size = New System.Drawing.Size(64, 17)
        Me.chkInactivo.TabIndex = 57
        Me.chkInactivo.Tag = "Familia inactiva"
        Me.chkInactivo.Text = "Inactivo"
        Me.chkInactivo.UseVisualStyleBackColor = True
        '
        'grpPatentes
        '
        Me.grpPatentes.Controls.Add(Me.lblPatente)
        Me.grpPatentes.Controls.Add(Me.cboPatente)
        Me.grpPatentes.Controls.Add(Me.Button1)
        Me.grpPatentes.Controls.Add(Me.Button2)
        Me.grpPatentes.Controls.Add(Me.grdPatente)
        Me.grpPatentes.Location = New System.Drawing.Point(51, 55)
        Me.grpPatentes.Name = "grpPatentes"
        Me.grpPatentes.Size = New System.Drawing.Size(227, 192)
        Me.grpPatentes.TabIndex = 48
        Me.grpPatentes.TabStop = False
        Me.grpPatentes.Text = "Patentes"
        '
        'lblPatente
        '
        Me.lblPatente.AutoSize = True
        Me.lblPatente.Location = New System.Drawing.Point(6, 22)
        Me.lblPatente.Name = "lblPatente"
        Me.lblPatente.Size = New System.Drawing.Size(44, 13)
        Me.lblPatente.TabIndex = 4
        Me.lblPatente.Text = "Patente"
        '
        'cboPatente
        '
        Me.cboPatente.FormattingEnabled = True
        Me.cboPatente.Location = New System.Drawing.Point(51, 19)
        Me.cboPatente.Name = "cboPatente"
        Me.cboPatente.Size = New System.Drawing.Size(143, 21)
        Me.cboPatente.TabIndex = 0
        Me.cboPatente.Tag = "Patente para asociar"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(145, 162)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Tag = "Quitar patente"
        Me.Button1.Text = "Quitar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(6, 162)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 2
        Me.Button2.Tag = "Agregar patente"
        Me.Button2.Text = "Agregar"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'grdPatente
        '
        Me.grdPatente.AllowUserToAddRows = False
        Me.grdPatente.AllowUserToDeleteRows = False
        Me.grdPatente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPatente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cNombre, Me.cEstado})
        Me.grdPatente.Location = New System.Drawing.Point(6, 46)
        Me.grdPatente.Name = "grdPatente"
        Me.grdPatente.ReadOnly = True
        Me.grdPatente.RowHeadersVisible = False
        Me.grdPatente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdPatente.Size = New System.Drawing.Size(214, 110)
        Me.grdPatente.TabIndex = 1
        '
        'cId
        '
        Me.cId.HeaderText = "id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cNombre
        '
        Me.cNombre.HeaderText = "Nombre Patente"
        Me.cNombre.Name = "cNombre"
        Me.cNombre.ReadOnly = True
        Me.cNombre.Width = 200
        '
        'cEstado
        '
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Visible = False
        '
        'lblMsgNoName
        '
        Me.lblMsgNoName.AutoSize = True
        Me.lblMsgNoName.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoName.Location = New System.Drawing.Point(14, 354)
        Me.lblMsgNoName.Name = "lblMsgNoName"
        Me.lblMsgNoName.Size = New System.Drawing.Size(198, 13)
        Me.lblMsgNoName.TabIndex = 59
        Me.lblMsgNoName.Text = "Debe ingresar un nombre para el usuario"
        Me.lblMsgNoName.Visible = False
        '
        'lblMsgNameDuplicated
        '
        Me.lblMsgNameDuplicated.AutoSize = True
        Me.lblMsgNameDuplicated.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNameDuplicated.Location = New System.Drawing.Point(14, 367)
        Me.lblMsgNameDuplicated.Name = "lblMsgNameDuplicated"
        Me.lblMsgNameDuplicated.Size = New System.Drawing.Size(206, 13)
        Me.lblMsgNameDuplicated.TabIndex = 62
        Me.lblMsgNameDuplicated.Text = "Ese nombre ya está en uso por otra familia"
        Me.lblMsgNameDuplicated.Visible = False
        '
        'lblMsgCantDelete
        '
        Me.lblMsgCantDelete.AutoSize = True
        Me.lblMsgCantDelete.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgCantDelete.Location = New System.Drawing.Point(14, 380)
        Me.lblMsgCantDelete.Name = "lblMsgCantDelete"
        Me.lblMsgCantDelete.Size = New System.Drawing.Size(376, 13)
        Me.lblMsgCantDelete.TabIndex = 65
        Me.lblMsgCantDelete.Text = "No se puede guardar patentes/familias porque dejaría patentes sin asignación"
        Me.lblMsgCantDelete.Visible = False
        '
        'frmFamilia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 402)
        Me.Controls.Add(Me.lblMsgCantDelete)
        Me.Controls.Add(Me.lblMsgNameDuplicated)
        Me.Controls.Add(Me.lblMsgNoName)
        Me.Controls.Add(Me.grpPatentes)
        Me.Controls.Add(Me.chkInactivo)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtId)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFamilia"
        Me.Text = "Familia"
        Me.grpPatentes.ResumeLayout(False)
        Me.grpPatentes.PerformLayout()
        CType(Me.grdPatente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtDescuento1 As System.Windows.Forms.TextBox
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents chkInactivo As System.Windows.Forms.CheckBox
    Friend WithEvents grpPatentes As System.Windows.Forms.GroupBox
    Friend WithEvents lblPatente As System.Windows.Forms.Label
    Friend WithEvents cboPatente As System.Windows.Forms.ComboBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents grdPatente As System.Windows.Forms.DataGridView
    Friend WithEvents lblMsgNoName As System.Windows.Forms.Label
    Friend WithEvents lblMsgNameDuplicated As System.Windows.Forms.Label
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblMsgCantDelete As System.Windows.Forms.Label
End Class
