﻿Public Class frmFamilia
    Public Property isNew As Boolean

    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If Not ValidateInput() Then
            Return
        End If

        Dim u As Familia

        'valido datos duplicados
        Dim familias = Familia.Listar()
        For Each usr As Familia In familias
            If Me.isNew Or usr.id <> CInt(txtId.Text) Then
                If (usr.nombre.Trim = txtNombre.Text.Trim) Then
                    MsgBox(lblMsgNameDuplicated.Text)
                    txtNombre.Focus()
                    Return
                End If
            End If
        Next

        'valido que la accion no deje patentes sin asignacion
        Dim patentes = New List(Of Patente)
        For Each r As DataGridViewRow In grdPatente.Rows
            Select Case r.Cells("cEstado").Value
                Case "B"
                    patentes.Add(New Patente(CInt(r.Cells("cId").Value)))
            End Select
        Next
        'me fijo si eliminar el usuario dejaria una patente sin asignacion, cancelo acción
        For Each p As Patente In patentes
            If Seguridad.GetCantidadUsuariosConPatente(p.descripcion) = 1 Then
                MsgBox(lblMsgCantDelete.Text + ": " + p.descripcion)
                Return
            End If
        Next

        If Me.isNew Then
            u = New Familia()
            u.nombre = txtNombre.Text
            u.inactivo = chkInactivo.Checked
            u.Agregar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Alta de familia: " + txtNombre.Text)
        Else
            u = New Familia(CInt(txtId.Text))
            u.nombre = txtNombre.Text
            u.inactivo = chkInactivo.Checked
            u.Modificar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Modifica familia: " + txtNombre.Text)
        End If

        'grabo patentes
        For Each r As DataGridViewRow In grdPatente.Rows
            Select Case r.Cells("cEstado").Value
                Case "A"
                    u.GrabarPatente(r.Cells("cId").Value)
                Case "B"
                    u.EliminarPatente(r.Cells("cId").Value)
            End Select
        Next

        Beep()
        Me.Close()
    End Sub

    Private Sub frmFamilia_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        'combo de Patentes
        Dim sourcePatentes As New Dictionary(Of Integer, String)()
        Dim Patentes = Patente.Listar()
        For Each f As Patente In Patentes
            sourcePatentes.Add(f.id, f.descripcion)
        Next
        cboPatente.DataSource = New BindingSource(sourcePatentes, Nothing)
        cboPatente.DisplayMember = "Value"
        cboPatente.ValueMember = "Key"

        'MultiIdioma.ConfigurarForm(Me)
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Function ValidateInput() As Boolean
        If String.IsNullOrEmpty(txtNombre.Text.Trim) Then
            MsgBox(lblMsgNoName.Text)
            txtNombre.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim exists = False
        For Each r As DataGridViewRow In grdPatente.Rows
            If (r.Cells("cId").Value = cboPatente.SelectedValue) Then
                exists = True
                If Not r.Visible Then
                    r.Visible = True
                    r.Cells("cEstado").Value = ""
                End If
                Exit For
            End If
        Next

        If Not exists Then
            grdPatente.Rows.Add(cboPatente.SelectedValue, cboPatente.Text, "A")
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Me.grdPatente.Rows.Count > 0 AndAlso Me.grdPatente.SelectedRows.Count = 1 Then
            If (grdPatente.SelectedRows(0).Cells("cEstado").Value = "A") Then
                grdPatente.Rows.Remove(grdPatente.SelectedRows(0))
            Else
                grdPatente.SelectedRows(0).Cells("cEstado").Value = "B"
                grdPatente.SelectedRows(0).Visible = False
            End If
        End If
    End Sub

    Private Sub frmFamilia_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            If Me.isNew Then
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Dar de alta una familia")
            Else
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Modificar familia")
            End If
        End If
    End Sub
End Class