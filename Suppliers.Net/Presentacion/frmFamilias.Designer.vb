﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFamilias
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBaja = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAlta = New System.Windows.Forms.Button()
        Me.grd = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cInactivo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.lblMsgCantDelete = New System.Windows.Forms.Label()
        Me.lblMsgConfirmDeletion = New System.Windows.Forms.Label()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBaja
        '
        Me.btnBaja.Location = New System.Drawing.Point(255, 260)
        Me.btnBaja.Name = "btnBaja"
        Me.btnBaja.Size = New System.Drawing.Size(75, 23)
        Me.btnBaja.TabIndex = 8
        Me.btnBaja.Tag = "Eliminar familia"
        Me.btnBaja.Text = "Baja"
        Me.btnBaja.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(157, 260)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 7
        Me.btnModificar.Tag = "Modificar familia"
        Me.btnModificar.Text = "Modificacion"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAlta
        '
        Me.btnAlta.Location = New System.Drawing.Point(46, 260)
        Me.btnAlta.Name = "btnAlta"
        Me.btnAlta.Size = New System.Drawing.Size(88, 23)
        Me.btnAlta.TabIndex = 6
        Me.btnAlta.Tag = "Nueva famlia"
        Me.btnAlta.Text = "Alta"
        Me.btnAlta.UseVisualStyleBackColor = True
        '
        'grd
        '
        Me.grd.AllowUserToAddRows = False
        Me.grd.AllowUserToDeleteRows = False
        Me.grd.AllowUserToResizeColumns = False
        Me.grd.AllowUserToResizeRows = False
        Me.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cNombre, Me.cInactivo})
        Me.grd.Location = New System.Drawing.Point(15, 9)
        Me.grd.MultiSelect = False
        Me.grd.Name = "grd"
        Me.grd.ReadOnly = True
        Me.grd.RowHeadersVisible = False
        Me.grd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grd.Size = New System.Drawing.Size(347, 235)
        Me.grd.TabIndex = 5
        '
        'cId
        '
        Me.cId.HeaderText = "Id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cNombre
        '
        Me.cNombre.HeaderText = "Nombre"
        Me.cNombre.Name = "cNombre"
        Me.cNombre.ReadOnly = True
        Me.cNombre.Width = 250
        '
        'cInactivo
        '
        Me.cInactivo.HeaderText = "Inactivo"
        Me.cInactivo.Name = "cInactivo"
        Me.cInactivo.ReadOnly = True
        Me.cInactivo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cInactivo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'lblMsgCantDelete
        '
        Me.lblMsgCantDelete.AutoSize = True
        Me.lblMsgCantDelete.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgCantDelete.Location = New System.Drawing.Point(12, 316)
        Me.lblMsgCantDelete.Name = "lblMsgCantDelete"
        Me.lblMsgCantDelete.Size = New System.Drawing.Size(315, 13)
        Me.lblMsgCantDelete.TabIndex = 62
        Me.lblMsgCantDelete.Text = "No se puede eliminar la familia porque está asignada a un usuario"
        Me.lblMsgCantDelete.Visible = False
        '
        'lblMsgConfirmDeletion
        '
        Me.lblMsgConfirmDeletion.AutoSize = True
        Me.lblMsgConfirmDeletion.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgConfirmDeletion.Location = New System.Drawing.Point(12, 303)
        Me.lblMsgConfirmDeletion.Name = "lblMsgConfirmDeletion"
        Me.lblMsgConfirmDeletion.Size = New System.Drawing.Size(197, 13)
        Me.lblMsgConfirmDeletion.TabIndex = 61
        Me.lblMsgConfirmDeletion.Text = "¿Desea eliminar la familia seleccionada?"
        Me.lblMsgConfirmDeletion.Visible = False
        '
        'frmFamilias
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(377, 296)
        Me.Controls.Add(Me.lblMsgCantDelete)
        Me.Controls.Add(Me.lblMsgConfirmDeletion)
        Me.Controls.Add(Me.btnBaja)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAlta)
        Me.Controls.Add(Me.grd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFamilias"
        Me.Text = "Familias"
        CType(Me.grd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBaja As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAlta As System.Windows.Forms.Button
    Friend WithEvents grd As System.Windows.Forms.DataGridView
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cInactivo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lblMsgCantDelete As System.Windows.Forms.Label
    Friend WithEvents lblMsgConfirmDeletion As System.Windows.Forms.Label
End Class
