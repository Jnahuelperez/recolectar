﻿Public Class frmFamilias

    Private Sub btnAlta_Click(sender As System.Object, e As System.EventArgs) Handles btnAlta.Click
        Dim frm As New frmFamilia
        frm.isNew = True
        frm.ShowDialog(Me)
        ActualizarGrilla()
    End Sub

    Private Sub frmFamilias_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ActualizarGrilla()

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        btnAlta.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Familia")
        btnBaja.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Familia")
        btnModificar.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Familia")
    End Sub

    Private Sub ActualizarGrilla()
        Dim familias = Familia.Listar()

        grd.Rows.Clear()

        For Each f As Familia In familias
            grd.Rows.Add(f.id, f.nombre, f.inactivo)
        Next
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If (grd.SelectedRows.Count = 1) Then
            Dim u = New Familia(CInt(grd.SelectedRows(0).Cells("cId").Value))

            Dim frm As New frmFamilia
            frm.txtId.Text = u.id.ToString
            frm.txtNombre.Text = u.nombre
            frm.chkInactivo.Checked = u.inactivo
            frm.chkInactivo.Enabled = u.inactivo

            For Each p As Patente In u.patentes
                frm.grdPatente.Rows.Add(p.id, p.descripcion)
            Next

            frm.isNew = False
            frm.ShowDialog(Me)
            ActualizarGrilla()
        End If
    End Sub

    Private Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
        Dim id As Integer

        If (grd.SelectedRows.Count = 1) Then
            id = CInt(grd.SelectedRows(0).Cells("cId").Value)

            If MsgBox(lblMsgConfirmDeletion.Text, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim u = New Familia(id)

                'me fijo si la familia está en uso
                If Familia.EnUso(id) Then
                    MsgBox(lblMsgCantDelete.Text)
                    Return
                End If

                'si llego hasta acá, elimino
                Familia.Eliminar(id)

                'registro en bitácora
                Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 1, "Elimina familia: " + u.nombre)

                'actualizo grilla
                ActualizarGrilla()
            End If
        End If
    End Sub

    Private Sub frmFamilias_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Familias")
        End If

    End Sub
End Class