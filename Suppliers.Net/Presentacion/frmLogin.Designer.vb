﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblMsgNoUser = New System.Windows.Forms.Label()
        Me.lblMsgNoPass = New System.Windows.Forms.Label()
        Me.lblMsgWrongUser = New System.Windows.Forms.Label()
        Me.lblMsgBlockedUser = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "User"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Password"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(94, 29)
        Me.txtUser.MaxLength = 20
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(126, 20)
        Me.txtUser.TabIndex = 2
        Me.txtUser.Tag = "Usuario"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(94, 65)
        Me.txtPassword.MaxLength = 20
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(126, 20)
        Me.txtPassword.TabIndex = 3
        Me.txtPassword.Tag = "Password"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(32, 110)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 4
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(135, 110)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblMsgNoUser
        '
        Me.lblMsgNoUser.AutoSize = True
        Me.lblMsgNoUser.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoUser.Location = New System.Drawing.Point(2, 164)
        Me.lblMsgNoUser.Name = "lblMsgNoUser"
        Me.lblMsgNoUser.Size = New System.Drawing.Size(125, 13)
        Me.lblMsgNoUser.TabIndex = 6
        Me.lblMsgNoUser.Text = "Debe ingresar un usuario"
        Me.lblMsgNoUser.Visible = False
        '
        'lblMsgNoPass
        '
        Me.lblMsgNoPass.AutoSize = True
        Me.lblMsgNoPass.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoPass.Location = New System.Drawing.Point(2, 177)
        Me.lblMsgNoPass.Name = "lblMsgNoPass"
        Me.lblMsgNoPass.Size = New System.Drawing.Size(136, 13)
        Me.lblMsgNoPass.TabIndex = 7
        Me.lblMsgNoPass.Text = "Debe ingresar un password"
        Me.lblMsgNoPass.Visible = False
        '
        'lblMsgWrongUser
        '
        Me.lblMsgWrongUser.AutoSize = True
        Me.lblMsgWrongUser.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgWrongUser.Location = New System.Drawing.Point(2, 190)
        Me.lblMsgWrongUser.Name = "lblMsgWrongUser"
        Me.lblMsgWrongUser.Size = New System.Drawing.Size(216, 13)
        Me.lblMsgWrongUser.TabIndex = 8
        Me.lblMsgWrongUser.Text = "Nombre de usuario o contraseña incorrectos"
        Me.lblMsgWrongUser.Visible = False
        '
        'lblMsgBlockedUser
        '
        Me.lblMsgBlockedUser.AutoSize = True
        Me.lblMsgBlockedUser.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgBlockedUser.Location = New System.Drawing.Point(2, 203)
        Me.lblMsgBlockedUser.Name = "lblMsgBlockedUser"
        Me.lblMsgBlockedUser.Size = New System.Drawing.Size(296, 13)
        Me.lblMsgBlockedUser.TabIndex = 9
        Me.lblMsgBlockedUser.Text = "El usuario está bloqueado. Comuniquese con el administrador"
        Me.lblMsgBlockedUser.Visible = False
        '
        'frmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(235, 317)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblMsgBlockedUser)
        Me.Controls.Add(Me.lblMsgWrongUser)
        Me.Controls.Add(Me.lblMsgNoPass)
        Me.Controls.Add(Me.lblMsgNoUser)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLogin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblMsgNoUser As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoPass As System.Windows.Forms.Label
    Friend WithEvents lblMsgWrongUser As System.Windows.Forms.Label
    Friend WithEvents lblMsgBlockedUser As System.Windows.Forms.Label
End Class
