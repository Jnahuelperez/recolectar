﻿Public Class frmLogin

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MultiIdioma.ConfigurarForm(Me)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        ValidarDatos()

        Dim user = Seguridad.EncriptarReversible(txtUser.Text)
        Dim pass = Seguridad.EncriptarMD5(txtPassword.Text)

        Dim usr = Usuario.Login(user, pass)

        If usr Is Nothing Then
            MsgBox(lblMsgWrongUser.Text)
        ElseIf usr.bloqueado Then
            MsgBox(lblMsgBlockedUser.Text)
        ElseIf Not usr.logueado Then
            MsgBox(lblMsgWrongUser.Text)
        Else
            Seguridad.currentUser = usr
            'si el login se logró
            'habilitar patentes del main
            frmMain.configMenu()
            Me.Close()
        End If
        txtUser.Focus()
    End Sub

    Private Sub Login(user As String, pass As String)
        Usuario.Login(user, pass)
    End Sub

    Private Function ValidarDatos() As Boolean
        If (txtUser.Text.Length = 0) Then
            MsgBox(lblMsgNoUser.Text)
            txtUser.Focus()
            Return False
        End If

        If (txtPassword.Text.Length = 0) Then
            MsgBox(lblMsgNoPass.Text)
            txtPassword.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If (Seguridad.currentUser Is Nothing) Then
            End
        Else
            Me.Close()
        End If

    End Sub

    Private Sub frmLogin_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Login")
        End If
    End Sub
End Class