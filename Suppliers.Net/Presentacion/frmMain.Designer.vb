﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.OperacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmitirPedidoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecibirPedidoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdministracionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SesionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoginToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecuperarContraseñaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarContraseñaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarIdiomaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeguridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BitácoraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FamiliasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AyudaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BaseDeDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RealizarBackupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RestaurarDesdeBackupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.dlgFile = New System.Windows.Forms.OpenFileDialog()
        Me.lblMsgRestoreBackupOK = New System.Windows.Forms.Label()
        Me.lblMsgIntegrityError = New System.Windows.Forms.Label()
        Me.tt = New System.Windows.Forms.ToolTip(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OperacionesToolStripMenuItem, Me.AdministracionToolStripMenuItem, Me.SesionToolStripMenuItem, Me.SeguridadToolStripMenuItem, Me.AyudaToolStripMenuItem, Me.BaseDeDatosToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(573, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'OperacionesToolStripMenuItem
        '
        Me.OperacionesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EmitirPedidoToolStripMenuItem, Me.RecibirPedidoToolStripMenuItem, Me.VentaToolStripMenuItem})
        Me.OperacionesToolStripMenuItem.Name = "OperacionesToolStripMenuItem"
        Me.OperacionesToolStripMenuItem.Size = New System.Drawing.Size(85, 20)
        Me.OperacionesToolStripMenuItem.Text = "Operaciones"
        '
        'EmitirPedidoToolStripMenuItem
        '
        Me.EmitirPedidoToolStripMenuItem.Name = "EmitirPedidoToolStripMenuItem"
        Me.EmitirPedidoToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.EmitirPedidoToolStripMenuItem.Text = "Emitir Pedido"
        '
        'RecibirPedidoToolStripMenuItem
        '
        Me.RecibirPedidoToolStripMenuItem.Name = "RecibirPedidoToolStripMenuItem"
        Me.RecibirPedidoToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.RecibirPedidoToolStripMenuItem.Text = "Recibir Pedido"
        '
        'VentaToolStripMenuItem
        '
        Me.VentaToolStripMenuItem.Name = "VentaToolStripMenuItem"
        Me.VentaToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.VentaToolStripMenuItem.Text = "Venta"
        '
        'AdministracionToolStripMenuItem
        '
        Me.AdministracionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductosToolStripMenuItem, Me.ProveedoresToolStripMenuItem})
        Me.AdministracionToolStripMenuItem.Name = "AdministracionToolStripMenuItem"
        Me.AdministracionToolStripMenuItem.Size = New System.Drawing.Size(100, 20)
        Me.AdministracionToolStripMenuItem.Text = "Administracion"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ProductosToolStripMenuItem.Text = "Productos"
        '
        'ProveedoresToolStripMenuItem
        '
        Me.ProveedoresToolStripMenuItem.Name = "ProveedoresToolStripMenuItem"
        Me.ProveedoresToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ProveedoresToolStripMenuItem.Text = "Proveedores"
        '
        'SesionToolStripMenuItem
        '
        Me.SesionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LoginToolStripMenuItem, Me.LogoutToolStripMenuItem, Me.RecuperarContraseñaToolStripMenuItem, Me.CambiarContraseñaToolStripMenuItem, Me.CambiarIdiomaToolStripMenuItem})
        Me.SesionToolStripMenuItem.Name = "SesionToolStripMenuItem"
        Me.SesionToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.SesionToolStripMenuItem.Text = "Sesion"
        '
        'LoginToolStripMenuItem
        '
        Me.LoginToolStripMenuItem.Name = "LoginToolStripMenuItem"
        Me.LoginToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.LoginToolStripMenuItem.Text = "Login"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'RecuperarContraseñaToolStripMenuItem
        '
        Me.RecuperarContraseñaToolStripMenuItem.Name = "RecuperarContraseñaToolStripMenuItem"
        Me.RecuperarContraseñaToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.RecuperarContraseñaToolStripMenuItem.Text = "Recuperar Contraseña"
        '
        'CambiarContraseñaToolStripMenuItem
        '
        Me.CambiarContraseñaToolStripMenuItem.Name = "CambiarContraseñaToolStripMenuItem"
        Me.CambiarContraseñaToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.CambiarContraseñaToolStripMenuItem.Text = "Cambiar Contraseña"
        '
        'CambiarIdiomaToolStripMenuItem
        '
        Me.CambiarIdiomaToolStripMenuItem.Name = "CambiarIdiomaToolStripMenuItem"
        Me.CambiarIdiomaToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.CambiarIdiomaToolStripMenuItem.Text = "Cambiar Idioma"
        '
        'SeguridadToolStripMenuItem
        '
        Me.SeguridadToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BitácoraToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.FamiliasToolStripMenuItem})
        Me.SeguridadToolStripMenuItem.Name = "SeguridadToolStripMenuItem"
        Me.SeguridadToolStripMenuItem.Size = New System.Drawing.Size(72, 20)
        Me.SeguridadToolStripMenuItem.Text = "Seguridad"
        '
        'BitácoraToolStripMenuItem
        '
        Me.BitácoraToolStripMenuItem.Name = "BitácoraToolStripMenuItem"
        Me.BitácoraToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.BitácoraToolStripMenuItem.Text = "Bitácora"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'FamiliasToolStripMenuItem
        '
        Me.FamiliasToolStripMenuItem.Name = "FamiliasToolStripMenuItem"
        Me.FamiliasToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.FamiliasToolStripMenuItem.Text = "Familias"
        '
        'AyudaToolStripMenuItem
        '
        Me.AyudaToolStripMenuItem.Name = "AyudaToolStripMenuItem"
        Me.AyudaToolStripMenuItem.Size = New System.Drawing.Size(53, 20)
        Me.AyudaToolStripMenuItem.Text = "Ayuda"
        '
        'BaseDeDatosToolStripMenuItem
        '
        Me.BaseDeDatosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RealizarBackupToolStripMenuItem, Me.RestaurarDesdeBackupToolStripMenuItem})
        Me.BaseDeDatosToolStripMenuItem.Name = "BaseDeDatosToolStripMenuItem"
        Me.BaseDeDatosToolStripMenuItem.Size = New System.Drawing.Size(92, 20)
        Me.BaseDeDatosToolStripMenuItem.Text = "Base de Datos"
        '
        'RealizarBackupToolStripMenuItem
        '
        Me.RealizarBackupToolStripMenuItem.Name = "RealizarBackupToolStripMenuItem"
        Me.RealizarBackupToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.RealizarBackupToolStripMenuItem.Text = "Realizar Backup"
        '
        'RestaurarDesdeBackupToolStripMenuItem
        '
        Me.RestaurarDesdeBackupToolStripMenuItem.Name = "RestaurarDesdeBackupToolStripMenuItem"
        Me.RestaurarDesdeBackupToolStripMenuItem.Size = New System.Drawing.Size(199, 22)
        Me.RestaurarDesdeBackupToolStripMenuItem.Text = "Restaurar desde Backup"
        '
        'lblMsgRestoreBackupOK
        '
        Me.lblMsgRestoreBackupOK.AutoSize = True
        Me.lblMsgRestoreBackupOK.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgRestoreBackupOK.Location = New System.Drawing.Point(12, 357)
        Me.lblMsgRestoreBackupOK.Name = "lblMsgRestoreBackupOK"
        Me.lblMsgRestoreBackupOK.Size = New System.Drawing.Size(200, 13)
        Me.lblMsgRestoreBackupOK.TabIndex = 60
        Me.lblMsgRestoreBackupOK.Text = "Restauración de base de datos realizada"
        Me.lblMsgRestoreBackupOK.Visible = False
        '
        'lblMsgIntegrityError
        '
        Me.lblMsgIntegrityError.AutoSize = True
        Me.lblMsgIntegrityError.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lblMsgIntegrityError.Location = New System.Drawing.Point(12, 370)
        Me.lblMsgIntegrityError.Name = "lblMsgIntegrityError"
        Me.lblMsgIntegrityError.Size = New System.Drawing.Size(461, 13)
        Me.lblMsgIntegrityError.TabIndex = 62
        Me.lblMsgIntegrityError.Text = "Error de Integridad en Base de Datos. El sistema se cerrará. Comuníquese con un a" & _
    "dministrador."
        Me.lblMsgIntegrityError.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(573, 295)
        Me.Controls.Add(Me.lblMsgIntegrityError)
        Me.Controls.Add(Me.lblMsgRestoreBackupOK)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMain"
        Me.Text = "Suppliers.Net"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents OperacionesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdministracionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SesionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SeguridadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AyudaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProveedoresToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoginToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BitácoraToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FamiliasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecuperarContraseñaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarContraseñaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BaseDeDatosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RealizarBackupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RestaurarDesdeBackupToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarIdiomaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmitirPedidoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecibirPedidoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VentaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dlgFile As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblMsgRestoreBackupOK As System.Windows.Forms.Label
    Friend WithEvents lblMsgIntegrityError As System.Windows.Forms.Label
    Friend WithEvents tt As System.Windows.Forms.ToolTip
End Class
