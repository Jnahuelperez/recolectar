﻿Imports System
Imports System.IO

Public Class frmMain

    Private Sub MenuStrip1_ItemClicked(sender As System.Object, e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles MenuStrip1.ItemClicked

    End Sub

    Private Sub TiposDeMovimientosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub SesionToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SesionToolStripMenuItem.Click

    End Sub

    Private Sub ProveedoresToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ProveedoresToolStripMenuItem.Click
        Dim frm As New frmProveedores
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub BitácoraToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles BitácoraToolStripMenuItem.Click
        Dim frm As New frmBitacora
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub UsuariosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles UsuariosToolStripMenuItem.Click
        Dim frm As New frmUsuarios
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub LoginToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LoginToolStripMenuItem.Click
        Dim frm As New frmLogin
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub RecuperarContraseñaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RecuperarContraseñaToolStripMenuItem.Click
        Dim frm As New frmRecuperarPass
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub CambiarContraseñaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CambiarContraseñaToolStripMenuItem.Click
        Dim frm As New frmCambiarPass
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub FamiliasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles FamiliasToolStripMenuItem.Click
        Dim frm As New frmFamilias
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub RealizarBackupToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RealizarBackupToolStripMenuItem.Click
        Dim frm As New frmRealizarBackup
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub RestaurarDesdeBackupToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RestaurarDesdeBackupToolStripMenuItem.Click
        If dlgFile.ShowDialog() = DialogResult.OK Then
            If Seguridad.RestaurarBackup(dlgFile.FileName) Then
                MsgBox(lblMsgRestoreBackupOK.Text)
            End If
        End If
    End Sub

    Private Sub CambiarIdiomaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CambiarIdiomaToolStripMenuItem.Click
        Dim frm As New frmSeleccionarIdioma
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub ProductosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ProductosToolStripMenuItem.Click
        Dim frm As New frmProductos
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub EmitirPedidoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EmitirPedidoToolStripMenuItem.Click
        Dim frm As New frmEmitirPedido
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub RecibirPedidoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RecibirPedidoToolStripMenuItem.Click
        Dim frm As New frmRecibirPedido
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Private Sub VentaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles VentaToolStripMenuItem.Click
        Dim frm As New frmVenta
        frm.MdiParent = Me
        frm.Show()
    End Sub

    Public Sub configMenu()
        ProveedoresToolStripMenuItem.Enabled = _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Proveedor") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Proveedor") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Proveedor")

        BitácoraToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Consultar Bitácora")

        UsuariosToolStripMenuItem.Enabled = _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Usuario") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Usuario") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Desbloquear Usuario") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Usuario")

        FamiliasToolStripMenuItem.Enabled = _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Familia") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Familia") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Familia")

        RealizarBackupToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Realizar Backup")

        RestaurarDesdeBackupToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Restaurar Backup")

        ProductosToolStripMenuItem.Enabled = _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Producto") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Producto") Or _
            Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Producto")

        EmitirPedidoToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Emitir Pedido")

        RecibirPedidoToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Recibir Pedido")

        VentaToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Realizar venta")

        RecuperarContraseñaToolStripMenuItem.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Recuperar contraseña")
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load



#Region "comentado"
        'levanto la connection string
        If Seguridad.connectionString = "" Or Not Seguridad.isConnectionStringValid(Seguridad.connectionString) Then
            Dim frm As New frmConfig
            'frm.MdiParent = Me
            frm.ShowDialog(Me)
            frmMain_Load(Me, New System.EventArgs)
        Else

            Console.WriteLine(Seguridad.EncriptarReversible("hernan"))

            'Console.WriteLine(Seguridad.EncriptarReversible("admin"))
            'la conexion está bien configurada
            Seguridad.connectionString = Seguridad.connectionString
            Dim erroresDeIntegridad = Seguridad.VerificarIntegridad
            Dim frm As New frmLogin
            frm.ShowDialog(Me)

            MultiIdioma.ConfigurarForm(Me)


            'en este punto, el usuario ya está logueado
            'TODO: verificar integridad
            If erroresDeIntegridad Then
                SetearAccesos()
            Else
                'si tiene permiso de recalcular, se lo permite. Si no, mensaje y cierra el programa
                If Seguridad.CheckPatente(Seguridad.currentUser.id, "Recuperar Integridad") Then
                    Dim frm1 As New frmErroresDeIntegridad()
                    frm1.MdiParent = Me
                    frm1.Show()
                Else
                    MsgBox(lblMsgIntegrityError.Text, MsgBoxStyle.Critical)
                    End
                End If
            End If
        End If
#End Region


    End Sub

    Private Sub SetearAccesos()
        'TODO: checkear patentes de pantalla principal
    End Sub

    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogoutToolStripMenuItem.Click
        Seguridad.currentUser.Logout()
        CloseAll()
    End Sub

    Private Sub CloseAll()
        End
    End Sub

    Private Sub frmMain_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown

    End Sub

    Private Sub AyudaToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AyudaToolStripMenuItem.Click
        Help.ShowHelp(Me, "Suppliers.chm")
    End Sub
End Class