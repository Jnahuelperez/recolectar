﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProducto
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtDescripcion = New System.Windows.Forms.TextBox()
        Me.lblRazonSocial = New System.Windows.Forms.Label()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.txtStockMinimo = New System.Windows.Forms.TextBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.chkInactivo = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtStockOptimo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.cboProveedor = New System.Windows.Forms.ComboBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.lblMsgNoMin = New System.Windows.Forms.Label()
        Me.lblMsgNoDescription = New System.Windows.Forms.Label()
        Me.lblMsgNoCode = New System.Windows.Forms.Label()
        Me.lblMsgNoOpt = New System.Windows.Forms.Label()
        Me.lblMsgNameDuplicated = New System.Windows.Forms.Label()
        Me.lblMsgCodeDuplicated = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtDescripcion
        '
        Me.txtDescripcion.Location = New System.Drawing.Point(104, 32)
        Me.txtDescripcion.MaxLength = 50
        Me.txtDescripcion.Name = "txtDescripcion"
        Me.txtDescripcion.Size = New System.Drawing.Size(227, 20)
        Me.txtDescripcion.TabIndex = 0
        Me.txtDescripcion.Tag = "Descripción del producto"
        '
        'lblRazonSocial
        '
        Me.lblRazonSocial.AutoSize = True
        Me.lblRazonSocial.Location = New System.Drawing.Point(12, 35)
        Me.lblRazonSocial.Name = "lblRazonSocial"
        Me.lblRazonSocial.Size = New System.Drawing.Size(63, 13)
        Me.lblRazonSocial.TabIndex = 1
        Me.lblRazonSocial.Text = "Descripción"
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(12, 61)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(73, 13)
        Me.lblTelefono.TabIndex = 3
        Me.lblTelefono.Text = "Stock Mínimo"
        '
        'txtStockMinimo
        '
        Me.txtStockMinimo.Location = New System.Drawing.Point(104, 58)
        Me.txtStockMinimo.MaxLength = 10
        Me.txtStockMinimo.Name = "txtStockMinimo"
        Me.txtStockMinimo.Size = New System.Drawing.Size(111, 20)
        Me.txtStockMinimo.TabIndex = 2
        Me.txtStockMinimo.Tag = "Stock mínimo del producto"
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(256, 173)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(175, 173)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 113)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Proveedor"
        '
        'chkInactivo
        '
        Me.chkInactivo.AutoSize = True
        Me.chkInactivo.Enabled = False
        Me.chkInactivo.Location = New System.Drawing.Point(104, 136)
        Me.chkInactivo.Name = "chkInactivo"
        Me.chkInactivo.Size = New System.Drawing.Size(64, 17)
        Me.chkInactivo.TabIndex = 4
        Me.chkInactivo.Tag = "Producto inactivo"
        Me.chkInactivo.Text = "Inactivo"
        Me.chkInactivo.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 87)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Stock Óptimo"
        '
        'txtStockOptimo
        '
        Me.txtStockOptimo.Location = New System.Drawing.Point(104, 84)
        Me.txtStockOptimo.MaxLength = 10
        Me.txtStockOptimo.Name = "txtStockOptimo"
        Me.txtStockOptimo.Size = New System.Drawing.Size(111, 20)
        Me.txtStockOptimo.TabIndex = 9
        Me.txtStockOptimo.Tag = "Stock deseado para el producto"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Código"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(104, 6)
        Me.txtCodigo.MaxLength = 20
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(111, 20)
        Me.txtCodigo.TabIndex = 11
        Me.txtCodigo.Tag = "Código del producto"
        '
        'cboProveedor
        '
        Me.cboProveedor.FormattingEnabled = True
        Me.cboProveedor.Location = New System.Drawing.Point(104, 109)
        Me.cboProveedor.Name = "cboProveedor"
        Me.cboProveedor.Size = New System.Drawing.Size(201, 21)
        Me.cboProveedor.TabIndex = 13
        Me.cboProveedor.Tag = "Proveedor del producto"
        '
        'btnSearch
        '
        Me.btnSearch.Image = Global.Presentacion.My.Resources.Resources.Search_icon1
        Me.btnSearch.Location = New System.Drawing.Point(306, 108)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(25, 23)
        Me.btnSearch.TabIndex = 14
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'txtId
        '
        Me.txtId.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtId.Location = New System.Drawing.Point(12, 225)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(100, 20)
        Me.txtId.TabIndex = 15
        Me.txtId.Text = "0"
        Me.txtId.Visible = False
        '
        'lblMsgNoMin
        '
        Me.lblMsgNoMin.AutoSize = True
        Me.lblMsgNoMin.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoMin.Location = New System.Drawing.Point(12, 282)
        Me.lblMsgNoMin.Name = "lblMsgNoMin"
        Me.lblMsgNoMin.Size = New System.Drawing.Size(189, 13)
        Me.lblMsgNoMin.TabIndex = 68
        Me.lblMsgNoMin.Text = "Debe indicar un valor mínimo de stock"
        Me.lblMsgNoMin.Visible = False
        '
        'lblMsgNoDescription
        '
        Me.lblMsgNoDescription.AutoSize = True
        Me.lblMsgNoDescription.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoDescription.Location = New System.Drawing.Point(12, 269)
        Me.lblMsgNoDescription.Name = "lblMsgNoDescription"
        Me.lblMsgNoDescription.Size = New System.Drawing.Size(145, 13)
        Me.lblMsgNoDescription.TabIndex = 67
        Me.lblMsgNoDescription.Text = "Debe indicar una descripción"
        Me.lblMsgNoDescription.Visible = False
        '
        'lblMsgNoCode
        '
        Me.lblMsgNoCode.AutoSize = True
        Me.lblMsgNoCode.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoCode.Location = New System.Drawing.Point(12, 256)
        Me.lblMsgNoCode.Name = "lblMsgNoCode"
        Me.lblMsgNoCode.Size = New System.Drawing.Size(197, 13)
        Me.lblMsgNoCode.TabIndex = 66
        Me.lblMsgNoCode.Text = "Debe indicar un código para el producto"
        Me.lblMsgNoCode.Visible = False
        '
        'lblMsgNoOpt
        '
        Me.lblMsgNoOpt.AutoSize = True
        Me.lblMsgNoOpt.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoOpt.Location = New System.Drawing.Point(12, 295)
        Me.lblMsgNoOpt.Name = "lblMsgNoOpt"
        Me.lblMsgNoOpt.Size = New System.Drawing.Size(186, 13)
        Me.lblMsgNoOpt.TabIndex = 69
        Me.lblMsgNoOpt.Text = "Debe indicar un valor óptimo de stock"
        Me.lblMsgNoOpt.Visible = False
        '
        'lblMsgNameDuplicated
        '
        Me.lblMsgNameDuplicated.AutoSize = True
        Me.lblMsgNameDuplicated.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNameDuplicated.Location = New System.Drawing.Point(12, 308)
        Me.lblMsgNameDuplicated.Name = "lblMsgNameDuplicated"
        Me.lblMsgNameDuplicated.Size = New System.Drawing.Size(238, 13)
        Me.lblMsgNameDuplicated.TabIndex = 70
        Me.lblMsgNameDuplicated.Text = "Esa descripcion ya está en uso por otro producto"
        Me.lblMsgNameDuplicated.Visible = False
        '
        'lblMsgCodeDuplicated
        '
        Me.lblMsgCodeDuplicated.AutoSize = True
        Me.lblMsgCodeDuplicated.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgCodeDuplicated.Location = New System.Drawing.Point(12, 321)
        Me.lblMsgCodeDuplicated.Name = "lblMsgCodeDuplicated"
        Me.lblMsgCodeDuplicated.Size = New System.Drawing.Size(216, 13)
        Me.lblMsgCodeDuplicated.TabIndex = 71
        Me.lblMsgCodeDuplicated.Text = "Ese código ya está en uso por otro producto"
        Me.lblMsgCodeDuplicated.Visible = False
        '
        'frmProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(343, 355)
        Me.Controls.Add(Me.lblMsgCodeDuplicated)
        Me.Controls.Add(Me.lblMsgNameDuplicated)
        Me.Controls.Add(Me.lblMsgNoOpt)
        Me.Controls.Add(Me.lblMsgNoMin)
        Me.Controls.Add(Me.lblMsgNoDescription)
        Me.Controls.Add(Me.lblMsgNoCode)
        Me.Controls.Add(Me.txtId)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.cboProveedor)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtStockOptimo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.chkInactivo)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.txtStockMinimo)
        Me.Controls.Add(Me.lblRazonSocial)
        Me.Controls.Add(Me.txtDescripcion)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProducto"
        Me.Text = "Producto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDescripcion As System.Windows.Forms.TextBox
    Friend WithEvents lblRazonSocial As System.Windows.Forms.Label
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtStockMinimo As System.Windows.Forms.TextBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkInactivo As System.Windows.Forms.CheckBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtStockOptimo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents cboProveedor As System.Windows.Forms.ComboBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents lblMsgNoMin As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoDescription As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoCode As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoOpt As System.Windows.Forms.Label
    Friend WithEvents lblMsgNameDuplicated As System.Windows.Forms.Label
    Friend WithEvents lblMsgCodeDuplicated As System.Windows.Forms.Label

End Class
