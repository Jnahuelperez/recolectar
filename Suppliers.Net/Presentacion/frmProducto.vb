﻿Public Class frmProducto
    Public isNew As Boolean


    Private Sub frmProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        MultiIdioma.ConfigurarForm(Me)

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Public Sub LoadProveedores()
        'combo de productoes
        Dim sourceproductoes As New Dictionary(Of Integer, String)()
        Dim proveedores = Proveedor.Listar()
        For Each f As Proveedor In proveedores
            If Not f.inactivo Then
                cboProveedor.Items.Add(New ListItem(f.id, f.razonSocial))
            End If
        Next
        cboProveedor.ValueMember = "Id"
        cboProveedor.DisplayMember = "Name"
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Not ValidateInput() Then
            Return
        End If

        Dim u As producto

        'valido datos duplicados
        Dim productos = Producto.Listar()
        For Each usr As Producto In productos
            If Me.isNew Or usr.id <> CInt(txtId.Text) Then
                If (usr.descripcion.Trim = txtDescripcion.Text.Trim) Then
                    MsgBox(lblMsgNameDuplicated.Text)
                    txtDescripcion.Focus()
                    Return
                End If

                If (usr.codigo.Trim = txtCodigo.Text.Trim) Then
                    MsgBox(lblMsgCodeDuplicated.Text)
                    txtCodigo.Focus()
                    Return
                End If
            End If
        Next

        If Me.isNew Then
            u = New producto()
            u.codigo = txtcodigo.Text
            u.descripcion = txtDescripcion.Text
            u.minimo = CInt(txtStockMinimo.Text)
            u.optimo = CInt(txtStockOptimo.Text)
            If Not cboProveedor.SelectedItem Is Nothing Then
                u.proveedor = New Proveedor(DirectCast(cboProveedor.SelectedItem, ListItem).Id)
            Else
                u.proveedor = New Proveedor()
            End If
            u.inactivo = chkInactivo.Checked
            u.Agregar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Alta de producto: " + txtCodigo.Text)
        Else
            u = New Producto(CInt(txtId.Text))
            u.descripcion = txtDescripcion.Text
            u.codigo = txtCodigo.Text
            u.minimo = CInt(txtStockMinimo.Text)
            u.optimo = CInt(txtStockOptimo.Text)
            If Not cboProveedor.SelectedItem Is Nothing Then
                u.proveedor = New Proveedor(DirectCast(cboProveedor.SelectedItem, ListItem).Id)
            Else
                u.proveedor = New Proveedor()
            End If
            u.inactivo = chkInactivo.Checked
            u.Modificar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Modifica producto: " + txtCodigo.Text)
        End If

        Me.Close()
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim frm As New frmCboSearch

        For Each o As Object In cboProveedor.Items
            Dim c = DirectCast(o, ListItem)
            frm.AddItem(c)
        Next

        If frm.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            cboProveedor.SelectedIndex = cboProveedor.FindStringExact(frm.SelectedName)
        End If
    End Sub

    Private Function ValidateInput() As Boolean
        If String.IsNullOrEmpty(txtCodigo.Text.Trim) Then
            MsgBox(lblMsgNoCode.Text)
            txtCodigo.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(txtDescripcion.Text.Trim) Then
            MsgBox(lblMsgNoDescription.Text)
            txtDescripcion.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(txtStockMinimo.Text.Trim) Then
            MsgBox(lblMsgNoMin.Text)
            txtStockMinimo.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(txtStockOptimo.Text.Trim) Then
            MsgBox(lblMsgNoOpt.Text)
            txtStockOptimo.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmProducto_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            If Me.isNew Then
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Dar de alta un producto")
            Else
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Modificar un producto")
            End If
        End If
    End Sub

    Private Sub txtStockMinimo_TextChanged(sender As Object, e As EventArgs) Handles txtStockMinimo.TextChanged
        Dim digitsOnly As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("[^\d]")
        DirectCast(sender, TextBox).Text = digitsOnly.Replace(DirectCast(sender, TextBox).Text, "")

    End Sub

    Private Sub txtStockOptimo_TextChanged(sender As Object, e As EventArgs) Handles txtStockOptimo.TextChanged
        Dim digitsOnly As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("[^\d]")
        DirectCast(sender, TextBox).Text = digitsOnly.Replace(DirectCast(sender, TextBox).Text, "")

    End Sub

    Private Sub txtStockMinimo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStockMinimo.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If

    End Sub

    Private Sub txtStockOptimo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStockOptimo.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If

    End Sub
End Class
