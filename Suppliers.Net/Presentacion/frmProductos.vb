﻿Public Class frmProductos
    'Private Listado As List(Of Producto)

    Private Sub frmProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'MultiIdioma.ConfigurarForm(Me)

        ActualizarGrilla()


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        btnAlta.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Producto")
        btnBaja.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Producto")
        btnModificar.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Producto")
    End Sub

    Private Sub ActualizarGrilla()
        Dim productos = Producto.Listar()

        grd.Rows.Clear()

        For Each f As Producto In productos
            grd.Rows.Add(f.id, f.codigo, f.descripcion, f.inactivo)
        Next
    End Sub

    Private Sub btnAlta_Click(sender As Object, e As EventArgs) Handles btnAlta.Click
        Dim frm As New frmProducto
        frm.LoadProveedores()
        frm.isNew = True
        frm.ShowDialog(Me)
        ActualizarGrilla()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If (grd.SelectedRows.Count = 1) Then
            Dim u = New Producto(CInt(grd.SelectedRows(0).Cells("cId").Value))

            Dim frm As New frmProducto
            frm.LoadProveedores()

            frm.txtId.Text = u.id.ToString
            frm.txtCodigo.Text = u.codigo
            frm.txtDescripcion.Text = u.descripcion
            frm.txtStockMinimo.Text = u.minimo.ToString
            frm.txtStockOptimo.Text = u.optimo.ToString
            frm.cboProveedor.SelectedIndex = frm.cboProveedor.FindStringExact(u.proveedor.razonSocial)
            frm.chkInactivo.Enabled = u.inactivo
            frm.chkInactivo.Checked = u.inactivo

            frm.isNew = False
            frm.ShowDialog(Me)
            ActualizarGrilla()
        End If
    End Sub

    Private Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
        Dim id As Integer

        If (grd.SelectedRows.Count = 1) Then
            id = CInt(grd.SelectedRows(0).Cells("cId").Value)

            If MsgBox(lblMsgConfirmDeletion.Text, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim p = New Producto(id)

                ''me fijo si la familia está en uso
                'If Familia.EnUso(id) Then
                '    MsgBox(lblMsgCantDelete.Text)
                'End If

                'si llego hasta acá, elimino
                Producto.Eliminar(id)

                'registro en bitácora
                Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 1, "Elimina producto: " + p.descripcion)

                'actualizo grilla
                ActualizarGrilla()
            End If
        End If
    End Sub

    Private Sub frmProductos_Enter(sender As Object, e As EventArgs) Handles MyBase.Enter
        ActualizarGrilla()
    End Sub

    Private Sub grd_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grd.CellContentClick

    End Sub

    Private Sub frmProductos_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Productos")
        End If
    End Sub
End Class
