﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.lblRazonSocial = New System.Windows.Forms.Label()
        Me.lblTelefono = New System.Windows.Forms.Label()
        Me.txtTelefono = New System.Windows.Forms.TextBox()
        Me.chkInactivo = New System.Windows.Forms.CheckBox()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.lblMsgNoName = New System.Windows.Forms.Label()
        Me.lblMsgNoPhone = New System.Windows.Forms.Label()
        Me.lblMsgNameDuplicated = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Location = New System.Drawing.Point(104, 12)
        Me.txtRazonSocial.MaxLength = 50
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(227, 20)
        Me.txtRazonSocial.TabIndex = 0
        Me.txtRazonSocial.Tag = "Razón social del proveedor"
        '
        'lblRazonSocial
        '
        Me.lblRazonSocial.AutoSize = True
        Me.lblRazonSocial.Location = New System.Drawing.Point(12, 15)
        Me.lblRazonSocial.Name = "lblRazonSocial"
        Me.lblRazonSocial.Size = New System.Drawing.Size(70, 13)
        Me.lblRazonSocial.TabIndex = 1
        Me.lblRazonSocial.Text = "Razón Social"
        '
        'lblTelefono
        '
        Me.lblTelefono.AutoSize = True
        Me.lblTelefono.Location = New System.Drawing.Point(12, 41)
        Me.lblTelefono.Name = "lblTelefono"
        Me.lblTelefono.Size = New System.Drawing.Size(49, 13)
        Me.lblTelefono.TabIndex = 3
        Me.lblTelefono.Text = "Teléfono"
        '
        'txtTelefono
        '
        Me.txtTelefono.Location = New System.Drawing.Point(104, 38)
        Me.txtTelefono.MaxLength = 20
        Me.txtTelefono.Name = "txtTelefono"
        Me.txtTelefono.Size = New System.Drawing.Size(111, 20)
        Me.txtTelefono.TabIndex = 2
        Me.txtTelefono.Tag = "teléfono de contacto"
        '
        'chkInactivo
        '
        Me.chkInactivo.AutoSize = True
        Me.chkInactivo.Location = New System.Drawing.Point(104, 64)
        Me.chkInactivo.Name = "chkInactivo"
        Me.chkInactivo.Size = New System.Drawing.Size(64, 17)
        Me.chkInactivo.TabIndex = 4
        Me.chkInactivo.Tag = "Proveedor inactivo"
        Me.chkInactivo.Text = "Inactivo"
        Me.chkInactivo.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(256, 88)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 5
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(175, 88)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 6
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'txtId
        '
        Me.txtId.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtId.Location = New System.Drawing.Point(12, 186)
        Me.txtId.MaxLength = 50
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(61, 20)
        Me.txtId.TabIndex = 7
        Me.txtId.Text = "0"
        Me.txtId.Visible = False
        '
        'lblMsgNoName
        '
        Me.lblMsgNoName.AutoSize = True
        Me.lblMsgNoName.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoName.Location = New System.Drawing.Point(12, 209)
        Me.lblMsgNoName.Name = "lblMsgNoName"
        Me.lblMsgNoName.Size = New System.Drawing.Size(147, 13)
        Me.lblMsgNoName.TabIndex = 63
        Me.lblMsgNoName.Text = "Debe indicar una razon social"
        Me.lblMsgNoName.Visible = False
        '
        'lblMsgNoPhone
        '
        Me.lblMsgNoPhone.AutoSize = True
        Me.lblMsgNoPhone.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoPhone.Location = New System.Drawing.Point(12, 222)
        Me.lblMsgNoPhone.Name = "lblMsgNoPhone"
        Me.lblMsgNoPhone.Size = New System.Drawing.Size(123, 13)
        Me.lblMsgNoPhone.TabIndex = 64
        Me.lblMsgNoPhone.Text = "Debe indicar un telefono"
        Me.lblMsgNoPhone.Visible = False
        '
        'lblMsgNameDuplicated
        '
        Me.lblMsgNameDuplicated.AutoSize = True
        Me.lblMsgNameDuplicated.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNameDuplicated.Location = New System.Drawing.Point(12, 235)
        Me.lblMsgNameDuplicated.Name = "lblMsgNameDuplicated"
        Me.lblMsgNameDuplicated.Size = New System.Drawing.Size(246, 13)
        Me.lblMsgNameDuplicated.TabIndex = 65
        Me.lblMsgNameDuplicated.Text = "Esa razon social ya está en uso por otro proveedor"
        Me.lblMsgNameDuplicated.Visible = False
        '
        'frmProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(343, 122)
        Me.Controls.Add(Me.lblMsgNameDuplicated)
        Me.Controls.Add(Me.lblMsgNoPhone)
        Me.Controls.Add(Me.lblMsgNoName)
        Me.Controls.Add(Me.txtId)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.chkInactivo)
        Me.Controls.Add(Me.lblTelefono)
        Me.Controls.Add(Me.txtTelefono)
        Me.Controls.Add(Me.lblRazonSocial)
        Me.Controls.Add(Me.txtRazonSocial)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProveedor"
        Me.Text = "Proveedor"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents lblRazonSocial As System.Windows.Forms.Label
    Friend WithEvents lblTelefono As System.Windows.Forms.Label
    Friend WithEvents txtTelefono As System.Windows.Forms.TextBox
    Friend WithEvents chkInactivo As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents lblMsgNoName As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoPhone As System.Windows.Forms.Label
    Friend WithEvents lblMsgNameDuplicated As System.Windows.Forms.Label

End Class
