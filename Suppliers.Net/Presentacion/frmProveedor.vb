﻿Public Class frmProveedor
    Public isNew As Boolean = False

    Private Sub frmProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Function ValidateInput() As Boolean
        If String.IsNullOrEmpty(txtRazonSocial.Text.Trim) Then
            MsgBox(lblMsgNoName.Text)
            txtRazonSocial.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(txtTelefono.Text.Trim) Then
            MsgBox(lblMsgNoPhone.Text)
            txtTelefono.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If Not ValidateInput() Then
            Return
        End If

        Dim u As Proveedor

        'valido datos duplicados
        Dim familias = Proveedor.Listar()
        For Each usr As Proveedor In familias
            If Me.isNew Or usr.id <> CInt(txtId.Text) Then
                If (usr.razonSocial.Trim = txtRazonSocial.Text.Trim) Then
                    MsgBox(lblMsgNameDuplicated.Text)
                    txtRazonSocial.Focus()
                    Return
                End If
            End If
        Next

        If Me.isNew Then
            u = New Proveedor()
            u.razonSocial = txtRazonSocial.Text
            u.telefono = txtTelefono.Text
            u.inactivo = chkInactivo.Checked
            u.Agregar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Alta de proveedor: " + txtRazonSocial.Text)
        Else
            u = New Proveedor(CInt(txtId.Text))
            u.telefono = txtTelefono.Text
            u.razonSocial = txtRazonSocial.Text
            u.inactivo = chkInactivo.Checked
            u.Modificar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Modifica proveedor: " + txtRazonSocial.Text)
        End If

        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub frmProveedor_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            If Me.isNew Then
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Dar de alta un proveedor")
            Else
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Modificar un proveedor")
            End If
        End If
    End Sub
End Class
