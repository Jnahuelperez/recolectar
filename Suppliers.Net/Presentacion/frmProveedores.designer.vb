﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProveedores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grd = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cRazonSocial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cTelefono = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cInactivo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnAlta = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnBaja = New System.Windows.Forms.Button()
        Me.lblMsgConfirmDeletion = New System.Windows.Forms.Label()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grd
        '
        Me.grd.AllowUserToAddRows = False
        Me.grd.AllowUserToDeleteRows = False
        Me.grd.AllowUserToResizeColumns = False
        Me.grd.AllowUserToResizeRows = False
        Me.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cRazonSocial, Me.cTelefono, Me.cInactivo})
        Me.grd.Location = New System.Drawing.Point(2, 2)
        Me.grd.MultiSelect = False
        Me.grd.Name = "grd"
        Me.grd.ReadOnly = True
        Me.grd.RowHeadersVisible = False
        Me.grd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grd.Size = New System.Drawing.Size(409, 235)
        Me.grd.TabIndex = 0
        '
        'cId
        '
        Me.cId.HeaderText = "Id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cRazonSocial
        '
        Me.cRazonSocial.HeaderText = "Razón Social"
        Me.cRazonSocial.Name = "cRazonSocial"
        Me.cRazonSocial.ReadOnly = True
        Me.cRazonSocial.Width = 200
        '
        'cTelefono
        '
        Me.cTelefono.HeaderText = "Teléfono"
        Me.cTelefono.Name = "cTelefono"
        Me.cTelefono.ReadOnly = True
        '
        'cInactivo
        '
        Me.cInactivo.HeaderText = "Inactivo"
        Me.cInactivo.Name = "cInactivo"
        Me.cInactivo.ReadOnly = True
        '
        'btnAlta
        '
        Me.btnAlta.Location = New System.Drawing.Point(32, 259)
        Me.btnAlta.Name = "btnAlta"
        Me.btnAlta.Size = New System.Drawing.Size(88, 23)
        Me.btnAlta.TabIndex = 1
        Me.btnAlta.Tag = "Nuevo proveedor"
        Me.btnAlta.Text = "Alta"
        Me.btnAlta.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(175, 259)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 3
        Me.btnModificar.Tag = "Modificar Proveedor"
        Me.btnModificar.Text = "Modificacion"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnBaja
        '
        Me.btnBaja.Location = New System.Drawing.Point(305, 259)
        Me.btnBaja.Name = "btnBaja"
        Me.btnBaja.Size = New System.Drawing.Size(75, 23)
        Me.btnBaja.TabIndex = 4
        Me.btnBaja.Tag = "Baja de proveedor"
        Me.btnBaja.Text = "Baja"
        Me.btnBaja.UseVisualStyleBackColor = True
        '
        'lblMsgConfirmDeletion
        '
        Me.lblMsgConfirmDeletion.AutoSize = True
        Me.lblMsgConfirmDeletion.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgConfirmDeletion.Location = New System.Drawing.Point(12, 354)
        Me.lblMsgConfirmDeletion.Name = "lblMsgConfirmDeletion"
        Me.lblMsgConfirmDeletion.Size = New System.Drawing.Size(216, 13)
        Me.lblMsgConfirmDeletion.TabIndex = 62
        Me.lblMsgConfirmDeletion.Text = "¿Desea eliminar el proveedor seleccionado?"
        Me.lblMsgConfirmDeletion.Visible = False
        '
        'frmProveedores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(412, 298)
        Me.Controls.Add(Me.lblMsgConfirmDeletion)
        Me.Controls.Add(Me.btnBaja)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAlta)
        Me.Controls.Add(Me.grd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmProveedores"
        Me.Text = "Proveedores"
        CType(Me.grd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grd As System.Windows.Forms.DataGridView
    Friend WithEvents btnAlta As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnBaja As System.Windows.Forms.Button
    Friend WithEvents lblMsgConfirmDeletion As System.Windows.Forms.Label
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cRazonSocial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cTelefono As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cInactivo As System.Windows.Forms.DataGridViewCheckBoxColumn

End Class
