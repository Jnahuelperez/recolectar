﻿Public Class frmProveedores
    'Private Listado As List(Of Proveedor)

    Private Sub frmProveedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ActualizarGrilla()


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        btnAlta.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Proveedor")
        btnBaja.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Proveedor")
        btnModificar.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Proveedor")
    End Sub

    Private Sub ActualizarGrilla()
        Dim Proveedores = Proveedor.Listar()

        grd.Rows.Clear()

        For Each f As Proveedor In Proveedores
            grd.Rows.Add(f.id, f.razonSocial, f.telefono, f.inactivo)
        Next
    End Sub

    Private Sub btnAlta_Click(sender As Object, e As EventArgs) Handles btnAlta.Click
        Dim frm As New frmProveedor
        frm.isNew = True
        frm.ShowDialog(Me)
        ActualizarGrilla()
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If (grd.SelectedRows.Count = 1) Then
            Dim u = New Proveedor(CInt(grd.SelectedRows(0).Cells("cId").Value))

            Dim frm As New frmProveedor
            frm.txtId.Text = u.id.ToString
            frm.txtRazonSocial.Text = u.razonSocial
            frm.txtTelefono.Text = u.telefono
            frm.chkInactivo.Enabled = u.inactivo
            frm.chkInactivo.Checked = u.inactivo

            frm.isNew = False
            frm.ShowDialog(Me)
            ActualizarGrilla()
        End If
    End Sub

    Private Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
        Dim id As Integer

        If (grd.SelectedRows.Count = 1) Then
            id = CInt(grd.SelectedRows(0).Cells("cId").Value)

            If MsgBox(lblMsgConfirmDeletion.Text, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim u = New Proveedor(id)

                'me fijo si la familia está en uso
                'If Familia.EnUso(id) Then
                '    'MsgBox(lblMsgCantDelete.Text)
                'End If

                'si llego hasta acá, elimino
                Proveedor.Eliminar(id)

                'registro en bitácora
                Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 1, "Elimina proveedor: " + u.razonSocial)

                'actualizo grilla
                ActualizarGrilla()
            End If
        End If
    End Sub

    Private Sub frmProveedores_Enter(sender As Object, e As EventArgs) Handles MyBase.Enter
        'ActualizarGrilla()
    End Sub

    Private Sub grd_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grd.CellContentClick

    End Sub

    Private Sub frmProveedores_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Proveedores")
        End If
    End Sub
End Class
