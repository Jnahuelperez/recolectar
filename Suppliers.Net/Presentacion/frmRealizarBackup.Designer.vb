﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRealizarBackup
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSeleccioneCarpeta = New System.Windows.Forms.Label()
        Me.lblTamañoMaximo = New System.Windows.Forms.Label()
        Me.chkMultivolumen = New System.Windows.Forms.CheckBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.txtFolder = New System.Windows.Forms.TextBox()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.lblMsgBackupOK = New System.Windows.Forms.Label()
        Me.cboFileSize = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'lblSeleccioneCarpeta
        '
        Me.lblSeleccioneCarpeta.AutoSize = True
        Me.lblSeleccioneCarpeta.Location = New System.Drawing.Point(13, 13)
        Me.lblSeleccioneCarpeta.Name = "lblSeleccioneCarpeta"
        Me.lblSeleccioneCarpeta.Size = New System.Drawing.Size(153, 13)
        Me.lblSeleccioneCarpeta.TabIndex = 0
        Me.lblSeleccioneCarpeta.Text = "Seleccione destino del backup"
        '
        'lblTamañoMaximo
        '
        Me.lblTamañoMaximo.AutoSize = True
        Me.lblTamañoMaximo.Location = New System.Drawing.Point(13, 106)
        Me.lblTamañoMaximo.Name = "lblTamañoMaximo"
        Me.lblTamañoMaximo.Size = New System.Drawing.Size(162, 13)
        Me.lblTamañoMaximo.TabIndex = 2
        Me.lblTamañoMaximo.Text = "Tamaño máximo del archivo (KB)"
        '
        'chkMultivolumen
        '
        Me.chkMultivolumen.AutoSize = True
        Me.chkMultivolumen.Location = New System.Drawing.Point(16, 71)
        Me.chkMultivolumen.Name = "chkMultivolumen"
        Me.chkMultivolumen.Size = New System.Drawing.Size(88, 17)
        Me.chkMultivolumen.TabIndex = 3
        Me.chkMultivolumen.Tag = "Generar backup en mútiples volumenes"
        Me.chkMultivolumen.Text = "Multivolumen"
        Me.chkMultivolumen.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(33, 181)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Location = New System.Drawing.Point(136, 181)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(75, 23)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'txtFolder
        '
        Me.txtFolder.Location = New System.Drawing.Point(16, 29)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtFolder.Size = New System.Drawing.Size(175, 20)
        Me.txtFolder.TabIndex = 7
        Me.txtFolder.Tag = "Destino del backup"
        '
        'btnBrowse
        '
        Me.btnBrowse.Location = New System.Drawing.Point(197, 29)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(35, 20)
        Me.btnBrowse.TabIndex = 8
        Me.btnBrowse.Tag = "Explorar sistema de archivos"
        Me.btnBrowse.Text = "..."
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'lblMsgBackupOK
        '
        Me.lblMsgBackupOK.AutoSize = True
        Me.lblMsgBackupOK.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgBackupOK.Location = New System.Drawing.Point(12, 219)
        Me.lblMsgBackupOK.Name = "lblMsgBackupOK"
        Me.lblMsgBackupOK.Size = New System.Drawing.Size(89, 13)
        Me.lblMsgBackupOK.TabIndex = 59
        Me.lblMsgBackupOK.Text = "Backup realizado"
        Me.lblMsgBackupOK.Visible = False
        '
        'cboFileSize
        '
        Me.cboFileSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFileSize.Enabled = False
        Me.cboFileSize.FormattingEnabled = True
        Me.cboFileSize.Items.AddRange(New Object() {"64", "128", "256", "512", "1024", "2048", "4096"})
        Me.cboFileSize.Location = New System.Drawing.Point(16, 122)
        Me.cboFileSize.Name = "cboFileSize"
        Me.cboFileSize.Size = New System.Drawing.Size(216, 21)
        Me.cboFileSize.TabIndex = 60
        Me.cboFileSize.Tag = "Tamaño de cada volumen"
        '
        'frmRealizarBackup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(246, 297)
        Me.Controls.Add(Me.cboFileSize)
        Me.Controls.Add(Me.lblMsgBackupOK)
        Me.Controls.Add(Me.btnBrowse)
        Me.Controls.Add(Me.txtFolder)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.chkMultivolumen)
        Me.Controls.Add(Me.lblTamañoMaximo)
        Me.Controls.Add(Me.lblSeleccioneCarpeta)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRealizarBackup"
        Me.Text = "Realizar Backup"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblSeleccioneCarpeta As System.Windows.Forms.Label
    Friend WithEvents lblTamañoMaximo As System.Windows.Forms.Label
    Friend WithEvents chkMultivolumen As System.Windows.Forms.CheckBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents lblMsgBackupOK As System.Windows.Forms.Label
    Friend WithEvents cboFileSize As System.Windows.Forms.ComboBox
End Class
