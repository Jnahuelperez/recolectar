﻿
Public Class frmRealizarBackup

    Private Sub frmRealizarBackup_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next


    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        If (FolderBrowserDialog1.ShowDialog() = DialogResult.OK) Then
            txtFolder.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        Dim fileSize As Integer
        Integer.TryParse(cboFileSize.Text, fileSize)

        If Seguridad.RealizarBackup(txtFolder.Text, chkMultivolumen.Checked, fileSize) Then
            MsgBox(lblMsgBackupOK.Text)
        End If
    End Sub

    Private Sub chkMultivolumen_CheckedChanged(sender As Object, e As EventArgs) Handles chkMultivolumen.CheckedChanged
        cboFileSize.Enabled = chkMultivolumen.Checked
    End Sub

    Private Sub frmRealizarBackup_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Realizar Backup")
        End If
    End Sub

    Private Sub cboFileSize_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFileSize.SelectedIndexChanged

    End Sub
End Class