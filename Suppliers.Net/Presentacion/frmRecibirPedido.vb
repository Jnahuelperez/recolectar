﻿Public Class frmRecibirPedido
    Private p As Pedido
    Private Sub frmRecibirPedido_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim id As Integer
        If (Integer.TryParse(txtPedido.Text, id)) Then
            p = New Pedido(id)
            If (p Is Nothing) Then
                txtPedido.Text = ""
                Beep()
            Else
                If p.usuarioReceptor Is Nothing Then
                    txtProveedor.Text = p.proveedor.razonSocial
                    grd.Rows.Clear()
                    If Not p.productos Is Nothing Then
                        For Each f As Producto In p.productos
                            grd.Rows.Add(f.id, f.descripcion, f.cantidadPedida, True)
                        Next
                    End If
                Else
                    txtPedido.Text = ""
                    Beep()
                End If
            End If
        Else
            txtPedido.Text = ""
            Beep()
        End If
    End Sub

    Private Sub btnRecibir_Click(sender As Object, e As EventArgs) Handles btnRecibir.Click
        p.fechaRecepcion = DateTime.Now
        p.usuarioReceptor = Seguridad.currentUser
        p.Modificar()

        For Each r As DataGridViewRow In grd.Rows
            If (r.Cells("cRecibido").Value) Then
                Dim m As New Movimiento
                m.producto = New Producto(CInt(r.Cells("cId").Value))
                m.cantidad = CInt(r.Cells("cCantidad").Value)
                m.usuario = Seguridad.currentUser
                m.tipoMovimientoId = 1
                m.Agregar()
            End If
        Next

        Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Recepción de pedido: " + p.id.ToString)
    End Sub

    Private Sub frmRecibirPedido_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Recibir Pedidos")
        End If
    End Sub

    Private Sub txtPedido_TextChanged(sender As Object, e As EventArgs) Handles txtPedido.TextChanged
        Dim digitsOnly As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("[^\d]")
        DirectCast(sender, TextBox).Text = digitsOnly.Replace(DirectCast(sender, TextBox).Text, "")
    End Sub

    Private Sub txtPedido_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPedido.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

End Class