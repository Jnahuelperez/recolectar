﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRecuperarPass
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblUser = New System.Windows.Forms.Label()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.btnRecuperar = New System.Windows.Forms.Button()
        Me.lblMsgWrongUser = New System.Windows.Forms.Label()
        Me.lblMsgNoUser = New System.Windows.Forms.Label()
        Me.lblMsgOK = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Location = New System.Drawing.Point(30, 25)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(32, 13)
        Me.lblUser.TabIndex = 0
        Me.lblUser.Text = "User:"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(90, 22)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(100, 20)
        Me.txtUser.TabIndex = 1
        Me.txtUser.Tag = "Nombre de usuario utilizado para login"
        '
        'btnRecuperar
        '
        Me.btnRecuperar.Location = New System.Drawing.Point(43, 67)
        Me.btnRecuperar.Name = "btnRecuperar"
        Me.btnRecuperar.Size = New System.Drawing.Size(131, 23)
        Me.btnRecuperar.TabIndex = 2
        Me.btnRecuperar.Text = "Recuperar Contraseña"
        Me.btnRecuperar.UseVisualStyleBackColor = True
        '
        'lblMsgWrongUser
        '
        Me.lblMsgWrongUser.AutoSize = True
        Me.lblMsgWrongUser.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgWrongUser.Location = New System.Drawing.Point(10, 114)
        Me.lblMsgWrongUser.Name = "lblMsgWrongUser"
        Me.lblMsgWrongUser.Size = New System.Drawing.Size(147, 13)
        Me.lblMsgWrongUser.TabIndex = 59
        Me.lblMsgWrongUser.Text = "El usuario ingresado no existe"
        Me.lblMsgWrongUser.Visible = False
        '
        'lblMsgNoUser
        '
        Me.lblMsgNoUser.AutoSize = True
        Me.lblMsgNoUser.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoUser.Location = New System.Drawing.Point(12, 127)
        Me.lblMsgNoUser.Name = "lblMsgNoUser"
        Me.lblMsgNoUser.Size = New System.Drawing.Size(140, 13)
        Me.lblMsgNoUser.TabIndex = 61
        Me.lblMsgNoUser.Text = "Debe ingresar un user name"
        Me.lblMsgNoUser.Visible = False
        '
        'lblMsgOK
        '
        Me.lblMsgOK.AutoSize = True
        Me.lblMsgOK.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgOK.Location = New System.Drawing.Point(17, 140)
        Me.lblMsgOK.Name = "lblMsgOK"
        Me.lblMsgOK.Size = New System.Drawing.Size(238, 13)
        Me.lblMsgOK.TabIndex = 62
        Me.lblMsgOK.Text = "Se envió la nueva contraseña al mail del usuario."
        Me.lblMsgOK.Visible = False
        '
        'frmRecuperarPass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(215, 109)
        Me.Controls.Add(Me.lblMsgOK)
        Me.Controls.Add(Me.lblMsgNoUser)
        Me.Controls.Add(Me.lblMsgWrongUser)
        Me.Controls.Add(Me.btnRecuperar)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.lblUser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmRecuperarPass"
        Me.Text = "Recuperar Contraseña"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents btnRecuperar As System.Windows.Forms.Button
    Friend WithEvents lblMsgWrongUser As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoUser As System.Windows.Forms.Label
    Friend WithEvents lblMsgOK As System.Windows.Forms.Label
End Class
