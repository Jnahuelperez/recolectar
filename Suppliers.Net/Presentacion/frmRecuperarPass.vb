﻿Public Class frmRecuperarPass

    Private Sub frmRecuperarPass_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub


    Private Function ValidateInput() As Boolean
        If String.IsNullOrEmpty(txtUser.Text.Trim) Then
            MsgBox(lblMsgNoUser.Text)
            txtUser.Focus()
            Return False
        End If

        If Not Usuario.UserExists(txtUser.Text.Trim) Then
            MsgBox(lblMsgWrongUser.Text)
            txtUser.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub btnRecuperar_Click(sender As Object, e As EventArgs) Handles btnRecuperar.Click
        If Not ValidateInput() Then
            Return
        End If

        Usuario.RecuperarContraseña(txtUser.Text.Trim)

        MsgBox(lblMsgOK.Text)

        txtUser.Clear()
    End Sub

    Private Sub frmRecuperarPass_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Recuperar contraseña")
        End If
    End Sub

    Private Sub txtUser_TextChanged(sender As Object, e As EventArgs) Handles txtUser.TextChanged

    End Sub
End Class