﻿Public Class frmSeleccionarIdioma

    Private Sub frmSeleccionarIdioma_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        Dim sourceIdiomas As New Dictionary(Of Integer, String)()
        Dim idiomas = MultiIdioma.GetIdiomas
        For Each dr As DataRow In idiomas.Tables(0).Rows
            sourceIdiomas.Add(CInt(dr.Item("id")), dr.Item("descripcion").ToString)
        Next

        cboIdiomas.DataSource = New BindingSource(sourceIdiomas, Nothing)
        cboIdiomas.DisplayMember = "Value"
        cboIdiomas.ValueMember = "Key"
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If cboIdiomas.SelectedValue() > 0 Then
            Seguridad.currentUser.CambiarIdioma(cboIdiomas.SelectedValue())

            For Each frm As Form In Application.OpenForms
                MultiIdioma.ConfigurarForm(frm)
            Next

            Me.Close()
        Else
            Beep()
        End If
    End Sub

    Private Sub frmSeleccionarIdioma_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Cambiar idioma")
        End If
    End Sub
End Class