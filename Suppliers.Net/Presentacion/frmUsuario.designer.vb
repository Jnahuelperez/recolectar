﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.txtDescuento1 = New System.Windows.Forms.TextBox()
        Me.grpFamlias = New System.Windows.Forms.GroupBox()
        Me.lblFamila = New System.Windows.Forms.Label()
        Me.cboFamilia = New System.Windows.Forms.ComboBox()
        Me.btnFamiliaQuitar = New System.Windows.Forms.Button()
        Me.btnFamiliaAgregar = New System.Windows.Forms.Button()
        Me.grdFamilia = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEstado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.lblEmail = New System.Windows.Forms.Label()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkInactivo = New System.Windows.Forms.CheckBox()
        Me.grpPatentes = New System.Windows.Forms.GroupBox()
        Me.lblPatente = New System.Windows.Forms.Label()
        Me.cboPatente = New System.Windows.Forms.ComboBox()
        Me.btnPatenteQuitar = New System.Windows.Forms.Button()
        Me.btnPatenteAgregar = New System.Windows.Forms.Button()
        Me.grdPatente = New System.Windows.Forms.DataGridView()
        Me.cIdP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombreP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cHabilitadoP = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cEstadoP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblMsgUserDuplicated = New System.Windows.Forms.Label()
        Me.lblMsgNoUser = New System.Windows.Forms.Label()
        Me.lblMsgNoEmail = New System.Windows.Forms.Label()
        Me.lblMsgNoName = New System.Windows.Forms.Label()
        Me.lblMsgEmailDuplicated = New System.Windows.Forms.Label()
        Me.lblMsgWrongEmailFormat = New System.Windows.Forms.Label()
        Me.lblMsgCantDelete = New System.Windows.Forms.Label()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.grpFamlias.SuspendLayout()
        CType(Me.grdFamilia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPatentes.SuspendLayout()
        CType(Me.grdPatente, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtId
        '
        Me.txtId.Enabled = False
        Me.txtId.Location = New System.Drawing.Point(38, 392)
        Me.txtId.Name = "txtId"
        Me.txtId.Size = New System.Drawing.Size(36, 20)
        Me.txtId.TabIndex = 0
        Me.txtId.Text = "0"
        Me.txtId.Visible = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 395)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(15, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "id"
        Me.Label1.Visible = False
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(123, 317)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(75, 23)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Tag = "Aceptar"
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(275, 317)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelar.TabIndex = 6
        Me.btnCancelar.Tag = "Cancelar"
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'txtDescuento1
        '
        Me.txtDescuento1.Location = New System.Drawing.Point(102, 64)
        Me.txtDescuento1.Name = "txtDescuento1"
        Me.txtDescuento1.Size = New System.Drawing.Size(242, 20)
        Me.txtDescuento1.TabIndex = 4
        '
        'grpFamlias
        '
        Me.grpFamlias.Controls.Add(Me.lblFamila)
        Me.grpFamlias.Controls.Add(Me.cboFamilia)
        Me.grpFamlias.Controls.Add(Me.btnFamiliaQuitar)
        Me.grpFamlias.Controls.Add(Me.btnFamiliaAgregar)
        Me.grpFamlias.Controls.Add(Me.grdFamilia)
        Me.grpFamlias.Location = New System.Drawing.Point(15, 107)
        Me.grpFamlias.Name = "grpFamlias"
        Me.grpFamlias.Size = New System.Drawing.Size(199, 192)
        Me.grpFamlias.TabIndex = 47
        Me.grpFamlias.TabStop = False
        Me.grpFamlias.Text = "Familias"
        '
        'lblFamila
        '
        Me.lblFamila.AutoSize = True
        Me.lblFamila.Location = New System.Drawing.Point(6, 22)
        Me.lblFamila.Name = "lblFamila"
        Me.lblFamila.Size = New System.Drawing.Size(39, 13)
        Me.lblFamila.TabIndex = 4
        Me.lblFamila.Text = "Familia"
        '
        'cboFamilia
        '
        Me.cboFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFamilia.FormattingEnabled = True
        Me.cboFamilia.Location = New System.Drawing.Point(51, 19)
        Me.cboFamilia.Name = "cboFamilia"
        Me.cboFamilia.Size = New System.Drawing.Size(143, 21)
        Me.cboFamilia.TabIndex = 0
        Me.cboFamilia.Tag = "Seleccione familia"
        '
        'btnFamiliaQuitar
        '
        Me.btnFamiliaQuitar.Location = New System.Drawing.Point(119, 162)
        Me.btnFamiliaQuitar.Name = "btnFamiliaQuitar"
        Me.btnFamiliaQuitar.Size = New System.Drawing.Size(75, 23)
        Me.btnFamiliaQuitar.TabIndex = 3
        Me.btnFamiliaQuitar.Tag = "Quitar Familia"
        Me.btnFamiliaQuitar.Text = "Quitar"
        Me.btnFamiliaQuitar.UseVisualStyleBackColor = True
        '
        'btnFamiliaAgregar
        '
        Me.btnFamiliaAgregar.Location = New System.Drawing.Point(6, 162)
        Me.btnFamiliaAgregar.Name = "btnFamiliaAgregar"
        Me.btnFamiliaAgregar.Size = New System.Drawing.Size(75, 23)
        Me.btnFamiliaAgregar.TabIndex = 2
        Me.btnFamiliaAgregar.Tag = "Agregar Familia"
        Me.btnFamiliaAgregar.Text = "Agregar"
        Me.btnFamiliaAgregar.UseVisualStyleBackColor = True
        '
        'grdFamilia
        '
        Me.grdFamilia.AllowUserToAddRows = False
        Me.grdFamilia.AllowUserToDeleteRows = False
        Me.grdFamilia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFamilia.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cNombre, Me.cEstado})
        Me.grdFamilia.Location = New System.Drawing.Point(5, 46)
        Me.grdFamilia.Name = "grdFamilia"
        Me.grdFamilia.ReadOnly = True
        Me.grdFamilia.RowHeadersVisible = False
        Me.grdFamilia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdFamilia.Size = New System.Drawing.Size(188, 110)
        Me.grdFamilia.TabIndex = 1
        '
        'cId
        '
        Me.cId.HeaderText = "id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cNombre
        '
        Me.cNombre.HeaderText = "Nombre Familia"
        Me.cNombre.Name = "cNombre"
        Me.cNombre.ReadOnly = True
        Me.cNombre.Width = 180
        '
        'cEstado
        '
        Me.cEstado.HeaderText = "Estado"
        Me.cEstado.Name = "cEstado"
        Me.cEstado.ReadOnly = True
        Me.cEstado.Visible = False
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Location = New System.Drawing.Point(12, 9)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(44, 13)
        Me.lblNombre.TabIndex = 49
        Me.lblNombre.Text = "Nombre"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(106, 6)
        Me.txtNombre.MaxLength = 50
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(212, 20)
        Me.txtNombre.TabIndex = 50
        Me.txtNombre.Tag = "Nombre del usuario"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(106, 32)
        Me.txtEmail.MaxLength = 50
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(212, 20)
        Me.txtEmail.TabIndex = 52
        Me.txtEmail.Tag = "Email de usuario"
        '
        'lblEmail
        '
        Me.lblEmail.AutoSize = True
        Me.lblEmail.Location = New System.Drawing.Point(12, 35)
        Me.lblEmail.Name = "lblEmail"
        Me.lblEmail.Size = New System.Drawing.Size(32, 13)
        Me.lblEmail.TabIndex = 51
        Me.lblEmail.Text = "Email"
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(106, 58)
        Me.txtUser.MaxLength = 8
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(98, 20)
        Me.txtUser.TabIndex = 54
        Me.txtUser.Tag = "User"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 53
        Me.Label3.Text = "User"
        '
        'chkInactivo
        '
        Me.chkInactivo.AutoSize = True
        Me.chkInactivo.Location = New System.Drawing.Point(106, 84)
        Me.chkInactivo.Name = "chkInactivo"
        Me.chkInactivo.Size = New System.Drawing.Size(64, 17)
        Me.chkInactivo.TabIndex = 57
        Me.chkInactivo.Tag = "Usuario inactivo"
        Me.chkInactivo.Text = "Inactivo"
        Me.chkInactivo.UseVisualStyleBackColor = True
        '
        'grpPatentes
        '
        Me.grpPatentes.Controls.Add(Me.lblPatente)
        Me.grpPatentes.Controls.Add(Me.cboPatente)
        Me.grpPatentes.Controls.Add(Me.btnPatenteQuitar)
        Me.grpPatentes.Controls.Add(Me.btnPatenteAgregar)
        Me.grpPatentes.Controls.Add(Me.grdPatente)
        Me.grpPatentes.Location = New System.Drawing.Point(220, 107)
        Me.grpPatentes.Name = "grpPatentes"
        Me.grpPatentes.Size = New System.Drawing.Size(227, 192)
        Me.grpPatentes.TabIndex = 48
        Me.grpPatentes.TabStop = False
        Me.grpPatentes.Text = "Patentes"
        '
        'lblPatente
        '
        Me.lblPatente.AutoSize = True
        Me.lblPatente.Location = New System.Drawing.Point(6, 22)
        Me.lblPatente.Name = "lblPatente"
        Me.lblPatente.Size = New System.Drawing.Size(44, 13)
        Me.lblPatente.TabIndex = 4
        Me.lblPatente.Text = "Patente"
        '
        'cboPatente
        '
        Me.cboPatente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPatente.FormattingEnabled = True
        Me.cboPatente.Location = New System.Drawing.Point(51, 19)
        Me.cboPatente.Name = "cboPatente"
        Me.cboPatente.Size = New System.Drawing.Size(143, 21)
        Me.cboPatente.TabIndex = 0
        Me.cboPatente.Tag = "Seleccione patente"
        '
        'btnPatenteQuitar
        '
        Me.btnPatenteQuitar.Location = New System.Drawing.Point(145, 162)
        Me.btnPatenteQuitar.Name = "btnPatenteQuitar"
        Me.btnPatenteQuitar.Size = New System.Drawing.Size(75, 23)
        Me.btnPatenteQuitar.TabIndex = 3
        Me.btnPatenteQuitar.Tag = "Quitar Patente"
        Me.btnPatenteQuitar.Text = "Quitar"
        Me.btnPatenteQuitar.UseVisualStyleBackColor = True
        '
        'btnPatenteAgregar
        '
        Me.btnPatenteAgregar.Location = New System.Drawing.Point(6, 162)
        Me.btnPatenteAgregar.Name = "btnPatenteAgregar"
        Me.btnPatenteAgregar.Size = New System.Drawing.Size(75, 23)
        Me.btnPatenteAgregar.TabIndex = 2
        Me.btnPatenteAgregar.Tag = "Agregar patente"
        Me.btnPatenteAgregar.Text = "Agregar"
        Me.btnPatenteAgregar.UseVisualStyleBackColor = True
        '
        'grdPatente
        '
        Me.grdPatente.AllowUserToAddRows = False
        Me.grdPatente.AllowUserToDeleteRows = False
        Me.grdPatente.AllowUserToResizeRows = False
        Me.grdPatente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdPatente.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cIdP, Me.cNombreP, Me.cHabilitadoP, Me.cEstadoP})
        Me.grdPatente.Location = New System.Drawing.Point(6, 46)
        Me.grdPatente.MultiSelect = False
        Me.grdPatente.Name = "grdPatente"
        Me.grdPatente.RowHeadersVisible = False
        Me.grdPatente.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdPatente.Size = New System.Drawing.Size(214, 110)
        Me.grdPatente.TabIndex = 1
        '
        'cIdP
        '
        Me.cIdP.HeaderText = "id"
        Me.cIdP.Name = "cIdP"
        Me.cIdP.ReadOnly = True
        Me.cIdP.Visible = False
        '
        'cNombreP
        '
        Me.cNombreP.HeaderText = "Nombre Patente"
        Me.cNombreP.Name = "cNombreP"
        Me.cNombreP.ReadOnly = True
        Me.cNombreP.Width = 150
        '
        'cHabilitadoP
        '
        Me.cHabilitadoP.HeaderText = "Habilitado"
        Me.cHabilitadoP.Name = "cHabilitadoP"
        Me.cHabilitadoP.Width = 60
        '
        'cEstadoP
        '
        Me.cEstadoP.HeaderText = "Estado"
        Me.cEstadoP.Name = "cEstadoP"
        '
        'lblMsgUserDuplicated
        '
        Me.lblMsgUserDuplicated.AutoSize = True
        Me.lblMsgUserDuplicated.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgUserDuplicated.Location = New System.Drawing.Point(12, 463)
        Me.lblMsgUserDuplicated.Name = "lblMsgUserDuplicated"
        Me.lblMsgUserDuplicated.Size = New System.Drawing.Size(222, 13)
        Me.lblMsgUserDuplicated.TabIndex = 61
        Me.lblMsgUserDuplicated.Text = "Ese username ya está en uso por otro usuario"
        Me.lblMsgUserDuplicated.Visible = False
        '
        'lblMsgNoUser
        '
        Me.lblMsgNoUser.AutoSize = True
        Me.lblMsgNoUser.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoUser.Location = New System.Drawing.Point(12, 450)
        Me.lblMsgNoUser.Name = "lblMsgNoUser"
        Me.lblMsgNoUser.Size = New System.Drawing.Size(140, 13)
        Me.lblMsgNoUser.TabIndex = 60
        Me.lblMsgNoUser.Text = "Debe ingresar un user name"
        Me.lblMsgNoUser.Visible = False
        '
        'lblMsgNoEmail
        '
        Me.lblMsgNoEmail.AutoSize = True
        Me.lblMsgNoEmail.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoEmail.Location = New System.Drawing.Point(12, 437)
        Me.lblMsgNoEmail.Name = "lblMsgNoEmail"
        Me.lblMsgNoEmail.Size = New System.Drawing.Size(115, 13)
        Me.lblMsgNoEmail.TabIndex = 59
        Me.lblMsgNoEmail.Text = "Debe ingresar un email"
        Me.lblMsgNoEmail.Visible = False
        '
        'lblMsgNoName
        '
        Me.lblMsgNoName.AutoSize = True
        Me.lblMsgNoName.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoName.Location = New System.Drawing.Point(12, 424)
        Me.lblMsgNoName.Name = "lblMsgNoName"
        Me.lblMsgNoName.Size = New System.Drawing.Size(198, 13)
        Me.lblMsgNoName.TabIndex = 58
        Me.lblMsgNoName.Text = "Debe ingresar un nombre para el usuario"
        Me.lblMsgNoName.Visible = False
        '
        'lblMsgEmailDuplicated
        '
        Me.lblMsgEmailDuplicated.AutoSize = True
        Me.lblMsgEmailDuplicated.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgEmailDuplicated.Location = New System.Drawing.Point(12, 476)
        Me.lblMsgEmailDuplicated.Name = "lblMsgEmailDuplicated"
        Me.lblMsgEmailDuplicated.Size = New System.Drawing.Size(200, 13)
        Me.lblMsgEmailDuplicated.TabIndex = 62
        Me.lblMsgEmailDuplicated.Text = "Ese email ya está en uso por otro usuario"
        Me.lblMsgEmailDuplicated.Visible = False
        '
        'lblMsgWrongEmailFormat
        '
        Me.lblMsgWrongEmailFormat.AutoSize = True
        Me.lblMsgWrongEmailFormat.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgWrongEmailFormat.Location = New System.Drawing.Point(12, 489)
        Me.lblMsgWrongEmailFormat.Name = "lblMsgWrongEmailFormat"
        Me.lblMsgWrongEmailFormat.Size = New System.Drawing.Size(137, 13)
        Me.lblMsgWrongEmailFormat.TabIndex = 63
        Me.lblMsgWrongEmailFormat.Text = "Formato de email incorrecto"
        Me.lblMsgWrongEmailFormat.Visible = False
        '
        'lblMsgCantDelete
        '
        Me.lblMsgCantDelete.AutoSize = True
        Me.lblMsgCantDelete.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgCantDelete.Location = New System.Drawing.Point(12, 502)
        Me.lblMsgCantDelete.Name = "lblMsgCantDelete"
        Me.lblMsgCantDelete.Size = New System.Drawing.Size(376, 13)
        Me.lblMsgCantDelete.TabIndex = 64
        Me.lblMsgCantDelete.Text = "No se puede guardar patentes/familias porque dejaría patentes sin asignación"
        Me.lblMsgCantDelete.Visible = False
        '
        'HelpProvider1
        '
        Me.HelpProvider1.HelpNamespace = "C:\Users\Hernan\Desktop\Suppliers.chm"
        '
        'frmUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(461, 519)
        Me.Controls.Add(Me.lblMsgCantDelete)
        Me.Controls.Add(Me.lblMsgWrongEmailFormat)
        Me.Controls.Add(Me.lblMsgEmailDuplicated)
        Me.Controls.Add(Me.lblMsgUserDuplicated)
        Me.Controls.Add(Me.lblMsgNoUser)
        Me.Controls.Add(Me.lblMsgNoEmail)
        Me.Controls.Add(Me.lblMsgNoName)
        Me.Controls.Add(Me.grpPatentes)
        Me.Controls.Add(Me.chkInactivo)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.lblEmail)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.grpFamlias)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtId)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.HelpProvider1.SetHelpNavigator(Me, System.Windows.Forms.HelpNavigator.Topic)
        Me.HelpProvider1.SetHelpString(Me, "1")
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUsuario"
        Me.HelpProvider1.SetShowHelp(Me, True)
        Me.Text = "Usuario"
        Me.grpFamlias.ResumeLayout(False)
        Me.grpFamlias.PerformLayout()
        CType(Me.grdFamilia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPatentes.ResumeLayout(False)
        Me.grpPatentes.PerformLayout()
        CType(Me.grdPatente, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtId As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents txtDescuento1 As System.Windows.Forms.TextBox
    Friend WithEvents grpFamlias As System.Windows.Forms.GroupBox
    Friend WithEvents btnFamiliaQuitar As System.Windows.Forms.Button
    Friend WithEvents btnFamiliaAgregar As System.Windows.Forms.Button
    Friend WithEvents grdFamilia As System.Windows.Forms.DataGridView
    Friend WithEvents lblFamila As System.Windows.Forms.Label
    Friend WithEvents cboFamilia As System.Windows.Forms.ComboBox
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents lblEmail As System.Windows.Forms.Label
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkInactivo As System.Windows.Forms.CheckBox
    Friend WithEvents grpPatentes As System.Windows.Forms.GroupBox
    Friend WithEvents lblPatente As System.Windows.Forms.Label
    Friend WithEvents cboPatente As System.Windows.Forms.ComboBox
    Friend WithEvents btnPatenteQuitar As System.Windows.Forms.Button
    Friend WithEvents btnPatenteAgregar As System.Windows.Forms.Button
    Friend WithEvents grdPatente As System.Windows.Forms.DataGridView
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cEstado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblMsgUserDuplicated As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoUser As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoEmail As System.Windows.Forms.Label
    Friend WithEvents lblMsgNoName As System.Windows.Forms.Label
    Friend WithEvents lblMsgEmailDuplicated As System.Windows.Forms.Label
    Friend WithEvents lblMsgWrongEmailFormat As System.Windows.Forms.Label
    Friend WithEvents lblMsgCantDelete As System.Windows.Forms.Label
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents cIdP As DataGridViewTextBoxColumn
    Friend WithEvents cNombreP As DataGridViewTextBoxColumn
    Friend WithEvents cHabilitadoP As DataGridViewCheckBoxColumn
    Friend WithEvents cEstadoP As DataGridViewTextBoxColumn
End Class
