﻿Imports System.Text.RegularExpressions

Public Class frmUsuario
    Public Property isNew As Boolean
    Private familias = New List(Of Familia)

    Private Sub btnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFamiliaAgregar.Click
        Dim exists = False
        For Each r As DataGridViewRow In grdFamilia.Rows
            If (r.Cells("cId").Value = cboFamilia.SelectedValue) Then
                exists = True
                If Not r.Visible Then
                    r.Visible = True
                End If
                Exit For
            End If
        Next

        If Not exists Then
            grdFamilia.Rows.Add(cboFamilia.SelectedValue, cboFamilia.Text, "A")
        End If
    End Sub

    Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFamiliaQuitar.Click
        'armo un listado de patentes de la famlia
        'Dim f = New Familia(CInt(grdFamilia.SelectedRows(0).Cells("cId").Value))

        ''me fijo si eliminar la familia dejaria una patente sin asignacion, cancelo acción
        'For Each p As Patente In f.patentes
        '    If Seguridad.GetCantidadUsuariosConPatente(p.descripcion) = 1 Then
        '        MsgBox(lblMsgCantDelete.Text + ": " + p.descripcion)
        '        Return
        '    End If
        'Next


        If Me.grdFamilia.Rows.Count > 0 AndAlso Me.grdFamilia.SelectedRows.Count = 1 Then
            If (grdFamilia.SelectedRows(0).Cells("cEstado").Value = "A") Then
                grdFamilia.Rows.Remove(grdFamilia.SelectedRows(0))
            Else
                grdFamilia.SelectedRows(0).Cells("cEstado").Value = "B"
                grdFamilia.SelectedRows(0).Visible = False
            End If
        End If
    End Sub


    Private Sub btnAceptar_Click(sender As System.Object, e As System.EventArgs) Handles btnAceptar.Click
        If Not ValidateInput() Then
            Return
        End If

        Dim u As Usuario

        'valido patentes
        Dim patentes = New List(Of Patente)

        'armo un listado de patentes a validar
        For Each r As DataGridViewRow In grdFamilia.Rows
            Select Case r.Cells("cEstado").Value
                Case "B"
                    Dim f = New Familia(CInt(r.Cells("cId").Value))
                    For Each p As Patente In f.patentes
                        patentes.Add(p)
                    Next
            End Select
        Next
        For Each r As DataGridViewRow In grdPatente.Rows
            Select Case r.Cells("cEstadoP").Value
                Case "B"
                    patentes.Add(New Patente(CInt(r.Cells("cIdP").Value)))
                Case "M"
                    If Not (r.Cells("cHabilitadoP").Value) Then
                        patentes.Add(New Patente(CInt(r.Cells("cIdP").Value)))
                    End If
            End Select
        Next
        For Each r As DataGridViewRow In grdFamilia.Rows
            If r.Cells("cEstado").Value <> "B" Then
                Dim f = New Familia(CInt(r.Cells("cId").Value))
                For Each p As Patente In f.patentes
                    If patentes.Count > 0 Then
                        For i As Integer = patentes.Count - 1 To 0 Step -1
                            If patentes(i).id = p.id Then
                                patentes.RemoveAt(i)
                            End If
                        Next
                    End If
                Next
            End If
        Next
        For Each r As DataGridViewRow In grdPatente.Rows
            If r.Cells("cEstadoP").Value <> "B" Then
                If r.Cells("cHabilitadoP").Value Then
                    If patentes.Count > 0 Then
                        For i As Integer = patentes.Count - 1 To 0 Step -1
                            If patentes(i).id = CInt(r.Cells("cIdP").Value) Then
                                patentes.RemoveAt(i)
                            End If
                        Next
                    End If
                End If
            End If
        Next
        For Each r As DataGridViewRow In grdPatente.Rows
            If r.Cells("cEstadoP").Value <> "B" Then
                If Not (r.Cells("cHabilitadoP").Value) Then
                    patentes.Add(New Patente(CInt(r.Cells("cIdP").Value)))
                End If
            End If
        Next

        'me fijo si eliminar el usuario dejaria una patente sin asignacion, cancelo acción
        For Each p As Patente In patentes
            If Seguridad.GetCantidadUsuariosConPatente(p.descripcion) = 1 Then
                MsgBox(lblMsgCantDelete.Text + ": " + p.descripcion)
                Return
            End If
        Next

        'valido datos duplicados
        Dim usuarios = Usuario.Listar()
        For Each usr As Usuario In usuarios
            If Me.isNew Or usr.id <> CInt(txtId.Text) Then
                If (usr.user.Trim = txtUser.Text.Trim) Then
                    MsgBox(lblMsgUserDuplicated.Text)
                    txtUser.Focus()
                    Return
                End If

                If (usr.email.Trim = txtEmail.Text.Trim) Then
                    MsgBox(lblMsgEmailDuplicated.Text)
                    txtEmail.Focus()
                    Return
                End If
            End If
        Next

        If Me.isNew Then
            u = New Usuario()
            u.user = txtUser.Text
            u.password = Seguridad.GenerarContraseña
            u.email = txtEmail.Text
            u.nombre = txtNombre.Text
            u.inactivo = chkInactivo.Checked
            u.Agregar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Alta de usuario: " + txtUser.Text)
        Else
            u = New Usuario(CInt(txtId.Text))
            u.user = txtUser.Text
            u.email = txtEmail.Text
            u.nombre = txtNombre.Text
            u.inactivo = chkInactivo.Checked
            u.Modificar()
            Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 0, "Modifica usuario: " + txtUser.Text)
        End If

        'grabo familias
        For Each r As DataGridViewRow In grdFamilia.Rows
            Select Case r.Cells("cEstado").Value
                Case "A"
                    u.GrabarFamilia(r.Cells("cId").Value)
                Case "B"
                    u.EliminarFamilia(r.Cells("cId").Value)
            End Select
        Next

        'grabo patentes
        For Each r As DataGridViewRow In grdPatente.Rows
            Select Case r.Cells("cEstadoP").Value
                Case "A"
                    u.GrabarPatente(r.Cells("cIdP").Value, r.Cells("cHabilitadoP").Value)
                Case "B"
                    u.EliminarPatente(r.Cells("cIdP").Value)
                Case "M"
                    u.ModificarPatente(r.Cells("cIdP").Value, r.Cells("cHabilitadoP").Value)
            End Select
        Next

        Beep()
        Me.Close()
    End Sub

    Private Sub frmUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        'combo de familias
        Dim sourceFamilias As New Dictionary(Of Integer, String)()
        familias = Familia.Listar()
        For Each f As Familia In familias
            If Not f.inactivo Then
                sourceFamilias.Add(f.id, f.nombre)
            End If
        Next
        cboFamilia.DataSource = New BindingSource(sourceFamilias, Nothing)
        cboFamilia.DisplayMember = "Value"
        If (sourceFamilias.Count > 0) Then
            cboFamilia.ValueMember = "Key"
        End If
        'combo de Patentes
        Dim sourcePatentes As New Dictionary(Of Integer, String)()
        Dim Patentes = Patente.Listar()
        For Each f As Patente In Patentes
            sourcePatentes.Add(f.id, f.descripcion)
        Next
        cboPatente.DataSource = New BindingSource(sourcePatentes, Nothing)
        cboPatente.DisplayMember = "Value"
        cboPatente.ValueMember = "Key"


        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()
    End Sub

    Private Sub btnPatenteAgregar_Click(sender As Object, e As EventArgs) Handles btnPatenteAgregar.Click
        Dim exists = False
        For Each r As DataGridViewRow In grdPatente.Rows
            If (r.Cells("cIdP").Value = cboPatente.SelectedValue) Then
                exists = True
                If Not r.Visible Then
                    r.Visible = True
                    r.Cells("cHabilitadoP").Value = True
                    r.Cells("cEstadoP").Value = "M"
                End If
                Exit For
            End If
        Next

        If Not exists Then
            grdPatente.Rows.Add(cboPatente.SelectedValue, cboPatente.Text, True, "A")
        End If
    End Sub

    Private Sub btnPatenteQuitar_Click(sender As Object, e As EventArgs) Handles btnPatenteQuitar.Click
        If Me.grdPatente.Rows.Count > 0 AndAlso Me.grdPatente.SelectedRows.Count = 1 Then
            If (grdPatente.SelectedRows(0).Cells("cEstadoP").Value = "A") Then
                grdPatente.Rows.Remove(grdPatente.SelectedRows(0))
            Else
                grdPatente.SelectedRows(0).Cells("cEstadoP").Value = "B"
                grdPatente.SelectedRows(0).Visible = False
            End If
        End If
    End Sub

    Private Sub grdPatente_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles grdPatente.CellValueChanged

    End Sub

    Private Function ValidateInput() As Boolean
        If String.IsNullOrEmpty(txtNombre.Text.Trim) Then
            MsgBox(lblMsgNoName.Text)
            txtNombre.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(txtEmail.Text.Trim) Then
            MsgBox(lblMsgNoEmail.Text)
            txtNombre.Focus()
            Return False
        End If

        If String.IsNullOrEmpty(txtUser.Text.Trim) Then
            MsgBox(lblMsgNoUser.Text)
            txtUser.Focus()
            Return False
        End If

        If Not Regex.IsMatch(txtEmail.Text, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$") Then
            MsgBox(lblMsgWrongEmailFormat.Text)
            txtEmail.Focus()
            Return False
        End If

        Return True
    End Function

    Private Sub grdPatente_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdPatente.CellContentClick
        If grdPatente.Columns(e.ColumnIndex).Name = "cHabilitadoP" Then
            If grdPatente.Rows(e.RowIndex).Cells("cEstadoP").Value <> "A" Then
                grdPatente.Rows(e.RowIndex).Cells("cEstadoP").Value = "M"
            End If
        End If
    End Sub

    Private Sub grdPatente_CurrentCellDirtyStateChanged(sender As Object, e As EventArgs) Handles grdPatente.CurrentCellDirtyStateChanged
        'If (grdPatente.IsCurrentCellDirty) Then
        '    grdPatente.CommitEdit(DataGridViewDataErrorContexts.Commit)
        'End If
    End Sub

    Private Sub frmUsuario_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            If Me.isNew Then
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Dar de alta un usuario")
            Else
                Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Modificar un usuario")
            End If
        End If
    End Sub
End Class