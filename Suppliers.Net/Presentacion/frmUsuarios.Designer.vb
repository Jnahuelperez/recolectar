﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBaja = New System.Windows.Forms.Button()
        Me.btnModificar = New System.Windows.Forms.Button()
        Me.btnAlta = New System.Windows.Forms.Button()
        Me.grd = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cNombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEmail = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cUser = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cInactivo = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.cBloqueado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnDesbloquear = New System.Windows.Forms.Button()
        Me.lblMsgConfirmDeletion = New System.Windows.Forms.Label()
        Me.lblMsgCantDelete = New System.Windows.Forms.Label()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnBaja
        '
        Me.btnBaja.Location = New System.Drawing.Point(292, 260)
        Me.btnBaja.Name = "btnBaja"
        Me.btnBaja.Size = New System.Drawing.Size(75, 23)
        Me.btnBaja.TabIndex = 8
        Me.btnBaja.Tag = "Baja de usuario"
        Me.btnBaja.Text = "Baja"
        Me.btnBaja.UseVisualStyleBackColor = True
        '
        'btnModificar
        '
        Me.btnModificar.Location = New System.Drawing.Point(163, 260)
        Me.btnModificar.Name = "btnModificar"
        Me.btnModificar.Size = New System.Drawing.Size(75, 23)
        Me.btnModificar.TabIndex = 7
        Me.btnModificar.Tag = "Modificar usuario"
        Me.btnModificar.Text = "Modificacion"
        Me.btnModificar.UseVisualStyleBackColor = True
        '
        'btnAlta
        '
        Me.btnAlta.Location = New System.Drawing.Point(21, 260)
        Me.btnAlta.Name = "btnAlta"
        Me.btnAlta.Size = New System.Drawing.Size(88, 23)
        Me.btnAlta.TabIndex = 6
        Me.btnAlta.Tag = "Alta de usuario"
        Me.btnAlta.Text = "Alta"
        Me.btnAlta.UseVisualStyleBackColor = True
        '
        'grd
        '
        Me.grd.AllowUserToAddRows = False
        Me.grd.AllowUserToDeleteRows = False
        Me.grd.AllowUserToResizeColumns = False
        Me.grd.AllowUserToResizeRows = False
        Me.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cNombre, Me.cEmail, Me.cUser, Me.cInactivo, Me.cBloqueado})
        Me.grd.Location = New System.Drawing.Point(6, 9)
        Me.grd.MultiSelect = False
        Me.grd.Name = "grd"
        Me.grd.ReadOnly = True
        Me.grd.RowHeadersVisible = False
        Me.grd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grd.Size = New System.Drawing.Size(506, 235)
        Me.grd.TabIndex = 5
        '
        'cId
        '
        Me.cId.HeaderText = "Id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cNombre
        '
        Me.cNombre.HeaderText = "Nombre"
        Me.cNombre.Name = "cNombre"
        Me.cNombre.ReadOnly = True
        '
        'cEmail
        '
        Me.cEmail.HeaderText = "Email"
        Me.cEmail.Name = "cEmail"
        Me.cEmail.ReadOnly = True
        '
        'cUser
        '
        Me.cUser.HeaderText = "Usuario"
        Me.cUser.Name = "cUser"
        Me.cUser.ReadOnly = True
        '
        'cInactivo
        '
        Me.cInactivo.HeaderText = "Inactivo"
        Me.cInactivo.Name = "cInactivo"
        Me.cInactivo.ReadOnly = True
        Me.cInactivo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.cInactivo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'cBloqueado
        '
        Me.cBloqueado.HeaderText = "Bloqueado"
        Me.cBloqueado.Name = "cBloqueado"
        Me.cBloqueado.ReadOnly = True
        '
        'btnDesbloquear
        '
        Me.btnDesbloquear.Location = New System.Drawing.Point(421, 260)
        Me.btnDesbloquear.Name = "btnDesbloquear"
        Me.btnDesbloquear.Size = New System.Drawing.Size(75, 23)
        Me.btnDesbloquear.TabIndex = 9
        Me.btnDesbloquear.Tag = "Desbloquear usuario"
        Me.btnDesbloquear.Text = "Desbloquear"
        Me.btnDesbloquear.UseVisualStyleBackColor = True
        '
        'lblMsgConfirmDeletion
        '
        Me.lblMsgConfirmDeletion.AutoSize = True
        Me.lblMsgConfirmDeletion.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgConfirmDeletion.Location = New System.Drawing.Point(3, 304)
        Me.lblMsgConfirmDeletion.Name = "lblMsgConfirmDeletion"
        Me.lblMsgConfirmDeletion.Size = New System.Drawing.Size(202, 13)
        Me.lblMsgConfirmDeletion.TabIndex = 59
        Me.lblMsgConfirmDeletion.Text = "¿Desea eliminar el usuario seleccionado?"
        Me.lblMsgConfirmDeletion.Visible = False
        '
        'lblMsgCantDelete
        '
        Me.lblMsgCantDelete.AutoSize = True
        Me.lblMsgCantDelete.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgCantDelete.Location = New System.Drawing.Point(3, 317)
        Me.lblMsgCantDelete.Name = "lblMsgCantDelete"
        Me.lblMsgCantDelete.Size = New System.Drawing.Size(362, 13)
        Me.lblMsgCantDelete.TabIndex = 60
        Me.lblMsgCantDelete.Text = "No se puede eliminar el usuario porque dejaría una patente sin asignación: "
        Me.lblMsgCantDelete.Visible = False
        '
        'frmUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 298)
        Me.Controls.Add(Me.lblMsgCantDelete)
        Me.Controls.Add(Me.lblMsgConfirmDeletion)
        Me.Controls.Add(Me.btnDesbloquear)
        Me.Controls.Add(Me.btnBaja)
        Me.Controls.Add(Me.btnModificar)
        Me.Controls.Add(Me.btnAlta)
        Me.Controls.Add(Me.grd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUsuarios"
        Me.Text = "Usuarios"
        CType(Me.grd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBaja As System.Windows.Forms.Button
    Friend WithEvents btnModificar As System.Windows.Forms.Button
    Friend WithEvents btnAlta As System.Windows.Forms.Button
    Friend WithEvents grd As System.Windows.Forms.DataGridView
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cNombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cEmail As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cUser As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cInactivo As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cBloqueado As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents btnDesbloquear As System.Windows.Forms.Button
    Friend WithEvents lblMsgConfirmDeletion As System.Windows.Forms.Label
    Friend WithEvents lblMsgCantDelete As System.Windows.Forms.Label
End Class
