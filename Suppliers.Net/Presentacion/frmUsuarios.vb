﻿Public Class frmUsuarios

    Private Sub btnAlta_Click(sender As System.Object, e As System.EventArgs) Handles btnAlta.Click
        Dim frm As New frmUsuario
        frm.isNew = True
        frm.ShowDialog(Me)
        ActualizarGrilla()
    End Sub

    Private Sub frmUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ActualizarGrilla()
        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next

        btnAlta.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Alta Usuario")
        btnBaja.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Dar de Baja Usuario")
        btnModificar.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Modificar Usuario")
        btnDesbloquear.Enabled = Seguridad.CheckPatente(Seguridad.currentUser.id, "Desbloquear Usuario")

    End Sub

    Private Sub ActualizarGrilla()
        Dim usuarios = Usuario.Listar()

        grd.Rows.Clear()

        For Each u As Usuario In usuarios
            grd.Rows.Add(u.id, u.nombre, u.email, u.user, u.inactivo, u.bloqueado)
        Next
    End Sub

    Private Sub btnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If (grd.SelectedRows.Count = 1) Then
            Dim u = New Usuario(CInt(grd.SelectedRows(0).Cells("cId").Value))

            Dim frm As New frmUsuario
            frm.txtId.Text = u.id.ToString
            frm.txtNombre.Text = u.nombre
            frm.txtEmail.Text = u.email
            frm.txtUser.Text = u.user
            frm.chkInactivo.Checked = u.inactivo
            frm.chkInactivo.Enabled = u.inactivo

            For Each p As KeyValuePair(Of Patente, Boolean) In u.patentes
                frm.grdPatente.Rows.Add(p.Key.id, p.Key.descripcion, p.Value, "")
            Next

            For Each f As Familia In u.familias
                frm.grdFamilia.Rows.Add(f.id, f.nombre, "")
            Next

            frm.isNew = False
            frm.ShowDialog(Me)
            ActualizarGrilla()
        End If
    End Sub

    Private Sub btnBaja_Click(sender As Object, e As EventArgs) Handles btnBaja.Click
        Dim patentes = New List(Of Patente)

        If (grd.SelectedRows.Count = 1) Then
            If MsgBox(lblMsgConfirmDeletion.Text, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim u = New Usuario(CInt(grd.SelectedRows(0).Cells("cId").Value))

                'armo un listado de patentes
                For Each f As Familia In u.familias
                    For Each p As Patente In f.patentes
                        patentes.Add(p)
                    Next
                Next

                For Each p As KeyValuePair(Of Patente, Boolean) In u.patentes
                    If p.Value Then
                        If Not patentes.Exists(Function(x) x.id = p.Key.id) Then
                            patentes.Add(p.Key)
                        End If
                    Else
                        If patentes.Exists(Function(x) x.id = p.Key.id) Then
                            patentes.Remove(p.Key)
                        End If
                    End If
                Next

                'me fijo si eliminar el usuario dejaria una patente sin asignacion, cancelo acción
                For Each p As Patente In patentes
                    If Seguridad.GetCantidadUsuariosConPatente(p.descripcion) = 1 Then
                        MsgBox(lblMsgCantDelete.Text + ": " + p.descripcion)
                        Return
                    End If
                Next

                'si llego hasta acá, elimino
                Usuario.Eliminar(CInt(grd.SelectedRows(0).Cells("cId").Value))

                'registro en bitácora
                Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 1, "Elimina usuario: " + u.user)

                'actualizo grilla
                ActualizarGrilla()
            End If
        End If
    End Sub

    Private Sub btnDesbloquear_Click(sender As Object, e As EventArgs) Handles btnDesbloquear.Click
        If (grd.SelectedRows.Count = 1) Then
            Dim id = CInt(grd.SelectedRows(0).Cells("cId").Value)
            If CBool(grd.SelectedRows(0).Cells("cBloqueado").Value) Then
                Usuario.Desbloquear(id)

                'registro en bitácora
                Bitacora.GrabarMovimiento(Seguridad.currentUser.id, 1, "Desbloquea usuario ID: " + id.ToString)

                'actualizo grilla
                ActualizarGrilla()
            Else
                Beep()
            End If
        End If
    End Sub

    Private Sub frmUsuarios_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Usuarios")
        End If
    End Sub
End Class