﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmVenta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grd = New System.Windows.Forms.DataGridView()
        Me.cId = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cProducto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cEliminar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.lblIngreseNumero = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.lblMsgNoProd = New System.Windows.Forms.Label()
        Me.lblEliminar = New System.Windows.Forms.Label()
        CType(Me.grd, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grd
        '
        Me.grd.AllowUserToAddRows = False
        Me.grd.AllowUserToDeleteRows = False
        Me.grd.AllowUserToResizeColumns = False
        Me.grd.AllowUserToResizeRows = False
        Me.grd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grd.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cId, Me.cProducto, Me.cEliminar})
        Me.grd.Location = New System.Drawing.Point(12, 37)
        Me.grd.MultiSelect = False
        Me.grd.Name = "grd"
        Me.grd.ReadOnly = True
        Me.grd.RowHeadersVisible = False
        Me.grd.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.grd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grd.Size = New System.Drawing.Size(419, 294)
        Me.grd.TabIndex = 2
        '
        'cId
        '
        Me.cId.HeaderText = "Id"
        Me.cId.Name = "cId"
        Me.cId.ReadOnly = True
        Me.cId.Visible = False
        '
        'cProducto
        '
        Me.cProducto.HeaderText = "Producto"
        Me.cProducto.Name = "cProducto"
        Me.cProducto.ReadOnly = True
        Me.cProducto.Width = 300
        '
        'cEliminar
        '
        Me.cEliminar.HeaderText = "Eliminar"
        Me.cEliminar.Name = "cEliminar"
        Me.cEliminar.ReadOnly = True
        Me.cEliminar.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(81, 362)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(93, 23)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnCancelar
        '
        Me.btnCancelar.Location = New System.Drawing.Point(265, 362)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(93, 23)
        Me.btnCancelar.TabIndex = 4
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'lblIngreseNumero
        '
        Me.lblIngreseNumero.AutoSize = True
        Me.lblIngreseNumero.Location = New System.Drawing.Point(12, 13)
        Me.lblIngreseNumero.Name = "lblIngreseNumero"
        Me.lblIngreseNumero.Size = New System.Drawing.Size(139, 13)
        Me.lblIngreseNumero.TabIndex = 6
        Me.lblIngreseNumero.Text = "Ingrese Código de Producto"
        '
        'txtCodigo
        '
        Me.txtCodigo.Location = New System.Drawing.Point(151, 10)
        Me.txtCodigo.MaxLength = 50
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(100, 20)
        Me.txtCodigo.TabIndex = 7
        Me.txtCodigo.Tag = "Ingrese el código del pedido"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(257, 8)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 8
        Me.btnBuscar.Text = "Aceptar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'lblMsgNoProd
        '
        Me.lblMsgNoProd.AutoSize = True
        Me.lblMsgNoProd.ForeColor = System.Drawing.Color.Coral
        Me.lblMsgNoProd.Location = New System.Drawing.Point(12, 405)
        Me.lblMsgNoProd.Name = "lblMsgNoProd"
        Me.lblMsgNoProd.Size = New System.Drawing.Size(153, 13)
        Me.lblMsgNoProd.TabIndex = 70
        Me.lblMsgNoProd.Text = "Código de producto inexistente"
        Me.lblMsgNoProd.Visible = False
        '
        'lblEliminar
        '
        Me.lblEliminar.AutoSize = True
        Me.lblEliminar.ForeColor = System.Drawing.Color.Coral
        Me.lblEliminar.Location = New System.Drawing.Point(12, 418)
        Me.lblEliminar.Name = "lblEliminar"
        Me.lblEliminar.Size = New System.Drawing.Size(43, 13)
        Me.lblEliminar.TabIndex = 71
        Me.lblEliminar.Text = "Eliminar"
        Me.lblEliminar.Visible = False
        '
        'frmVenta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(444, 390)
        Me.Controls.Add(Me.lblEliminar)
        Me.Controls.Add(Me.lblMsgNoProd)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.txtCodigo)
        Me.Controls.Add(Me.lblIngreseNumero)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.grd)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmVenta"
        Me.Text = "Venta"
        CType(Me.grd, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grd As System.Windows.Forms.DataGridView
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents lblIngreseNumero As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents cId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cProducto As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cEliminar As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents lblMsgNoProd As System.Windows.Forms.Label
    Friend WithEvents lblEliminar As System.Windows.Forms.Label
End Class
