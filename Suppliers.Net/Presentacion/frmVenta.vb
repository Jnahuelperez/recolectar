﻿Public Class frmVenta

    Private Sub frmVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        MultiIdioma.ConfigurarForm(Me)
        Dim tt = frmMain.tt
        For Each c As Control In Me.Controls
            If c.Controls.Count > 0 Then
                For Each cc As Control In c.Controls
                    If Not cc.Tag Is Nothing Then
                        tt.SetToolTip(cc, cc.Tag.ToString)
                    End If
                Next
            Else
                If Not c.Tag Is Nothing Then
                    tt.SetToolTip(c, c.Tag.ToString)
                End If
            End If
        Next
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim p = New Producto(Me.txtCodigo.Text)
        If p.id > 0 Then
            grd.Rows.Add(p.id, p.descripcion, lblEliminar.text)
        Else
            MsgBox(lblMsgNoProd.Text)
        End If
        txtCodigo.Text = ""
    End Sub

    Private Sub grd_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grd.CellContentClick
        Dim senderGrid = DirectCast(sender, DataGridView)

        If TypeOf senderGrid.Columns(e.ColumnIndex) Is DataGridViewButtonColumn AndAlso e.RowIndex >= 0 Then
            grd.Rows.RemoveAt(e.RowIndex)
        End If
    End Sub

    Private Sub txtCodigo_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCodigo.KeyDown

        If e.KeyCode = Keys.Enter Then
            Dim p = New Producto(Me.txtCodigo.Text)
            If p.id > 0 Then
                grd.Rows.Add(p.id, p.descripcion, lblEliminar.Text)
            Else
                MsgBox(lblMsgNoProd.Text)
            End If
            txtCodigo.Text = ""
        End If
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Close()

    End Sub

    Private Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        For Each r As DataGridViewRow In grd.Rows
            Dim m As New Movimiento
            m.producto = New Producto(CInt(r.Cells("cId").Value))
            m.cantidad = 1
            m.usuario = Seguridad.currentUser
            m.tipoMovimientoId = 2
            m.Agregar()
        Next

        grd.Rows.Clear()
        txtCodigo.Text = ""
    End Sub

    Private Sub frmVenta_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Keys.F1 Then
            Help.ShowHelp(Me, "Suppliers.chm", HelpNavigator.KeywordIndex, "Ventas")
        End If
    End Sub
End Class