﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace UI.Contenedores
{
    public partial class CRUDContenedores : Form
    {
        public Contenedor mContedorAEditar = new Contenedor();
        internal Constantes.TipoDeOperacion Operacion { get; set; }
        public CRUDContenedores()
        {
            InitializeComponent();
        }

        private void CRUDContenedores_Load(object sender, EventArgs e)
        {
            txtID.Enabled = false;

            cmbEstado.Items.Add("Activo");
            cmbEstado.Items.Add("Inactivo");
            cmbEstado.Items.Add("Recolectar");
            cmbEstado.Items.Add("Bloqueado");
            cmbEstado.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbEstado.SelectedIndex = 0;

            InicializarPagina();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }


        private void InicializarPagina()
        {
            switch (Operacion)
            {
                case Constantes.TipoDeOperacion.Alta:
                    LimpiarCampos();
                    cmbEstado.Enabled = false;
                    txtCod.Text = "";
                    txtUbi.Text = "LAT,LONG";
                    txtCap.Text = "1000";
                    break;
                case Constantes.TipoDeOperacion.Modificacion:
                    if (mContedorAEditar == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGNoUserSelect", Login.UsuarioLogeado.UserIdioma),
                            Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    else
                    {
                        CargarCampos(mContedorAEditar);
                    }
                    break;
                case Constantes.TipoDeOperacion.Baja:
                    if (mContedorAEditar == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGNoUserSelect", Login.UsuarioLogeado.UserIdioma),
                            Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    else
                    {
                        CargarCampos(mContedorAEditar);
                        cmbEstado.SelectedIndex = 1;
                        cmbEstado.Enabled = false;
                        DeshabilitarCampos();
                    }
                    break;
                case Constantes.TipoDeOperacion.Consulta:
                    if (mContedorAEditar == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGNoUserSelect", Login.UsuarioLogeado.UserIdioma),
                            Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    CargarCampos(mContedorAEditar);
                    DeshabilitarCampos();
                    this.Controls.Remove(this.btnAceptar);
                    break;
            }
        }

        private void LimpiarCampos()
        {
            txtCap.Text = "";
            txtCod.Text = "";
            txtID.Text = "";
            txtUbi.Text = "";
        }

        private void CargarCampos(Contenedor pContenedor)
        {
            txtID.Text = pContenedor.id.ToString();
            txtCap.Text = pContenedor.Capacidad.ToString();
            txtCod.Text = pContenedor.Detalle;
            txtUbi.Text = pContenedor.Ubicacion;
        }

        private void DeshabilitarCampos()
        {
            txtCap.Enabled = false;
            txtCod.Enabled = false;
            txtID.Enabled = false;
            txtUbi.Enabled = false;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
                try
                {
                    switch (Operacion)
                    {
                        case Constantes.TipoDeOperacion.Alta:
                            mContedorAEditar.Detalle = txtCod.Text;
                            mContedorAEditar.Capacidad = int.Parse(txtCap.Text);
                            mContedorAEditar.Ubicacion = txtUbi.Text;
                            mContedorAEditar.Crear(mContedorAEditar);
                            this.Close();
                            break;
                        case Constantes.TipoDeOperacion.Modificacion:
                            mContedorAEditar.Detalle = txtCod.Text;
                            mContedorAEditar.Capacidad = int.Parse(txtCap.Text);
                            mContedorAEditar.Ubicacion = txtUbi.Text;
                            mContedorAEditar.id = int.Parse(txtID.Text);
                        mContedorAEditar.Estado = cmbEstado.SelectedItem.ToString();
                        mContedorAEditar.Modificar(mContedorAEditar);
                            this.Close();
                            break;
                        case Constantes.TipoDeOperacion.Baja:
                            mContedorAEditar.Detalle = txtCod.Text;
                        mContedorAEditar.Capacidad = int.Parse(txtCap.Text);
                            mContedorAEditar.Ubicacion = txtUbi.Text;
                            mContedorAEditar.id = int.Parse(txtID.Text);
                            DialogResult Alerta = MessageBox.Show(Idioma.GetMSGLanguaje("MSGConfirmAction", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("TitleConfirm", Login.UsuarioLogeado.UserIdioma), MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (Alerta == DialogResult.Yes)
                            {
                                mContedorAEditar.Baja(mContedorAEditar);
                            }
                            else
                            {
                                MessageBox.Show(
                                    Idioma.GetMSGLanguaje("MSGOperCancelada", Login.UsuarioLogeado.UserIdioma),
                                    Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            this.Close();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        Idioma.GetMSGLanguaje("MSGErrorOnRuntime", Login.UsuarioLogeado.UserIdioma) + "\n" + ex.Message,
                        Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
        }
    }
}
