﻿namespace UI
{
    partial class FRMContenedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIndcContam = new System.Windows.Forms.Label();
            this.txtIndCont = new System.Windows.Forms.TextBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.lblPeso = new System.Windows.Forms.Label();
            this.chkAneg = new System.Windows.Forms.CheckBox();
            this.btnNotifEst = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIndcContam
            // 
            this.lblIndcContam.AutoSize = true;
            this.lblIndcContam.Location = new System.Drawing.Point(212, 15);
            this.lblIndcContam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIndcContam.Name = "lblIndcContam";
            this.lblIndcContam.Size = new System.Drawing.Size(61, 13);
            this.lblIndcContam.TabIndex = 2;
            this.lblIndcContam.Text = "Indice Cont";
            // 
            // txtIndCont
            // 
            this.txtIndCont.Location = new System.Drawing.Point(275, 11);
            this.txtIndCont.Margin = new System.Windows.Forms.Padding(2);
            this.txtIndCont.Name = "txtIndCont";
            this.txtIndCont.Size = new System.Drawing.Size(52, 20);
            this.txtIndCont.TabIndex = 3;
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(275, 41);
            this.txtPeso.Margin = new System.Windows.Forms.Padding(2);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(52, 20);
            this.txtPeso.TabIndex = 5;
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Location = new System.Drawing.Point(212, 43);
            this.lblPeso.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(31, 13);
            this.lblPeso.TabIndex = 4;
            this.lblPeso.Text = "Peso";
            // 
            // chkAneg
            // 
            this.chkAneg.AutoSize = true;
            this.chkAneg.Location = new System.Drawing.Point(215, 69);
            this.chkAneg.Margin = new System.Windows.Forms.Padding(2);
            this.chkAneg.Name = "chkAneg";
            this.chkAneg.Size = new System.Drawing.Size(88, 17);
            this.chkAneg.TabIndex = 7;
            this.chkAneg.Text = "Anegamiento";
            this.chkAneg.UseVisualStyleBackColor = true;
            // 
            // btnNotifEst
            // 
            this.btnNotifEst.Location = new System.Drawing.Point(227, 101);
            this.btnNotifEst.Margin = new System.Windows.Forms.Padding(2);
            this.btnNotifEst.Name = "btnNotifEst";
            this.btnNotifEst.Size = new System.Drawing.Size(100, 39);
            this.btnNotifEst.TabIndex = 8;
            this.btnNotifEst.Text = "Notificar estado";
            this.btnNotifEst.UseVisualStyleBackColor = true;
            this.btnNotifEst.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 11);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(195, 129);
            this.dataGridView1.TabIndex = 9;
            // 
            // FRMContenedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 152);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnNotifEst);
            this.Controls.Add(this.chkAneg);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.lblPeso);
            this.Controls.Add(this.txtIndCont);
            this.Controls.Add(this.lblIndcContam);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FRMContenedor";
            this.Text = "Contenedor Simulado";
            this.Load += new System.EventHandler(this.FRMContenedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIndcContam;
        private System.Windows.Forms.TextBox txtIndCont;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.CheckBox chkAneg;
        private System.Windows.Forms.Button btnNotifEst;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}