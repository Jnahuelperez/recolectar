﻿using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class FRMContenedor : Form
    {
        Contenedor mContenedorBL = new Contenedor();
        public FRMContenedor()
        {

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void FRMContenedor_Load(object sender, EventArgs e)
        {
            txtIndCont.Text = "0";
            txtPeso.Text = "0";
            dataGridView1.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dataGridView1.Columns.Add(GetDGVColumns("dgvDetalle"), GetDGVColumns("dgvDetalle"));
            dataGridView1.EditMode = DataGridViewEditMode.EditProgrammatically;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;


            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }

        private void ActualizarGrilla()
        {
            dataGridView1.Rows.Clear();
            foreach (Contenedor mC in mContenedorBL.Listar())
                dataGridView1.Rows.Add(mC.id, mC.Detalle);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Contenedor mC = new Contenedor();
            mC.id = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            mC = mContenedorBL.Obtener(mC);
            Contenedor.Movimiento mContMov = new Contenedor.Movimiento();
            mContMov.idContenedor = int.Parse(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
            mContMov.IndiceCont = int.Parse(txtIndCont.Text);
            mContMov.PesoAgregado = int.Parse(txtPeso.Text);
            if (chkAneg.Checked)
                mContMov.Anegamiento = true;
            mContenedorBL.NotificarEstado(mContMov,mC);
        }
    }
}
