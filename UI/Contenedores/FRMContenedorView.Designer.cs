﻿namespace UI
{
    partial class FRMContenedorView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIndiceCont = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtContenedor = new System.Windows.Forms.TextBox();
            this.dgvContenedorMovimientos = new System.Windows.Forms.DataGridView();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chartPeso = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chartCont = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chartOcup = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label8 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContenedorMovimientos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPeso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCont)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOcup)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(8, 96);
            this.checkBox1.Margin = new System.Windows.Forms.Padding(2);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(88, 17);
            this.checkBox1.TabIndex = 15;
            this.checkBox1.Text = "Anegamiento";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // txtPeso
            // 
            this.txtPeso.Location = new System.Drawing.Point(76, 71);
            this.txtPeso.Margin = new System.Windows.Forms.Padding(2);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.Size = new System.Drawing.Size(52, 20);
            this.txtPeso.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Peso";
            // 
            // txtIndiceCont
            // 
            this.txtIndiceCont.Location = new System.Drawing.Point(76, 48);
            this.txtIndiceCont.Margin = new System.Windows.Forms.Padding(2);
            this.txtIndiceCont.Name = "txtIndiceCont";
            this.txtIndiceCont.Size = new System.Drawing.Size(52, 20);
            this.txtIndiceCont.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 50);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Indice Cont";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Contenedor";
            // 
            // txtContenedor
            // 
            this.txtContenedor.Location = new System.Drawing.Point(76, 3);
            this.txtContenedor.Margin = new System.Windows.Forms.Padding(2);
            this.txtContenedor.Name = "txtContenedor";
            this.txtContenedor.Size = new System.Drawing.Size(82, 20);
            this.txtContenedor.TabIndex = 16;
            // 
            // dgvContenedorMovimientos
            // 
            this.dgvContenedorMovimientos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContenedorMovimientos.Location = new System.Drawing.Point(200, 3);
            this.dgvContenedorMovimientos.Margin = new System.Windows.Forms.Padding(2);
            this.dgvContenedorMovimientos.Name = "dgvContenedorMovimientos";
            this.dgvContenedorMovimientos.RowHeadersWidth = 82;
            this.dgvContenedorMovimientos.RowTemplate.Height = 33;
            this.dgvContenedorMovimientos.Size = new System.Drawing.Size(505, 108);
            this.dgvContenedorMovimientos.TabIndex = 17;
            // 
            // txtEstado
            // 
            this.txtEstado.Location = new System.Drawing.Point(76, 25);
            this.txtEstado.Margin = new System.Windows.Forms.Padding(2);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(82, 20);
            this.txtEstado.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 27);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Estado";
            // 
            // chartPeso
            // 
            chartArea1.Name = "ChartArea1";
            this.chartPeso.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartPeso.Legends.Add(legend1);
            this.chartPeso.Location = new System.Drawing.Point(8, 144);
            this.chartPeso.Margin = new System.Windows.Forms.Padding(2);
            this.chartPeso.Name = "chartPeso";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Peso";
            this.chartPeso.Series.Add(series1);
            this.chartPeso.Size = new System.Drawing.Size(283, 190);
            this.chartPeso.TabIndex = 20;
            this.chartPeso.Text = "Variacion de peso";
            this.chartPeso.Click += new System.EventHandler(this.chart1_Click);
            // 
            // chartCont
            // 
            chartArea2.Name = "ChartArea1";
            this.chartCont.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartCont.Legends.Add(legend2);
            this.chartCont.Location = new System.Drawing.Point(310, 144);
            this.chartCont.Margin = new System.Windows.Forms.Padding(2);
            this.chartCont.Name = "chartCont";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "IndCont";
            this.chartCont.Series.Add(series2);
            this.chartCont.Size = new System.Drawing.Size(283, 190);
            this.chartCont.TabIndex = 21;
            this.chartCont.Text = "Variacion de peso";
            this.chartCont.Click += new System.EventHandler(this.chart2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 127);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 22;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 127);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(156, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Variacion de peso por actividad";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(308, 127);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(187, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Indice de contaminacion por actividad";
            // 
            // chartOcup
            // 
            chartArea3.Name = "ChartArea1";
            this.chartOcup.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chartOcup.Legends.Add(legend3);
            this.chartOcup.Location = new System.Drawing.Point(609, 144);
            this.chartOcup.Margin = new System.Windows.Forms.Padding(2);
            this.chartOcup.Name = "chartOcup";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.StackedBar100;
            series3.Legend = "Legend1";
            series3.Name = "Ocupacion";
            this.chartOcup.Series.Add(series3);
            this.chartOcup.Size = new System.Drawing.Size(283, 190);
            this.chartOcup.TabIndex = 25;
            this.chartOcup.Text = "Variacion de peso";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(606, 127);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "Nivel de ocupacion";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(729, 36);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(123, 40);
            this.button1.TabIndex = 27;
            this.button1.Text = "Imprimir reporte";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // FRMContenedorView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 345);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.chartOcup);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chartCont);
            this.Controls.Add(this.chartPeso);
            this.Controls.Add(this.txtEstado);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgvContenedorMovimientos);
            this.Controls.Add(this.txtContenedor);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.txtPeso);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtIndiceCont);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FRMContenedorView";
            this.Text = "Informacion de Contenedor";
            this.Load += new System.EventHandler(this.FRMABMContenedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvContenedorMovimientos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPeso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartCont)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartOcup)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIndiceCont;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtContenedor;
        private System.Windows.Forms.DataGridView dgvContenedorMovimientos;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPeso;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartCont;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartOcup;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
    }
}