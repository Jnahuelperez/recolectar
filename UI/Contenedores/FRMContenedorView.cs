﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using BL;

namespace UI
{
    public partial class FRMContenedorView : Form
    {
        public Contenedor mContenedor = new Contenedor();
        public FRMContenedorView()
        {
            InitializeComponent();
        }
        private void FRMABMContenedor_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvContenedorMovimientos.Columns.Add("ID", "ID");
            dgvContenedorMovimientos.Columns.Add("Fecha", "Fecha");
            dgvContenedorMovimientos.Columns.Add("Gramos", "Gramos");
            dgvContenedorMovimientos.Columns.Add("Contaminacion", "Contaminacion");
            dgvContenedorMovimientos.Columns.Add("Anegamiento", "Anegamiento");
            dgvContenedorMovimientos.Columns.Add("Estado", "Estado");

            dgvContenedorMovimientos.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvContenedorMovimientos.AllowUserToAddRows = false;
            dgvContenedorMovimientos.AllowUserToDeleteRows = false;
            dgvContenedorMovimientos.AllowUserToResizeRows = false;
            dgvContenedorMovimientos.RowHeadersVisible = false;
            dgvContenedorMovimientos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvContenedorMovimientos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvContenedorMovimientos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvContenedorMovimientos.RowTemplate.Height = 30;
            #endregion
            txtContenedor.Text = mContenedor.Detalle;
            txtEstado.Text = mContenedor.Estado;
            txtIndiceCont.Text = mContenedor.IndiceContaminacion.ToString();
            txtPeso.Text = mContenedor.Peso.ToString();

            if (mContenedor.Anegado)
                checkBox1.Checked=true;

            txtContenedor.Enabled = false;
            txtEstado.Enabled = false;
            txtIndiceCont.Enabled = false;
            txtPeso.Enabled = false;
            checkBox1.Enabled = false;

            ActualizarGrilla();
            CargarGraficos();
        }

        private void CargarGraficos()
        {
            foreach (Contenedor.Movimiento mV in mContenedor.ListarMovimientos())
            {
                if (mV.idContenedor == mContenedor.id)
                {
                    Contenedor mC = new Contenedor();
                    mC.id = mV.idContenedor;
                    mC = mContenedor.Obtener(mC);
                    chartPeso.Series["Peso"].Points.AddXY(mC.Peso, mV.Fecha);
                    chartCont.Series["IndCont"].Points.AddXY(mC.IndiceContaminacion, mV.Fecha);
                    chartOcup.Series["Ocupacion"].Points.AddY(((mC.Peso*100)/mC.Capacidad));
                }
            }
        }


        private void ActualizarGrilla()
        {
            dgvContenedorMovimientos.Rows.Clear();
            foreach (Contenedor.Movimiento mContMov in mContenedor.ListarMovimientos())
            {
                if (mContMov.idContenedor == mContenedor.id)
                {
                    Contenedor mC = new Contenedor();
                    mC.id = mContMov.idContenedor;
                    mC = mC.Obtener(mC);
                    dgvContenedorMovimientos.Rows.Add(mContMov.id, mContMov.Fecha, mContMov.PesoAgregado,
                        mContMov.IndiceCont.ToString(), mContMov.Anegamiento.ToString(), mContMov.Estado);
                }
            }
        }


        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void chart2_Click(object sender, EventArgs e)
        {

        }
    }
}
