﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;
using UI.Contenedores;

namespace UI
{
    public partial class LISTCONTENEDOR : Form
    {
        Contenedor mContenedor = new Contenedor();
        public LISTCONTENEDOR()
        {
            InitializeComponent();
        }

        private void LISTCONTENEDOR_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvContenedores.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvDetalle"), GetDGVColumns("dgvDetalle"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvEstado"), GetDGVColumns("dgvEstado"));
            dgvContenedores.Columns["ID"].Visible = false;

            dgvContenedores.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvContenedores.AllowUserToAddRows = false;
            dgvContenedores.AllowUserToDeleteRows = false;
            dgvContenedores.AllowUserToResizeColumns = false;
            dgvContenedores.AllowUserToResizeRows = false;
            dgvContenedores.RowHeadersVisible = false;
            dgvContenedores.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvContenedores.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvContenedores.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvContenedores.RowTemplate.Height = 30;
            #endregion

            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }


        private void ActualizarGrilla()
        {
            dgvContenedores.Rows.Clear();
            foreach (Contenedor mC in mContenedor.Listar())
                dgvContenedores.Rows.Add(mC.id, mC.Detalle, mC.Estado);
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Contenedor mC = new Contenedor();
            FRMContenedorView mForm = new FRMContenedorView();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.StartPosition = FormStartPosition.CenterParent;

            mC.id = int.Parse(dgvContenedores.SelectedRows[0].Cells[0].Value.ToString());
            mForm.mContenedor = mContenedor.Obtener(mC);

            mForm.ShowDialog(this);
        }

        private void OpenCRUD(Constantes.TipoDeOperacion pOperacion)
        {
            Contenedor mC = new Contenedor();
            CRUDContenedores mForm = new CRUDContenedores();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.StartPosition = FormStartPosition.CenterParent;
            mForm.Operacion = pOperacion;
            if (pOperacion == Constantes.TipoDeOperacion.Alta)
            {}
            else
            {
                mC.id = int.Parse(dgvContenedores.SelectedRows[0].Cells[0].Value.ToString());
                mForm.mContedorAEditar = mContenedor.Obtener(mC);
            }
            try
            {
                mForm.ShowDialog(this);
            }catch(Exception ex) { MessageBox.Show(ex.Message); }

            ActualizarGrilla();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Alta;
            OpenCRUD(mOperacion);
        }

        private void btnMod_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Modificacion;
            OpenCRUD(mOperacion);
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Baja;
            OpenCRUD(mOperacion);
        }
    }
}
