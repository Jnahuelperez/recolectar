﻿namespace UI
{
    partial class FRMInicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblIni1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblIni2 = new System.Windows.Forms.Label();
            this.lblIni3 = new System.Windows.Forms.Label();
            this.lblIni4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblIni1
            // 
            this.lblIni1.AutoSize = true;
            this.lblIni1.Location = new System.Drawing.Point(6, 5);
            this.lblIni1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIni1.Name = "lblIni1";
            this.lblIni1.Size = new System.Drawing.Size(99, 13);
            this.lblIni1.TabIndex = 0;
            this.lblIni1.Text = "Iniciando Sistema...";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(8, 28);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(247, 13);
            this.progressBar1.TabIndex = 1;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblIni2
            // 
            this.lblIni2.AutoSize = true;
            this.lblIni2.Location = new System.Drawing.Point(6, 50);
            this.lblIni2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIni2.Name = "lblIni2";
            this.lblIni2.Size = new System.Drawing.Size(109, 13);
            this.lblIni2.TabIndex = 2;
            this.lblIni2.Text = "Checking config file...";
            // 
            // lblIni3
            // 
            this.lblIni3.AutoSize = true;
            this.lblIni3.Location = new System.Drawing.Point(6, 71);
            this.lblIni3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIni3.Name = "lblIni3";
            this.lblIni3.Size = new System.Drawing.Size(100, 13);
            this.lblIni3.TabIndex = 3;
            this.lblIni3.Text = "Connecting to DB...";
            this.lblIni3.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblIni4
            // 
            this.lblIni4.AutoSize = true;
            this.lblIni4.Location = new System.Drawing.Point(6, 93);
            this.lblIni4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIni4.Name = "lblIni4";
            this.lblIni4.Size = new System.Drawing.Size(86, 13);
            this.lblIni4.TabIndex = 4;
            this.lblIni4.Text = "Integrity check...";
            // 
            // FRMInicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 128);
            this.Controls.Add(this.lblIni4);
            this.Controls.Add(this.lblIni3);
            this.Controls.Add(this.lblIni2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.lblIni1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FRMInicio";
            this.Text = "Inicio";
            this.Load += new System.EventHandler(this.Inicio_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIni1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblIni2;
        private System.Windows.Forms.Label lblIni3;
        private System.Windows.Forms.Label lblIni4;
    }
}