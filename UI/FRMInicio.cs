﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using BL;
using System.IO;
using System.Configuration;

namespace UI
{
    public partial class FRMInicio : Form
    {
        BL.Iniciador mIniciador = new Iniciador();
        public FRMInicio()
        {
            InitializeComponent();
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {
            this.timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.progressBar1.Increment(1);
        }
        
        private void Inicio_Load(object sender, EventArgs e)
        {
            //Load idioma de config file
            string mIdioma = ConfigurationManager.AppSettings.Get("IdiomaUsuario");
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(mIdioma));

            #region LOAD
            //this.Show();
            try
            {
                string mProjectPath = Directory.GetParent(Environment.CurrentDirectory).Parent.FullName;
                string AppConfigpath = mProjectPath + "/App.config"; //Config File location
                try
                {
                    if (!CheckConfigFile(AppConfigpath))
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGCheckConfigFile", mIdioma),
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                }
                catch (Exception ex) { throw (ex); }
                try
                {
                    if (!CheckDBConnection())
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGCheckDBConnection", mIdioma), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                } catch(Exception ex) { throw (ex); }
                try
                {
                    CheckIntegrity();
                }
                catch(Exception ex) 
                {
                    MessageBox.Show(Idioma.GetMSGLanguaje("MSGCheckIntegrity", mIdioma) + "\n"+ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                MessageBox.Show(Idioma.GetMSGLanguaje("MSGAllChecksOk", mIdioma), "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                FRMLogin mLogin = new FRMLogin();
                mLogin.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Idioma.GetMSGLanguaje("MSGIniError", mIdioma) + "\n" + ex.Message,
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Close();
        }
        #endregion

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Check Config file Exist
        private bool CheckConfigFile(string pPath)
        {
            if(mIniciador.CheckConfigFile(pPath))
            {
                this.progressBar1.Increment(33);
                return true;
            }
            else
            {
                return false;
            }
        }
            
        #endregion

        #region Check DB Connection
        public bool CheckDBConnection()
        {
            try
            {
                mIniciador.ConnectToDB();
                this.progressBar1.Increment(33);
                return true;

            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        #endregion

        #region Check Integrity
        public void CheckIntegrity()
        {
            try
            {
                mIniciador.CheckIntegrity();
                this.progressBar1.Increment(34);
            }
            catch(Exception ex) { throw (ex); }
        }
        #endregion
        private void button2_Click(object sender, EventArgs e)
        {
            FRMCIFRAdo mCfr = new FRMCIFRAdo();
            mCfr.Show();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
