﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;
using System.Configuration;
using System.Collections.Specialized;

namespace UI
{
    public partial class FRMLogin : Form
    {
        BL.Login mLogin = new Login();
        Usuario mUser = new Usuario();
        public FRMLogin()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnIng_Click(object sender, EventArgs e)
        {
            mUser.user = txtUser.Text;
            try
            {
                mLogin.ValidarLogin(txtUser.Text, txtPass.Text);
                FRMPrincipal mForm = new FRMPrincipal();
                mForm.MinimizeBox = false;
                mForm.MaximizeBox = false;
                mForm.StartPosition = FormStartPosition.CenterParent;
                // Get Permisos
                mForm.mFamilias = mUser.GetFamiliasByUser(mUser);
                mForm.mPatentes = mUser.GetPatentesByUser(mUser);
                Login.UsuarioLogeado = mUser.Obtener(mUser);
                this.Hide();
                mForm.ShowDialog(this);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FRMLogin_Load(object sender, EventArgs e)
        {
            //Load idioma de config file
            string mIdioma = ConfigurationManager.AppSettings.Get("IdiomaUsuario");
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(mIdioma));
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
    }
}
