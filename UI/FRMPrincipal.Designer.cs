﻿namespace UI
{
    partial class FRMPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.sesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iniciarSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarContrasenaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cambiarIdiomaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seguridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bitacoraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarUsuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarPuestosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cifrarDescifrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contenedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarContenedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.simularContenedorFisicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RutasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarRutasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recoleccionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RecorridosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contenedoresToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sesionToolStripMenuItem,
            this.seguridadToolStripMenuItem,
            this.contenedoresToolStripMenuItem,
            this.RutasToolStripMenuItem,
            this.recoleccionToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1022, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            this.menuStrip.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip_ItemClicked);
            // 
            // sesionToolStripMenuItem
            // 
            this.sesionToolStripMenuItem.DoubleClickEnabled = true;
            this.sesionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarSesionToolStripMenuItem,
            this.cambiarContrasenaToolStripMenuItem,
            this.cambiarIdiomaToolStripMenuItem,
            this.cerrarSesionToolStripMenuItem});
            this.sesionToolStripMenuItem.Name = "sesionToolStripMenuItem";
            this.sesionToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.sesionToolStripMenuItem.Text = "Sesion";
            // 
            // iniciarSesionToolStripMenuItem
            // 
            this.iniciarSesionToolStripMenuItem.Name = "iniciarSesionToolStripMenuItem";
            this.iniciarSesionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.iniciarSesionToolStripMenuItem.Text = "Iniciar Sesion";
            this.iniciarSesionToolStripMenuItem.Click += new System.EventHandler(this.iniciarSesionToolStripMenuItem_Click);
            // 
            // cambiarContrasenaToolStripMenuItem
            // 
            this.cambiarContrasenaToolStripMenuItem.Name = "cambiarContrasenaToolStripMenuItem";
            this.cambiarContrasenaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cambiarContrasenaToolStripMenuItem.Text = "Cambiar contraseña";
            this.cambiarContrasenaToolStripMenuItem.Click += new System.EventHandler(this.RecuperarContraseñaToolStripMenuItem_Click);
            // 
            // cambiarIdiomaToolStripMenuItem
            // 
            this.cambiarIdiomaToolStripMenuItem.Name = "cambiarIdiomaToolStripMenuItem";
            this.cambiarIdiomaToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cambiarIdiomaToolStripMenuItem.Text = "Cambiar Idioma";
            this.cambiarIdiomaToolStripMenuItem.Click += new System.EventHandler(this.cambiarIdiomaToolStripMenuItem_Click);
            // 
            // cerrarSesionToolStripMenuItem
            // 
            this.cerrarSesionToolStripMenuItem.Name = "cerrarSesionToolStripMenuItem";
            this.cerrarSesionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.cerrarSesionToolStripMenuItem.Text = "Cerrar Sesion";
            this.cerrarSesionToolStripMenuItem.Click += new System.EventHandler(this.cerrarSesionToolStripMenuItem_Click);
            // 
            // seguridadToolStripMenuItem
            // 
            this.seguridadToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bitacoraToolStripMenuItem,
            this.gestionarUsuariosToolStripMenuItem,
            this.gestionarPuestosToolStripMenuItem,
            this.backupToolStripMenuItem,
            this.restoreToolStripMenuItem,
            this.cifrarDescifrarToolStripMenuItem});
            this.seguridadToolStripMenuItem.Name = "seguridadToolStripMenuItem";
            this.seguridadToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.seguridadToolStripMenuItem.Text = "Seguridad";
            this.seguridadToolStripMenuItem.Click += new System.EventHandler(this.seguridadToolStripMenuItem_Click);
            // 
            // bitacoraToolStripMenuItem
            // 
            this.bitacoraToolStripMenuItem.Name = "bitacoraToolStripMenuItem";
            this.bitacoraToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.bitacoraToolStripMenuItem.Text = "Bitacora";
            this.bitacoraToolStripMenuItem.Click += new System.EventHandler(this.BitacoraToolStripMenuItem_Click);
            // 
            // gestionarUsuariosToolStripMenuItem
            // 
            this.gestionarUsuariosToolStripMenuItem.Name = "gestionarUsuariosToolStripMenuItem";
            this.gestionarUsuariosToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.gestionarUsuariosToolStripMenuItem.Text = "Gestionar Usuarios";
            this.gestionarUsuariosToolStripMenuItem.Click += new System.EventHandler(this.gestionarUsuariosToolStripMenuItem1_Click);
            // 
            // gestionarPuestosToolStripMenuItem
            // 
            this.gestionarPuestosToolStripMenuItem.Name = "gestionarPuestosToolStripMenuItem";
            this.gestionarPuestosToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.gestionarPuestosToolStripMenuItem.Text = "Gestionar Puestos";
            this.gestionarPuestosToolStripMenuItem.Click += new System.EventHandler(this.gestionarPuestosToolStripMenuItem1_Click);
            // 
            // backupToolStripMenuItem
            // 
            this.backupToolStripMenuItem.Name = "backupToolStripMenuItem";
            this.backupToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.backupToolStripMenuItem.Text = "Backup";
            this.backupToolStripMenuItem.Click += new System.EventHandler(this.backupToolStripMenuItem_Click_1);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.restoreToolStripMenuItem_Click_1);
            // 
            // cifrarDescifrarToolStripMenuItem
            // 
            this.cifrarDescifrarToolStripMenuItem.Name = "cifrarDescifrarToolStripMenuItem";
            this.cifrarDescifrarToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.cifrarDescifrarToolStripMenuItem.Text = "Cifrar - Descifrar";
            this.cifrarDescifrarToolStripMenuItem.Click += new System.EventHandler(this.cifrarDescifrarToolStripMenuItem_Click);
            // 
            // contenedoresToolStripMenuItem
            // 
            this.contenedoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionarContenedoresToolStripMenuItem,
            this.simularContenedorFisicoToolStripMenuItem});
            this.contenedoresToolStripMenuItem.Name = "contenedoresToolStripMenuItem";
            this.contenedoresToolStripMenuItem.Size = new System.Drawing.Size(93, 20);
            this.contenedoresToolStripMenuItem.Text = "Contenedores";
            this.contenedoresToolStripMenuItem.Click += new System.EventHandler(this.contenedoresToolStripMenuItem_Click);
            // 
            // gestionarContenedoresToolStripMenuItem
            // 
            this.gestionarContenedoresToolStripMenuItem.Name = "gestionarContenedoresToolStripMenuItem";
            this.gestionarContenedoresToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.gestionarContenedoresToolStripMenuItem.Text = "Gestionar contenedores";
            this.gestionarContenedoresToolStripMenuItem.Click += new System.EventHandler(this.gestionarContenedoresToolStripMenuItem_Click);
            // 
            // simularContenedorFisicoToolStripMenuItem
            // 
            this.simularContenedorFisicoToolStripMenuItem.Name = "simularContenedorFisicoToolStripMenuItem";
            this.simularContenedorFisicoToolStripMenuItem.Size = new System.Drawing.Size(213, 22);
            this.simularContenedorFisicoToolStripMenuItem.Text = "Simular Contenedor Fisico";
            this.simularContenedorFisicoToolStripMenuItem.Click += new System.EventHandler(this.simularContenedorFisicoToolStripMenuItem_Click);
            // 
            // RutasToolStripMenuItem
            // 
            this.RutasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionarRutasToolStripMenuItem});
            this.RutasToolStripMenuItem.Name = "RutasToolStripMenuItem";
            this.RutasToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.RutasToolStripMenuItem.Text = "Rutas";
            this.RutasToolStripMenuItem.Click += new System.EventHandler(this.gestionDeRutasToolStripMenuItem_Click);
            // 
            // gestionarRutasToolStripMenuItem
            // 
            this.gestionarRutasToolStripMenuItem.Name = "gestionarRutasToolStripMenuItem";
            this.gestionarRutasToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.gestionarRutasToolStripMenuItem.Text = "Gestionar Rutas";
            this.gestionarRutasToolStripMenuItem.Click += new System.EventHandler(this.gestionarRutasToolStripMenuItem_Click);
            // 
            // recoleccionToolStripMenuItem
            // 
            this.recoleccionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RecorridosToolStripMenuItem});
            this.recoleccionToolStripMenuItem.Name = "recoleccionToolStripMenuItem";
            this.recoleccionToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.recoleccionToolStripMenuItem.Text = "Recoleccion";
            // 
            // RecorridosToolStripMenuItem
            // 
            this.RecorridosToolStripMenuItem.Name = "RecorridosToolStripMenuItem";
            this.RecorridosToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.RecorridosToolStripMenuItem.Text = "Recorridos";
            this.RecorridosToolStripMenuItem.Click += new System.EventHandler(this.misRecorridosToolStripMenuItem_Click);
            // 
            // contenedoresToolStripMenuItem1
            // 
            this.contenedoresToolStripMenuItem1.Name = "contenedoresToolStripMenuItem1";
            this.contenedoresToolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\RecolectAR\\UI\\user_manual.chm\\about.htm";
            // 
            // FRMPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1022, 651);
            this.Controls.Add(this.menuStrip);
            this.HelpButton = true;
            this.helpProvider1.SetHelpKeyword(this, "");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.helpProvider1.SetHelpString(this, "");
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "FRMPrincipal";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem seguridadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bitacoraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sesionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iniciarSesionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarContrasenaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cambiarIdiomaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RutasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contenedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarContenedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cifrarDescifrarToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem gestionarUsuariosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarPuestosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        ////private System.Windows.Forms.ToolStripMenuItem contenedoresToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem gestionarRutasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem contenedoresToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem recoleccionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RecorridosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem simularContenedorFisicoToolStripMenuItem;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}



