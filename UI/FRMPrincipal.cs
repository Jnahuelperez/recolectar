﻿using System;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using System.Windows.Forms;
using BL;
using System.Collections.Generic;
using UI.Sesion;
using UI.Seguridad;
using System.Configuration;

namespace UI
{
    public partial class FRMPrincipal : Form
    {
        internal Usuario mUser = new Usuario();
        private int childFormNumber = 0;
        internal Familia mFamilia = new Familia();
        internal List<Patente> mPatentes = new List<Patente>();
        internal List<Familia> mFamilias = new List<Familia>();
        List<ToolStripMenuItem> allItems = new List<ToolStripMenuItem>();
        //internal Usuario mUsuarioActivo = new Usuario();

        public FRMPrincipal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        internal void Principal_Load(object sender, EventArgs e)
        {
            Deshabilitarcontroles();
            foreach (Familia mF in mFamilias)
            { 
                foreach(Patente mP in mF.GetPatentesFamilia(mF))
                {
                    mPatentes.Add(mP);
                }
            }
            habilitarControles();
            iniciarSesionToolStripMenuItem.Enabled = false;
            CargarIdiomaMenues(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }
        internal void CargarIdiomaMenues(List<Idioma.Controles> pControles)
        {
            foreach (ToolStripMenuItem C in this.menuStrip.Items)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                    foreach (ToolStripItem mT in C.DropDownItems)
                        if (mIC.objeto == mT.Name)
                            mT.Text = mIC.valor;
                }
            }
        }
        private void Deshabilitarcontroles()
        {
            foreach (ToolStripMenuItem mToolStrip in this.menuStrip.Items)
            {
                foreach(ToolStripItem mT in mToolStrip.DropDownItems)
                    mT.Enabled = false;
            }
        }

        private void habilitarControles()
        {
            foreach (ToolStripMenuItem mToolStrip in this.menuStrip.Items)
            {
                foreach (ToolStripItem mT in mToolStrip.DropDownItems)
                    foreach (Patente mP in mPatentes)
                        if (mP.detalle + "ToolStripMenuItem" == mT.Name)
                            mT.Enabled = true;
            }
        }

        #region ToolStrip

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void FamiliaPatenteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void RecuperarContraseñaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMCambiarPWD mForm = new FRMCambiarPWD();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
            CargarIdiomaMenues(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void BitacoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMBitacora mForm = new FRMBitacora();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void CrearCopiaRespaldoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void RestaurarCopiaRespaldoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void ModificarIdiomaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMIdioma mForm = new FRMIdioma();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void iniciarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMLogin mLogin = new FRMLogin();
            mLogin.Show();
        }

        private void seguridadToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void datosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cambiarIdiomaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMIdioma mForm = new FRMIdioma(this);
            //mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void cerrarSesionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult mResult = MessageBox.Show(Idioma.GetMSGLanguaje("MSGLogoutConfirm",Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("MSGConfirmAction", Login.UsuarioLogeado.UserIdioma), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(mResult == DialogResult.Yes)
            {
                Login mlogin = new Login();
                mlogin.Logout(Login.UsuarioLogeado);
                this.Close();
            }
        }

        private void backupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionarUsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LISTusuarios mForm = new LISTusuarios();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void backupRestoreToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void backupToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FRMBackup mForm = new FRMBackup();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void restoreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMRestoreDB mForm = new FRMRestoreDB();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void gestionDeRutasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void misPedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cifrarDescifrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMCIFRAdo mForm = new FRMCIFRAdo();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        #endregion

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }

        private void gestionarPuestosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LISTPuesto mForm = new LISTPuesto();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void gestionarUsuariosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            LISTusuarios mForm = new LISTusuarios();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void contenedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionarRutasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LISTRutas mForm = new LISTRutas();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void asignacionDeRutasToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionarContenedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LISTCONTENEDOR mForm = new LISTCONTENEDOR();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void misRecorridosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMRecorridos mForm = new FRMRecorridos();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void gestionarPedidosDeRevisionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LSTPedidos mForm = new LSTPedidos();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void backupToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FRMBackup mForm = new FRMBackup();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void restoreToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            FRMRestoreDB mForm = new FRMRestoreDB();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();

        }

        private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void admEmpleadosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FRMPrincipal_Click(object sender, EventArgs e)
        {
            CargarIdiomaMenues(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void FRMPrincipal_MdiChildActivate(object sender, EventArgs e)
        {
            CargarIdiomaMenues(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void FRMPrincipal_Layout(object sender, LayoutEventArgs e)
        {
            CargarIdiomaMenues(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void simularContenedorFisicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FRMContenedor mForm = new FRMContenedor();
            mForm.MdiParent = this;
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }
    }
}
