﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class LSTPedidos : Form
    {
        public LSTPedidos()
        {
            InitializeComponent();
        }

        private void LSTPedidos_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvPedidos.Columns.Add("ID", "ID");
            dgvPedidos.Columns.Add("Tipo", "Tipo");
            dgvPedidos.Columns.Add("Contenedor", "Contenedor");
            dgvPedidos.Columns.Add("Fecha alta", "Fecha alta");
            dgvPedidos.Columns.Add("Estado", "Estado");
            dgvPedidos.Columns.Add("Asignado", "Asignado");
            dgvPedidos.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvPedidos.AllowUserToAddRows = false;
            dgvPedidos.AllowUserToDeleteRows = false;
            dgvPedidos.AllowUserToResizeColumns = false;
            dgvPedidos.AllowUserToResizeRows = false;
            dgvPedidos.RowHeadersVisible = false;
            dgvPedidos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPedidos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvPedidos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvPedidos.RowTemplate.Height = 30;

            dgvPedidos.Rows.Add("1", "Desinfeccion", "CONT001", "24/06/2020", "Abierto", "NO");
            dgvPedidos.Rows.Add("1", "Bloqueado", "CONT001", "24/06/2020", "Abierto", "NPerez");
            dgvPedidos.Rows.Add("1", "Otro", "CONT001", "24/06/2020", "Abierto", "NO");
            dgvPedidos.Rows.Add("1", "Bloqueado", "CONT001", "24/06/2020", "Abierto", "CRoig");
            dgvPedidos.Rows.Add("1", "Otro", "CONT001", "24/06/2020", "Abierto", "DEspinoza");
            dgvPedidos.Rows.Add("1", "Desinfeccion", "CONT001", "24/06/2020", "Abierto", "AGomez");
            dgvPedidos.Rows.Add("1", "Desinfeccion", "CONT001", "24/06/2020", "Abierto", "SAversa");
            dgvPedidos.Rows.Add("1", "Bloqueado", "CONT001", "24/06/2020", "Abierto", "NO");
            dgvPedidos.Rows.Add("1", "Desinfeccion", "CONT001", "24/06/2020", "Abierto", "SLopez");
            dgvPedidos.Rows.Add("1", "Otro", "CONT001", "24/06/2020", "Abierto", "NO");
            #endregion
        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            FRMPedRevision mForm = new FRMPedRevision();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();

        }
    }
}
