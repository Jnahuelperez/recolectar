﻿using SERV;
using System;
using System.Windows.Forms;
using UI.Seguridad;
using UI.Sesion;

namespace UI
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FRMInicio());
        }
    }
}
