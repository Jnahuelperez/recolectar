﻿namespace UI
{
    partial class FRMMisRecorridos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCont = new System.Windows.Forms.DataGridView();
            this.btnNotifRecol = new System.Windows.Forms.Button();
            this.grpContenedores = new System.Windows.Forms.GroupBox();
            this.lblDistancia = new System.Windows.Forms.Label();
            this.btnCrearPedRev = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.btnFinalizar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCont)).BeginInit();
            this.grpContenedores.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCont
            // 
            this.dgvCont.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCont.Location = new System.Drawing.Point(13, 51);
            this.dgvCont.Margin = new System.Windows.Forms.Padding(2);
            this.dgvCont.Name = "dgvCont";
            this.dgvCont.RowHeadersWidth = 82;
            this.dgvCont.RowTemplate.Height = 33;
            this.dgvCont.Size = new System.Drawing.Size(226, 411);
            this.dgvCont.TabIndex = 0;
            // 
            // btnNotifRecol
            // 
            this.btnNotifRecol.Location = new System.Drawing.Point(13, 465);
            this.btnNotifRecol.Margin = new System.Windows.Forms.Padding(2);
            this.btnNotifRecol.Name = "btnNotifRecol";
            this.btnNotifRecol.Size = new System.Drawing.Size(101, 61);
            this.btnNotifRecol.TabIndex = 2;
            this.btnNotifRecol.Text = "Notificar Recoleccion";
            this.btnNotifRecol.UseVisualStyleBackColor = true;
            this.btnNotifRecol.Click += new System.EventHandler(this.button1_Click);
            // 
            // grpContenedores
            // 
            this.grpContenedores.Controls.Add(this.lblDistancia);
            this.grpContenedores.Controls.Add(this.btnCrearPedRev);
            this.grpContenedores.Controls.Add(this.dgvCont);
            this.grpContenedores.Controls.Add(this.btnNotifRecol);
            this.grpContenedores.Location = new System.Drawing.Point(12, 77);
            this.grpContenedores.Margin = new System.Windows.Forms.Padding(2);
            this.grpContenedores.Name = "grpContenedores";
            this.grpContenedores.Padding = new System.Windows.Forms.Padding(2);
            this.grpContenedores.Size = new System.Drawing.Size(248, 530);
            this.grpContenedores.TabIndex = 4;
            this.grpContenedores.TabStop = false;
            this.grpContenedores.Text = "Contenedores del recorrido";
            // 
            // lblDistancia
            // 
            this.lblDistancia.AutoSize = true;
            this.lblDistancia.Location = new System.Drawing.Point(10, 25);
            this.lblDistancia.Name = "lblDistancia";
            this.lblDistancia.Size = new System.Drawing.Size(57, 13);
            this.lblDistancia.TabIndex = 4;
            this.lblDistancia.Text = "Distancia: ";
            // 
            // btnCrearPedRev
            // 
            this.btnCrearPedRev.Location = new System.Drawing.Point(135, 466);
            this.btnCrearPedRev.Margin = new System.Windows.Forms.Padding(2);
            this.btnCrearPedRev.Name = "btnCrearPedRev";
            this.btnCrearPedRev.Size = new System.Drawing.Size(104, 60);
            this.btnCrearPedRev.TabIndex = 3;
            this.btnCrearPedRev.Text = "Crear pedido de revision";
            this.btnCrearPedRev.UseVisualStyleBackColor = true;
            this.btnCrearPedRev.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnIniciar);
            this.groupBox1.Controls.Add(this.btnFinalizar);
            this.groupBox1.Location = new System.Drawing.Point(9, 7);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(251, 53);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recorrido";
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(3, 16);
            this.btnIniciar.Margin = new System.Windows.Forms.Padding(2);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(114, 25);
            this.btnIniciar.TabIndex = 5;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // btnFinalizar
            // 
            this.btnFinalizar.Location = new System.Drawing.Point(128, 16);
            this.btnFinalizar.Margin = new System.Windows.Forms.Padding(2);
            this.btnFinalizar.Name = "btnFinalizar";
            this.btnFinalizar.Size = new System.Drawing.Size(114, 25);
            this.btnFinalizar.TabIndex = 4;
            this.btnFinalizar.Text = "Finalizar";
            this.btnFinalizar.UseVisualStyleBackColor = true;
            // 
            // FRMMisRecorridos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 618);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grpContenedores);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FRMMisRecorridos";
            this.Text = "FRMMisRecorridos";
            this.Load += new System.EventHandler(this.FRMMisRecorridos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCont)).EndInit();
            this.grpContenedores.ResumeLayout(false);
            this.grpContenedores.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCont;
        private System.Windows.Forms.Button btnNotifRecol;
        private System.Windows.Forms.GroupBox grpContenedores;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Button btnFinalizar;
        private System.Windows.Forms.Button btnCrearPedRev;
        private System.Windows.Forms.Label lblDistancia;
    }
}