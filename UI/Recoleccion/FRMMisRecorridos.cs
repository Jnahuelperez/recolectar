﻿using BL;
using Org.BouncyCastle.Crypto.Engines;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using GoogleApi.Entities.Common;
using GoogleApi.Entities.Maps.Directions.Request;
using GoogleApi.Entities.Maps.Directions.Response;
using GMap.NET.MapProviders;
using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;
using GMap.NET.ObjectModel;

namespace UI
{
    public partial class FRMMisRecorridos : Form
    {
        internal Ruta mRuta = new Ruta();
        internal Scoring mScoBL = new Scoring();
        public FRMMisRecorridos()
        {
            InitializeComponent();
        }

        private void FRMMisRecorridos_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvCont.Columns.Add("ID", "ID");
            dgvCont.Columns.Add("Contenedor", "Contenedor");
            dgvCont.Columns.Add("Estado", "Estado");

            dgvCont.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvCont.AllowUserToAddRows = false;
            dgvCont.AllowUserToDeleteRows = false;
            dgvCont.AllowUserToResizeColumns = false;
            dgvCont.AllowUserToResizeRows = false;
            dgvCont.RowHeadersVisible = false;
            dgvCont.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvCont.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvCont.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvCont.RowTemplate.Height = 30;
            #endregion

            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("FRMMisRecorridos", Login.UsuarioLogeado.UserIdioma);
        }

        //private void DibujarMapa()
        //{
        //    GMapProviders.GoogleMap.ApiKey = "AIzaSyDumfrErD3hk8OeOZbjee5-QGHpy1-t18A";
        //    GMaps.Instance.Mode = AccessMode.ServerOnly;
        //    double DuracionRuta = 0;
        //    double Lat;
        //    double Long;
        //    //Personalizacion del mapa
        //    gmap.MapProvider = GMapProviders.BingMap;
        //    gmap.Zoom = 20;
        //    gmap.MinZoom = 5;
        //    gmap.MaxZoom = 90;
        //    gmap.DragButton = MouseButtons.Left;
        //    gmap.Position = new GMap.NET.PointLatLng(-34.607220, -58.445662);
        //    gmap.ShowCenter = false;
        //    gmap.Zoom = gmap.Zoom + 1;
        //    gmap.Zoom = gmap.Zoom - 1;

        //    Lista de puntos a agregar al mapa
        //    GMapOverlay Marcadores = new GMapOverlay("Marcadores");
        //    List<PointLatLng> ListaPuntos = new List<PointLatLng>();
        //    Obtenemos ubicacion de cada contenedor, creamos el marcador y lo agrwgamos a la lista
        //    foreach (Contenedor mC in mRuta.ListaContenedores)
        //    {
        //        Lat = Convert.ToDouble(mC.Ubicacion.Split(',')[0], System.Globalization.CultureInfo.InvariantCulture);
        //        Long = Convert.ToDouble(mC.Ubicacion.Split(',')[1], System.Globalization.CultureInfo.InvariantCulture);
        //        ListaPuntos.Add(new PointLatLng(Lat, Long));
        //        GMapMarker mMarcador = new GMarkerGoogle(
        //            new PointLatLng(Lat, Long),
        //            GMarkerGoogleType.green_dot)
        //        { ToolTipText = "Cod: " + mC.Detalle + "\n" + "Peso: " + mC.Peso + "\n" + "ID: " + mC.id };
        //        Marcadores.Markers.Add(mMarcador);
        //    }
        //    GMapRoute route = new GMapRoute(ListaPuntos, "Recorrido");
        //    GMapOverlay routesOverlay = new GMapOverlay("Myroutes");
        //    route.Stroke.Color = Color.Red;
        //    route.Stroke.Width = 5;
        //    routesOverlay.Routes.Add(route);

        //    gmap.Overlays.Add(Marcadores);
        //    gmap.UpdateRouteLocalPosition(route);

        //}
        private void button2_Click(object sender, EventArgs e)
        {
            FRMPedRevision mForm = new FRMPedRevision();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }

        private void ActualizarGrilla()
        {
            dgvCont.Rows.Clear();
            foreach (Contenedor mC in mRuta.ListaContenedores)
            {
                dgvCont.Rows.Add(mC.id,mC.Detalle,mC.Estado);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Contenedor mC = new Contenedor();

        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            Scoring mS = new Scoring();
            mRuta.Estado = BL.Constantes.EstadoRuta.En_curso.ToString();
            mRuta.Modificar(mRuta);
            mS.FechaComienzo = DateTime.Now;
            mS.idRuta = mRuta.id;
            mS.idUsuario = Login.UsuarioLogeado.id;
            mS.idZona = mRuta.idZona;
            mS.TiempoRecCalculado = 180;
            mScoBL.Crear(mS);
        }

        private void elementHost1_ChildChanged(object sender, System.Windows.Forms.Integration.ChildChangedEventArgs e)
        {

        }
    }
}
