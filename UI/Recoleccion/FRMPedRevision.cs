﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class FRMPedRevision : Form
    {
        public FRMPedRevision()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void FRMPedRevision_Load(object sender, EventArgs e)
        {
            cmbEstado.Items.Add("Abierto");
            cmbEstado.Enabled = false;
            cmbEstado.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbTipo.Items.Add("Atascado");
            cmbTipo.Items.Add("Desinfeccion");
            cmbTipo.Items.Add("Otro");

            #region Formato Grilla
            dgvRevisiones.Columns.Add("ID", "ID");
            dgvRevisiones.Columns.Add("Operador", "Operador");
            dgvRevisiones.Columns.Add("Fecha", "Fecha");

            dgvRevisiones.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvRevisiones.AllowUserToAddRows = false;
            dgvRevisiones.AllowUserToDeleteRows = false;
            dgvRevisiones.AllowUserToResizeColumns = false;
            dgvRevisiones.AllowUserToResizeRows = false;
            dgvRevisiones.RowHeadersVisible = false;
            dgvRevisiones.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvRevisiones.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvRevisiones.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvRevisiones.RowTemplate.Height = 30;
            #endregion
        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            FRMAddRevision mForm = new FRMAddRevision();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.Show();
        }
    }
}
