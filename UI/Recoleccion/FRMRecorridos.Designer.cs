﻿namespace UI
{
    partial class FRMRecorridos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRecorridos = new System.Windows.Forms.DataGridView();
            this.btnAcceder = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecorridos)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvRecorridos
            // 
            this.dgvRecorridos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRecorridos.Location = new System.Drawing.Point(6, 6);
            this.dgvRecorridos.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.dgvRecorridos.Name = "dgvRecorridos";
            this.dgvRecorridos.RowHeadersWidth = 82;
            this.dgvRecorridos.RowTemplate.Height = 33;
            this.dgvRecorridos.Size = new System.Drawing.Size(388, 176);
            this.dgvRecorridos.TabIndex = 0;
            // 
            // btnAcceder
            // 
            this.btnAcceder.Location = new System.Drawing.Point(6, 190);
            this.btnAcceder.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnAcceder.Name = "btnAcceder";
            this.btnAcceder.Size = new System.Drawing.Size(62, 21);
            this.btnAcceder.TabIndex = 1;
            this.btnAcceder.Text = "Acceder";
            this.btnAcceder.UseVisualStyleBackColor = true;
            this.btnAcceder.Click += new System.EventHandler(this.button1_Click);
            // 
            // FRMRecorridos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 218);
            this.Controls.Add(this.btnAcceder);
            this.Controls.Add(this.dgvRecorridos);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FRMRecorridos";
            this.Text = "Recorridos";
            this.Load += new System.EventHandler(this.FRMRecorridos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRecorridos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRecorridos;
        private System.Windows.Forms.Button btnAcceder;
    }
}