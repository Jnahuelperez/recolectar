﻿using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class FRMRecorridos : Form
    {
        Ruta mRutaBL = new Ruta();
        public FRMRecorridos()
        {
            InitializeComponent();
        }

        private void FRMRecorridos_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvRecorridos.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvRecorridos.Columns.Add(GetDGVColumns("dgvRuta"), GetDGVColumns("dgvRuta"));
            dgvRecorridos.Columns.Add(GetDGVColumns("dgvDistancia"), GetDGVColumns("dgvDistancia"));
            dgvRecorridos.Columns.Add(GetDGVColumns("dgvContenedores"), GetDGVColumns("dgvContenedores"));
            dgvRecorridos.Columns.Add(GetDGVColumns("dgvEstado"), GetDGVColumns("dgvEstado"));

            dgvRecorridos.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvRecorridos.AllowUserToAddRows = false;
            dgvRecorridos.AllowUserToDeleteRows = false;
            dgvRecorridos.AllowUserToResizeColumns = false;
            dgvRecorridos.AllowUserToResizeRows = false;
            dgvRecorridos.RowHeadersVisible = false;
            dgvRecorridos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvRecorridos.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvRecorridos.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvRecorridos.RowTemplate.Height = 30;
            #endregion

            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("FRMRecorridos", Login.UsuarioLogeado.UserIdioma);
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Ruta mR = new Ruta();
            FRMMisRecorridos mForm = new FRMMisRecorridos();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mR.id = int.Parse(dgvRecorridos.SelectedRows[0].Cells[0].Value.ToString());
            mForm.mRuta = mRutaBL.Obtener(mR);
            mForm.Show();
        }

        private void ActualizarGrilla()
        {
            mRutaBL.idUsuarioRuta = Login.UsuarioLogeado.id;
            dgvRecorridos.Rows.Clear();
            foreach(Ruta mR in mRutaBL.GetRutasAsigByUser(mRutaBL))
            {
                if(mR.idUsuarioRuta == Login.UsuarioLogeado.id)
                    dgvRecorridos.Rows.Add(mR.id, mR.Detalle, mR.Longitud, mR.ListaContenedores.Count, mR.Estado);
            }
        }
    }
}
