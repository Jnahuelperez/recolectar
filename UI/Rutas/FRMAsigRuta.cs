﻿using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class FRMAsigRuta : Form
    {
        internal Constantes.TipoDeOperacion Operacion { get; set; }
        public Ruta mRuta = new Ruta();
        public Usuario mUsuBL = new Usuario();
        public FRMAsigRuta()
        {
            InitializeComponent();
        }

        private void FRMAsigRuta_Load(object sender, EventArgs e)
        {
            txtRuta.Enabled = false;
            foreach (Usuario mU in mUsuBL.Listar())
                cmbUsuarios.Items.Add(mU.user + " - " + mU.Apellido + ", " + mU.Nombre);
            cmbUsuarios.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbUsuarios.SelectedIndex = 0;
            InicializarForm();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));

        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private void InicializarForm()
        {
            txtRuta.Text = mRuta.Detalle;
            if (Operacion == Constantes.TipoDeOperacion.Baja)
            {
                cmbUsuarios.SelectedIndex = mRuta.idUsuarioRuta;
                cmbUsuarios.Enabled = false;
            }

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string[] mNombreUsuario = cmbUsuarios.SelectedItem.ToString().Split(' ');
            Usuario mU = new Usuario();
            mU.user = mNombreUsuario[0];
            try
            {
                mU = mUsuBL.Obtener(mU);
                if (Operacion == Constantes.TipoDeOperacion.Alta)
                    mRuta.AsignarRuta(mRuta, mU);
                if (Operacion == Constantes.TipoDeOperacion.Baja)
                    mRuta.DesAsignarRuta(mRuta);
                MessageBox.Show(
                    Idioma.GetMSGLanguaje("MSGOperComEx", Login.UsuarioLogeado.UserIdioma), 
                    Idioma.GetMSGLanguaje("MSGSysMsg", Login.UsuarioLogeado.UserIdioma), 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);
            } 
            catch(Exception ex) { 
                MessageBox.Show(ex.Message,
                    Idioma.GetMSGLanguaje("MSGSysMsg", Login.UsuarioLogeado.UserIdioma), 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Error); }
            this.Close();
        }
    }
}
