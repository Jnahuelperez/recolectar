﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace UI
{
    public partial class FRMRutas : Form
    {
        internal Constantes.TipoDeOperacion Operacion { get; set; }
        Contenedor mContenedorBL = new BL.Contenedor();
        Ruta mRutaBL = new Ruta();
        public Ruta mRutaAEditar = new Ruta();
        public FRMRutas()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }



        private void FRMRutas_Load(object sender, EventArgs e)
        {
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            txtid.Enabled = false;
            #region Grilla
            dgvContenedores.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvDetalle"), GetDGVColumns("dgvDetalle"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvUbic"), GetDGVColumns("dgvUbic"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvContam"), GetDGVColumns("dgvContam"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvPeso"), GetDGVColumns("dgvPeso"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvEstado"), GetDGVColumns("dgvEstado"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvEstado"), GetDGVColumns("dgvEstado"));
            dgvContenedores.Columns.Add(GetDGVColumns("dgvInusual"), GetDGVColumns("dgvInusual"));

            dgvContenedores.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvContenedores.AllowUserToAddRows = false;
            dgvContenedores.AllowUserToDeleteRows = false;
            dgvContenedores.AllowUserToResizeRows = false;
            dgvContenedores.RowHeadersVisible = false;
            dgvContenedores.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvContenedores.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvContenedores.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvContenedores.RowTemplate.Height = 30;
            #endregion

            //Contenedores
            foreach (Contenedor mC in mContenedorBL.Listar())
                cmbContenedor.Items.Add(mC.Detalle);
            cmbContenedor.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbContenedor.SelectedIndex = 0;

            //Zonas
            foreach (Zona mZ in Zona.ObtenerZonas())
                cmbZona.Items.Add(mZ.Detalle);
            cmbZona.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbZona.SelectedIndex = 0;

            //Criticidad
            string[] Criticidad = { "Alto", "Medio", "Bajo" };
            foreach (string mS in Criticidad)
                cmbCriticidad.Items.Add(mS);
            cmbCriticidad.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbCriticidad.SelectedIndex = 0;

            //Estado
            string[] Estados = {"Pendiente","En Curso","Finalizada"};
            foreach (string mS in Estados)
                cmbEstado.Items.Add(mS);
            cmbEstado.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbEstado.SelectedIndex = 0;

            //Aux
            txtCodigo.Text = "";
            txtLong.Text = "10000";

            InicializarPagina();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void LoadContenedores()
        {
            mRutaAEditar.ListaContenedores = mContenedorBL.GetContByRute(mRutaAEditar.id);
            dgvContenedores.Rows.Clear();
            foreach (Contenedor mC in mRutaAEditar.ListaContenedores)
                dgvContenedores.Rows.Add(mC.id, mC.Detalle, mC.Ubicacion, mC.IndiceContaminacion, mC.Peso, mC.Estado, mC.Anegado, mC.Inusual);
        }

        private void InicializarPagina()
        {
            switch (Operacion)
            {
                case Constantes.TipoDeOperacion.Alta:
                    break;
                case Constantes.TipoDeOperacion.Modificacion:
                    LoadObjetos();
                    LoadContenedores();
                    break;
                case Constantes.TipoDeOperacion.Baja:
                    LoadObjetos();
                    LoadContenedores();
                    txtid.Enabled = false;
                    txtCodigo.Enabled = false;
                    cmbContenedor.Enabled = false;
                    cmbCriticidad.Enabled = false;
                    btnQuitar.Enabled = false;
                    btnAgregar.Enabled = false;
                    txtLong.Enabled = false;
                    break;
                case Constantes.TipoDeOperacion.Consulta:
                    LoadObjetos();
                    LoadContenedores();
                    txtid.Enabled = false;
                    txtCodigo.Enabled = false;
                    cmbContenedor.Enabled = false;
                    cmbCriticidad.Enabled = false;
                    txtLong.Enabled = false;
                    btnQuitar.Enabled = false;
                    chkActivo.Enabled = false;
                    btnAgregar.Enabled = false;
                    break;
            }
        }

        private void LoadObjetos()
        {
            txtid.Text = mRutaAEditar.id.ToString();
            txtCodigo.Text = mRutaAEditar.Detalle;
            if (mRutaAEditar.Activo)
                chkActivo.Checked = true;
            cmbZona.SelectedIndex = mRutaAEditar.idZona;
            cmbCriticidad.SelectedItem = mRutaAEditar.Criticidad;
            txtLong.Text = mRutaAEditar.Longitud.ToString();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                switch (Operacion)
                {
                    case Constantes.TipoDeOperacion.Alta:
                        this.Close();
                        ValorizarEntidad();
                        ValorizarContenedores();
                        mRutaBL.Crear(mRutaAEditar);
                        break;
                    case Constantes.TipoDeOperacion.Modificacion:
                        ValorizarEntidad();
                        ValorizarContenedores();
                        mRutaBL.Modificar(mRutaAEditar);
                        this.Close();
                        break;
                    case Constantes.TipoDeOperacion.Baja:
                        ValorizarEntidad();
                        ValorizarContenedores();
                        DialogResult Alerta = MessageBox.Show(Idioma.GetMSGLanguaje("MSGConfirmAction", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("TitleConfirm", Login.UsuarioLogeado.UserIdioma), MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (Alerta == DialogResult.Yes)
                        {
                            mRutaBL.Baja(mRutaAEditar);
                        }
                        else
                        {
                            MessageBox.Show(
                                Idioma.GetMSGLanguaje("MSGOperCancelada", Login.UsuarioLogeado.UserIdioma),
                                Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        this.Close();
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    Idioma.GetMSGLanguaje("MSGErrorOnRuntime", Login.UsuarioLogeado.UserIdioma) + "\n" + ex.Message,
                    Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void ValorizarContenedores()
        {
            List<Contenedor> mContenedores = new List<Contenedor>();
            foreach (DataGridViewRow mRow in dgvContenedores.Rows)
            {
                Contenedor mC = new Contenedor();
                mC.id = int.Parse(mRow.Cells[0].Value.ToString());
                mContenedores.Add(mC);
            }
            mRutaAEditar.ListaContenedores = mContenedores;
        }

        private void ValorizarEntidad()
        {
            if (Operacion != Constantes.TipoDeOperacion.Alta)
            {
                mRutaAEditar.id = int.Parse(txtid.Text);
                if (chkActivo.Checked)
                    mRutaAEditar.Activo = true;
                else
                    mRutaAEditar.Activo = false;
                mRutaAEditar.Estado = cmbEstado.SelectedItem.ToString();
            }
            mRutaAEditar.Detalle = txtCodigo.Text;
            mRutaAEditar.Criticidad = cmbCriticidad.SelectedItem.ToString();
            mRutaAEditar.Longitud = int.Parse(txtLong.Text);
            mRutaAEditar.idZona = cmbZona.SelectedIndex + 1;
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAddCont_Click(object sender, EventArgs e)
        {
            Contenedor mC = new Contenedor();
            mC.Detalle = cmbContenedor.SelectedItem.ToString();
            mC = mContenedorBL.ObtenerByName(mC.Detalle);
            dgvContenedores.Rows.Add(mC.id, mC.Detalle, mC.Ubicacion, mC.IndiceContaminacion, 
                mC.Peso, mC.Estado, mC.Anegado, mC.Inusual);

        }

        private void btnDelCont_Click(object sender, EventArgs e)
        {
            if(dgvContenedores.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow mRow in dgvContenedores.SelectedRows)
                {
                    dgvContenedores.Rows.Remove(mRow);
                }
            }
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }
    }
}
