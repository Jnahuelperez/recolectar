﻿namespace UI
{
    partial class LISTRutas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvRutas = new System.Windows.Forms.DataGridView();
            this.btnCrear = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnVer = new System.Windows.Forms.Button();
            this.btnAsignar = new System.Windows.Forms.Button();
            this.grpABMRutas = new System.Windows.Forms.GroupBox();
            this.grpRecolRutas = new System.Windows.Forms.GroupBox();
            this.btnDesAsignar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRutas)).BeginInit();
            this.grpABMRutas.SuspendLayout();
            this.grpRecolRutas.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvRutas
            // 
            this.dgvRutas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRutas.Location = new System.Drawing.Point(116, 6);
            this.dgvRutas.Margin = new System.Windows.Forms.Padding(2);
            this.dgvRutas.Name = "dgvRutas";
            this.dgvRutas.ReadOnly = true;
            this.dgvRutas.RowHeadersWidth = 82;
            this.dgvRutas.RowTemplate.Height = 33;
            this.dgvRutas.Size = new System.Drawing.Size(408, 209);
            this.dgvRutas.TabIndex = 0;
            // 
            // btnCrear
            // 
            this.btnCrear.Location = new System.Drawing.Point(5, 46);
            this.btnCrear.Margin = new System.Windows.Forms.Padding(2);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(82, 20);
            this.btnCrear.TabIndex = 1;
            this.btnCrear.Text = "Crear";
            this.btnCrear.UseVisualStyleBackColor = true;
            this.btnCrear.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(5, 70);
            this.btnModificar.Margin = new System.Windows.Forms.Padding(2);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(82, 20);
            this.btnModificar.TabIndex = 2;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(5, 94);
            this.btnEliminar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(82, 20);
            this.btnEliminar.TabIndex = 3;
            this.btnEliminar.Text = "Deshabilitar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnVer
            // 
            this.btnVer.Location = new System.Drawing.Point(5, 22);
            this.btnVer.Margin = new System.Windows.Forms.Padding(2);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(82, 20);
            this.btnVer.TabIndex = 4;
            this.btnVer.Text = "Ver";
            this.btnVer.UseVisualStyleBackColor = true;
            this.btnVer.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnAsignar
            // 
            this.btnAsignar.Location = new System.Drawing.Point(5, 25);
            this.btnAsignar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAsignar.Name = "btnAsignar";
            this.btnAsignar.Size = new System.Drawing.Size(82, 20);
            this.btnAsignar.TabIndex = 5;
            this.btnAsignar.Text = "Asignar";
            this.btnAsignar.UseVisualStyleBackColor = true;
            this.btnAsignar.UseWaitCursor = true;
            this.btnAsignar.Click += new System.EventHandler(this.btnAsignar_Click);
            // 
            // grpABMRutas
            // 
            this.grpABMRutas.Controls.Add(this.btnVer);
            this.grpABMRutas.Controls.Add(this.btnCrear);
            this.grpABMRutas.Controls.Add(this.btnModificar);
            this.grpABMRutas.Controls.Add(this.btnEliminar);
            this.grpABMRutas.Location = new System.Drawing.Point(6, 7);
            this.grpABMRutas.Margin = new System.Windows.Forms.Padding(2);
            this.grpABMRutas.Name = "grpABMRutas";
            this.grpABMRutas.Padding = new System.Windows.Forms.Padding(2);
            this.grpABMRutas.Size = new System.Drawing.Size(100, 121);
            this.grpABMRutas.TabIndex = 6;
            this.grpABMRutas.TabStop = false;
            this.grpABMRutas.Text = "ABM Rutas";
            // 
            // grpRecolRutas
            // 
            this.grpRecolRutas.Controls.Add(this.btnDesAsignar);
            this.grpRecolRutas.Controls.Add(this.btnAsignar);
            this.grpRecolRutas.Location = new System.Drawing.Point(6, 138);
            this.grpRecolRutas.Margin = new System.Windows.Forms.Padding(2);
            this.grpRecolRutas.Name = "grpRecolRutas";
            this.grpRecolRutas.Padding = new System.Windows.Forms.Padding(2);
            this.grpRecolRutas.Size = new System.Drawing.Size(100, 77);
            this.grpRecolRutas.TabIndex = 7;
            this.grpRecolRutas.TabStop = false;
            this.grpRecolRutas.Text = "Recoleccion";
            this.grpRecolRutas.UseWaitCursor = true;
            // 
            // btnDesAsignar
            // 
            this.btnDesAsignar.Location = new System.Drawing.Point(5, 49);
            this.btnDesAsignar.Margin = new System.Windows.Forms.Padding(2);
            this.btnDesAsignar.Name = "btnDesAsignar";
            this.btnDesAsignar.Size = new System.Drawing.Size(82, 20);
            this.btnDesAsignar.TabIndex = 6;
            this.btnDesAsignar.Text = "Desasignar";
            this.btnDesAsignar.UseVisualStyleBackColor = true;
            this.btnDesAsignar.UseWaitCursor = true;
            this.btnDesAsignar.Click += new System.EventHandler(this.button5_Click);
            // 
            // LISTRutas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 223);
            this.Controls.Add(this.grpRecolRutas);
            this.Controls.Add(this.grpABMRutas);
            this.Controls.Add(this.dgvRutas);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "LISTRutas";
            this.Text = "Gestion de Rutas";
            this.Load += new System.EventHandler(this.LISTRutas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRutas)).EndInit();
            this.grpABMRutas.ResumeLayout(false);
            this.grpRecolRutas.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvRutas;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnVer;
        private System.Windows.Forms.Button btnAsignar;
        private System.Windows.Forms.GroupBox grpABMRutas;
        private System.Windows.Forms.GroupBox grpRecolRutas;
        private System.Windows.Forms.Button btnDesAsignar;
    }
}