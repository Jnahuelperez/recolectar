﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace UI
{
    public partial class LISTRutas : Form
    {
        Ruta mRutaBL = new Ruta();
        Contenedor mContenedorBL = new Contenedor();
        //Contenedor
        public LISTRutas()
        {
            InitializeComponent();
        }
        private void LISTRutas_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvRutas.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvRutas.Columns.Add(GetDGVColumns("dgvDetalle"), GetDGVColumns("dgvDetalle"));
            dgvRutas.Columns.Add(GetDGVColumns("dgvZona"), GetDGVColumns("dgvZona"));
            dgvRutas.Columns.Add(GetDGVColumns("dgvContenedores"), GetDGVColumns("dgvContenedores"));
            dgvRutas.Columns.Add(GetDGVColumns("dgvCriticidad"), GetDGVColumns("dgvCriticidad"));
            dgvRutas.Columns.Add(GetDGVColumns("dgvLongitud"), GetDGVColumns("dgvLongitud"));
            dgvRutas.Columns.Add(GetDGVColumns("dgvActivo"), GetDGVColumns("dgvActivo"));

            dgvRutas.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvRutas.AllowUserToAddRows = false;
            dgvRutas.AllowUserToDeleteRows = false;
            dgvRutas.AllowUserToResizeColumns = true;
            dgvRutas.AllowUserToResizeRows = false;
            dgvRutas.RowHeadersVisible = false;
            dgvRutas.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvRutas.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            dgvRutas.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvRutas.RowTemplate.Height = 30;
            #endregion
            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("LISTRutas", Login.UsuarioLogeado.UserIdioma);
        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Baja;
            OpenCRUD(mOperacion);
        }
        private void ActualizarGrilla()
        {
            string mZona;
            dgvRutas.Rows.Clear();
            foreach (Ruta mR in mRutaBL.Listar())
            {
                mZona = Zona.GetZonaById(new Zona() { id = mR.idZona }).Detalle;
                dgvRutas.Rows.Add(mR.id, mR.Detalle, mZona,
                    mContenedorBL.GetContByRute(mR.id).Count(), mR.Criticidad, mR.Longitud, mR.Activo);
            }
        }
        private void OpenCRUD(Constantes.TipoDeOperacion pOperacion)
        {
            Ruta mR = new Ruta();
            FRMRutas mForm = new FRMRutas();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.StartPosition = FormStartPosition.CenterParent;
            mForm.Operacion = pOperacion;

            if(pOperacion != Constantes.TipoDeOperacion.Alta)
            {
                if (dgvRutas.SelectedRows.Count > 0)
                {
                    mR.id = int.Parse(dgvRutas.SelectedRows[0].Cells[0].Value.ToString());
                    mForm.mRutaAEditar = mRutaBL.Obtener(mR);
                }
                else
                    MessageBox.Show("Debe seleccionar un registro", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            mForm.ShowDialog(this);
            ActualizarGrilla();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Modificacion;
            OpenCRUD(mOperacion);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Alta;
            OpenCRUD(mOperacion);
        }
        private void btnAsignar_Click(object sender, EventArgs e)
        {
            if (dgvRutas.SelectedRows.Count > 0)
            {
                FRMAsigRuta mForm = new FRMAsigRuta();
                mForm.MinimizeBox = false;
                mForm.MaximizeBox = false;
                mForm.StartPosition = FormStartPosition.CenterParent;
                mForm.mRuta = mRutaBL.Obtener(new Ruta() { id = int.Parse(dgvRutas.SelectedRows[0].Cells[0].Value.ToString())});
                mForm.Operacion = Constantes.TipoDeOperacion.Alta;
                mForm.ShowDialog(this);
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (dgvRutas.SelectedRows.Count > 0)
            {
                FRMAsigRuta mForm = new FRMAsigRuta();
                mForm.MinimizeBox = false;
                mForm.MaximizeBox = false;
                mForm.StartPosition = FormStartPosition.CenterParent;
                mForm.mRuta = mRutaBL.Obtener(new Ruta() { id = int.Parse(dgvRutas.SelectedRows[0].Cells[0].Value.ToString())});
                mForm.Operacion = Constantes.TipoDeOperacion.Baja;
                mForm.ShowDialog(this);
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Consulta;
            OpenCRUD(mOperacion);
        }
    }
}
