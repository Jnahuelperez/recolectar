﻿namespace UI
{
    partial class CRUDFamilia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvPatentes = new System.Windows.Forms.DataGridView();
            this.grpboxPatente = new System.Windows.Forms.GroupBox();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.cmbPatente = new System.Windows.Forms.ComboBox();
            this.lblFamilia = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.txtFam = new System.Windows.Forms.TextBox();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPatentes)).BeginInit();
            this.grpboxPatente.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvPatentes
            // 
            this.dgvPatentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPatentes.Location = new System.Drawing.Point(4, 36);
            this.dgvPatentes.Margin = new System.Windows.Forms.Padding(2);
            this.dgvPatentes.Name = "dgvPatentes";
            this.dgvPatentes.RowHeadersWidth = 82;
            this.dgvPatentes.RowTemplate.Height = 33;
            this.dgvPatentes.Size = new System.Drawing.Size(298, 169);
            this.dgvPatentes.TabIndex = 0;
            this.dgvPatentes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // grpboxPatente
            // 
            this.grpboxPatente.Controls.Add(this.btnQuitar);
            this.grpboxPatente.Controls.Add(this.btnAgregar);
            this.grpboxPatente.Controls.Add(this.cmbPatente);
            this.grpboxPatente.Controls.Add(this.dgvPatentes);
            this.grpboxPatente.Location = new System.Drawing.Point(14, 50);
            this.grpboxPatente.Margin = new System.Windows.Forms.Padding(2);
            this.grpboxPatente.Name = "grpboxPatente";
            this.grpboxPatente.Padding = new System.Windows.Forms.Padding(2);
            this.grpboxPatente.Size = new System.Drawing.Size(306, 242);
            this.grpboxPatente.TabIndex = 1;
            this.grpboxPatente.TabStop = false;
            this.grpboxPatente.Text = "Patentes";
            // 
            // btnQuitar
            // 
            this.btnQuitar.Location = new System.Drawing.Point(162, 209);
            this.btnQuitar.Margin = new System.Windows.Forms.Padding(2);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(68, 25);
            this.btnQuitar.TabIndex = 4;
            this.btnQuitar.Text = "Quitar";
            this.btnQuitar.UseVisualStyleBackColor = true;
            this.btnQuitar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(61, 209);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(68, 25);
            this.btnAgregar.TabIndex = 3;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // cmbPatente
            // 
            this.cmbPatente.FormattingEnabled = true;
            this.cmbPatente.Location = new System.Drawing.Point(103, 11);
            this.cmbPatente.Margin = new System.Windows.Forms.Padding(2);
            this.cmbPatente.Name = "cmbPatente";
            this.cmbPatente.Size = new System.Drawing.Size(181, 21);
            this.cmbPatente.TabIndex = 1;
            // 
            // lblFamilia
            // 
            this.lblFamilia.AutoSize = true;
            this.lblFamilia.Location = new System.Drawing.Point(11, 9);
            this.lblFamilia.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFamilia.Name = "lblFamilia";
            this.lblFamilia.Size = new System.Drawing.Size(39, 13);
            this.lblFamilia.TabIndex = 2;
            this.lblFamilia.Text = "Familia";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(126, 296);
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(82, 31);
            this.btnAceptar.TabIndex = 5;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // txtFam
            // 
            this.txtFam.Location = new System.Drawing.Point(75, 6);
            this.txtFam.Margin = new System.Windows.Forms.Padding(2);
            this.txtFam.Name = "txtFam";
            this.txtFam.Size = new System.Drawing.Size(245, 20);
            this.txtFam.TabIndex = 6;
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Location = new System.Drawing.Point(242, 31);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(56, 17);
            this.chkActivo.TabIndex = 8;
            this.chkActivo.Text = "Activo";
            this.chkActivo.UseVisualStyleBackColor = true;
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\RecolectAR\\UI\\user_manual.chm";
            // 
            // CRUDFamilia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(331, 332);
            this.Controls.Add(this.chkActivo);
            this.Controls.Add(this.txtFam);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.lblFamilia);
            this.Controls.Add(this.grpboxPatente);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CRUDFamilia";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Administracion de Puestos";
            this.Load += new System.EventHandler(this.CRUDFamilia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPatentes)).EndInit();
            this.grpboxPatente.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvPatentes;
        private System.Windows.Forms.GroupBox grpboxPatente;
        private System.Windows.Forms.ComboBox cmbPatente;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Label lblFamilia;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.TextBox txtFam;
        private System.Windows.Forms.CheckBox chkActivo;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}