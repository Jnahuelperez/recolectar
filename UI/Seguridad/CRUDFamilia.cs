﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace UI
{
    public partial class CRUDFamilia : Form
    {
        #region Propiedades globales
        internal Constantes.TipoDeOperacion Operacion { get; set; }
        internal Familia mFamilia = new Familia();
        internal Patente mPat = new Patente();
        internal List<Patente> mPatentes = new List<Patente>();
        #endregion
        public CRUDFamilia()
        {
            InitializeComponent();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CRUDFamilia_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvPatentes.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvPatentes.Columns.Add(GetDGVColumns("dgvPatente"), GetDGVColumns("dgvPatente"));
            dgvPatentes.Columns["id"].Visible = false;

            dgvPatentes.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvPatentes.AllowUserToAddRows = false;
            dgvPatentes.AllowUserToDeleteRows = false;
            dgvPatentes.AllowUserToResizeColumns = false;
            dgvPatentes.AllowUserToResizeRows = false;
            dgvPatentes.RowHeadersVisible = false;
            dgvPatentes.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPatentes.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvPatentes.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;

            #endregion
            foreach (Patente mP in mPat.Listar())
                cmbPatente.Items.Add(mP.detalle);
            cmbPatente.DropDownStyle = ComboBoxStyle.DropDownList;
            InicializarPagina();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("CRUDFamilia", Login.UsuarioLogeado.UserIdioma);
        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }
        private void InicializarPagina()
        {
            switch (Operacion)
            {
                case Constantes.TipoDeOperacion.Alta:
                    LimpiarCampos();
                    cmbPatente.SelectedItem = 1;
                    btnAceptar.Text = Idioma.GetMSGLanguaje("btnCrear", Login.UsuarioLogeado.UserIdioma);
                    txtFam.Text = "";
                    chkActivo.Checked = true;
                    chkActivo.Enabled = false;
                    break;
                case Constantes.TipoDeOperacion.Modificacion:
                    if (mFamilia == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGFamiliaNula", Login.UsuarioLogeado.UserIdioma));
                        this.Close();
                    }
                    //mFamilia.ListaPatentes = mFamilia.GetPatentesFamilia(mFamilia);
                    CargarCampos(mFamilia);
                    btnAceptar.Text = Idioma.GetMSGLanguaje("btnModificar", Login.UsuarioLogeado.UserIdioma);
                    break;
                case Constantes.TipoDeOperacion.Baja:
                    if (mFamilia == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGFamiliaNula", Login.UsuarioLogeado.UserIdioma));
                        this.Close();
                    }
                    CargarCampos(mFamilia);
                    DeshabilitarCampos();
                    btnAceptar.Enabled = true;
                    btnAceptar.Text = Idioma.GetMSGLanguaje("btnEliminar", Login.UsuarioLogeado.UserIdioma);

                    break;
                case Constantes.TipoDeOperacion.Consulta:
                    if (mFamilia == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGFamiliaNula", Login.UsuarioLogeado.UserIdioma));
                        this.Close();
                        CargarCampos(mFamilia);
                        DeshabilitarCampos();
                        this.Controls.Remove(this.btnAceptar);
                    }
                        break;                    
            }
        }
        private void DeshabilitarCampos()
        {
                txtFam.Enabled = false;
                cmbPatente.Enabled = false;
                btnAceptar.Enabled = false;
                btnAgregar.Enabled = false;
                btnQuitar.Enabled = false;
        }
        private void LimpiarCampos()
        {
            txtFam.Text = "";
        }
        private void CargarCampos(Familia pF)
        {
            txtFam.Text = pF.detalle;
            chkActivo.Checked = pF.Activo;
            foreach (Patente mP in pF.ListaPatentes)
                dgvPatentes.Rows.Add(mP.id, mP.detalle);
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            if (cmbPatente.SelectedItem != null)
                dgvPatentes.Rows.Add(cmbPatente.SelectedIndex + 1, cmbPatente.SelectedItem.ToString());
            else
                MessageBox.Show(Idioma.GetMSGLanguaje("MSGPatNonSel", Login.UsuarioLogeado.UserIdioma));
        }
        private void ActualizarGrilla()
        {
            dgvPatentes.Rows.Clear();
            foreach (Patente mP in mPatentes)
                dgvPatentes.Rows.Add(mP.id, mP.detalle);
        }
        private void btnEliminar_Click(object sender, EventArgs e)
        {
                foreach (DataGridViewRow mRow in dgvPatentes.SelectedRows)
                    dgvPatentes.Rows.Remove(mRow);
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario mUser = new Usuario();
                switch (Operacion)
                {
                    case Constantes.TipoDeOperacion.Alta:
                        ValorizarEntidad(mFamilia);
                        mFamilia.Insertar(mFamilia);
                        this.Close();
                        break;
                    case Constantes.TipoDeOperacion.Modificacion:
                        ValorizarEntidad(mFamilia);
                        mFamilia.Guardar(mFamilia);
                        this.Close();
                        break;
                    case Constantes.TipoDeOperacion.Baja:
                        ValorizarEntidad(mFamilia);
                        DialogResult Alerta = MessageBox.Show(Idioma.GetMSGLanguaje("MSGConfirmRemFam", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("TitleConfirmAction", Login.UsuarioLogeado.UserIdioma), MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (Alerta == DialogResult.Yes)
                        {
                            mFamilia.Activo = false;
                            mFamilia.Eliminar(mFamilia);
                        }
                        else
                        {
                            MessageBox.Show(Idioma.GetMSGLanguaje("MSGOperCancel", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("MSGSysMsg", Login.UsuarioLogeado.UserIdioma), MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        this.Close();
                        break;
                }
            }
            catch(Exception ex) 
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void ValorizarEntidad(Familia pF)
        {
            //Grabamos las patentes
            List<Patente> mListPat = new List<Patente>();
            foreach (DataGridViewRow mRow in dgvPatentes.Rows)
            {
                Patente mP = new Patente();
                mP.id = int.Parse(mRow.Cells[0].Value.ToString());
                mP.detalle = mRow.Cells[1].Value.ToString();
                mListPat.Add(mP);
            }
            //Actualizamos nombre de la familia
            mFamilia.detalle = txtFam.Text;
            if (chkActivo.Checked)
                mFamilia.Activo = true;
            else
                mFamilia.Activo = false;
            mFamilia.ListaPatentes = mListPat;
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
