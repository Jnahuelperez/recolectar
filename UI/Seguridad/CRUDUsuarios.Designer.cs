﻿namespace UI
{
    partial class CRUDUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gboxUsuario = new System.Windows.Forms.GroupBox();
            this.chkPWDReset = new System.Windows.Forms.CheckBox();
            this.lblidUsuario = new System.Windows.Forms.Label();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblEmail = new System.Windows.Forms.Label();
            this.cmbIdioma = new System.Windows.Forms.ComboBox();
            this.lblIdioma = new System.Windows.Forms.Label();
            this.chkBloqueado = new System.Windows.Forms.CheckBox();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.lblContrasena = new System.Windows.Forms.Label();
            this.txtContrasena = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblApellido = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.groupBoxPat = new System.Windows.Forms.GroupBox();
            this.cmbPat = new System.Windows.Forms.ComboBox();
            this.btnEliminarPat = new System.Windows.Forms.Button();
            this.btnAgregarPat = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dgvPat = new System.Windows.Forms.DataGridView();
            this.groupBoxFam = new System.Windows.Forms.GroupBox();
            this.cmbFamil = new System.Windows.Forms.ComboBox();
            this.btnEliminarFam = new System.Windows.Forms.Button();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvFam = new System.Windows.Forms.DataGridView();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.gboxUsuario.SuspendLayout();
            this.groupBoxPat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPat)).BeginInit();
            this.groupBoxFam.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFam)).BeginInit();
            this.SuspendLayout();
            // 
            // gboxUsuario
            // 
            this.gboxUsuario.Controls.Add(this.chkPWDReset);
            this.gboxUsuario.Controls.Add(this.lblidUsuario);
            this.gboxUsuario.Controls.Add(this.txtEmail);
            this.gboxUsuario.Controls.Add(this.lblEmail);
            this.gboxUsuario.Controls.Add(this.cmbIdioma);
            this.gboxUsuario.Controls.Add(this.lblIdioma);
            this.gboxUsuario.Controls.Add(this.chkBloqueado);
            this.gboxUsuario.Controls.Add(this.chkActivo);
            this.gboxUsuario.Controls.Add(this.lblContrasena);
            this.gboxUsuario.Controls.Add(this.txtContrasena);
            this.gboxUsuario.Controls.Add(this.txtUsuario);
            this.gboxUsuario.Controls.Add(this.txtApellido);
            this.gboxUsuario.Controls.Add(this.txtNombre);
            this.gboxUsuario.Controls.Add(this.lblUsuario);
            this.gboxUsuario.Controls.Add(this.lblApellido);
            this.gboxUsuario.Controls.Add(this.lblNombre);
            this.gboxUsuario.Location = new System.Drawing.Point(9, 10);
            this.gboxUsuario.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gboxUsuario.Name = "gboxUsuario";
            this.gboxUsuario.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.gboxUsuario.Size = new System.Drawing.Size(388, 145);
            this.gboxUsuario.TabIndex = 0;
            this.gboxUsuario.TabStop = false;
            this.gboxUsuario.Text = "Usuario";
            this.gboxUsuario.Enter += new System.EventHandler(this.gboxUsuario_Enter);
            // 
            // chkPWDReset
            // 
            this.chkPWDReset.AutoSize = true;
            this.chkPWDReset.Location = new System.Drawing.Point(208, 59);
            this.chkPWDReset.Name = "chkPWDReset";
            this.chkPWDReset.Size = new System.Drawing.Size(96, 17);
            this.chkPWDReset.TabIndex = 15;
            this.chkPWDReset.Text = "Reiniciar PWD";
            this.chkPWDReset.UseVisualStyleBackColor = true;
            // 
            // lblidUsuario
            // 
            this.lblidUsuario.AutoSize = true;
            this.lblidUsuario.Location = new System.Drawing.Point(60, 0);
            this.lblidUsuario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblidUsuario.Name = "lblidUsuario";
            this.lblidUsuario.Size = new System.Drawing.Size(35, 13);
            this.lblidUsuario.TabIndex = 14;
            this.lblidUsuario.Text = "label1";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(76, 85);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(212, 20);
            this.txtEmail.TabIndex = 13;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(9, 85);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(32, 13);
            this.lblEmail.TabIndex = 12;
            this.lblEmail.Text = "eMail";
            // 
            // cmbIdioma
            // 
            this.cmbIdioma.FormattingEnabled = true;
            this.cmbIdioma.Location = new System.Drawing.Point(76, 113);
            this.cmbIdioma.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIdioma.Name = "cmbIdioma";
            this.cmbIdioma.Size = new System.Drawing.Size(120, 21);
            this.cmbIdioma.TabIndex = 11;
            // 
            // lblIdioma
            // 
            this.lblIdioma.AutoSize = true;
            this.lblIdioma.Location = new System.Drawing.Point(10, 115);
            this.lblIdioma.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIdioma.Name = "lblIdioma";
            this.lblIdioma.Size = new System.Drawing.Size(38, 13);
            this.lblIdioma.TabIndex = 10;
            this.lblIdioma.Text = "Idioma";
            // 
            // chkBloqueado
            // 
            this.chkBloqueado.AutoSize = true;
            this.chkBloqueado.Location = new System.Drawing.Point(302, 115);
            this.chkBloqueado.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkBloqueado.Name = "chkBloqueado";
            this.chkBloqueado.Size = new System.Drawing.Size(77, 17);
            this.chkBloqueado.TabIndex = 9;
            this.chkBloqueado.Text = "Bloqueado";
            this.chkBloqueado.UseVisualStyleBackColor = true;
            this.chkBloqueado.CheckedChanged += new System.EventHandler(this.chkBloqueado_CheckedChanged);
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Location = new System.Drawing.Point(302, 85);
            this.chkActivo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(56, 17);
            this.chkActivo.TabIndex = 7;
            this.chkActivo.Text = "Activo";
            this.chkActivo.UseVisualStyleBackColor = true;
            this.chkActivo.CheckedChanged += new System.EventHandler(this.CheckBox1_CheckedChanged);
            // 
            // lblContrasena
            // 
            this.lblContrasena.AutoSize = true;
            this.lblContrasena.Location = new System.Drawing.Point(190, 59);
            this.lblContrasena.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblContrasena.Name = "lblContrasena";
            this.lblContrasena.Size = new System.Drawing.Size(61, 13);
            this.lblContrasena.TabIndex = 8;
            this.lblContrasena.Text = "Contraseña";
            // 
            // txtContrasena
            // 
            this.txtContrasena.Location = new System.Drawing.Point(257, 56);
            this.txtContrasena.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtContrasena.Name = "txtContrasena";
            this.txtContrasena.PasswordChar = '*';
            this.txtContrasena.Size = new System.Drawing.Size(103, 20);
            this.txtContrasena.TabIndex = 7;
            this.txtContrasena.Click += new System.EventHandler(this.txtContrasena_Click);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(257, 24);
            this.txtUsuario.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(103, 20);
            this.txtUsuario.TabIndex = 6;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(76, 55);
            this.txtApellido.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(103, 20);
            this.txtApellido.TabIndex = 5;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(76, 24);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(103, 20);
            this.txtNombre.TabIndex = 4;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(190, 25);
            this.lblUsuario.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(43, 13);
            this.lblUsuario.TabIndex = 2;
            this.lblUsuario.Text = "Usuario";
            // 
            // lblApellido
            // 
            this.lblApellido.AutoSize = true;
            this.lblApellido.Location = new System.Drawing.Point(9, 58);
            this.lblApellido.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblApellido.Name = "lblApellido";
            this.lblApellido.Size = new System.Drawing.Size(44, 13);
            this.lblApellido.TabIndex = 1;
            this.lblApellido.Text = "Apellido";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(9, 27);
            this.lblNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(44, 13);
            this.lblNombre.TabIndex = 0;
            this.lblNombre.Text = "Nombre";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(175, 377);
            this.btnAceptar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(67, 22);
            this.btnAceptar.TabIndex = 5;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // groupBoxPat
            // 
            this.groupBoxPat.Controls.Add(this.cmbPat);
            this.groupBoxPat.Controls.Add(this.btnEliminarPat);
            this.groupBoxPat.Controls.Add(this.btnAgregarPat);
            this.groupBoxPat.Controls.Add(this.label3);
            this.groupBoxPat.Controls.Add(this.dgvPat);
            this.groupBoxPat.Location = new System.Drawing.Point(208, 166);
            this.groupBoxPat.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxPat.Name = "groupBoxPat";
            this.groupBoxPat.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxPat.Size = new System.Drawing.Size(192, 206);
            this.groupBoxPat.TabIndex = 7;
            this.groupBoxPat.TabStop = false;
            this.groupBoxPat.Text = "Patente";
            // 
            // cmbPat
            // 
            this.cmbPat.FormattingEnabled = true;
            this.cmbPat.Location = new System.Drawing.Point(76, 24);
            this.cmbPat.Margin = new System.Windows.Forms.Padding(2);
            this.cmbPat.Name = "cmbPat";
            this.cmbPat.Size = new System.Drawing.Size(103, 21);
            this.cmbPat.TabIndex = 13;
            // 
            // btnEliminarPat
            // 
            this.btnEliminarPat.Location = new System.Drawing.Point(102, 172);
            this.btnEliminarPat.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminarPat.Name = "btnEliminarPat";
            this.btnEliminarPat.Size = new System.Drawing.Size(66, 21);
            this.btnEliminarPat.TabIndex = 4;
            this.btnEliminarPat.Text = "Quitar";
            this.btnEliminarPat.UseVisualStyleBackColor = true;
            this.btnEliminarPat.Click += new System.EventHandler(this.btnDelPat_Click);
            // 
            // btnAgregarPat
            // 
            this.btnAgregarPat.Location = new System.Drawing.Point(12, 172);
            this.btnAgregarPat.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregarPat.Name = "btnAgregarPat";
            this.btnAgregarPat.Size = new System.Drawing.Size(66, 21);
            this.btnAgregarPat.TabIndex = 3;
            this.btnAgregarPat.Text = "Agregar";
            this.btnAgregarPat.UseVisualStyleBackColor = true;
            this.btnAgregarPat.Click += new System.EventHandler(this.btnAddPat_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 25);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Patente";
            // 
            // dgvPat
            // 
            this.dgvPat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPat.Location = new System.Drawing.Point(4, 57);
            this.dgvPat.Margin = new System.Windows.Forms.Padding(2);
            this.dgvPat.Name = "dgvPat";
            this.dgvPat.RowHeadersWidth = 82;
            this.dgvPat.RowTemplate.Height = 33;
            this.dgvPat.Size = new System.Drawing.Size(184, 97);
            this.dgvPat.TabIndex = 0;
            // 
            // groupBoxFam
            // 
            this.groupBoxFam.Controls.Add(this.cmbFamil);
            this.groupBoxFam.Controls.Add(this.btnEliminarFam);
            this.groupBoxFam.Controls.Add(this.btnAgregar);
            this.groupBoxFam.Controls.Add(this.label2);
            this.groupBoxFam.Controls.Add(this.dgvFam);
            this.groupBoxFam.Location = new System.Drawing.Point(12, 166);
            this.groupBoxFam.Margin = new System.Windows.Forms.Padding(2);
            this.groupBoxFam.Name = "groupBoxFam";
            this.groupBoxFam.Padding = new System.Windows.Forms.Padding(2);
            this.groupBoxFam.Size = new System.Drawing.Size(192, 206);
            this.groupBoxFam.TabIndex = 6;
            this.groupBoxFam.TabStop = false;
            this.groupBoxFam.Text = "Familias";
            // 
            // cmbFamil
            // 
            this.cmbFamil.FormattingEnabled = true;
            this.cmbFamil.Location = new System.Drawing.Point(76, 24);
            this.cmbFamil.Margin = new System.Windows.Forms.Padding(2);
            this.cmbFamil.Name = "cmbFamil";
            this.cmbFamil.Size = new System.Drawing.Size(103, 21);
            this.cmbFamil.TabIndex = 12;
            // 
            // btnEliminarFam
            // 
            this.btnEliminarFam.Location = new System.Drawing.Point(110, 172);
            this.btnEliminarFam.Margin = new System.Windows.Forms.Padding(2);
            this.btnEliminarFam.Name = "btnEliminarFam";
            this.btnEliminarFam.Size = new System.Drawing.Size(66, 21);
            this.btnEliminarFam.TabIndex = 4;
            this.btnEliminarFam.Text = "Quitar";
            this.btnEliminarFam.UseVisualStyleBackColor = true;
            this.btnEliminarFam.Click += new System.EventHandler(this.btnDelFam_Click);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(12, 172);
            this.btnAgregar.Margin = new System.Windows.Forms.Padding(2);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(66, 21);
            this.btnAgregar.TabIndex = 3;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAddFam_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 25);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Familia";
            // 
            // dgvFam
            // 
            this.dgvFam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFam.Location = new System.Drawing.Point(4, 57);
            this.dgvFam.Margin = new System.Windows.Forms.Padding(2);
            this.dgvFam.Name = "dgvFam";
            this.dgvFam.RowHeadersWidth = 82;
            this.dgvFam.RowTemplate.Height = 33;
            this.dgvFam.Size = new System.Drawing.Size(184, 97);
            this.dgvFam.TabIndex = 0;
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\RecolectAR\\UI\\user_manual.chm";
            // 
            // CRUDUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 405);
            this.Controls.Add(this.groupBoxPat);
            this.Controls.Add(this.groupBoxFam);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.gboxUsuario);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.Name = "CRUDUsuarios";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "                                                           ";
            this.Load += new System.EventHandler(this.CRUDUsuarios_Load);
            this.gboxUsuario.ResumeLayout(false);
            this.gboxUsuario.PerformLayout();
            this.groupBoxPat.ResumeLayout(false);
            this.groupBoxPat.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPat)).EndInit();
            this.groupBoxFam.ResumeLayout(false);
            this.groupBoxFam.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gboxUsuario;
        private System.Windows.Forms.CheckBox chkActivo;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblApellido;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.CheckBox chkBloqueado;
        private System.Windows.Forms.Label lblContrasena;
        private System.Windows.Forms.TextBox txtContrasena;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.ComboBox cmbIdioma;
        private System.Windows.Forms.Label lblIdioma;
        private System.Windows.Forms.GroupBox groupBoxPat;
        private System.Windows.Forms.Button btnEliminarPat;
        private System.Windows.Forms.Button btnAgregarPat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgvPat;
        private System.Windows.Forms.GroupBox groupBoxFam;
        private System.Windows.Forms.Button btnEliminarFam;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvFam;
        private System.Windows.Forms.ComboBox cmbPat;
        private System.Windows.Forms.ComboBox cmbFamil;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.CheckBox chkPWDReset;
        private System.Windows.Forms.Label lblidUsuario;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}