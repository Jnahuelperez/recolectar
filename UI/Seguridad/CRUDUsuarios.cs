﻿using System;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using BL;
using System.Collections.Generic;

namespace UI
{
    public partial class CRUDUsuarios : Form
    {
        #region Propiedades globales
        internal Constantes.TipoDeOperacion Operacion { get; set; }
        internal Usuario mUsuarioAEditar = new Usuario();
        internal Familia mFamilia = new Familia();
        internal List<Familia> ListaFamilias = new List<Familia>();
        internal List<Patente> ListaPatentes = new List<Patente>();
        internal Patente mPat = new Patente();
        internal SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        internal class Cambios
        {
            public int id { get; set; }
            public string detalle { get; set; }
            public string Tipo { get; set; }
            public int cambio { get; set; }
        }

        internal List<Cambios> MisCambios = new List<Cambios>();
        #endregion
        public CRUDUsuarios()
        {
            InitializeComponent();
        }

        private void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void CRUDUsuarios_Load(object sender, EventArgs e)
        {
            #region Grillas
            dgvFam.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvPat.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvFam.Columns.Add(GetDGVColumns("dgvFamFam"), GetDGVColumns("dgvFamFam"));
            dgvPat.Columns.Add(GetDGVColumns("dgvPatPat"), GetDGVColumns("dgvPatPat"));
            dgvFam.Columns[GetDGVColumns("dgvID")].Visible = false;
            dgvPat.Columns[GetDGVColumns("dgvID")].Visible = false;

            dgvPat.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvPat.AllowUserToAddRows = false;
            dgvPat.AllowUserToDeleteRows = false;
            dgvPat.AllowUserToResizeColumns = false;
            dgvPat.AllowUserToResizeRows = false;
            dgvPat.RowHeadersVisible = false;
            dgvPat.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvPat.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvPat.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvPat.RowTemplate.Height = 30;
            dgvFam.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvFam.AllowUserToAddRows = false;
            dgvFam.AllowUserToDeleteRows = false;
            dgvFam.AllowUserToResizeColumns = false;
            dgvFam.AllowUserToResizeRows = false;
            dgvFam.RowHeadersVisible = false;
            dgvFam.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvFam.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvFam.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvFam.RowTemplate.Height = 30;
            #endregion

            #region ComboBox
            cmbFamil.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbPat.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbIdioma.DropDownStyle = ComboBoxStyle.DropDownList;
            //Agregar elementos al Combo
            foreach (Familia mF in mFamilia.Listar())
            {
                cmbFamil.Items.Add(mF.detalle);
            }
            foreach (BL.Idioma mI in BL.Idioma.ListarIdiomas())
            {
                cmbIdioma.Items.Add(mI.Detalle);
            }
            foreach (Patente mP in mPat.Listar())
            {
                cmbPat.Items.Add(mP.detalle);
            }
            #endregion

            InicializarPagina();

            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("CRUDUsuarios", Login.UsuarioLogeado.UserIdioma);

        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }
        private void InicializarPagina()
        {
            txtContrasena.Enabled = false;
            chkPWDReset.Hide();
            switch (Operacion)
            {
                case Constantes.TipoDeOperacion.Alta:
                    LimpiarCampos();
                    lblidUsuario.Visible = false;
                    cmbIdioma.SelectedItem = 1;
                    btnAceptar.Text = "Crear";
                    chkActivo.Checked = true;
                    chkActivo.Enabled = false;
                    chkBloqueado.Checked = false;
                    chkBloqueado.Enabled = false;
                    //groupBoxFam.Enabled = false;
                    //groupBoxPat.Enabled = false;
                    break;
                case Constantes.TipoDeOperacion.Modificacion:
                    if (mUsuarioAEditar == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGNoUserSelect", Login.UsuarioLogeado.UserIdioma),
                            Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    else
                    {
                        CargarCampos(mUsuarioAEditar);
                        btnAceptar.Text = "Modificar";
                        chkPWDReset.Show();
                        txtContrasena.Hide();
                        lblContrasena.Hide();
                    }
                    break;
                case Constantes.TipoDeOperacion.Baja:
                    if (mUsuarioAEditar == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGNoUserSelect", Login.UsuarioLogeado.UserIdioma),
                            Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    else
                    {
                        chkPWDReset.Hide();
                        CargarCampos(mUsuarioAEditar);
                        DeshabilitarCampos();
                        btnAceptar.Text = "Eliminar";
                    }
                    break;
                case Constantes.TipoDeOperacion.Consulta:
                    if (mUsuarioAEditar == null)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGNoUserSelect", Login.UsuarioLogeado.UserIdioma), 
                            Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma), 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                    CargarCampos(mUsuarioAEditar);
                    DeshabilitarCampos();
                    this.Controls.Remove(this.btnAceptar);
                    break;
            }
        }
        private void DeshabilitarCampos()
        {
            foreach (Control C in this.Controls)
                if (C.Name != "btnAceptar")
                    C.Enabled = false;
        }
        private void LimpiarCampos()
        {
            foreach (Control C in this.Controls)
                if (C is TextBox)
                {
                    C.Text = "";
                }

        }
        private void CargarCampos(Usuario pUsuario)
        {
            lblidUsuario.Text = mUsuarioAEditar.id.ToString();
            txtNombre.Text = mUsuarioAEditar.Nombre;
            txtApellido.Text = mUsuarioAEditar.Apellido;
            txtUsuario.Text = mUsuarioAEditar.user;
            txtEmail.Text = mUsuarioAEditar.email;
            //txtContrasena.Text = mUsuarioAEditar.pwd;
            cmbIdioma.SelectedIndex = BL.Idioma.Obtener(mUsuarioAEditar.idIdioma).id-1;
            if (mUsuarioAEditar.Activo == true)
                chkActivo.Checked = true;
            if (mUsuarioAEditar.Bloqueado == true)
                chkBloqueado.Checked = true;
            ObtenerFamiliasUsuario(mUsuarioAEditar);
            ObtenerPatentesUsuario(mUsuarioAEditar);
        }
        private void ObtenerFamiliasUsuario(Usuario pUsuario)
        {
            dgvFam.Rows.Clear();
            foreach (Familia mF in mUsuarioAEditar.GetFamiliasByUser(pUsuario))
                dgvFam.Rows.Add(mF.id, mF.detalle);
        }
        private void ObtenerPatentesUsuario(Usuario pUsuario)
        {
            dgvPat.Rows.Clear();
            foreach (Patente mP in mUsuarioAEditar.GetPatentesByUser(pUsuario))
                dgvPat.Rows.Add(mP.id, mP.detalle);
        }
        private void ValorizarEntidad(Usuario pUsuario)
        {
            mUsuarioAEditar.Nombre = txtNombre.Text;
            mUsuarioAEditar.Apellido = txtApellido.Text;
            mUsuarioAEditar.email = txtEmail.Text.ToLower();
            mUsuarioAEditar.user = txtUsuario.Text.ToLower();
            if (chkActivo.Checked)
            { mUsuarioAEditar.Activo = true; }
            else
            { mUsuarioAEditar.Activo = false; }
            if (chkBloqueado.Checked)
            { mUsuarioAEditar.Bloqueado = true; }
            else
            { mUsuarioAEditar.Bloqueado = false; }
            mUsuarioAEditar.idIdioma = cmbIdioma.SelectedIndex + 1;

            ValorizarPermisos();
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (checkInput())
            {
                Usuario mUser = new Usuario();
                try
                {
                    switch (Operacion)
                    {
                        case Constantes.TipoDeOperacion.Alta:
                            ValorizarEntidad(mUsuarioAEditar);
                            mUsuarioAEditar.CreatedOn = DateTime.Now;
                            mUser.Insertar(mUsuarioAEditar);
                            this.Close();
                            break;
                        case Constantes.TipoDeOperacion.Modificacion:
                            ValorizarEntidad(mUsuarioAEditar);
                            if (chkPWDReset.Checked)
                                mUser.Guardar(mUsuarioAEditar, true);
                            else
                                mUser.Guardar(mUsuarioAEditar);
                            this.Close();
                            break;
                        case Constantes.TipoDeOperacion.Baja:
                            ValorizarEntidad(mUsuarioAEditar);
                            DialogResult Alerta = MessageBox.Show(Idioma.GetMSGLanguaje("MSGConfirmAction", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("TitleConfirm", Login.UsuarioLogeado.UserIdioma), MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                            if (Alerta == DialogResult.Yes)
                            {
                                mUser.Eliminar(mUsuarioAEditar);
                            }
                            else
                            {
                                MessageBox.Show(
                                    Idioma.GetMSGLanguaje("MSGOperCancelada", Login.UsuarioLogeado.UserIdioma), 
                                    Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma), 
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            this.Close();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        Idioma.GetMSGLanguaje("MSGErrorOnRuntime", Login.UsuarioLogeado.UserIdioma) + "\n" +ex.Message,
                        Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(Idioma.GetMSGLanguaje("MSGCampoMatchExcep", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ValorizarPermisos()
        {
            int mIDUsuario = 0;
            if (Operacion == Constantes.TipoDeOperacion.Modificacion || Operacion == Constantes.TipoDeOperacion.Baja)
                mIDUsuario = int.Parse(lblidUsuario.Text);
            foreach (DataGridViewRow mRow in dgvFam.Rows)
            {
                Familia mF = new Familia();
                mF.id = int.Parse(mRow.Cells[0].Value.ToString());
                mF.detalle = mRow.Cells[1].Value.ToString();
                mF.idUsuario = mIDUsuario;
                ListaFamilias.Add(mF);
            }
            foreach (DataGridViewRow mRow in dgvPat.Rows)
            {
                Patente mP = new Patente();
                mP.id = int.Parse(mRow.Cells[0].Value.ToString());
                mP.detalle = mRow.Cells[1].Value.ToString();
                mP.idUsuario = mIDUsuario;
                ListaPatentes.Add(mP);
            }
            if (chkPWDReset.Checked)
                mUsuarioAEditar.pwdCambio = true;
            mUsuarioAEditar.Familias = ListaFamilias;
            mUsuarioAEditar.Patentes = ListaPatentes;
        }
        private void lblPuesto_Click(object sender, EventArgs e)
        {

        }
        private void gboxUsuario_Enter(object sender, EventArgs e)
        {

        }
        private void btnAddFam_Click(object sender, EventArgs e)
        {
            dgvFam.Rows.Add(cmbFamil.SelectedIndex + 1, cmbFamil.SelectedItem);
        }
        private void btnAddPat_Click(object sender, EventArgs e)
        {
            Patente mP = new Patente();
            mP.id = cmbPat.SelectedIndex + 1;
            mP.detalle = cmbPat.SelectedItem.ToString();
            mP.idUsuario = mUsuarioAEditar.id;
            dgvPat.Rows.Add(cmbPat.SelectedIndex + 1, cmbPat.SelectedItem);
        }
        private void btnDelFam_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow mRow in dgvFam.SelectedRows)
            {
                dgvFam.Rows.Remove(mRow);
            }
        }
        private void btnDelPat_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow mRow in dgvPat.SelectedRows)
            {
                //Patente mP = new Patente();
                //mP.id = int.Parse(mRow.Cells[0].Value.ToString());
                //mP.detalle = mRow.Cells[1].Value.ToString();
                //mP.idUsuario = mUsuarioAEditar.id;
                dgvPat.Rows.Remove(mRow);
            }
        }
        private void txtContrasena_Click(object sender, EventArgs e)
        {
            MessageBox.Show("La clave solo puede ser generada por el usuario. Durante el alta el sistema la genera aleatoriamente y envia por correo.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        private void chkBloqueado_CheckedChanged(object sender, EventArgs e)
        {
            if (!chkBloqueado.Checked)
                mUsuarioAEditar.Bloqueado = false;
        }
        private bool checkInput()
        {
            bool mResult = true;
            Regex mPWDRegex = new Regex(@"^[a-zA-Z0-9_]+$");
            if (!mPWDRegex.IsMatch(txtNombre.Text) || !mPWDRegex.IsMatch(txtApellido.Text) || !mPWDRegex.IsMatch(txtUsuario.Text))
                return false;
            else
            {
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(txtEmail.Text);
                if (!match.Success)
                    return false;
            }
            return mResult;
        }
    }
}
