﻿namespace UI
{
    partial class FRMBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDestino = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnEjecutar = new System.Windows.Forms.Button();
            this.lblPartes = new System.Windows.Forms.Label();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lblDestino
            // 
            this.lblDestino.AutoSize = true;
            this.lblDestino.Location = new System.Drawing.Point(12, 14);
            this.lblDestino.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDestino.Name = "lblDestino";
            this.lblDestino.Size = new System.Drawing.Size(69, 13);
            this.lblDestino.TabIndex = 0;
            this.lblDestino.Text = "Ruta Destino";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(85, 11);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(324, 20);
            this.textBox1.TabIndex = 1;
            // 
            // btnEjecutar
            // 
            this.btnEjecutar.Location = new System.Drawing.Point(426, 42);
            this.btnEjecutar.Margin = new System.Windows.Forms.Padding(2);
            this.btnEjecutar.Name = "btnEjecutar";
            this.btnEjecutar.Size = new System.Drawing.Size(120, 28);
            this.btnEjecutar.TabIndex = 2;
            this.btnEjecutar.Text = "Ejecutar";
            this.btnEjecutar.UseVisualStyleBackColor = true;
            this.btnEjecutar.Click += new System.EventHandler(this.Button1_Click);
            // 
            // lblPartes
            // 
            this.lblPartes.AutoSize = true;
            this.lblPartes.Location = new System.Drawing.Point(12, 42);
            this.lblPartes.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPartes.Name = "lblPartes";
            this.lblPartes.Size = new System.Drawing.Size(121, 13);
            this.lblPartes.TabIndex = 3;
            this.lblPartes.Text = "Dividir archivo en partes";
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(426, 9);
            this.btnExaminar.Margin = new System.Windows.Forms.Padding(2);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(120, 25);
            this.btnExaminar.TabIndex = 5;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\RecolectAR\\UI\\user_manual.chm";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(138, 39);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(64, 21);
            this.comboBox1.TabIndex = 7;
            // 
            // FRMBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 82);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.lblPartes);
            this.Controls.Add(this.btnEjecutar);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblDestino);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FRMBackup";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Backup";
            this.Load += new System.EventHandler(this.FRMBackup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDestino;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnEjecutar;
        private System.Windows.Forms.Label lblPartes;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}