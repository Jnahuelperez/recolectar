﻿using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;

namespace UI
{
    public partial class FRMBackup : Form
    {
        public FRMBackup()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (checkInput())
            {
                DialogResult mPreg = MessageBox.Show(Idioma.GetMSGLanguaje("MSGConfirmBKP", Login.UsuarioLogeado.UserIdioma),
                    Idioma.GetMSGLanguaje("MSGSysMsg", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (mPreg == DialogResult.Yes)
                {
                    try
                    {
                        DBAdmin mDBAdmin = new DBAdmin();
                        string mResult = mDBAdmin.RealizarBackup(int.Parse(comboBox1.SelectedItem.ToString()));
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGBKPCreado", Login.UsuarioLogeado.UserIdioma)
                            + "\n" +
                            Idioma.GetMSGLanguaje("MSGPath", Login.UsuarioLogeado.UserIdioma)
                            + " \t" + textBox1.Text + "\n" +
                            Idioma.GetMSGLanguaje("MSFAmountFiles", Login.UsuarioLogeado.UserIdioma)
                            + " \t" + comboBox1.SelectedItem.ToString() + "\n");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(Idioma.GetMSGLanguaje("MSGErrorOnRuntime", Login.UsuarioLogeado.UserIdioma)
                            + "\n" + ex.Message, Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show(Idioma.GetMSGLanguaje("MSGCampoMatchExcep", Login.UsuarioLogeado.UserIdioma), Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }
        private bool checkInput()
        {
            bool mResult = true;
            Regex mPWDRegex = new Regex(@"^(?:[a-zA-Z]\:|\\\\[\w\.]+\\[\w.$]+)\\(?:[\w]+\\)*\w([\w.])+$");
            if (!mPWDRegex.IsMatch(textBox1.Text))
                return false;
            return mResult;
        }


        private void FRMBackup_Load(object sender, EventArgs e)
        {
            for (int i = 1; i < 100; i++)
                comboBox1.Items.Add(i);
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox1.SelectedIndex = 0;
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    if (keyData == Keys.F1)
        //    {
        //        Help.ShowHelp(this, Environment.CurrentDirectory + @"\user_manual.chm", "Backup.htm");
        //    }
        //    return true;
        //}

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                textBox1.Text = fbd.SelectedPath;
            }
        }
    }
}
