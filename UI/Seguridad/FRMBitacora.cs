﻿using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using iText.Kernel.Pdf;
using iText.Kernel.Colors;
using iText.Kernel.Pdf.Canvas.Draw;
using iText.Kernel.Pdf.Canvas;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using iText.IO.Image;
using iText.Kernel.Geom;
using System.Drawing;
using System.Drawing.Imaging;

namespace UI
{
    public partial class FRMBitacora : Form
    {
        Usuario mU = new Usuario();
        public FRMBitacora()
        {
            InitializeComponent();
        }

        private void Bitacora_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvBitacora.Columns.Add(GetDGVColumns("dgvBitacoraID"), GetDGVColumns("dgvBitacoraID"));
            dgvBitacora.Columns.Add(GetDGVColumns("dgvBitacoraFecha"), GetDGVColumns("dgvBitacoraFecha"));
            dgvBitacora.Columns.Add(GetDGVColumns("dgvBitacoraUsuario"), GetDGVColumns("dgvBitacoraUsuario"));
            dgvBitacora.Columns.Add(GetDGVColumns("dgvBitacoraCriticidad"), GetDGVColumns("dgvBitacoraCriticidad"));
            dgvBitacora.Columns.Add(GetDGVColumns("dgvBitacoraMovimiento"), GetDGVColumns("dgvBitacoraMovimiento"));
            dgvBitacora.Columns["ID"].Visible = false;

            dgvBitacora.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvBitacora.AllowUserToAddRows = false;
            dgvBitacora.AllowUserToDeleteRows = false;
            dgvBitacora.AllowUserToResizeColumns = true;
            dgvBitacora.AllowUserToResizeRows = false;
            dgvBitacora.RowHeadersVisible = false;
            dgvBitacora.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvBitacora.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvBitacora.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            #endregion

            //ComboBox
            cmbUsuario.Items.Add("ALL");
            foreach (string mS in Bitacora.ListarUsuarios())
                cmbUsuario.Items.Add(mS);
            cmbCriticidad.Items.Add("ALL");
            cmbCriticidad.Items.Add("CRIT");
            cmbCriticidad.Items.Add("WARN");
            cmbCriticidad.Items.Add("INFO");
            cmbCriticidad.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbUsuario.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbCriticidad.SelectedIndex = 0;
            cmbUsuario.SelectedIndex = 0;

            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("FRMBitacora", Login.UsuarioLogeado.UserIdioma);
        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }


        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Bitacora mB = new Bitacora();
                mB.FechaDesde = dtpFechaDesde.Value.Date;
                mB.FechaHasta = dtpFechaHasta.Value.Date;
                mB.Usuario = cmbUsuario.SelectedItem.ToString();
                mB.Nivel = cmbCriticidad.SelectedItem.ToString();
                ActualizarGrilla(mB);
            }
            catch(Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void txtFDesde_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Bitacora mB = new Bitacora();
            mB.FechaDesde = dtpFechaDesde.Value.Date;
            mB.FechaHasta = dtpFechaHasta.Value.Date;
            mU.user = cmbUsuario.SelectedItem.ToString();
            mB.idUsuario = mU.Obtener(mU).id;
            mB.Nivel = cmbCriticidad.SelectedItem.ToString();
            ActualizarGrilla(mB);
        }

        private void ActualizarGrilla(Bitacora pBitacora)
        {
            dgvBitacora.Rows.Clear();
            foreach (Bitacora mReg in pBitacora.Listar(pBitacora))
                dgvBitacora.Rows.Add(mReg.id, mReg.Fecha, mReg.Usuario, mReg.Nivel, mReg.Detalle);
        }

        private void btnExportToPDF_Click(object sender, EventArgs e)
        {
            try
            {
                string mPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString();
                string mFileName = @"report_"+this.Text+"_"+ DateTime.Now.ToString("yyyyMMddHHmmss")+@".pdf";
                string mFileResult = System.IO.Path.Combine(mPath, mFileName);
                    ExportPDF(mFileResult);
                    MessageBox.Show(Idioma.GetMSGLanguaje(
                    "PDFReportCreated", Login.UsuarioLogeado.UserIdioma)
                    + "\n" + mFileResult,
                    Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(Idioma.GetMSGLanguaje(
                    "MSGErrorOnRuntime", Login.UsuarioLogeado.UserIdioma) 
                    + "\n"+ ex.Message, 
                    Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ExportPDF(string pPath)
        {
            
            PdfWriter writer = new PdfWriter(pPath);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);
            pdf.SetDefaultPageSize(PageSize.A4);

            // Header
            Paragraph header = new Paragraph(Idioma.GetMSGLanguaje("PDFHeader", Login.UsuarioLogeado.UserIdioma) + " " + this.Text)
               .SetTextAlignment(TextAlignment.CENTER)
               .SetFontSize(20);
            Paragraph newline = new Paragraph(new Text("\n"));
            document.Add(newline);
            document.Add(header);

            // Add sub-header
            Paragraph subheader = new Paragraph(
                Idioma.GetMSGLanguaje("PDFCreatedBY", Login.UsuarioLogeado.UserIdioma) + " " + Login.UsuarioLogeado.user +"\t"+ DateTime.Now.ToString())
               .SetTextAlignment(TextAlignment.CENTER)
               .SetFontSize(15);
            document.Add(subheader);

            // Line separator
            LineSeparator ls = new LineSeparator(new SolidLine());
            document.Add(ls);

            // Add disclaimer
            Paragraph paragraph1 = new Paragraph(
                Idioma.GetMSGLanguaje("PDFDisclaimer", Login.UsuarioLogeado.UserIdioma));
            document.Add(paragraph1);

            //// Add image
            //Image img = new Image(ImageDataFactory
            //   .Create(@"..\..\image.jpg"))
            //   .SetTextAlignment(TextAlignment.CENTER);
            //document.Add(img);

            // Table
            Table table = new Table(new float[] { 4,20, 13, 13, 50 });
            table.SetWidthPercent(100);
            table.AddHeaderCell(GetDGVColumns("dgvBitacoraID"))
                .SetFontSize(10)
                .SetBackgroundColor(iText.Kernel.Colors.CalGray.LIGHT_GRAY)
                .SetTextAlignment(TextAlignment.CENTER);
            table.AddHeaderCell(GetDGVColumns("dgvBitacoraFecha"))
                .SetBackgroundColor(iText.Kernel.Colors.CalGray.LIGHT_GRAY);
            table.AddHeaderCell(GetDGVColumns("dgvBitacoraUsuario"))
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(10)
                .SetBackgroundColor(iText.Kernel.Colors.CalGray.LIGHT_GRAY);
            table.AddHeaderCell(GetDGVColumns("dgvBitacoraCriticidad"))
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(10)
                .SetBackgroundColor(iText.Kernel.Colors.CalGray.LIGHT_GRAY);
            table.AddHeaderCell(GetDGVColumns("dgvBitacoraMovimiento"))
                .SetFontSize(10)
                .SetBackgroundColor(iText.Kernel.Colors.CalGray.LIGHT_GRAY);

            foreach (DataGridViewRow mRow in dgvBitacora.Rows)
            {
                Cell mID = new Cell()
                    .SetTextAlignment(TextAlignment.CENTER)
                    .SetFontSize(8)
                    .SetBackgroundColor(iText.Kernel.Colors.CalGray.WHITE)
                    .Add(new Paragraph(mRow.Cells[0].Value.ToString()));
                Cell mFecha = new Cell()
                    .SetFontSize(8)
                    .SetBackgroundColor(iText.Kernel.Colors.CalGray.WHITE)
                    .Add(new Paragraph(mRow.Cells[1].Value.ToString()));
                Cell mMovimiento = new Cell()
                    .SetFontSize(8)
                    .SetBackgroundColor(iText.Kernel.Colors.CalGray.WHITE)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(mRow.Cells[2].Value.ToString()));
                Cell mUsuario = new Cell()
                    .SetFontSize(8)
                    .SetBackgroundColor(iText.Kernel.Colors.CalGray.WHITE)
                    .SetTextAlignment(TextAlignment.CENTER)
                    .Add(new Paragraph(mRow.Cells[3].Value.ToString()));
                Cell mCriticidad = new Cell()
                    .SetFontSize(8)
                    .SetBackgroundColor(iText.Kernel.Colors.CalGray.WHITE)
                    .SetTextAlignment(TextAlignment.LEFT)
                    .Add(new Paragraph(mRow.Cells[4].Value.ToString()));
                
                table.AddCell(mID);
                table.AddCell(mFecha);
                table.AddCell(mMovimiento);
                table.AddCell(mUsuario);
                table.AddCell(mCriticidad);
            }
            document.Add(table);
            //// Hyper link
            //Link link = new Link("click here",
            //   iText.Kernel.Pdf.Action.PdfAction.CreateURI("https://www.google.com"));
            //Paragraph hyperLink = new Paragraph("Please ")
            //   .Add(link.SetBold().SetUnderline()
            //   .SetItalic().SetFontColor(iText.Kernel.Colors.ColorConstants.BLUE))
            //   .Add(" to go www.google.com.");
            //document.Add(hyperLink);

            // Page numbers
            int n = pdf.GetNumberOfPages();
            for (int i = 1; i <= n; i++)
            {
                document.ShowTextAligned(new Paragraph(String
                   .Format("page" + i + " of " + n)),
                   559, 806, i, TextAlignment.RIGHT,
                   VerticalAlignment.TOP, 0);
            }

            // Close document
            document.Close();
        }
    }
}

