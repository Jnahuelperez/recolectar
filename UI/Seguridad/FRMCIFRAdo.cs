﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SERV;
using BL;

namespace UI
{
    public partial class FRMCIFRAdo : Form
    {
        SERV.Seguridad.Cifrado mCifra = new SERV.Seguridad.Cifrado();
        Integridad mInt = new Integridad();

        public FRMCIFRAdo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtResultado.Text = SERV.Seguridad.Cifrado.CifrarTexto(txtSuperior.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            txtResultado.Text = SERV.Seguridad.Cifrado.DescifrarTexto(txtSuperior.Text);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            txtResultado.Text = mCifra.GetHASH(txtSuperior.Text);
        }

        private void FRMCIFRAdo_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            DBAdmin mdbadm = new DBAdmin();
            txtresult.Text = mdbadm.GetDVV(txtTablaNom.Text);
        }
    }
}
