﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class Restaurar_BD : Form
    {
        public Restaurar_BD()
        {
            InitializeComponent();
        }

        private void Restaurar_BD_Load(object sender, EventArgs e)
        {
            txtRuta.Enabled = false;
            btnRestaurar.Enabled = false;
        }

        private void BtnExaminar_Click(object sender, EventArgs e)
        {
            OpenFileDialog fdlg = new OpenFileDialog();
            fdlg.Title = "C# Corner Open File Dialog";
            fdlg.InitialDirectory = @"c:\";
            fdlg.Filter = "All files (*.*)|*.*|All files (*.*)|*.*";
            fdlg.FilterIndex = 2;
            fdlg.RestoreDirectory = true;
            if (fdlg.ShowDialog() == DialogResult.OK)
            {
                txtRuta.Text = fdlg.FileName;
            }
        }
    }
}
