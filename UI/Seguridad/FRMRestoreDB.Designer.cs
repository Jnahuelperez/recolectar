﻿namespace UI.Seguridad
{
    partial class FRMRestoreDB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUbicacion = new System.Windows.Forms.Label();
            this.txtUbicacion = new System.Windows.Forms.TextBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.dgvRestoreFiles = new System.Windows.Forms.DataGridView();
            this.btnRestoreDB = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRestoreFiles)).BeginInit();
            this.SuspendLayout();
            // 
            // lblUbicacion
            // 
            this.lblUbicacion.AutoSize = true;
            this.lblUbicacion.Location = new System.Drawing.Point(13, 13);
            this.lblUbicacion.Name = "lblUbicacion";
            this.lblUbicacion.Size = new System.Drawing.Size(55, 13);
            this.lblUbicacion.TabIndex = 0;
            this.lblUbicacion.Text = "Ubicacion";
            // 
            // txtUbicacion
            // 
            this.txtUbicacion.Location = new System.Drawing.Point(74, 10);
            this.txtUbicacion.Name = "txtUbicacion";
            this.txtUbicacion.Size = new System.Drawing.Size(239, 20);
            this.txtUbicacion.TabIndex = 1;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(319, 8);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(75, 23);
            this.btnExaminar.TabIndex = 2;
            this.btnExaminar.Text = "Examinar";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvRestoreFiles
            // 
            this.dgvRestoreFiles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRestoreFiles.Location = new System.Drawing.Point(16, 36);
            this.dgvRestoreFiles.Name = "dgvRestoreFiles";
            this.dgvRestoreFiles.Size = new System.Drawing.Size(378, 150);
            this.dgvRestoreFiles.TabIndex = 3;
            // 
            // btnRestoreDB
            // 
            this.helpProvider1.SetHelpNavigator(this.btnRestoreDB, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.btnRestoreDB.Location = new System.Drawing.Point(158, 195);
            this.btnRestoreDB.Name = "btnRestoreDB";
            this.helpProvider1.SetShowHelp(this.btnRestoreDB, true);
            this.btnRestoreDB.Size = new System.Drawing.Size(86, 23);
            this.btnRestoreDB.TabIndex = 4;
            this.btnRestoreDB.Text = "Restaurar DB";
            this.btnRestoreDB.UseVisualStyleBackColor = true;
            this.btnRestoreDB.Click += new System.EventHandler(this.button2_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\RecolectAR\\UI\\user_manual.chm";
            // 
            // FRMRestoreDB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 230);
            this.Controls.Add(this.btnRestoreDB);
            this.Controls.Add(this.dgvRestoreFiles);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.txtUbicacion);
            this.Controls.Add(this.lblUbicacion);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.Name = "FRMRestoreDB";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "FRMRestoreDB";
            this.Load += new System.EventHandler(this.FRMRestoreDB_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRestoreFiles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUbicacion;
        private System.Windows.Forms.TextBox txtUbicacion;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.DataGridView dgvRestoreFiles;
        private System.Windows.Forms.Button btnRestoreDB;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}