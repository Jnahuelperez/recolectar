﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using BL;

namespace UI.Seguridad
{
    public partial class FRMRestoreDB : Form
    {
        string[] files;
        DBAdmin mDBAdmin = new DBAdmin();
        public FRMRestoreDB()
        {
            InitializeComponent();
        }

        private void FRMRestoreDB_Load(object sender, EventArgs e)
        {
            dgvRestoreFiles.Columns.Add(GetDGVColumns("dgvArchivos"), GetDGVColumns("dgvArchivos"));
            dgvRestoreFiles.RowHeadersVisible = false;
            dgvRestoreFiles.AllowUserToAddRows = false;
            dgvRestoreFiles.AllowUserToDeleteRows = false;
            dgvRestoreFiles.EditMode = DataGridViewEditMode.EditProgrammatically;
            txtUbicacion.Enabled = false;
            dgvRestoreFiles.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvRestoreFiles.AutoResizeColumns();

            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            this.Text = Idioma.GetMSGLanguaje("FRMRestoreDB", Login.UsuarioLogeado.UserIdioma);

        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                files = Directory.GetFiles(fbd.SelectedPath);
                foreach (string mF in files)
                    dgvRestoreFiles.Rows.Add(mF);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            List<string> mArchivos = new List<string>();

            try
            {
                DialogResult mResult = MessageBox.Show(
                    Idioma.GetMSGLanguaje("MSGRestoreConf", Login.UsuarioLogeado.UserIdioma),
                    Idioma.GetMSGLanguaje("TitleConfirm", Login.UsuarioLogeado.UserIdioma), 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (mResult == DialogResult.Yes)
                {
                    foreach (DataGridViewRow mRow in dgvRestoreFiles.Rows)
                        mArchivos.Add(mRow.Cells[0].Value.ToString());
                    try { mDBAdmin.RestaurarBackup(mArchivos); }
                    catch(Exception ex) { MessageBox.Show(ex.Message); }
                }
                MessageBox.Show(
                    Idioma.GetMSGLanguaje("MSGRestoreEndOk", Login.UsuarioLogeado.UserIdioma),
                    Idioma.GetMSGLanguaje("TitleMSG", Login.UsuarioLogeado.UserIdioma),
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex) { throw (ex); }
        }
    }
}
