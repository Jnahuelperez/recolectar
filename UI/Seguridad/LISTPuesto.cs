﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace UI
{
    public partial class LISTPuesto : Form
    {
        Familia mFam = new Familia();
        public LISTPuesto()
        {
            InitializeComponent();
        }
        private void LISTfamiliapatente_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvFamilia.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvFamilia.Columns.Add(GetDGVColumns("dgvFamFam"), GetDGVColumns("dgvFamFam"));
            dgvFamilia.Columns.Add(GetDGVColumns("dgvFamAct"), GetDGVColumns("dgvFamAct"));
            dgvFamilia.Columns[GetDGVColumns("dgvID")].Visible = false;

            dgvFamilia.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvFamilia.AllowUserToAddRows = false;
            dgvFamilia.AllowUserToDeleteRows = false;
            dgvFamilia.AllowUserToResizeColumns = false;
            dgvFamilia.AllowUserToResizeRows = false;
            dgvFamilia.RowHeadersVisible = false;
            dgvFamilia.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvFamilia.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvFamilia.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            #endregion
            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private void BtnAlta_Click(object sender, EventArgs e)
        {

            CRUDFamilia mForm = new CRUDFamilia();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.StartPosition = FormStartPosition.CenterParent;
            mForm.Operacion = Constantes.TipoDeOperacion.Alta;
            mForm.ShowDialog(this);
            ActualizarGrilla();
        }
        private void btnMod_Click(object sender, EventArgs e)
        {
            if (dgvFamilia.SelectedRows.Count > 0)
            {
                mFam.id = int.Parse(dgvFamilia.SelectedRows[0].Cells[0].Value.ToString());
                mFam.detalle = dgvFamilia.SelectedRows[0].Cells[1].Value.ToString();
                CRUDFamilia mForm = new CRUDFamilia();
                mForm.MinimizeBox = false;
                mForm.MaximizeBox = false;
                mForm.StartPosition = FormStartPosition.CenterParent;
                mForm.Operacion = Constantes.TipoDeOperacion.Modificacion;
                mForm.mFamilia = mFam.Obtener(mFam);
                mForm.ShowDialog(this);
                ActualizarGrilla();
            }
        }
        private void btnBaja_Click(object sender, EventArgs e)
        {
            if (dgvFamilia.SelectedRows.Count > 0)
            {
                mFam.id = int.Parse(dgvFamilia.SelectedRows[0].Cells[0].Value.ToString());
                mFam.detalle = dgvFamilia.SelectedRows[0].Cells[1].Value.ToString();
                CRUDFamilia mForm = new CRUDFamilia();
                mForm.MinimizeBox = false;
                mForm.MaximizeBox = false;
                mForm.StartPosition = FormStartPosition.CenterParent;
                mForm.Operacion = Constantes.TipoDeOperacion.Baja;
                mForm.mFamilia = mFam.Obtener(mFam);
                mForm.ShowDialog(this);
                ActualizarGrilla();
            }
        }
        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }
        private void ActualizarGrilla()
        {
            dgvFamilia.Rows.Clear();
            foreach (Familia mF in mFam.Listar())
                dgvFamilia.Rows.Add(mF.id, mF.detalle, mF.Activo.ToString());
        }
    }
}
