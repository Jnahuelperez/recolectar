﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using BL;

namespace UI
{
    public partial class LISTusuarios : Form
    {
        internal Usuario mUsu = new Usuario();
        public LISTusuarios()
        {
            InitializeComponent();
        }

        private void LISTusuarios_Load(object sender, EventArgs e)
        {
            #region Formato Grilla
            dgvUsuarios.Columns.Add(GetDGVColumns("dgvID"), GetDGVColumns("dgvID"));
            dgvUsuarios.Columns.Add(GetDGVColumns("dgvUsuariosNom"), GetDGVColumns("dgvUsuariosNom"));
            dgvUsuarios.Columns.Add(GetDGVColumns("dgvUsuariosApe"), GetDGVColumns("dgvUsuariosApe"));
            dgvUsuarios.Columns.Add(GetDGVColumns("dgvUsuariosUsu"), GetDGVColumns("dgvUsuariosUsu"));
            dgvUsuarios.Columns.Add(GetDGVColumns("dgvUsuariosAct"), GetDGVColumns("dgvUsuariosAct"));
            dgvUsuarios.Columns.Add(GetDGVColumns("dgvUsuariosBloq"), GetDGVColumns("dgvUsuariosBloq"));
            dgvUsuarios.Columns[GetDGVColumns("dgvID")].Visible = false;

            dgvUsuarios.EditMode = DataGridViewEditMode.EditProgrammatically;
            dgvUsuarios.AllowUserToAddRows = false;
            dgvUsuarios.AllowUserToDeleteRows = false;
            dgvUsuarios.AllowUserToResizeColumns = false;
            dgvUsuarios.AllowUserToResizeRows = false;
            dgvUsuarios.RowHeadersVisible = false;
            dgvUsuarios.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvUsuarios.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgvUsuarios.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dgvUsuarios.RowTemplate.Height = 30;
            #endregion
            ActualizarGrilla();
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }

        private string GetDGVColumns(string pstring)
        {
            return Idioma.GetMSGLanguaje(pstring, Login.UsuarioLogeado.UserIdioma);
        }

        private void BtnAlta_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Alta;
            OpenCRUD(mOperacion);
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvUsuarios.SelectedRows.Count > 0)
                {
                    Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Modificacion;
                    OpenCRUD(mOperacion);
                    ActualizarGrilla();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Debe seleccionar un registro para poder modificarlo\n" + ex.Message, "Registro no seleccionado", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void BtnVer_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Consulta;
            OpenCRUD(mOperacion);
        }

        private void OpenCRUD(Constantes.TipoDeOperacion pOperacion)
        {
            mUsu.id = int.Parse(dgvUsuarios.SelectedRows[0].Cells[0].Value.ToString());
            mUsu.user = dgvUsuarios.SelectedRows[0].Cells[3].Value.ToString();
            CRUDUsuarios mForm = new CRUDUsuarios();
            mForm.MinimizeBox = false;
            mForm.MaximizeBox = false;
            mForm.StartPosition = FormStartPosition.CenterParent;
            mForm.Operacion = pOperacion;
            mForm.mUsuarioAEditar = mUsu.Obtener(mUsu);
            mForm.ShowDialog(this);
            ActualizarGrilla();
        }

        private void BtnBaja_Click(object sender, EventArgs e)
        {
            Constantes.TipoDeOperacion mOperacion = Constantes.TipoDeOperacion.Baja;
            OpenCRUD(mOperacion);
        }

        private void ActualizarGrilla()
        {
            dgvUsuarios.Rows.Clear();
            foreach (Usuario mU in mUsu.Listar())
            {
                dgvUsuarios.Rows.Add(mU.id, mU.Nombre, mU.Apellido, mU.user, mU.Activo.ToString(), mU.Bloqueado.ToString());
            }
        }
    }
}
