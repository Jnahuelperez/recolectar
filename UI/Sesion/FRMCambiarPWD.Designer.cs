﻿namespace UI.Sesion
{
    partial class FRMCambiarPWD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblContrasena = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.chkMostrar = new System.Windows.Forms.CheckBox();
            this.lblPWDReq = new System.Windows.Forms.Label();
            this.lblPWDReq1 = new System.Windows.Forms.Label();
            this.lblPWDReq2 = new System.Windows.Forms.Label();
            this.lblPWDReq3 = new System.Windows.Forms.Label();
            this.lblPWDReq4 = new System.Windows.Forms.Label();
            this.lblPWDReq5 = new System.Windows.Forms.Label();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.SuspendLayout();
            // 
            // lblContrasena
            // 
            this.lblContrasena.AutoSize = true;
            this.lblContrasena.Location = new System.Drawing.Point(333, 7);
            this.lblContrasena.Name = "lblContrasena";
            this.lblContrasena.Size = new System.Drawing.Size(61, 13);
            this.lblContrasena.TabIndex = 0;
            this.lblContrasena.Text = "Contrasena";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(336, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.PasswordChar = '*';
            this.textBox1.Size = new System.Drawing.Size(161, 20);
            this.textBox1.TabIndex = 1;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(389, 49);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 31);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.button1_Click);
            // 
            // chkMostrar
            // 
            this.chkMostrar.AutoSize = true;
            this.chkMostrar.Location = new System.Drawing.Point(436, 3);
            this.chkMostrar.Name = "chkMostrar";
            this.chkMostrar.Size = new System.Drawing.Size(61, 17);
            this.chkMostrar.TabIndex = 4;
            this.chkMostrar.Text = "Mostrar";
            this.chkMostrar.UseVisualStyleBackColor = true;
            this.chkMostrar.CheckedChanged += new System.EventHandler(this.chkMostrar_CheckedChanged);
            this.chkMostrar.CheckStateChanged += new System.EventHandler(this.chkMostrar_CheckStateChanged);
            // 
            // lblPWDReq
            // 
            this.lblPWDReq.AutoSize = true;
            this.lblPWDReq.Location = new System.Drawing.Point(12, 13);
            this.lblPWDReq.MaximumSize = new System.Drawing.Size(380, 0);
            this.lblPWDReq.Name = "lblPWDReq";
            this.lblPWDReq.Size = new System.Drawing.Size(63, 13);
            this.lblPWDReq.TabIndex = 3;
            this.lblPWDReq.Text = "lblPWDReq";
            // 
            // lblPWDReq1
            // 
            this.lblPWDReq1.AutoSize = true;
            this.lblPWDReq1.Location = new System.Drawing.Point(12, 26);
            this.lblPWDReq1.MaximumSize = new System.Drawing.Size(380, 0);
            this.lblPWDReq1.Name = "lblPWDReq1";
            this.lblPWDReq1.Size = new System.Drawing.Size(69, 13);
            this.lblPWDReq1.TabIndex = 5;
            this.lblPWDReq1.Text = "lblPWDReq1";
            // 
            // lblPWDReq2
            // 
            this.lblPWDReq2.AutoSize = true;
            this.lblPWDReq2.Location = new System.Drawing.Point(12, 39);
            this.lblPWDReq2.MaximumSize = new System.Drawing.Size(380, 0);
            this.lblPWDReq2.Name = "lblPWDReq2";
            this.lblPWDReq2.Size = new System.Drawing.Size(69, 13);
            this.lblPWDReq2.TabIndex = 6;
            this.lblPWDReq2.Text = "lblPWDReq2";
            // 
            // lblPWDReq3
            // 
            this.lblPWDReq3.AutoSize = true;
            this.lblPWDReq3.Location = new System.Drawing.Point(12, 52);
            this.lblPWDReq3.MaximumSize = new System.Drawing.Size(380, 0);
            this.lblPWDReq3.Name = "lblPWDReq3";
            this.lblPWDReq3.Size = new System.Drawing.Size(69, 13);
            this.lblPWDReq3.TabIndex = 7;
            this.lblPWDReq3.Text = "lblPWDReq3";
            // 
            // lblPWDReq4
            // 
            this.lblPWDReq4.AutoSize = true;
            this.lblPWDReq4.Location = new System.Drawing.Point(12, 65);
            this.lblPWDReq4.MaximumSize = new System.Drawing.Size(380, 0);
            this.lblPWDReq4.Name = "lblPWDReq4";
            this.lblPWDReq4.Size = new System.Drawing.Size(69, 13);
            this.lblPWDReq4.TabIndex = 8;
            this.lblPWDReq4.Text = "lblPWDReq4";
            // 
            // lblPWDReq5
            // 
            this.lblPWDReq5.AutoSize = true;
            this.lblPWDReq5.Location = new System.Drawing.Point(12, 78);
            this.lblPWDReq5.MaximumSize = new System.Drawing.Size(380, 0);
            this.lblPWDReq5.Name = "lblPWDReq5";
            this.lblPWDReq5.Size = new System.Drawing.Size(69, 13);
            this.lblPWDReq5.TabIndex = 9;
            this.lblPWDReq5.Text = "lblPWDReq5";
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "C:\\RecolectAR\\UI\\user_manual.chm";
            // 
            // FRMCambiarPWD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 109);
            this.Controls.Add(this.lblPWDReq5);
            this.Controls.Add(this.lblPWDReq4);
            this.Controls.Add(this.lblPWDReq3);
            this.Controls.Add(this.lblPWDReq2);
            this.Controls.Add(this.lblPWDReq1);
            this.Controls.Add(this.chkMostrar);
            this.Controls.Add(this.lblPWDReq);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblContrasena);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.Name = "FRMCambiarPWD";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cambiar contrasenia";
            this.Load += new System.EventHandler(this.FRMCambiarPWD_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblContrasena;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.CheckBox chkMostrar;
        private System.Windows.Forms.Label lblPWDReq;
        private System.Windows.Forms.Label lblPWDReq1;
        private System.Windows.Forms.Label lblPWDReq2;
        private System.Windows.Forms.Label lblPWDReq3;
        private System.Windows.Forms.Label lblPWDReq4;
        private System.Windows.Forms.Label lblPWDReq5;
        private System.Windows.Forms.HelpProvider helpProvider1;
    }
}