﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace UI.Sesion
{
    public partial class FRMCambiarPWD : Form
    {
        internal Usuario mUsuarioActivo = new Usuario();
        public FRMCambiarPWD()
        {
            InitializeComponent();
        }

        private void FRMCambiarPWD_Load(object sender, EventArgs e)
        {
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
        }
        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
            this.Text = Idioma.GetMSGLanguaje("FRMCambiarPWD", Login.UsuarioLogeado.UserIdioma);
        }
        private void chkMostrar_CheckStateChanged(object sender, EventArgs e)
        {
            
        }
        private void chkMostrar_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMostrar.Checked)
                textBox1.PasswordChar = '\0';
            else
                textBox1.PasswordChar = '*';
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Usuario mU = new Usuario();
                mU = Login.UsuarioLogeado;
                mU.pwd = textBox1.Text;
                mU.PWDSelfService(Login.UsuarioLogeado);
                MessageBox.Show(Idioma.GetMSGLanguaje("MSGPWDSelfServiceSucess", Login.UsuarioLogeado.UserIdioma));
                textBox1.Text = "\0";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrio un error:" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
