﻿using BL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class FRMIdioma : Form
    {
        public FRMPrincipal mdiChild { get; set; }
        public FRMIdioma()
        {
            InitializeComponent();
        }

        public FRMIdioma(FRMPrincipal pForm)
        {
            mdiChild = pForm;
            InitializeComponent();
        }
        private void FRMIdioma_Load(object sender, EventArgs e)
        {
            CargarIdioma(Idioma.ObtenerIdiomaDeControles(Login.UsuarioLogeado.UserIdioma));
            cmbIdioma.DropDownStyle = ComboBoxStyle.DropDownList;
            foreach (Idioma mIdi in Idioma.ListarIdiomas())
            {
                cmbIdioma.Items.Add(mIdi.Detalle);
            }
            this.Text = Idioma.GetMSGLanguaje("FRMIdioma", Login.UsuarioLogeado.UserIdioma);
        }

        private void CargarIdioma(List<Idioma.Controles> pControles)
        {
            foreach (Control C in this.Controls)
            {
                foreach (Idioma.Controles mIC in pControles)
                {
                    if (mIC.objeto == C.Name)
                        C.Text = mIC.valor;
                }
            }
        }
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            Usuario mU = new Usuario();
            mU = Login.UsuarioLogeado;
            mU.idIdioma = cmbIdioma.SelectedIndex + 1;
            mU.UserIdioma = cmbIdioma.SelectedItem.ToString();
            mU.Actualizar(mU);
            MessageBox.Show(Idioma.GetMSGLanguaje("MSGIdiomaActualizadoSuccess", Login.UsuarioLogeado.UserIdioma));
            mdiChild.CargarIdiomaMenues(Idioma.ObtenerIdiomaDeControles(cmbIdioma.SelectedItem.ToString()));
            this.Close();
        }

    }
}
